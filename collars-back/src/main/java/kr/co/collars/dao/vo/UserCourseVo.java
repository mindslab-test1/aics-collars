package kr.co.collars.dao.vo;

import lombok.Data;

import java.sql.Date;

@Data
public class UserCourseVo {
    private int id;
    private int userId;
    private int courseId;
    private int completed;
    private int active;
    private Date startDate;
    private Date endDate;
}
