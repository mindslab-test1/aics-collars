package kr.co.collars.dao.vo;

import lombok.Data;

@Data
public class CourseVo {
    private int id;
    private String productId;
    private String title;
    private String description;
    private float lev;
    private int price;
    private String thumbnail;
}
