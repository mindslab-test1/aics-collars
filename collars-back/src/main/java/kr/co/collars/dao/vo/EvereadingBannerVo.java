package kr.co.collars.dao.vo;

import lombok.Data;

@Data
public class EvereadingBannerVo {
    private Integer id;
    private String name;
    private String url;
    private Integer active;
}
