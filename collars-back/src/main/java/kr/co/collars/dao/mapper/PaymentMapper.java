package kr.co.collars.dao.mapper;

import kr.co.collars.api.inapp.model.PaymentVO;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentMapper {
    public void savePaymentResult(PaymentVO paymentVO);
}
