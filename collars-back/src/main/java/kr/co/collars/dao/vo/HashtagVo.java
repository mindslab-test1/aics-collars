package kr.co.collars.dao.vo;

import lombok.Data;

@Data
public class HashtagVo {
    private Integer id;
    private String tag;
}
