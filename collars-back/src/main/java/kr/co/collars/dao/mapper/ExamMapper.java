package kr.co.collars.dao.mapper;

import kr.co.collars.api.exam.model.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExamMapper {

    /*
     * exam template1
     * 처리하는 부분
     * */
    public Exam1VO getQTextExam1(Exam1VO exam1VO);

    public Exam1VO getExam1Answer(int id);

    public Exam1VO getExam1Tag(int id);

    public ExamScoreVO getExam1Score(ExamRequestDto examRequestDto);

    int getExam1Count();

    /*
     * exam template2
     * 처리하는 부분
     * */
    public Exam2VO getQTextExam2(Exam2VO exam2VO);

    public Exam2VO getExam2Answer(int id);

    public Exam2VO getExam2Tag(int id);

    int getExam2Count();

    /*
    * exam template3
    * 처리하는 부분
    * */
    public Exam3VO getQTextExam3(Exam3VO exam3VO);

    public Exam3VO getExam3Answer(int id);

    public Exam3VO getExam3Tag(int id);

    int getExam3Count();

    /*
    * exam template4
    * 처리하는 부분
    * */
    public Exam4VO getQTextExam4(Exam4VO exam4VO);

    public Exam4VO getExam4Answer(int id);

    public Exam4VO getExam4Tag(int id);

    ExamScoreVO getExam4CorrectScore(ExamRequestDto examRequestDto);

    List<ExamScoreVO> getExam4IfScore(ExamRequestDto examRequestDto);

    List<ExamScoreVO> getExam4Correct10(ExamRequestDto examRequestDto);

    List<ExamScoreVO> getExam4IF10(ExamRequestDto examRequestDto);

    int getExam4Count();


    /*
     * exam template5
     * 처리하는 부분
     * */
    public Exam5VO getQTextExam5(Exam5VO exam5VO);

    public Exam5VO getExam5Answer(int id);

    public Exam5VO getExam5Tag(int id);

    ExamScoreVO getExam5CorrectScore(ExamRequestDto examRequestDto);

    List<ExamScoreVO> getExam5IfScore(ExamRequestDto examRequestDto);

    List<ExamScoreVO> getExam5Correct10(ExamRequestDto examRequestDto);

    List<ExamScoreVO> getExam5IF10(ExamRequestDto examRequestDto);

    int getExam5Count();


    //최종 결과 저장
    public void saveExamResult(ExamResultVO examResultVO);

}
