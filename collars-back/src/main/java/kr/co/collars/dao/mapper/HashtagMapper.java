package kr.co.collars.dao.mapper;

import kr.co.collars.dao.vo.HashtagVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HashtagMapper {
    // CREATE
    public int addTag(HashtagVo tag);
    // READ
    public List<HashtagVo> getAllTags();
    public List<HashtagVo> getTags(List<String> tags);
    public HashtagVo getTag(String tag);
}
