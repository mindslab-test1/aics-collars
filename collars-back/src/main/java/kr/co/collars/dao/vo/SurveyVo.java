package kr.co.collars.dao.vo;

import lombok.Data;

@Data
public class SurveyVo {
    private int id;
    private String title;
    private String question;
    private String answer;
    private String single;
    private String active;
    private String type;
}
