package kr.co.collars.dao.mapper;

import kr.co.collars.api.evereading.model.GetColumnReq;
import kr.co.collars.dao.vo.CourseVo;
import kr.co.collars.dao.vo.EvereadingBannerVo;
import kr.co.collars.dao.vo.EvereadingVo;
import kr.co.collars.dao.vo.HashtagVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EvereadingMapper {
    // CREATE
    public int addColumn(EvereadingVo evereadingVo);
    public int setTags(Integer evereadingId, List<Integer> tagIds);
    public int addBanner(EvereadingBannerVo evereadingBannerVo);
    // READ
    public List<HashtagVo> getAllSectors(String lang);
    public EvereadingVo getColumn(Integer id);
    public List<EvereadingVo> getColumns(GetColumnReq getColumnReq);
    public List<EvereadingBannerVo> getBanners();
    // Update
    public int updateThumbnail(EvereadingVo evereadingVo);
    public int updateImage(EvereadingVo evereadingVo);

}
