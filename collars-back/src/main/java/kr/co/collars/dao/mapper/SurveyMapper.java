package kr.co.collars.dao.mapper;
import kr.co.collars.dao.vo.SurveyVo;
import kr.co.collars.dao.vo.UserSurveyVo;
import kr.co.collars.dao.vo.UserVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SurveyMapper {
    // CREATE
    public int setUserData(List<UserSurveyVo> userSurveyVo);
    // READ
    public String checkDataExist(UserVo userVo);
    public List<SurveyVo> getData();
    public List<UserSurveyVo> getUserData(int userId);
}
