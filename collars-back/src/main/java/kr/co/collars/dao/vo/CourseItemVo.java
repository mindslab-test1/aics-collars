package kr.co.collars.dao.vo;

import lombok.Data;

@Data
public class CourseItemVo {
    private Integer id;
    private Integer courseId;
    private Integer mediaId;
    private Integer mediaOrder;
    private String mediaName;
}
