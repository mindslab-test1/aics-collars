package kr.co.collars.dao.vo;

import lombok.Data;

import java.sql.Date;

@Data
public class EvereadingVo {
    private Integer id;
    private String name;
    private String url;
    private Date createDate;
    private String thumbnail;
    private Integer lev;
    private Integer sectorKor;
    private Integer sectorEng;
    private String sectorKorTag;
    private String sectorEngTag;
    private String image;
}
