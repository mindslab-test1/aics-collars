package kr.co.collars.dao.mapper;

import kr.co.collars.api.twocents.model.GetContentReq;
import kr.co.collars.dao.vo.EvereadingVo;
import kr.co.collars.dao.vo.TwoCentsVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TwoCentsMapper {
    // CREATE
    public int addContent(TwoCentsVo twoCentsVo);

    // READ
    public TwoCentsVo getContent(Integer id);
    public List<TwoCentsVo> getContents(GetContentReq getContentReq);

    // UPDATE
    public int updateUrl(TwoCentsVo twoCentsVo);
    public int updateImage(@Param("id") Integer id, @Param("target") String target, @Param("url") String url);
}
