package kr.co.collars.dao.vo;

import lombok.Data;

import java.sql.Date;

@Data
public class UserVo {
    private int id;
    private String email;
    private String name;
    private String password;
    private String provider;
    private Date signUpDate;
    private Date signInDate;
}
