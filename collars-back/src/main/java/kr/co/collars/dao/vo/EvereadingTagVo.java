package kr.co.collars.dao.vo;

import lombok.Data;

@Data
public class EvereadingTagVo {
    private Integer id;
    private Integer evereadingId;
    private Integer tagId;
}
