package kr.co.collars.dao.mapper;

import kr.co.collars.api.exam.model.Exam2VO;
import kr.co.collars.api.exam.model.Exam4VO;
import kr.co.collars.api.exam.model.Exam5VO;
import kr.co.collars.api.lecture.model.*;
import kr.co.collars.api.practice.model.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LectureMapper {

    List<UserCourseVO> getUserCourse(String email);

    List<CourseMeidaVO> getLectureList(int courseId);

    LectureMediaVO getLectureName(PracticeRequestDto practiceRequestDto);

    LectureMediaVO getLectureData(LectureRequestDto lectureRequestDto);

    List<MediaTextVO> getLectureText(LectureRequestDto lectureRequestDto);

    List<PracticeWordVO> getPracticeWord(PracticeRequestDto practiceRequestDto);

    List<PracticeSentenceVO> getPracticeSentence(PracticeRequestDto practiceRequestDto);

    List<PracticePronVO> getPracticePron(PracticeRequestDto practiceRequestDto);

    List<PracticeQuizVO> getPracticeQuiz(PracticeRequestDto practiceRequestDto);

    int getMediaId(PracticeRequestDto practiceRequestDto);

    void saveWordResult(PracticeResultVO practiceResultVO);

    void saveSentenceResult(PracticeResultVO practiceResultVO);

    void savePronResult(PracticeResultVO practiceResultVO);

    void saveQuizResult(PracticeResultVO practiceResultVO);

    PracticeResultVO getPracticeCount(PracticeRequestDto practiceRequestDto);

    Integer getFinishedDay(String email);

    void completeLecture(PracticeResultVO practiceResultVO);

    void userFinishCourse(PracticeRequestDto practiceRequestDto);

    void updateFinishedDay(PracticeRequestDto practiceRequestDto);

    Exam2VO getQuiz1Tmp2(PracticeRequestDto practiceRequestDto);

    Exam4VO getQuiz2Tmp4(PracticeRequestDto practiceRequestDto);

    Exam5VO getQuiz3Tmp5(PracticeRequestDto practiceRequestDto);

    Exam4VO getQuiz4Tmp4(PracticeRequestDto practiceRequestDto);
}
