package kr.co.collars.dao.mapper;

import kr.co.collars.api.inapp.model.InappRequestDTO;
import kr.co.collars.dao.vo.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseMapper {
    public int addCourse(CourseVo courseVo);
    public int addCourseItem(List<CourseItemVo> courseItemVo);
    public int addMedia(MediaVo mediaVo);
    public List<CourseVo> getAllCourse();
    public List<UserCourseVo> getUserCourseHistory(UserVo userVo);
    public CourseVo getCourseInfo(int courseId);
    public int updateCourseThumbnail(CourseVo courseVo);
    public int addUserCourse(InappRequestDTO inappRequestDTO);
    public void addCourseMedia(InappRequestDTO inappRequestDTO);
}
