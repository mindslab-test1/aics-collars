package kr.co.collars.dao.vo;

import lombok.Data;

import java.sql.Date;

@Data
public class UserSurveyVo {
    private int id;
    private int userId;
    private String questionId;
    private String answerId;
    private Date lastUpdate;
}
