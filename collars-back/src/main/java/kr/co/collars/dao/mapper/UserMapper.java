package kr.co.collars.dao.mapper;

import kr.co.collars.dao.vo.UserDataVo;
import kr.co.collars.dao.vo.UserVo;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {
    public int signUp(UserVo userVo);
    public UserVo validUser(UserVo userVo);
    public UserVo getUserId(UserVo userVo);
    public UserDataVo getUserData(UserVo userVo);
    public int updateLastSignIn(UserVo userVo);
    public int updateProvider(UserVo userVo);

}

