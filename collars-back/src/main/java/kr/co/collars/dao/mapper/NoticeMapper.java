package kr.co.collars.dao.mapper;

import kr.co.collars.api.notice.model.GetNoticeReq;
import kr.co.collars.dao.vo.NoticeVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoticeMapper {
    // CREATE
    public int setNotice(NoticeVo noticeVo);
    // READ
    public List<NoticeVo> getNotice(GetNoticeReq getNoticeReq);
}
