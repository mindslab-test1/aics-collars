package kr.co.collars.dao.vo;

import lombok.Data;

import java.sql.Date;

@Data
public class TwoCentsVo {
    private Integer id;
    private String name;
    private String url;
    private Date createDate;
    private Integer lev;
    private Integer sectorKor;
    private Integer sectorEng;
    private String thumbnail;
    private String background;
    private String image;
}
