package kr.co.collars.dao.vo;

import lombok.Data;

@Data
public class MediaVo {
    private int id;
    private String name;
    private String url;
    private String createDate;
}
