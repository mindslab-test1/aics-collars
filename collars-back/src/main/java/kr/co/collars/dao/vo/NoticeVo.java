package kr.co.collars.dao.vo;

import lombok.Data;

import java.sql.Date;

@Data
public class NoticeVo {
    private int id;
    private String title;
    private String contents;
    private Date createDate;
}
