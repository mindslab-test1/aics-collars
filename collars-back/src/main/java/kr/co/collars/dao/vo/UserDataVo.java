package kr.co.collars.dao.vo;

import lombok.Data;

@Data
public class UserDataVo {
    private String name;
    private String signUpDate;
    private String courseName;
    private String first;
    private String survey;
}
