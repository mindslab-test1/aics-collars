package kr.co.collars.api.survey.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SurveyUserDataResult {
    private String question;
    private List<String> answer;
}
