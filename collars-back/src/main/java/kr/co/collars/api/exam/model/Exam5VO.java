package kr.co.collars.api.exam.model;

import lombok.Data;

@Data
public class Exam5VO extends CommonDto{
    int id;
    String pText;
    String qText;
    String correctAnswer;
    String expressTag;
    String wordArray;
    int examNum;
}
