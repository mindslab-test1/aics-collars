package kr.co.collars.api.practice.model;

import kr.co.collars.api.exam.model.Exam2VO;
import kr.co.collars.api.exam.model.Exam4VO;
import kr.co.collars.api.exam.model.Exam5VO;
import lombok.Data;

@Data
public class PracticeQuizDTO {
    Exam2VO quiz1;
    Exam4VO quiz2;
    Exam5VO quiz3;
    Exam4VO quiz4;
}
