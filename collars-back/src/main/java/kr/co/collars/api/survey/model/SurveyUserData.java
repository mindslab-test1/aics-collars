package kr.co.collars.api.survey.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
public class SurveyUserData {
    private int userId;
    private List<SurveyUserDataResult> surveyData;
}
