package kr.co.collars.api.practice.service;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserRecord;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import kr.co.collars.api.auth.service.AuthService;
import kr.co.collars.api.exam.model.Exam2VO;
import kr.co.collars.api.exam.model.Exam4VO;
import kr.co.collars.api.exam.model.Exam5VO;
import kr.co.collars.api.exception.InternalServerException;
import kr.co.collars.api.lecture.model.LectureMediaVO;
import kr.co.collars.api.lecture.model.LectureRequestDto;
import kr.co.collars.api.practice.model.*;
import kr.co.collars.dao.mapper.LectureMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Slf4j
@Service
public class PracticeService {

    @Autowired
    LectureMapper lectureMapper;

    @Autowired
    AuthService authService;

    public String getUserInfo(String uid){
        UserRecord userRecord = null;
        try{
            userRecord = FirebaseAuth.getInstance().getUser(uid);
        }catch (Exception e){
            e.printStackTrace();
        }

        return userRecord.getEmail();
    }

    public List<PracticeWordVO> getPracticeWord(PracticeRequestDto practiceRequestDto){
        try {
            return lectureMapper.getPracticeWord(practiceRequestDto);
        }catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }

    public List<PracticeSentenceVO> getPracticeSentence(PracticeRequestDto practiceRequestDto){
        try {
            return lectureMapper.getPracticeSentence(practiceRequestDto);
        }catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }

    public List<PracticePronVO> getPracticePron(PracticeRequestDto practiceRequestDto){
        try {
            return lectureMapper.getPracticePron(practiceRequestDto);
        }catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }

    public void savePracticeResult(PracticeRequestDto practiceRequestDto) {
        try{
            practiceRequestDto.setUserId(authService.getUserId(practiceRequestDto.getUid()));
        }catch (Exception e){
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "LectureService.getLectureStartDate", "get userId error");
        }

        try{
            //int mediaId = lectureMapper.getMediaId(practiceRequestDto);

            for(int i=0;i<practiceRequestDto.getQuestion().size();i++){
                PracticeResultVO practiceResultVO = new PracticeResultVO();
                practiceResultVO.setUserId(practiceRequestDto.getUserId());
                practiceResultVO.setQuestion(practiceRequestDto.getQuestion().get(i));
                practiceResultVO.setUserAnswer(practiceRequestDto.getUserAnswer().get(i));
                practiceResultVO.setMediaId(practiceRequestDto.getMediaId());
                if("pron".equals(practiceRequestDto.getPractice()) || "quiz".equals(practiceRequestDto.getPractice())) {
                    practiceResultVO.setScore(practiceRequestDto.getScore().get(i));
                }
                else practiceResultVO.setCorrect(practiceRequestDto.getCorrect().get(i));

                if("word".equals(practiceRequestDto.getPractice())) lectureMapper.saveWordResult(practiceResultVO);
                else if("sentence".equals(practiceRequestDto.getPractice())) lectureMapper.saveSentenceResult(practiceResultVO);
                else if("pron".equals(practiceRequestDto.getPractice())) lectureMapper.savePronResult(practiceResultVO);
                else if("quiz".equals(practiceRequestDto.getPractice())) lectureMapper.saveQuizResult(practiceResultVO);
            }
        }catch (Exception e){
            log.debug(e.getMessage());
        }
    }

    public PracticeResultVO getPracticeCountService(PracticeRequestDto practiceRequestDto){
        try{
            PracticeResultVO practiceResultVORes = lectureMapper.getPracticeCount(practiceRequestDto);

            return practiceResultVORes;
        }catch (Exception e){
            log.debug("@ -----> PracticeService.getPracticeCountService() >> {}", e.getMessage());
        }

        return null;
    }

    public JsonObject getAllPracticeData(PracticeRequestDto practiceRequestDto){

        JsonObject jsonObjectTotal = new JsonObject();
        practiceRequestDto.setEmail(getUserInfo(practiceRequestDto.getUid()));

        try{
            LectureMediaVO lecture = lectureMapper.getLectureName(practiceRequestDto);
            jsonObjectTotal.addProperty("lectureName", lecture.getName());
        }catch (Exception e){
            e.printStackTrace();
        }

        try{
            Gson gson = new Gson();
            JsonObject checkJson = new JsonObject();
            JsonObject dataJson = new JsonObject();
            PracticeResultVO practiceCheck = getPracticeCountService(practiceRequestDto);
            boolean allFinshed = true;

            //pron, word, sentence순으로 학습 했는지 확인
            if(practiceCheck.getIsStudyLecture() == 0){
                checkJson.addProperty("lectureCheck", false);
                allFinshed = false;
            }
            else checkJson.addProperty("lectureCheck", true);

            if(practiceCheck.getIsStudyPron() != 0) {
                checkJson.addProperty("pronCheck", true);
                dataJson.addProperty("pronData", gson.toJson(lectureMapper.getPracticePron(practiceRequestDto)));
            }else{
                checkJson.addProperty("pronCheck", false);
                allFinshed = false;
            }

            if(practiceCheck.getIsStudyWord() != 0) {
                checkJson.addProperty("wordCheck", true);
                dataJson.addProperty("wordData", gson.toJson(lectureMapper.getPracticeWord(practiceRequestDto)));
            }else{
                checkJson.addProperty("wordCheck", false);
                allFinshed = false;
            }

            if(practiceCheck.getIsStudySentence() != 0) {
                checkJson.addProperty("sentenceCheck", true);
                dataJson.addProperty("sentenceData", gson.toJson(lectureMapper.getPracticeSentence(practiceRequestDto)));
            }else {
                checkJson.addProperty("sentenceCheck", false);
                allFinshed = false;
            }

            if(practiceCheck.getIsStudyQuiz() != 0){
                checkJson.addProperty("quizCheck", true);
                PracticeQuizDTO practiceQuizDTO = getQuizData(practiceRequestDto);

                List<PracticeQuizVO> practiceQuizVOList = new ArrayList<>();

                PracticeQuizVO quiz1 = new PracticeQuizVO();
                PracticeQuizVO quiz2 = new PracticeQuizVO();
                PracticeQuizVO quiz3 = new PracticeQuizVO();
                PracticeQuizVO quiz4 = new PracticeQuizVO();

                quiz1.setQuestion(practiceQuizDTO.getQuiz1().getQText());
                quiz1.setCorrectAnswer(practiceQuizDTO.getQuiz1().getCorrect());

                quiz2.setQuestion(practiceQuizDTO.getQuiz2().getQText2());
                quiz2.setCorrectAnswer(practiceQuizDTO.getQuiz2().getCorrectAnswer());

                quiz3.setQuestion(practiceQuizDTO.getQuiz3().getQText());
                quiz3.setCorrectAnswer(practiceQuizDTO.getQuiz3().getCorrectAnswer());

                quiz4.setQuestion(practiceQuizDTO.getQuiz4().getQText2());
                quiz4.setCorrectAnswer(practiceQuizDTO.getQuiz4().getCorrectAnswer());

                practiceQuizVOList.add(quiz1);
                practiceQuizVOList.add(quiz2);
                practiceQuizVOList.add(quiz3);
                practiceQuizVOList.add(quiz4);

                dataJson.addProperty("quizData", gson.toJson(practiceQuizVOList));

            }else{
                checkJson.addProperty("quizCheck", false);
                allFinshed = false;
            }

            jsonObjectTotal.add("check", checkJson);
            jsonObjectTotal.add("data", dataJson);

            if(allFinshed == true){
                lectureMapper.updateFinishedDay(practiceRequestDto);
            }

            log.debug(" jsonObjectTotal ====> {}", jsonObjectTotal);
        }catch (Exception e){
            e.printStackTrace();
        }

        return jsonObjectTotal;
    }

    public void completeLecture(PracticeRequestDto practiceRequestDto){
        PracticeResultVO practiceResultVO = new PracticeResultVO();

        try{
            practiceRequestDto.setUserId(authService.getUserId(practiceRequestDto.getUid()));
        }catch (Exception e){
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "LectureService.getLectureStartDate", "get userId error");
        }

        try{
            practiceResultVO.setUserId(practiceRequestDto.getUserId());
            practiceResultVO.setMediaId(practiceRequestDto.getMediaId());

            lectureMapper.completeLecture(practiceResultVO);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public PracticeQuizDTO getQuizData(PracticeRequestDto practiceRequestDto){
        PracticeQuizDTO practiceQuizDTO = new PracticeQuizDTO();

        try{
            Exam2VO exam2VO = lectureMapper.getQuiz1Tmp2(practiceRequestDto);
            practiceQuizDTO.setQuiz1(exam2VO);

            Exam4VO exam4VO = lectureMapper.getQuiz2Tmp4(practiceRequestDto);
            practiceQuizDTO.setQuiz2(exam4VO);

            Exam5VO exam5VO = lectureMapper.getQuiz3Tmp5(practiceRequestDto);
            practiceQuizDTO.setQuiz3(exam5VO);

            exam4VO = lectureMapper.getQuiz4Tmp4(practiceRequestDto);
            practiceQuizDTO.setQuiz4(exam4VO);
        }catch (Exception e){
            log.debug("ERROR {}", e.getMessage());
        }

        return practiceQuizDTO;
    }
}
