package kr.co.collars.api.survey.controller;

import com.google.gson.JsonElement;
import kr.co.collars.api.auth.model.AuthInfo;
import kr.co.collars.api.exception.InternalServerException;
import kr.co.collars.api.survey.model.SurveyUser;
import kr.co.collars.api.survey.model.SurveyUserData;
import kr.co.collars.api.survey.service.SurveyService;
import kr.co.collars.common.CommonRes;
import kr.co.collars.dao.vo.UserSurveyVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/survey")
public class SurveyController {
    @Autowired
    SurveyService surveyService;

    @RequestMapping(value = "/getData", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody ResponseEntity<Object> getData(){
        try {
            String result = surveyService.getData();
            return ResponseEntity.status(HttpStatus.OK).body(new CommonRes(null, result));
        } catch (Exception e) {
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "Survey [getData]", e.getMessage());
        }
    }


    @RequestMapping(value = "/setUserData", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public @ResponseBody ResponseEntity<Object> setUserData(
            @RequestBody Map<String,Object> params,
            HttpServletRequest request
    ){
        try {
            return surveyService.setUserData(params);
//            return ResponseEntity.status(HttpStatus.OK).body(new CommonRes("Data is inserted", result));
        } catch (Exception e) {
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "Survey [setUserData]", e.getMessage());
        }
    }
}
