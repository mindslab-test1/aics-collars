package kr.co.collars.api.exam.model;

import lombok.Data;

@Data
public class ExamRequestDto{
    int id;
    int examNum;
    String answer;
}
