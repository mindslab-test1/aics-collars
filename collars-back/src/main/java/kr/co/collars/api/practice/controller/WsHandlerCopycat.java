package kr.co.collars.api.practice.controller;

import kr.co.collars.api.practice.service.CopycatService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.BinaryWebSocketHandler;

@Slf4j
@Component
public class WsHandlerCopycat extends BinaryWebSocketHandler {

    private final CopycatService copycatService;

    WsHandlerCopycat(CopycatService copycatService) {this.copycatService = copycatService;}

    @Override
    protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) throws Exception {
        byte[] byte_buff = message.getPayload().array();

        if(byte_buff.length == 1) copycatService.getPronResult(session);
        else copycatService.sttGrpcOnMessage(message.getPayload());

        super.handleBinaryMessage(session, message);
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        log.debug("@ ------> WsHandlerCopycat.afterConnectionEstablished >> {}", session.getAttributes().toString());
        copycatService.sttGrpcConn(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        log.debug("@ <----- WsHandlerCopycat.afterConnectionClosed() >> {}", session.getAttributes().toString());
    }


}
