package kr.co.collars.api.exam.service;

import kr.co.collars.api.exam.model.Exam4VO;
import kr.co.collars.api.exam.model.ExamRequestDto;
import kr.co.collars.api.exam.model.ExamScoreVO;
import kr.co.collars.common.CommonConvert;
import kr.co.collars.dao.mapper.ExamMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class ExamQ4Service extends CommonExamService {

    @Autowired
    ExamMapper examMapper;

    public Exam4VO getQuestion(Exam4VO exam4VO){
        int exam4Count = examMapper.getExam4Count();
        int randomExamNum = (int)Math.random()*exam4Count+1;
        exam4VO.setExamNum(randomExamNum);

        return examMapper.getQTextExam4(exam4VO);
    }

    public int getScore(int examNum, String inputAnswer){
        inputAnswer = preProcess(inputAnswer);
        int score =-1;

        ExamRequestDto examRequestDto = new ExamRequestDto();
        examRequestDto.setExamNum(examNum);
        examRequestDto.setAnswer(inputAnswer.replaceAll(" ",""));

        try{
            ExamScoreVO examScoreVO = examMapper.getExam4CorrectScore(examRequestDto);
            if(examScoreVO == null){
                List<ExamScoreVO> examScoreVOIf = examMapper.getExam4IfScore(examRequestDto);
                score = ifSocre(examScoreVOIf, inputAnswer);

                List<ExamScoreVO> examScoreVOIf10 = examMapper.getExam4IF10(examRequestDto);
                score = containWordCheck(examScoreVOIf10, inputAnswer, score);

            }else if(examScoreVO != null){
                score = examScoreVO.getScore();
                List<ExamScoreVO> examScoreVOCorrect10 = examMapper.getExam4Correct10(examRequestDto);
                score = containWordCheck(examScoreVOCorrect10, inputAnswer, score);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        if(score<0) return 0;
        else return score;
    }

}
