package kr.co.collars.api.inapp.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class PaymentVO {
    int id;
    int userCourseId;
    String orderId;
    int price;
}
