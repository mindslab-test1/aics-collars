package kr.co.collars.api.survey.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SurveyUser {
    private String email;
}
