package kr.co.collars.api.evereading.controller;

import kr.co.collars.api.evereading.model.GetAllSectorsReq;
import kr.co.collars.api.evereading.model.GetColumnReq;
import kr.co.collars.api.evereading.service.EvereadingService;
import kr.co.collars.common.CommonRes;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.bind.BindResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/evereading")
public class EvereadingController {

    @Autowired
    EvereadingService evereadingService;

    @RequestMapping(value = "getAllSectors", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity getAllSectors(
            @Valid @RequestBody GetAllSectorsReq params,
            BindingResult bindingResult
    ) {
        String lang = params.getLang().toLowerCase();
        if (!lang.equals("kor") && !lang.equals("eng")) {
            return ResponseEntity.status(400).body(new CommonRes("Lang have to be 'kor' or 'eng'", null));
        }
        try {
            return evereadingService.getAllSectors(lang);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonRes(e.getMessage(), null));
        }
    }

    @RequestMapping(value = "/getColumns", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity getColumns(
            @Valid @RequestBody GetColumnReq params,
            BindingResult bindingResult
    ) throws BindException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        try {
            return evereadingService.getColumns(params);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonRes(e.getMessage(), null));
        }
    }

    @RequestMapping(value = "/addColumn", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity addColumn(
            @RequestParam String sector_kor,
            @RequestParam String sector_eng,
            @RequestParam MultipartFile file,
            @RequestParam Integer level,
            @RequestParam(required = false) List<String> tags
    ) {
        String orgFilename = file.getOriginalFilename();
        String extention = FilenameUtils.getExtension(orgFilename);

        if (!extention.equals("json")) {
            return ResponseEntity.status(400).body(new CommonRes("File extension have to be json", null));
        }
        try {
            return evereadingService.addColumn(sector_kor.toLowerCase(), sector_eng.toLowerCase(), file, level, tags);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonRes(e.getMessage(), null));
        }
    }

    @RequestMapping(value = "/getBanners", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity getBanners() {
        try {
            return evereadingService.getBanners();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonRes(e.getMessage(), null));
        }
    }

    @RequestMapping(value = "/addBanner", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity addBanner(
            @RequestParam(required = false) String name,
            @RequestParam MultipartFile file
    ) throws Exception {

        try {
            return evereadingService.addBanner(name, file);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonRes(e.getMessage(), null));
        }
    }

    @RequestMapping(value = "/updateThumbnail", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity updateThumbnail(
            @RequestParam Integer id,
            @RequestParam MultipartFile file
    ) {
        if(file.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CommonRes("file required", null));
        }

        String type = file.getContentType().split("/")[0];
        if (!type.equals("image")) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CommonRes("Invalid file type: Image file required", null));
        }

        try {
            return evereadingService.updateThumbnail(id, file);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonRes(e.getMessage(), null));
        }
    }

    @RequestMapping(value = "/updateImage", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity updateImage(
            @RequestParam Integer id,
            @RequestParam MultipartFile file
    ) {
        if(file.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CommonRes("file required", null));
        }

        String type = file.getContentType().split("/")[0];
        if (!type.equals("image")) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CommonRes("Invalid file type: Image file required", null));
        }

        try {
            return evereadingService.updateImage(id, file);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonRes(e.getMessage(), null));
        }
    }
}
