package kr.co.collars.api.exception.handler;

import kr.co.collars.api.exception.BaseException;
import kr.co.collars.api.exception.MediaApiResponseError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CollarsApiExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(CollarsApiExceptionHandler.class);

    @ExceptionHandler(BaseException.class)
    public ResponseEntity<Object> handle(BaseException exception, WebRequest request){
        logger.warn("handleBaseException", exception);
        MediaApiResponseError mediaApiResponseError = MediaApiResponseError.builder()
                .statusCode(exception.getStatusCode().value())
                .code(exception.getCode())
                .message(exception.getMessage())
                .build();
        return handleExceptionInternal(exception, mediaApiResponseError, new HttpHeaders(), exception.getStatusCode(), request);
    }

}
