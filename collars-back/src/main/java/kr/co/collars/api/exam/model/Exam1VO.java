package kr.co.collars.api.exam.model;

import lombok.Data;

@Data
public class Exam1VO extends CommonDto{
    int id;
    int examNum;
    String pText;
    String qText;
    String correctAnswer;
    String accuracyTag;
    String pronTag;
}
