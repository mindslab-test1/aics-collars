package kr.co.collars.api.twocents.controller;

import kr.co.collars.api.twocents.model.GetContentReq;
import kr.co.collars.api.twocents.service.TwoCentsService;
import kr.co.collars.common.CommonRes;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "/twocents")
public class TwoCentsController {
    @Autowired
    TwoCentsService twoCentsService;

    // CREATE
    @RequestMapping(value = "/addContent", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity addContent(
            @RequestParam(required = false) String name,
            @RequestParam MultipartFile file,
            @RequestParam Integer level
    ) {
        String orgFilename = file.getOriginalFilename();
        String extention = FilenameUtils.getExtension(orgFilename);

        if (!extention.equals("json")) {
            return ResponseEntity.status(400).body(new CommonRes("File extension have to be json", null));
        }
        try {
            return twoCentsService.addContent(name, file, level);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonRes(e.getMessage(), null));
        }
    }
    // READ
    @RequestMapping(value = "/getContents", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity getContent(
            @Valid @RequestBody GetContentReq params,
            BindingResult bindingResult
    ) throws BindException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        try {
            return twoCentsService.getContents(params);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonRes(e.getMessage(), null));
        }
    }
    // UPDATE
    @RequestMapping(value = "/updateAudio", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity updateAudio(
            @RequestParam Integer id,
            @RequestParam List<MultipartFile> files
    ) throws Exception {
        try {
            return twoCentsService.updateAudio(id, files);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonRes(e.getMessage(), null));
        }
    }

    @RequestMapping(value = "/updateImage", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity updateImage(
            @RequestParam Integer id,
            @RequestParam String target,
            @RequestParam MultipartFile file
    ){
        if(file.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CommonRes("File required", null));
        }
        if(!target.equalsIgnoreCase("thumbnail")&&!target.equalsIgnoreCase("background")&&!target.equalsIgnoreCase("image")){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CommonRes("Invalid target", null));
        }

        try {
            return twoCentsService.updateImage(id, target, file);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonRes(e.getMessage(), null));
        }
    }
}
