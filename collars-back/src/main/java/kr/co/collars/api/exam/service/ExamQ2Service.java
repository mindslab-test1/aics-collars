package kr.co.collars.api.exam.service;

import kr.co.collars.api.exam.model.Exam2VO;
import kr.co.collars.dao.mapper.ExamMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExamQ2Service extends CommonExamService{

    @Autowired
    ExamMapper examMapper;

    public Exam2VO getQuestion(Exam2VO exam2VO){
        int exam2Count = examMapper.getExam2Count();
        int randomExamNum = (int)Math.random()*exam2Count+1;
        exam2VO.setExamNum(randomExamNum);

        return examMapper.getQTextExam2(exam2VO);
    }

    public int getScore(int qNum, String inputAnswer){
        Exam2VO exam2VO = examMapper.getExam2Answer(qNum);
        int score = -1;

        if(correctScore(exam2VO.getCorrect(), inputAnswer)) score = 100;
        else score = 0;

        return score;
    }
}
