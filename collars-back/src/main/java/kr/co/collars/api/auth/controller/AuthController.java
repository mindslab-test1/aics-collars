package kr.co.collars.api.auth.controller;

import kr.co.collars.api.auth.model.AuthInfo;
import kr.co.collars.api.auth.model.AuthInfoDefault;
import kr.co.collars.api.auth.service.AuthService;
import kr.co.collars.api.exception.InternalServerException;
import kr.co.collars.common.CommonRes;
import kr.co.collars.dao.vo.UserDataVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@RestController
@RequestMapping(value = "/auth")
public class AuthController {
    @Autowired
    AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody String Auth() {
        System.out.println("WHAT THE HELL");
        return "I AM ALIVE";
    }

    @RequestMapping(value = "/getCustomToken", method = RequestMethod.POST, produces = "application/json;charset=UTF-8" )
    public @ResponseBody ResponseEntity<Object> getCustomToken(
            @RequestBody AuthInfo authInfo,
            HttpServletRequest request
    ){
        try {
            String customToken = authService.getCustomToken(authInfo);
            return ResponseEntity.status(HttpStatus.OK).body(new CommonRes(null, customToken));
        } catch (Exception e) {
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "Auth [getCustomToken]", e.getMessage());
        }
    }

    @RequestMapping(value = "/validUser", method = RequestMethod.POST, produces = "application/json;charset=UTF-8" )
    public @ResponseBody ResponseEntity<Object> validUser(
            @RequestBody AuthInfo authInfo,
            HttpServletRequest request
    ){
        try {
            boolean result = authService.validUser(authInfo);
            return ResponseEntity.status(HttpStatus.OK).body(new CommonRes(null, result));
        } catch (Exception e) {
//            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "Auth [validUser]", e.getMessage());
            return ResponseEntity.status(HttpStatus.OK).body(new CommonRes(e.getMessage(), false));
        }
    }

    @RequestMapping(value = "getUserData", method = RequestMethod.POST, consumes =  "application/json;charset=UTF-8")
    public @ResponseBody ResponseEntity getUserData(
            @RequestBody AuthInfoDefault authInfo,
            HttpServletRequest request
    ){
        try {
            UserDataVo result = authService.getUserData(authInfo);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(new CommonRes(null, result));
        }catch (Exception e){
            return ResponseEntity.ok().body(new CommonRes(e.getMessage(), null));
        }
    }

}
