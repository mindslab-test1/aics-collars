package kr.co.collars.api.twocents.service;

import com.google.gson.Gson;
import kr.co.collars.api.twocents.data.ScriptVo;
import kr.co.collars.api.twocents.model.GetContentReq;
import kr.co.collars.common.CommonRes;
import kr.co.collars.dao.mapper.HashtagMapper;
import kr.co.collars.dao.mapper.TwoCentsMapper;

import kr.co.collars.dao.vo.TwoCentsVo;
import kr.co.collars.external_service.vo.ResponseUploadedFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.util.*;

@Slf4j
@Transactional
@Service
public class TwoCentsService {
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    TwoCentsMapper twoCentsMapper;
    @Autowired
    HashtagMapper hashtagMapper;

    @Value("${media.server.url}")
    private String MEDIA_SERVER_URL;
    @Value("${media.server.port}")
    private String MEDIA_SERVER_PORT;
    @Value("${media.server.dir}")
    private String MEDIA_SERVER_DIR;

    // CREATE
    public ResponseEntity addContent(String name, MultipartFile file, Integer level) {
        String uploadUrl = MEDIA_SERVER_URL + "/media/file:upload";
        String deleteUrl = MEDIA_SERVER_URL + "/media/file:delete";

        ResponseEntity<ResponseUploadedFileInfo> resMedia = addToMediaServer(URI.create(uploadUrl), file);
        String mediaPath = extractMediaPath(resMedia.getBody().getFileDownloadUri());
        if (resMedia.getStatusCode().value() != 201) {
            return ResponseEntity.status(402).body(new CommonRes("Media Server Error: Can't upload file.", null));
        }

        try {
            // AddContent
            String filename;
            if (name == null) {
                filename = FilenameUtils.getBaseName(file.getOriginalFilename());
            } else {
                filename = name;
            }
            System.out.println(filename);
            TwoCentsVo twoCentsVo = new TwoCentsVo();
            twoCentsVo.setName(filename);
            twoCentsVo.setUrl(mediaPath);
            twoCentsVo.setLev(level);

            twoCentsMapper.addContent(twoCentsVo);
        } catch (Exception e) {
            removeFromMediaServer(URI.create(deleteUrl), mediaPath);
            return ResponseEntity.status(500).body(new CommonRes(e.getMessage(), null));
        }
        return ResponseEntity.status(HttpStatus.OK).body(new CommonRes("success", null));
    }
    // READ
    public ResponseEntity getContents(GetContentReq params){
        List<TwoCentsVo> results = twoCentsMapper.getContents(params);
        for(int i = 0; i < results.size(); i++){
            if(results.get(i).getThumbnail()!=null)results.get(i).setThumbnail(MEDIA_SERVER_URL + results.get(i).getThumbnail());
            if(results.get(i).getBackground()!=null)results.get(i).setBackground(MEDIA_SERVER_URL + results.get(i).getBackground());
            if(results.get(i).getImage()!=null)results.get(i).setImage(MEDIA_SERVER_URL + results.get(i).getImage());
        }
        return ResponseEntity.status(HttpStatus.OK).body(new CommonRes("success", results));
    }
    // UPDATE
    public ResponseEntity updateAudio(Integer id, List<MultipartFile> files) throws Exception {
        String uploadUrl = MEDIA_SERVER_URL + "/media/file:upload";
        String deleteUrl = MEDIA_SERVER_URL + "/media/file:delete";

        List<String> audioLinks = new ArrayList<>();
        String newMediaPath = "";
        try {
            // Get info of contents
            TwoCentsVo twoCentsVo = twoCentsMapper.getContent(id);
            String filePath = MEDIA_SERVER_URL + twoCentsVo.getUrl();
            ScriptVo script = readJsonFromURL(filePath);
            if (files.size() != script.getContent().size()) {
                return ResponseEntity.status(400).body(new CommonRes("The number of files does not match.", null));
            }
            List<String> oldAudioLinks = new ArrayList<>();
            for (int i = 0; i < script.getContent().size(); i++) {
                String oldAduioLink = script.getContent().get(i).getAudio();
                if (!oldAduioLink.equals("") && oldAduioLink != null) {
                    oldAudioLinks.add(script.getContent().get(i).getAudio());
                }
            }
            // Add audio file and mapping url
            for (MultipartFile file : files) {
                ResponseEntity<ResponseUploadedFileInfo> resMedia = addToMediaServer(URI.create(uploadUrl), file);
                String mediaPath = extractMediaPath(resMedia.getBody().getFileDownloadUri());
                audioLinks.add(mediaPath);
            }
            for (int i = 0; i < script.getContent().size(); i++) {
                script.getContent().get(i).setAudio(audioLinks.get(i));
            }

            Gson gson = new Gson();
            String newScript = gson.toJson(script);
            ByteArrayResource fileResource = new ByteArrayResource(Objects.requireNonNull(newScript.getBytes())) {
                @Override
                public String getFilename() {
                    return twoCentsVo.getName() + ".json";
                }
            };
            ResponseEntity<ResponseUploadedFileInfo> resMedia = addToMediaServer(URI.create(uploadUrl), fileResource);
            newMediaPath = extractMediaPath(resMedia.getBody().getFileDownloadUri());
            if (resMedia.getStatusCode().value() != 201) {
                return ResponseEntity.status(402).body(new CommonRes("Media Server Error: Can't upload file.", null));
            }
            String oldMediaPath = twoCentsVo.getUrl();
            twoCentsVo.setUrl(newMediaPath);
            twoCentsMapper.updateUrl(twoCentsVo);
            removeFromMediaServer(URI.create(deleteUrl), oldMediaPath);
            for (String oldAudioLink : oldAudioLinks) {
                removeFromMediaServer(URI.create(deleteUrl), oldAudioLink);
            }
            return ResponseEntity.status(200).body(new CommonRes("success", null));
        }catch (Exception e){
            if(audioLinks.size() > 0){
                for (String audioLink : audioLinks) {
                    removeFromMediaServer(URI.create(deleteUrl), audioLink);
                }
            }
            if(!newMediaPath.equals("")){
                removeFromMediaServer(URI.create(deleteUrl), newMediaPath);
            }
            return ResponseEntity.status(500).body(new CommonRes(e.getMessage(), null));
        }
    }
    public ResponseEntity updateImage(Integer id, String target, MultipartFile file) throws Exception {
        String uploadUrl = MEDIA_SERVER_URL + "/media/file:upload";
        String deleteUrl = MEDIA_SERVER_URL + "/media/file:delete";

        TwoCentsVo current = twoCentsMapper.getContent(id);
        System.out.println(current);
        ResponseEntity<ResponseUploadedFileInfo> resMedia = addToMediaServer(URI.create(uploadUrl), file);
        String mediaPath = extractMediaPath(resMedia.getBody().getFileDownloadUri());
        if(resMedia.getStatusCode().value() != 201){
            return ResponseEntity.status(402).body(new CommonRes("Media Server Error: Can't upload thumbnail image.", null));
        }
        try{
            twoCentsMapper.updateImage(id, target.toLowerCase(), mediaPath);
            String oldImagePath = "";
            switch (target.toLowerCase()){
                case "thumbnail":
                    if(current.getThumbnail()!=null) oldImagePath = current.getThumbnail();
                    break;
                case "background":
                    if(current.getBackground()!=null) oldImagePath = current.getBackground();
                    break;
                case "image":
                    if(current.getImage() != null) oldImagePath = current.getImage();
                    break;
                default:
                    break;
            }
            System.out.println(mediaPath);
            if(!oldImagePath.equals("")) removeFromMediaServer(URI.create(deleteUrl), oldImagePath);
        }catch (Exception e){
            removeFromMediaServer(URI.create(deleteUrl), mediaPath);
            throw new Exception("updateImage: " + e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.OK).body(new CommonRes("success", mediaPath));
    }


    // Function
    private ScriptVo readJsonFromURL(String url) {
        ResponseEntity<ScriptVo> test = restTemplate.getForEntity(url, ScriptVo.class);
        return test.getBody();
    }
    private ResponseEntity<ResponseUploadedFileInfo> addToMediaServer(URI uri, ByteArrayResource file) {
        HttpHeaders requestMasterHeaders = new HttpHeaders();
        requestMasterHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", file);
        body.add("uploadDirectory", MEDIA_SERVER_DIR);
        HttpEntity<MultiValueMap<String, Object>> requestMasterHttpEntity = new HttpEntity<>(body, requestMasterHeaders);
        return restTemplate.exchange(uri,
                HttpMethod.POST,
                requestMasterHttpEntity,
                new ParameterizedTypeReference<ResponseUploadedFileInfo>() {
                });
    }
    private ResponseEntity<ResponseUploadedFileInfo> addToMediaServer(URI uri, MultipartFile multipartFile) {
        HttpHeaders requestMasterHeaders = new HttpHeaders();
        requestMasterHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", multipartFile.getResource());
        body.add("uploadDirectory", MEDIA_SERVER_DIR);
        HttpEntity<MultiValueMap<String, Object>> requestMasterHttpEntity = new HttpEntity<>(body, requestMasterHeaders);
        return restTemplate.exchange(uri,
                HttpMethod.POST,
                requestMasterHttpEntity,
                new ParameterizedTypeReference<ResponseUploadedFileInfo>() {
                });
    }
    private ResponseEntity<Void> removeFromMediaServer(URI uri, String path) {
        String formattedPath = path.split("/media")[1];

        HttpHeaders requestMasterHeaders = new HttpHeaders();
        requestMasterHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map<String, String> body = new HashMap<>();
        body.put("deleteFileFullPath", formattedPath);
        HttpEntity<Map<String, String>> requestMasterHttpEntity = new HttpEntity<>(body, requestMasterHeaders);
        return restTemplate.exchange(uri, HttpMethod.DELETE, requestMasterHttpEntity, Void.class);
    }

    private String extractMediaPath(String url) {
        String result[] = url.split("(?=/media/aics)");
        return result[1];
    }
}
