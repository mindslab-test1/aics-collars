package kr.co.collars.api.twocents.data;

import lombok.Data;

import java.util.List;

@Data
public class ScriptVo {
    private TitleVo title;
    private List<ContentVo> content;
    private List<VocabularyVo> vocabulary;
}
