package kr.co.collars.api.practice.model;

import lombok.Data;

import java.util.List;

@Data
public class PracticeRequestDto {
    int userId;
    int mediaId;
    String uid;
    String course;
    String email;
    int lectureNum;
    String practice;
    List<String> question;
    List<String> userAnswer;
    List<Integer> correct;
    List<String> score;
    List<String> correctAnswer;
}
