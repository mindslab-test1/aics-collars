package kr.co.collars.api.twocents.data;

import lombok.Data;

@Data
public class VocabularyVo {
    private Integer index;
    private String eng;
    private String kor;
}
