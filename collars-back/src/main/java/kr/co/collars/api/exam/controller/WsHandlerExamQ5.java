package kr.co.collars.api.exam.controller;

import kr.co.collars.api.exam.service.ExamQ5Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.handler.BinaryWebSocketHandler;

import java.nio.charset.StandardCharsets;

@Slf4j
@Configuration
@EnableWebSocket
public class WsHandlerExamQ5 extends BinaryWebSocketHandler {

    private final ExamQ5Service examQ5Service;

    WsHandlerExamQ5(ExamQ5Service examQ5Service){ this.examQ5Service = examQ5Service; }

    @Override
    protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) throws Exception{
        byte[] byte_buff = message.getPayload().array();

        if(byte_buff.length == 1)  examQ5Service.getPronResult(session);
        else if(byte_buff.length>1 && byte_buff.length<100){
            examQ5Service.setExamNum(new String(byte_buff, StandardCharsets.UTF_8));
        }
        else examQ5Service.sttGrpcOnMessage(message.getPayload());

        super.handleBinaryMessage(session, message);
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        log.debug("@ -----> WsHandlerPron.afterConnectionEstablished() >> {}", session.getAttributes().toString());
        examQ5Service.sttGrpcConn(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception{
        log.debug("@ <----- WsHandlerPron.afterConnectionClosed() >> {}", session.getAttributes().toString());
    }
}
