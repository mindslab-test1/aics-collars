package kr.co.collars.api.stt.vo;

import lombok.Data;

@Data
public class SttResponse {
    String status;
    String data;
    SttExtra extra_data;

    @Data
    public class SttExtra {
        String stt_length;
        String stt_duration;
        String stt_data;

    }
}
