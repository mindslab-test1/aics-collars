package kr.co.collars.api.inapp.controller;

import kr.co.collars.api.inapp.model.InappRequestDTO;
import kr.co.collars.api.inapp.service.InappService;
import kr.co.collars.common.CommonRes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(value = "/inapp")
public class InappController {
    InappService inappService;

    InappController(InappService inappService){
        this.inappService = inappService;
    }

    @PostMapping("/consumeGet")
    public ResponseEntity getConsumeProduct(@RequestBody InappRequestDTO inappRequestDTO) throws Exception{
        CommonRes commonRes = inappService.purchaseCheck(inappRequestDTO);
        return ResponseEntity.status(HttpStatus.OK).body(commonRes);
    }
}
