package kr.co.collars.api.twocents.data;

import lombok.Data;

@Data
public class ContentVo {
    private String floating;
    private String name;
    private String eng;
    private String kor;
    private String audio;
}
