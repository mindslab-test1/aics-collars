package kr.co.collars.api.auth.service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import kr.co.collars.api.auth.model.AuthInfo;
import kr.co.collars.api.auth.model.AuthInfoDefault;
import kr.co.collars.api.exception.InternalServerException;
import kr.co.collars.dao.mapper.UserMapper;
import kr.co.collars.dao.vo.UserDataVo;
import kr.co.collars.dao.vo.UserVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;

import java.io.FileInputStream;
import java.util.Arrays;

@Slf4j
@Transactional
@Service
public class AuthService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    ResourceLoader resourceLoader;
    // Constructor
    public AuthService() {
        try {
            ClassPathResource serviceAccount = new ClassPathResource("key/serviceAccountKey.json");
            /*FileInputStream serviceAccount =
                    new FileInputStream(ResourceUtils.getFile("classpath:key/serviceAccountKey.json"));*/
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount.getInputStream()))
                    .build();
            FirebaseApp.initializeApp(options);
        }catch (Exception e){
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "Auth", e.getMessage());
        }
    }


    // Service
    public String getCustomToken(AuthInfo authInfo) throws FirebaseAuthException {
        UserRecord userRecord = getUserWithFirebaseAdmin(authInfo);
        if(userRecord != null) {
            validUserWithDB(userRecord, authInfo.getProvider());
            // Generate Custom Token : String
            return FirebaseAuth.getInstance().createCustomToken(userRecord.getUid());
        }
        else{
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "Auth [getCustomToken]", "UNKNOWN ERROR");
        }
    }
    public boolean validUser(AuthInfo authInfo) throws FirebaseAuthException {
        UserRecord userRecord = getUserWithFirebaseAdmin(authInfo);
        if(userRecord != null){
            validUserWithDB(userRecord, authInfo.getProvider());
            return true;
        }
        else{
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "Auth [validUser]", "UNKNOWN ERROR");
        }
    }

    public UserDataVo getUserData(AuthInfoDefault authInfo) throws FirebaseAuthException {
        // Todo: if(!authInfo.getUid()) => TRUE
        UserDataVo userData = new UserDataVo();
        try {
            UserRecord userRecord = FirebaseAuth.getInstance().getUser(authInfo.getUid());
            UserVo userVo = new UserVo();
            userVo.setEmail(userRecord.getEmail());
            userData = userMapper.getUserData(userVo);
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return userData;
    }

    // Function
    public UserRecord getUserWithFirebaseAdmin(AuthInfo params) throws FirebaseAuthException {
        UserRecord userRecord = null;
        try {
            userRecord = FirebaseAuth.getInstance().getUserByEmail(params.getEmail());
        } catch (FirebaseAuthException e) {
            if(e.getErrorCode().equals("user-not-found")){
                UserRecord.CreateRequest newUser = new UserRecord.CreateRequest();
                newUser.setEmail(params.getEmail());
                newUser.setDisplayName(params.getName());
                // Create new firebase user : UserRecord
                userRecord = FirebaseAuth.getInstance().createUser(newUser);
            }else{
                throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "Auth [getUserWithFirebaseAdmin]", e.getMessage());
            }
        }
        return userRecord;
    }
    public void validUserWithDB(UserRecord userRecord, String provider) {
        UserVo userVo = new UserVo();
        userVo.setEmail(userRecord.getEmail());
        UserVo userData = userMapper.validUser(userVo);

        if(userData == null){
            AddUserIntoDB(userRecord, provider);
            userData = userMapper.validUser(userVo);
        }else {
            String[] providerArray = userData.getProvider().split(",");
            if(!Arrays.stream(providerArray).anyMatch(provider::equals)){
                String newProvider = userData.getProvider()+","+provider;
                userData.setProvider(newProvider);
                userMapper.updateProvider(userData);
            }
        }
        userMapper.updateLastSignIn(userData);
    }
    public void AddUserIntoDB(UserRecord userRecord, String provider){
        UserVo userVo = new UserVo();
        userVo.setEmail(userRecord.getEmail());
        userVo.setName(userRecord.getDisplayName());
        userVo.setProvider(provider);
        int result = 0;
        try {
            result = userMapper.signUp(userVo); // If Success result is 1
        }catch (Exception e){
            e.printStackTrace();
        }
        if(result != 1) { throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "Auth [AddUserIntoDB]", "Couldn't add new user");}
    }

    public int getUserId(String uid){
        UserVo userVo = new UserVo();

        try{
            UserRecord userRecord = FirebaseAuth.getInstance().getUser(uid);
            UserVo userVoRequest = new UserVo();
            userVoRequest.setEmail(userRecord.getEmail());
            userVo = userMapper.getUserId(userVoRequest);
        }catch (Exception e){
            e.printStackTrace();
        }

        return userVo.getId();
    }
}
