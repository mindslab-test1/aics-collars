package kr.co.collars.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalServerException extends BaseException {
    private final HttpStatus statusCode;
    private final String code;
    private final String message;

    public InternalServerException(HttpStatus statusCode, String code, String message) {
        super(statusCode, code, message);
        this.statusCode = statusCode;
        this.code = code;
        this.message = message;
    }
}
