package kr.co.collars.api.itf.vo;

import lombok.Data;

@Data
public class ItfRequest {
    String context;
}
