package kr.co.collars.api.lecture.model;

import lombok.Data;

@Data
public class LectureRequestDto {
    int userId;
    String uid;
    int mediaId;
    String email;
    String course;
    int lectureNum;
}
