package kr.co.collars.api.inapp.service;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.androidpublisher.AndroidPublisher;
import com.google.api.services.androidpublisher.AndroidPublisherScopes;
import com.google.api.services.androidpublisher.model.ProductPurchase;
import com.google.api.services.androidpublisher.model.ProductPurchasesAcknowledgeRequest;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserRecord;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import kr.co.collars.api.exception.InternalServerException;
import kr.co.collars.api.inapp.model.InappRequestDTO;
import kr.co.collars.api.inapp.model.PaymentVO;
import kr.co.collars.common.CommonRes;
import kr.co.collars.dao.mapper.CourseMapper;
import kr.co.collars.dao.mapper.PaymentMapper;
import kr.co.collars.dao.mapper.UserMapper;
import kr.co.collars.dao.vo.CourseVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

@Slf4j
@Component
@Transactional
public class InappService {
    String packageName = "kr.co.collars.collarsApp";
    String productId;
    String purchaseToken;

    UserMapper userMapper;
    CourseMapper courseMapper;
    PaymentMapper paymentMapper;

    InappService(UserMapper userMapper, CourseMapper courseMapper, PaymentMapper paymentMapper){
        this.userMapper = userMapper;
        this.courseMapper = courseMapper;
        this.paymentMapper = paymentMapper;
    }

    public String getUserEmail(String uid){

        try{
            UserRecord userRecord = FirebaseAuth.getInstance().getUser(uid);
            return userRecord.getEmail();
        }catch (Exception e){
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "InappService.getUserEmail", "GET USER EMAIL ERROR: "+e.getMessage());
        }
    }

    public CommonRes purchaseCheck(InappRequestDTO inappRequestDTO) throws IOException, GeneralSecurityException {

        this.purchaseToken = inappRequestDTO.getPurchaseToken();
        this.productId = inappRequestDTO.getProductId();

        JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
        HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        ClassPathResource serviceAccount = new ClassPathResource("key/serviceAccountKey.json");

        GoogleCredentials credentials = GoogleCredentials.fromStream(serviceAccount.getInputStream())
                .createScoped(Collections.singleton(AndroidPublisherScopes.ANDROIDPUBLISHER));

        HttpCredentialsAdapter httpCredentialsAdapter = new HttpCredentialsAdapter(credentials);

        return purchaseConsumeGet(httpTransport, JSON_FACTORY, httpCredentialsAdapter, inappRequestDTO);
    }

    public CommonRes purchaseConsumeGet(HttpTransport httpTransport, JsonFactory JSON_FACTORY, HttpRequestInitializer httpRequestInitializer, InappRequestDTO inappRequestDTO) throws IOException{

        inappRequestDTO.setEmail(getUserEmail(inappRequestDTO.getUid()));


        AndroidPublisher publisher = new AndroidPublisher.Builder(httpTransport, JSON_FACTORY, httpRequestInitializer)
                .setApplicationName(packageName)
                .build();

        /* purchase acknowledge */
        if(inappRequestDTO.getEmail() != null || inappRequestDTO.getEmail() != "") {
            ProductPurchasesAcknowledgeRequest productPurchasesAcknowledgeRequest = new ProductPurchasesAcknowledgeRequest();
            AndroidPublisher.Purchases.Products.Acknowledge acknowledge = publisher.purchases().products().acknowledge(packageName, productId, purchaseToken, productPurchasesAcknowledgeRequest);

            AndroidPublisher.Purchases.Products.Get get = publisher.purchases().products().get(acknowledge.getPackageName(), acknowledge.getProductId(), acknowledge.getToken());
            ProductPurchase productPurchase = get.execute();

            JsonElement payment = JsonParser.parseString(productPurchase.toPrettyString()).getAsJsonObject();

            log.debug("@@@ PAYMENT CONSUME GET RESULT : {}", payment.toString());

            if (Integer.parseInt(payment.getAsJsonObject().get("purchaseState").toString()) == 0) {
                insertUserCourse(inappRequestDTO);

                CourseVo courseVo = courseMapper.getCourseInfo(inappRequestDTO.getCourseId());

                savePaymentResult(inappRequestDTO, payment.getAsJsonObject(), courseVo);

                return new CommonRes("SUCCESS", courseVo);
            }
            else return new CommonRes("FAIL", null);
        }else {
            return new CommonRes("FAIL", null);
        }
    }

    public void insertUserCourse(InappRequestDTO inappRequestDTO) {

        try {
           courseMapper.addUserCourse(inappRequestDTO); //insert후 생성된 pk 값이 inappRequest안에 들어감
           courseMapper.addCourseMedia(inappRequestDTO);
        } catch (Exception e) {
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "InappService.insertUserCourse", "INSERT USER COURSE ERROR: "+e.getMessage());
        }
    }

    public void savePaymentResult(InappRequestDTO inappRequestDTO, JsonObject payment, CourseVo courseVo){
        PaymentVO paymentVO = new PaymentVO();
        paymentVO.setUserCourseId(inappRequestDTO.getUserCourseId());
        paymentVO.setPrice(courseVo.getPrice());
        paymentVO.setOrderId(payment.get("orderId").toString());

        try{
            paymentMapper.savePaymentResult(paymentVO);
        }catch (Exception e){
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "InappService.savePaymentResult", "SAVE PAYMENT ERROR: "+e.getMessage());
        }
    }

}
