package kr.co.collars.api.exam.model;

import lombok.Data;

@Data
public class CommonDto {
    String email;
    int id;
    int preExamNum;
}
