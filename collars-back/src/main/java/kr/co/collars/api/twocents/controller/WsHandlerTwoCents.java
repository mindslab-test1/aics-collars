package kr.co.collars.api.twocents.controller;

import kr.co.collars.api.practice.service.CopycatService;
import kr.co.collars.api.twocents.service.TwoCentsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.BinaryWebSocketHandler;

import java.util.Arrays;

@Slf4j
@Component
public class WsHandlerTwoCents extends BinaryWebSocketHandler {

    @Autowired
    TwoCentsService twoCentsService;

    @Autowired
    CopycatService copycatService;

    @Override
    protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) throws Exception {
        System.out.println("IN");
        byte[] bytes = message.getPayload().array();

//        if(bytes.length == 1) copycatService.getPronResult(session);
//        else
        copycatService.sttGrpcOnMessage(message.getPayload());

        super.handleBinaryMessage(session, message);
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        super.afterConnectionEstablished(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        super.afterConnectionClosed(session, status);
    }
}
