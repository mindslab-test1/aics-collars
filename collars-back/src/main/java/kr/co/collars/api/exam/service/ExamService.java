package kr.co.collars.api.exam.service;

import com.google.api.client.json.Json;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import kr.co.collars.api.auth.service.AuthService;
import kr.co.collars.api.exam.model.*;
import kr.co.collars.api.exception.InternalServerException;
import kr.co.collars.dao.mapper.ExamMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ExamService {

    @Autowired
    ExamMapper examMapper;

    @Autowired
    AuthService authService;

    public void saveExamResult(ExamResultVO examResultVO){
        ExamResultVO finalResultVO = new ExamResultVO();
        try{
            examResultVO.setUserId(authService.getUserId(examResultVO.getUid()));
        }catch (Exception e){
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "LectureService.getLectureStartDate", "get userId error");
        }

        try{
            Exam1VO exam1VO = examMapper.getExam1Tag(examResultVO.getExamNumList().get(0));
            Exam2VO exam2VO = examMapper.getExam2Tag(examResultVO.getExamNumList().get(1));
            Exam3VO exam3VO = examMapper.getExam3Tag(examResultVO.getExamNumList().get(2));
            Exam4VO exam4VO = examMapper.getExam4Tag(examResultVO.getExamNumList().get(3));
            Exam5VO exam5VO = examMapper.getExam5Tag(examResultVO.getExamNumList().get(4));

            finalResultVO.setUserId(examResultVO.getUserId());
            finalResultVO.setPron(new Gson().fromJson(exam1VO.getPronTag(), JsonObject.class).get(examResultVO.getPron()).toString());
            finalResultVO.setAccuracy(new Gson().fromJson(exam1VO.getAccuracyTag(), JsonObject.class).get(examResultVO.getAccuracy()).toString());
            finalResultVO.setVoca(new Gson().fromJson(exam2VO.getVocaTag(), JsonObject.class).get("1").toString());
            finalResultVO.setCulture(new Gson().fromJson(exam3VO.getCultureTag(), JsonObject.class).get(examResultVO.getCulture()).toString());
            finalResultVO.setManner(new Gson().fromJson(exam4VO.getMannerTag(), JsonObject.class).get(examResultVO.getManner()).toString());
            finalResultVO.setExpress(new Gson().fromJson(exam5VO.getExpressTag(), JsonObject.class).get(examResultVO.getManner()).toString());

            log.info("{}", finalResultVO);
        }catch (Exception e){
            e.printStackTrace();
            //throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "ExamService.saveExamResult", "DB select error");
        }

        try{
            examMapper.saveExamResult(finalResultVO);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
