package kr.co.collars.api.practice.model;

import lombok.Data;

import java.util.List;

@Data
public class PracticeResultVO {
    int id;
    int userId;
    int mediaId;
    String question;
    String userAnswer;
    int correct;
    String score;
    //int countResult;
    int completeLecture;
    int isStudyLecture;
    int isStudyPron;
    int isStudyWord;
    int isStudySentence;
    int isStudyQuiz;
}
