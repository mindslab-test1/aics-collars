package kr.co.collars.api.course.controller;

import com.google.firebase.auth.FirebaseAuthException;
import kr.co.collars.api.auth.model.AuthInfoDefault;
import kr.co.collars.api.course.model.AddCourseReq;
import kr.co.collars.api.course.service.CourseService;
import kr.co.collars.common.CommonRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/course")
public class CourseController {

    @Autowired
    CourseService courseService;

    @RequestMapping(value = "/addCourse", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity addCourse(
            @Valid @RequestBody AddCourseReq params,
            BindingResult bindingResult
    ) throws BindException {

        if (bindingResult.hasErrors()) { throw new BindException(bindingResult); }

        try {
            return courseService.addCourse(params);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonRes(e.getMessage(),null));
        }
    }

    @RequestMapping(value = "/updateThumbnail", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity updateThumbnail(
            @RequestParam Integer id,
    @RequestParam MultipartFile thumbnail
    ) throws Exception {
        try {
            return courseService.updateThumbnail(id, thumbnail);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonRes(e.getMessage(),null));
        }
    }

    @RequestMapping(value = "/addMedia", method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<CommonRes> addMedia(
            @RequestParam(required = false) String name,
            @RequestParam MultipartFile video,
            HttpServletRequest request
    ){
        try {
            return courseService.addMedia(name, video);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonRes(e.getMessage(),null));
        }
    }

    @RequestMapping(value = "/randomSuggest", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity randomSuggest(
            @RequestBody AuthInfoDefault authInfo,
            HttpServletRequest request
    ) throws FirebaseAuthException {
        try{
            return courseService.randomSuggest(authInfo);
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonRes(e.getMessage(),null));
        }
    }

}
