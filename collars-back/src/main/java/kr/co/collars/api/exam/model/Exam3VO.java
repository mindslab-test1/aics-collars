package kr.co.collars.api.exam.model;

import lombok.Data;

@Data
public class Exam3VO extends CommonDto{
    int examNum;
    String qText1;
    String qText2;
    String correct;
    String cultureTag;
}
