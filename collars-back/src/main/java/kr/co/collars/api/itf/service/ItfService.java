package kr.co.collars.api.itf.service;

import kr.co.collars.api.itf.client.ItfGrpcCleint;
import kr.co.collars.api.itf.vo.ItfGrpcResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItfService {
    @Autowired
    ItfGrpcCleint itfGrpcCleint;

    public ItfGrpcResponse doItf(String context){
        itfGrpcCleint.setDestination("114.108.173.112", 35019);
        //return itfGrpcCleint.getItf("환전신청");
        return itfGrpcCleint.getItf(context);
    }
}
