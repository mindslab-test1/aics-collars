package kr.co.collars.api.evereading.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class GetColumnReq {
    @NotNull
    private Integer tagId;
    @NotNull
    private Integer current;
    @NotNull
    private Integer increment;

}
