package kr.co.collars.api.lecture.model;

import lombok.Data;

@Data
public class MediaTextVO {
    int id;
    String mediaId;
    String subject;
    String content;
}
