package kr.co.collars.api.practice.model;

import lombok.Data;

@Data
public class PracticeQuizVO {
    int id;
    int userId;
    int mediaId;
    String question;
    String userAnswer;
    String score;
    String correctAnswer;
    int ucourseId;
}
