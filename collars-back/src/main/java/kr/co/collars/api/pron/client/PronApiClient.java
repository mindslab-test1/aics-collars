package kr.co.collars.api.pron.client;

import kr.co.collars.api.pron.vo.PronResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Service
public class PronApiClient {
    @Value("${api.id}")
    private String API_ID;

    @Value("${api.key}")
    private String API_KEY;

    @Value("${maum.pron.api}")
    private String PRON_API;

    @Value("${maum.pron.user}")
    private String USER_ID;

    @Value("${maum.pron.model}")
    private String MODEL;

    @Autowired
    RestTemplate restTemplate;

    public PronResponse maumPronCall(File audio, String text){
        try {
            List<MediaType> acceptableMediaType = new ArrayList<MediaType>();
            acceptableMediaType.add(MediaType.APPLICATION_JSON);

            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(acceptableMediaType);

            MultiValueMap<String, Object> mvm = new LinkedMultiValueMap<String, Object>();
            mvm.add("file", new FileSystemResource(audio));
            mvm.add("apiId", API_ID);
            mvm.add("apiKey", API_KEY);
            mvm.add("userId", USER_ID);
            mvm.add("model", MODEL);
            mvm.add("answerText", text);

            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<MultiValueMap<String, Object>>(mvm, headers);

            ResponseEntity<PronResponse> response = restTemplate.exchange(PRON_API, HttpMethod.POST, requestEntity, PronResponse.class);

            return response.getBody();
        }catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

}
