package kr.co.collars.api.exam.model;

import lombok.Data;

@Data
public class Exam4VO extends CommonDto{
    int id;
    int examNum;
    String qText1;
    String qText2;
    String correctAnswer;
    String mannerTag;
}
