package kr.co.collars.api.twocents.data;

import lombok.Data;

@Data
public class TitleVo {
    private String eng;
    private String kor;
}
