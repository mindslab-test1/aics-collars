package kr.co.collars.api.lecture.model;

import lombok.Data;

@Data
public class LectureMediaVO {
    int id;
    String url;
    String name;
    String title;
}

