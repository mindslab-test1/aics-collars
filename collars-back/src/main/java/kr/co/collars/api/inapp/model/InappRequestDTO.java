package kr.co.collars.api.inapp.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor
public class InappRequestDTO {

    @NotBlank
    String uid;

    @NonNull
    Integer courseId;

    @NotBlank
    String purchaseToken;

    @NotBlank
    String productId;

    String email;

    int userCourseId;

}
