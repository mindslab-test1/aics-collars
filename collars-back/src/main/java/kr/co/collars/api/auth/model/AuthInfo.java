package kr.co.collars.api.auth.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthInfo {
    private String provider;
    private String email;
    private String password;
    private String name;
}
