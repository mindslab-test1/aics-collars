package kr.co.collars.api.itf.controller;

import kr.co.collars.api.itf.vo.ItfGrpcResponse;
import kr.co.collars.api.itf.vo.ItfRequest;
import kr.co.collars.api.itf.service.ItfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/maumAi")
public class ItfController {

    @Autowired
    ItfService itfService;

    @PostMapping(value = "/itf")
    public ItfGrpcResponse itfRestCall(@RequestBody ItfRequest request){

        return itfService.doItf(request.getContext());
    }
}
