package kr.co.collars.api.practice.controller;

import com.google.api.Http;
import kr.co.collars.api.lecture.model.LectureRequestDto;
import kr.co.collars.api.practice.model.CopycatVO;
import kr.co.collars.api.practice.model.PracticeRequestDto;
import kr.co.collars.api.practice.model.PracticeResultVO;
import kr.co.collars.api.practice.model.PracticeWordVO;
import kr.co.collars.api.practice.service.PracticeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.ws.Response;
import java.util.LinkedList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/practice")
public class PracticeController {

    private final PracticeService practiceService;

    PracticeController(PracticeService practiceService){
        this.practiceService = practiceService;
    }

    @PostMapping("/getCopycatData")
    public ResponseEntity getCopycatData(@RequestBody PracticeRequestDto practiceRequestDto){
        HttpHeaders resHeaders = new HttpHeaders();
        resHeaders.add("Content-Type", "application/json;charset=UTF-8");

        return new ResponseEntity(practiceService.getPracticePron(practiceRequestDto), resHeaders, HttpStatus.OK);
    }

    @PostMapping("/getWordData")
    public ResponseEntity getWordPracticeData(@RequestBody PracticeRequestDto practiceRequestDto){
        HttpHeaders resHeaders = new HttpHeaders();
        resHeaders.add("Content-Type", "application/json;charset=UTF-8");

        return new ResponseEntity(practiceService.getPracticeWord(practiceRequestDto), resHeaders, HttpStatus.OK);
    }

    @PostMapping("/getSentenceData")
    public ResponseEntity getSentenceData(@RequestBody PracticeRequestDto practiceRequestDto){
        HttpHeaders resHeaders = new HttpHeaders();
        resHeaders.add("Content-Type", "application/json;charset=UTF-8");

        return new ResponseEntity(practiceService.getPracticeSentence(practiceRequestDto), resHeaders, HttpStatus.OK);
    }

    @PostMapping("/savePracticeResult")
    public ResponseEntity savePracticeResult(@RequestBody PracticeRequestDto practiceRequestDto){
        practiceService.savePracticeResult(practiceRequestDto);

        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/getPracticeCount")
    public ResponseEntity getPracticeCount(@RequestBody PracticeRequestDto practiceRequestDto){
        return new ResponseEntity(practiceService.getPracticeCountService(practiceRequestDto), HttpStatus.OK);
    }

    @PostMapping("/getAllPracticeData")
    public ResponseEntity getAllPracticeData(@RequestBody PracticeRequestDto practiceRequestDto){
        HttpHeaders resHeaders = new HttpHeaders();
        resHeaders.add("Content-Type", "application/json;charset=UTF-8");
        return new ResponseEntity(practiceService.getAllPracticeData(practiceRequestDto).toString(), resHeaders, HttpStatus.OK);
    }

    @PostMapping("/completeLecture")
    public ResponseEntity completeLecture(@RequestBody PracticeRequestDto practiceRequestDto){
        practiceService.completeLecture(practiceRequestDto);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/getQuizData")
    public ResponseEntity getQuizData(@RequestBody PracticeRequestDto practiceRequestDto){
        HttpHeaders resHeaders = new HttpHeaders();
        resHeaders.add("Content-Type", "application/json;charset=UTF-8");

        return new ResponseEntity(practiceService.getQuizData(practiceRequestDto), resHeaders, HttpStatus.OK);
    }
}
