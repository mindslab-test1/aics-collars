package kr.co.collars.api.practice.model;

import lombok.Data;

@Data
public class PracticePronVO {
    int id;
    int mediaId;
    String sentence;
}
