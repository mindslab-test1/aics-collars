package kr.co.collars.api.course.model;

import kr.co.collars.dao.vo.CourseItemVo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
public class AddCourseReq {
    @NotBlank
    private String title;
    private String productId;
    @NotBlank
    private String description;
    @NotNull
    private Float level;
    @NotNull
    private Integer price;

    private List<CourseItemVo> mediaList;
}
