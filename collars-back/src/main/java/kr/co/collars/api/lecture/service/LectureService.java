package kr.co.collars.api.lecture.service;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserRecord;
import kr.co.collars.api.auth.service.AuthService;
import kr.co.collars.api.exception.InternalServerException;
import kr.co.collars.api.lecture.model.*;
import kr.co.collars.api.practice.model.PracticeRequestDto;
import kr.co.collars.api.practice.model.PracticeResultVO;
import kr.co.collars.api.practice.service.PracticeService;
import kr.co.collars.dao.mapper.LectureMapper;
import kr.co.collars.dao.vo.UserVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class LectureService {

    @Value("${media.server.url}")
    String mediaUrl;
    @Autowired
    LectureMapper lectureMapper;
    @Autowired
    AuthService authService;
    @Autowired
    PracticeService practiceService;

    public String getUserInfo(String uid){
        UserRecord userRecord = null;
        try{
            userRecord = FirebaseAuth.getInstance().getUser(uid);
        }catch (Exception e){
            e.printStackTrace();
        }

        return userRecord.getEmail();
    }

    public LectureResponseDto getUserCourseInfo(PracticeRequestDto practiceRequestDto){
        practiceRequestDto.setEmail(getUserInfo(practiceRequestDto.getUid()));

        List<UserCourseVO> userCourseVOList = new LinkedList<>();
        LectureResponseDto lectureResponseDto = new LectureResponseDto();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        List<CourseMeidaVO> courseMeidaVOList = new LinkedList<>();

        try {
            userCourseVOList = lectureMapper.getUserCourse(practiceRequestDto.getEmail());

            lectureResponseDto.setUserCourseList(userCourseVOList);
            for(UserCourseVO userCourseVO : userCourseVOList) {
                if(userCourseVO.getCompleted() == 0) {
                    courseMeidaVOList = lectureMapper.getLectureList(userCourseVO.getId());
                    lectureResponseDto.setStartDate(format.format(userCourseVO.getStartDate()));
                    lectureResponseDto.setCourseMeidaVO(courseMeidaVOList);
                    break;
                }
            }
        }catch (Exception e){
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "LectureService.getLectureStartDate", "get lecture start date error");
            //e.printStackTrace();
        }

        //user의 학습 진도 상황 파악
        List<Boolean> completeCheck  = new LinkedList<>();
        try{
            Integer finshedDay = lectureMapper.getFinishedDay(practiceRequestDto.getEmail());
            if(finshedDay != null) {
                for (int i = 1; i <= 3; i++) {
                    if (finshedDay >= i) completeCheck.add(true);
                    else completeCheck.add(false);
                }
                lectureResponseDto.setIsStudyCompleted(completeCheck);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        /*try{
            for(int i=0;i<courseMeidaVOList.size();i++){
                practiceRequestDto.setMediaId(courseMeidaVOList.get(i).getMediaId());
                List<PracticeResultVO> practiceResultVORes = practiceService.getPracticeCountService(practiceRequestDto);
                boolean isComplte = true;
                for(int j=0;j<practiceResultVORes.size();j++){
                    if(practiceResultVORes.get(j).getCountResult()<1) isComplte = false;
                }
                completeCheck.add(isComplte);
            }

            lectureResponseDto.setIsStudyCompleted(completeCheck);
        }catch (Exception e){

        }*/

        return lectureResponseDto;
    }

    public LectureMediaVO getLectureName(PracticeRequestDto practiceRequestDto){
        LectureMediaVO name= new LectureMediaVO();
        try{
            name = lectureMapper.getLectureName(practiceRequestDto);
        }catch (Exception e){
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "LectureService.getLectureStartDate", "get lecture name error");
            //e.printStackTrace();
        }

        return name;
    }

    public LectureResponseDto getLectureData(LectureRequestDto lectureRequestDto){
        try{
            LectureMediaVO lectureMediaVO = lectureMapper.getLectureData(lectureRequestDto);
            lectureMediaVO.setUrl(mediaUrl+lectureMediaVO.getUrl());

            List<MediaTextVO> mediaTextVOList = lectureMapper.getLectureText(lectureRequestDto);
            LectureResponseDto lectureResponseDto = new LectureResponseDto();
            lectureResponseDto.setMediaUrl(lectureMediaVO.getUrl());
            lectureResponseDto.setMediaTextVO(mediaTextVOList);
            lectureResponseDto.setTitle(lectureMediaVO.getTitle());

            log.debug("### lectureMediaVO.getUrl {}", lectureResponseDto.getMediaUrl());

            return lectureResponseDto;
        }catch (Exception e){
            log.debug(e.getMessage());
        }
        return null;
    }

    public String finishCourse(PracticeRequestDto practiceRequestDto){
        try{
            practiceRequestDto.setUserId(authService.getUserId(practiceRequestDto.getUid()));
            lectureMapper.userFinishCourse(practiceRequestDto);

            return "SUCCESS";
        }catch (Exception e){
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "LectureService.finishCourse", "Request update finished user fail");
        }
    }
}
