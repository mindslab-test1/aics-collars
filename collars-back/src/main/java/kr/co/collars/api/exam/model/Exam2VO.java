package kr.co.collars.api.exam.model;

import lombok.Data;

@Data
public class Exam2VO extends CommonDto{
    int examNum;
    String pText;
    String qText;
    String mulChoice1;
    String mulChoice2;
    String mulChoice3;
    String mulChoice4;
    String correct;
    String vocaTag;
}
