package kr.co.collars.api.stt.client;

import kr.co.collars.api.stt.vo.SttResponse;
import kr.co.collars.common.CommonConvert;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class MaumRestClient {

    @Value("${engedu.stt.api}")
    private String API_URL;

    @Value("${api.id}")
    private String API_ID;

    @Value("${api.key}")
    private String API_KEY;

    @Value("${engedu.stt.cmd}")
    private String CMD;

    @Value("${engedu.stt.lang}")
    private String LANG;

    @Value("${engedu.stt.sampling}")
    private String SAMPLING;

    @Value("${engedu.stt.level}")
    private String LEVEL;

    @Autowired
    RestTemplate restTemplate;

    public SttResponse muamSttCall(MultipartFile audio){
        try {
            File convFile = CommonConvert.convertFile(audio);

            List<MediaType> acceptableMediaType = new ArrayList<MediaType>();
            acceptableMediaType.add(MediaType.MULTIPART_FORM_DATA);

            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(acceptableMediaType);

            MultiValueMap<String, Object> mvm = new LinkedMultiValueMap<String, Object>();
            mvm.add("file", new FileSystemResource(convFile));
            mvm.add("ID", API_ID);
            mvm.add("key", API_KEY);
            mvm.add("cmd", CMD);
            mvm.add("lang", LANG);
            mvm.add("sampling", SAMPLING);
            mvm.add("level", LEVEL);

            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<MultiValueMap<String, Object>>(mvm, headers);

            ResponseEntity<SttResponse> response = restTemplate.exchange(API_URL, HttpMethod.POST, requestEntity, SttResponse.class);

            return response.getBody();
        }catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

}
