package kr.co.collars.api.exam.service;

import kr.co.collars.api.exam.model.Exam3VO;
import kr.co.collars.common.CommonConvert;
import kr.co.collars.dao.mapper.ExamMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class ExamQ3Service extends CommonExamService{

    @Autowired
    ExamMapper examMapper;

    public Exam3VO getQuestion(Exam3VO exam3VO){
        int exam3Count = examMapper.getExam3Count();
        int randomExamNum = (int)Math.random()*exam3Count+1;
        exam3VO.setExamNum(randomExamNum);

        return examMapper.getQTextExam3(exam3VO);
    }

    public int getScore(int qNum, String inputAnswer){
        Exam3VO exam3VO = examMapper.getExam3Answer(qNum);
        inputAnswer = preProcess(inputAnswer);
        int score = -1;

        if(correctScore(exam3VO.getCorrect(), inputAnswer)) score = 100;
        else score = 0;

        return score;
    }

}
