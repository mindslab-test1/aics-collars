package kr.co.collars.api.evereading.service;

import kr.co.collars.api.evereading.model.GetColumnReq;
import kr.co.collars.common.CommonRes;
import kr.co.collars.dao.mapper.EvereadingMapper;
import kr.co.collars.dao.mapper.HashtagMapper;
import kr.co.collars.dao.vo.EvereadingBannerVo;
import kr.co.collars.dao.vo.EvereadingTagVo;
import kr.co.collars.dao.vo.EvereadingVo;
import kr.co.collars.dao.vo.HashtagVo;
import kr.co.collars.external_service.vo.ResponseUploadedFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Slf4j
@Transactional
@Service
public class EvereadingService {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    EvereadingMapper evereadingMapper;
    @Autowired
    HashtagMapper hashtagMapper;

    @Value("${media.server.url}")
    private String MEDIA_SERVER_URL;
    @Value("${media.server.port}")
    private String MEDIA_SERVER_PORT;
    @Value("${media.server.dir}")
    private String MEDIA_SERVER_DIR;
    public ResponseEntity getAllSectors(String lang){
        List<HashtagVo> result = evereadingMapper.getAllSectors(lang);
        return ResponseEntity.status(HttpStatus.OK).body(new CommonRes("success", result));
    }
    public ResponseEntity getColumns(GetColumnReq params){
        List<EvereadingVo> results = evereadingMapper.getColumns(params);
        for(int i = 0; i < results.size(); i++){
            results.get(i).setThumbnail(MEDIA_SERVER_URL + results.get(i).getThumbnail());
            results.get(i).setImage(MEDIA_SERVER_URL + results.get(i).getImage());
        }
        return ResponseEntity.status(HttpStatus.OK).body(new CommonRes("success", results));
    }
    public ResponseEntity addColumn(String sector_kor,String sector_eng, MultipartFile file, Integer level, List<String> tags){
        String url = MEDIA_SERVER_URL + "/media/file:upload";
        ResponseEntity<ResponseUploadedFileInfo> resMedia = addToMediaServer(URI.create(url), file);
        String mediaPath = extractMediaPath(resMedia.getBody().getFileDownloadUri());
        if(resMedia.getStatusCode().value() != 201){
            return ResponseEntity.status(402).body(new CommonRes("Media Server Error: Can't upload thumbnail image.", null));
        }
        try {
            // Sector
            HashtagVo sectorKorTag = hashtagMapper.getTag(sector_kor);
            if(sectorKorTag == null){
                HashtagVo newKorTag = new HashtagVo();
                newKorTag.setTag(sector_kor);
                hashtagMapper.addTag(newKorTag);
                sectorKorTag = newKorTag;
            }
            HashtagVo sectorEngTag = hashtagMapper.getTag(sector_eng);
            if(sectorEngTag == null){
                HashtagVo newEngTag = new HashtagVo();
                newEngTag.setTag(sector_eng);
                hashtagMapper.addTag(newEngTag);
                sectorEngTag = newEngTag;
            }

            // AddColumn
            String filename = FilenameUtils.getBaseName(file.getOriginalFilename());
            EvereadingVo evereadingVo = new EvereadingVo();
            evereadingVo.setName(filename);
            evereadingVo.setLev(level);
            evereadingVo.setUrl(mediaPath);
            evereadingVo.setSectorKor(sectorKorTag.getId());
            evereadingVo.setSectorEng(sectorEngTag.getId());
            evereadingMapper.addColumn(evereadingVo);

            if (tags != null) {
                // 기존 태그 확인
                List<HashtagVo> hashtagVos = hashtagMapper.getTags(tags);
                List<String> storedTags = new ArrayList<>();
                for (HashtagVo hashtagVo : hashtagVos) {
                    storedTags.add(hashtagVo.getTag());
                }
                // 없는 태그 추가 & 맵핑할 태그 리스트화
                List<Integer> tagIds = new ArrayList<>();
                for (String tag : tags) {
                    if (storedTags.contains(tag)) {
                        int index = storedTags.indexOf(tag);
                        tagIds.add(hashtagVos.get(index).getId());
                    } else {
                        HashtagVo newTag = new HashtagVo();
                        newTag.setTag(tag);
                        hashtagMapper.addTag(newTag);
                        tagIds.add(newTag.getId());
                    }
                }
                // 태그 맵핑
                evereadingMapper.setTags(evereadingVo.getId(), tagIds);
            }
        }catch (Exception e){
            return ResponseEntity.status(500).body(new CommonRes(e.getMessage(), null));
        }
        return ResponseEntity.status(HttpStatus.OK).body(new CommonRes("success", null));
    }
    public ResponseEntity getBanners(){
        List<EvereadingBannerVo> results = evereadingMapper.getBanners();
        for(int i = 0; i < results.size(); i++){
            results.get(i).setUrl(MEDIA_SERVER_URL + results.get(i).getUrl());
        }
        return ResponseEntity.status(HttpStatus.OK).body(new CommonRes("success", results));
    }
    public ResponseEntity addBanner(String name, MultipartFile file) throws Exception {
        String url = MEDIA_SERVER_URL + "/media/file:upload";
        ResponseEntity<ResponseUploadedFileInfo> resMedia = addToMediaServer(URI.create(url), file);
        String mediaPath = extractMediaPath(resMedia.getBody().getFileDownloadUri());
        if(resMedia.getStatusCode().value() != 201){
            return ResponseEntity.status(402).body(new CommonRes("Media Server Error: Can't upload thumbnail image.", null));
        }

        EvereadingBannerVo evereadingBannerVo = new EvereadingBannerVo();
        System.out.println(name);
        if(name != null){evereadingBannerVo.setName(name);}
        else{evereadingBannerVo.setName(FilenameUtils.getBaseName(file.getOriginalFilename()));}
        evereadingBannerVo.setUrl(mediaPath);

        try{
            evereadingMapper.addBanner(evereadingBannerVo);
        }catch (Exception e){
            log.error(e.getMessage());
            throw new Exception("addBanner: " + e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.OK).body(new CommonRes("success", evereadingBannerVo));
    }
    public ResponseEntity updateThumbnail(Integer id, MultipartFile file) throws Exception {
        String uploadUrl = MEDIA_SERVER_URL + "/media/file:upload";
        String deleteUrl = MEDIA_SERVER_URL + "/media/file:delete";

        EvereadingVo current = evereadingMapper.getColumn(id);

        ResponseEntity<ResponseUploadedFileInfo> resMedia = addToMediaServer(URI.create(uploadUrl), file);
        String mediaPath = extractMediaPath(resMedia.getBody().getFileDownloadUri());
        if(resMedia.getStatusCode().value() != 201){
            return ResponseEntity.status(402).body(new CommonRes("Media Server Error: Can't upload thumbnail image.", null));
        }

        EvereadingVo evereadingVo = new EvereadingVo();
        evereadingVo.setId(id);
        evereadingVo.setThumbnail(mediaPath);

        try{
            evereadingMapper.updateThumbnail(evereadingVo);
            if(current.getThumbnail() != null){
                removeFromMediaServer(URI.create(deleteUrl), current.getThumbnail());
            }
        }catch (Exception e){
            log.error(e.getMessage());
            removeFromMediaServer(URI.create(deleteUrl), mediaPath);
            throw new Exception("updateThumbnail: " + e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.OK).body(new CommonRes("success", evereadingVo));
    }
    public ResponseEntity updateImage(Integer id, MultipartFile file) throws Exception {
        String uploadUrl = MEDIA_SERVER_URL + "/media/file:upload";
        String deleteUrl = MEDIA_SERVER_URL + "/media/file:delete";

        EvereadingVo current = evereadingMapper.getColumn(id);

        ResponseEntity<ResponseUploadedFileInfo> resMedia = addToMediaServer(URI.create(uploadUrl), file);
        String mediaPath = extractMediaPath(resMedia.getBody().getFileDownloadUri());
        if(resMedia.getStatusCode().value() != 201){
            return ResponseEntity.status(402).body(new CommonRes("Media Server Error: Can't upload thumbnail image.", null));
        }

        EvereadingVo evereadingVo = new EvereadingVo();
        evereadingVo.setId(id);
        evereadingVo.setImage(mediaPath);

        try{
            evereadingMapper.updateImage(evereadingVo);
            if(current.getImage() != null){
                removeFromMediaServer(URI.create(deleteUrl), current.getImage());
            }
        }catch (Exception e){
            log.error(e.getMessage());
            removeFromMediaServer(URI.create(deleteUrl), mediaPath);
            throw new Exception("updateThumbnail: " + e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.OK).body(new CommonRes("success", evereadingVo));
    }

    // FUNCTION
    private ResponseEntity<ResponseUploadedFileInfo> addToMediaServer(URI uri, MultipartFile multipartFile){
        HttpHeaders requestMasterHeaders = new HttpHeaders();
        requestMasterHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", multipartFile.getResource());
        body.add("uploadDirectory", MEDIA_SERVER_DIR);
        HttpEntity<MultiValueMap<String, Object>> requestMasterHttpEntity = new HttpEntity<>(body, requestMasterHeaders);
        return restTemplate.exchange(uri,
                HttpMethod.POST,
                requestMasterHttpEntity,
                new ParameterizedTypeReference<ResponseUploadedFileInfo>() {});
    }

    private ResponseEntity<Void> removeFromMediaServer(URI uri, String path){
        String formattedPath = path.split("/media")[1];

        HttpHeaders requestMasterHeaders = new HttpHeaders();
        requestMasterHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map<String, String> body = new HashMap<>();
        body.put("deleteFileFullPath", formattedPath);
        HttpEntity<Map<String, String>> requestMasterHttpEntity = new HttpEntity<>(body, requestMasterHeaders);
        return restTemplate.exchange(uri, HttpMethod.DELETE, requestMasterHttpEntity, Void.class);
    }
    private String extractMediaPath(String url){
        String result[] = url.split("(?=/media/aics)");
        return result[1];
    }
}
