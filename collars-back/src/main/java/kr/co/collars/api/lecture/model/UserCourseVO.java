package kr.co.collars.api.lecture.model;

import lombok.*;

import java.util.Date;

@Data
public class UserCourseVO {
    int id;
    int userId;
    int active;
    int completed;
    Date startDate;
    Date endDate;

    //CLS_COURSE
    String title;
    String description;
    int lev;
}
