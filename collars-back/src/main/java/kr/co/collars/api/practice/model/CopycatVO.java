package kr.co.collars.api.practice.model;

import lombok.Data;

@Data
public class CopycatVO {
    int id;
    String speech;
    String translate;
}
