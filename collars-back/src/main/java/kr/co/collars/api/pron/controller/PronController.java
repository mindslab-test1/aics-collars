package kr.co.collars.api.pron.controller;

import kr.co.collars.api.pron.client.PronApiClient;
import kr.co.collars.api.pron.vo.PronResponse;
import kr.co.collars.common.CommonConvert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/maumAi/engedu")
public class PronController {

    @Autowired
    PronApiClient pronApiClient;

    @PostMapping(value = "/pronReactive")
    @ResponseBody
    public PronResponse requestPron(@RequestParam MultipartFile audio) throws IOException {
        File convFile = CommonConvert.convertFile(audio);
        return  pronApiClient.maumPronCall(convFile, "");
    }
}
