package kr.co.collars.api.exam.service;

import kr.co.collars.api.exam.model.ExamScoreVO;
import lombok.extern.slf4j.Slf4j;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class CommonExamService {

    /*
     * 완전 matching 체크
     * */
    public boolean correctScore(String correct, String inputAnswer){
        List<String> answers = new ArrayList<>();

        if(correct.contains("@@")) answers = Arrays.asList(correct.split("@@"));
        else answers.add(correct);

        for(String answer : answers) {
            if (answer.equals(inputAnswer)) return true;
        }

        return false;
    }

    /*
     * 순서에 관계있는 if문 체크
     * */
    public int ifSocre(List<ExamScoreVO> examScoreVOs, String inputAnswer){
        boolean checkResult = false;
        inputAnswer = inputAnswer.replaceAll(" ", "");

        for(ExamScoreVO examScoreVO : examScoreVOs){
            int preIndex = -1;
            checkResult = true;
            List<String> orderAns = Arrays.asList(examScoreVO.getSentence().split("\\+"));

            for(String order : orderAns){
                int indexOrder = inputAnswer.indexOf(order.trim().replaceAll(" ", ""));

                if(indexOrder<0 && preIndex>=indexOrder) {
                    checkResult = false;
                }
                else preIndex = indexOrder;

            }

            if(checkResult) return examScoreVO.getScore();
        }

        return 0;
    }

    /*
     * 특정 단어가 포함되어 있는지 체크
     * */
    public int containWordCheck(List<ExamScoreVO> examScoreVOS, String inputAnswer, int score){
        List<String> answers = new ArrayList<>();

        for(ExamScoreVO examScoreVO : examScoreVOS) {
            if (inputAnswer.contains(examScoreVO.getSentence())) {
                List<String> words = Arrays.asList(inputAnswer.split(" "));
                for(String word : words){
                    if(word.equals(examScoreVO.getSentence())){
                        score = score + examScoreVO.getScore();
                        break;
                    }
                }
            }
        }

        return score;
    }

    public static String preProcess(String str){
        return removeSpecialChar(str).toLowerCase().replaceAll("\\.", "").trim();
    }


    /*
    * 특수문자 제거
    * */
    public static String removeSpecialChar(String str){
        String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
        str = str.replace(match, "").replaceAll("\\-", " ");
        log.info(" removeSepcialChar ---> {}", str);
        return str;
    }


    /*
    * 숫자를 한글로
    * */
    public static String intToHangul(String numString){
        String[] han1 = {"","일","이","삼","사","오","육","칠","팔","구"};
        String[] han2 = {"","십","백","천"};
        String[] han3 = {"","만","억"};

        StringBuffer result = new StringBuffer();
        int num_len = numString.length();

        for(int i=num_len-1; i>=0; i--){
            if(i==9 && han1[Integer.parseInt(numString.substring(num_len-i-1, num_len-i))] == "일"){

            }

            if(han1[Integer.parseInt(numString.substring(num_len-i-1, num_len-i))]=="일" && num_len-i != num_len) {
                result.append(han1[0]);
            }
            else {
                result.append(han1[Integer.parseInt(numString.substring(num_len-i-1, num_len-i))]);
            }

            if(Integer.parseInt(numString.substring(num_len-i-1, num_len-i))>0)
                result.append(han2[i%4]);

            if(i%4==0)
                result.append(han3[i/3]);
        }

        return result.toString();
    }


    /*
    * 숫자를 영어 단어로 변환
    * */


    private static final String[] tensNames = {
            "",
            " ten",
            " twenty",
            " thirty",
            " forty",
            " fifty",
            " sixty",
            " seventy",
            " eighty",
            " ninety"
    };

    private static final String[] numNames = {
            "",
            " one",
            " two",
            " three",
            " four",
            " five",
            " six",
            " seven",
            " eight",
            " nine",
            " ten",
            " eleven",
            " twelve",
            " thirteen",
            " fourteen",
            " fifteen",
            " sixteen",
            " seventeen",
            " eighteen",
            " nineteen"
    };

    private static String convertLessThanOneThousand(int number) {
        String soFar;

        if (number % 100 < 20){
            soFar = numNames[number % 100];
            number /= 100;
        }
        else {
            soFar = numNames[number % 10];
            number /= 10;

            soFar = tensNames[number % 10] + soFar;
            number /= 10;
        }
        if (number == 0) return soFar;
        return numNames[number] + " hundred" + soFar;
    }


    public static String englishNumberToWords(long number) {
        // 0 to 999 999 999 999
        if (number == 0) { return "zero"; }

        String snumber = Long.toString(number);

        // pad with "0"
        String mask = "000000000000";
        DecimalFormat df = new DecimalFormat(mask);
        snumber = df.format(number);

        // XXXnnnnnnnnn
        int billions = Integer.parseInt(snumber.substring(0,3));
        // nnnXXXnnnnnn
        int millions  = Integer.parseInt(snumber.substring(3,6));
        // nnnnnnXXXnnn
        int hundredThousands = Integer.parseInt(snumber.substring(6,9));
        // nnnnnnnnnXXX
        int thousands = Integer.parseInt(snumber.substring(9,12));

        String tradBillions;
        switch (billions) {
            case 0:
                tradBillions = "";
                break;
            case 1 :
                tradBillions = convertLessThanOneThousand(billions) + " billion ";
                break;
            default :
                tradBillions = convertLessThanOneThousand(billions) + " billion ";
        }
        String result =  tradBillions;

        String tradMillions;
        switch (millions) {
            case 0:
                tradMillions = "";
                break;
            case 1 :
                tradMillions = convertLessThanOneThousand(millions)
                        + " million ";
                break;
            default :
                tradMillions = convertLessThanOneThousand(millions)
                        + " million ";
        }
        result =  result + tradMillions;

        String tradHundredThousands;
        switch (hundredThousands) {
            case 0:
                tradHundredThousands = "";
                break;
            case 1 :
                tradHundredThousands = "one thousand ";
                break;
            default :
                tradHundredThousands = convertLessThanOneThousand(hundredThousands)
                        + " thousand ";
        }
        result =  result + tradHundredThousands;

        String tradThousand;
        tradThousand = convertLessThanOneThousand(thousands);
        result =  result + tradThousand;

        // remove extra spaces!
        return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", " ");
    }

}
