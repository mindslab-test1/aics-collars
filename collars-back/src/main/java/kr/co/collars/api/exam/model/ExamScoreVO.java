package kr.co.collars.api.exam.model;

import lombok.Data;

@Data
public class ExamScoreVO {
    int id;
    int examNum;
    String sentence;
    int score;
    String correctIf;
}
