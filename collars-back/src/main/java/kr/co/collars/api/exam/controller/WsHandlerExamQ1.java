package kr.co.collars.api.exam.controller;

import kr.co.collars.api.exam.service.ExamQ1Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.BinaryWebSocketHandler;

import java.nio.charset.StandardCharsets;


@Slf4j
@Component
public class WsHandlerExamQ1 extends BinaryWebSocketHandler{

    private final ExamQ1Service examQ1Service;

    WsHandlerExamQ1(ExamQ1Service examQ1Service){
        this.examQ1Service = examQ1Service;
    }


    @Override
    protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) throws Exception{
        byte[] byte_buff = message.getPayload().array();

        if(byte_buff.length == 1)  examQ1Service.getPronResult(session);
        else if(byte_buff.length>1 && byte_buff.length<100){
            log.debug("@ -----> Exam Num result {}", new String(byte_buff, StandardCharsets.UTF_8));
            examQ1Service.setExamNum(new String(byte_buff, StandardCharsets.UTF_8));
        }
        else examQ1Service.sttGrpcOnMessage(message.getPayload());

        super.handleBinaryMessage(session, message);
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        log.debug("@ -----> WsHandlerPron.afterConnectionEstablished() >> {}", session.getAttributes().toString());
        examQ1Service.sttGrpcConn(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception{
        log.debug("@ <----- WsHandlerPron.afterConnectionClosed() >> {}", session.getAttributes().toString());
        examQ1Service.gRpcShutdown();
    }
}
