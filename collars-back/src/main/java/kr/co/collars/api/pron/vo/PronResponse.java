package kr.co.collars.api.pron.vo;

import lombok.Data;

import java.util.List;

@Data
public class PronResponse {

    int score;
    String sttResult;
    Result result;

    @Data
    public static class Result {
        String resultText;
        String answerText;
        String recordUrl;
        double speedScore;
        List<Words> words;
        double resCode;
        double rhythmScore;
        double segmentalScore;
        double sentenceScore;
        double totalScore;
        double pronScore;
        double intonationScore;

        @Data
        public static class Words {
            String word;
            double pronScore;
        }
    }
}
