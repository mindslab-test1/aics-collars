package kr.co.collars.api.exam.service;

import com.google.gson.Gson;
import kr.co.collars.api.exam.model.Exam1VO;
import kr.co.collars.api.exam.model.ExamRequestDto;
import kr.co.collars.api.exam.model.ExamScoreVO;
import kr.co.collars.api.exception.InternalServerException;
import kr.co.collars.api.itf.service.ItfService;
import kr.co.collars.api.itf.vo.ItfGrpcResponse;
import kr.co.collars.api.pron.client.PronApiClient;
import kr.co.collars.api.pron.vo.PronResponse;
import kr.co.collars.api.stt.client.SttClient;
import kr.co.collars.api.stt.client.MaumRestClient;
import kr.co.collars.api.stt.client.SttHandler;
import kr.co.collars.api.stt.vo.SttResponse;
import kr.co.collars.common.CommonConvert;
import kr.co.collars.dao.mapper.ExamMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

@Slf4j
@Component
public class ExamQ1Service extends CommonExamService{

    @Value("${engedu.stt.gApi}")
    private String engEduIp;
    @Value("${engedu.stt.gPort}")
    private int port;
    @Value("${engedu.stt.gSampling}")
    private int sampling;

    @Autowired
    ExamMapper examMapper;

    private final PronApiClient pronApiClient;
    private final MaumRestClient maumRestClient;
    private final ItfService itfService;

    private SttClient sttClient = null;
    int examNum;
    boolean sessionConnStatus;

    public ExamQ1Service(PronApiClient pronApiClient, MaumRestClient maumRestClient, ItfService itfService){
        this.pronApiClient = pronApiClient;
        this.maumRestClient = maumRestClient;
        this.itfService = itfService;
    }

    public Exam1VO getQuestion(Exam1VO exam1VO){
        int exam1Count = examMapper.getExam1Count();
        int randomExamNum = (int)Math.random()*exam1Count+1;
        exam1VO.setExamNum(randomExamNum);

        return examMapper.getQTextExam1(exam1VO);
    }


    public void setExamNum(String examNumStr){
        examNum = Integer.parseInt(examNumStr.split("-")[1]);
    }

    /*
    * stt를 gprc로 가져오지 않고 wav파일 통째로
    * maum rest api를 찌르는 경우
    * */
    public PronResponse requestTest(@RequestParam MultipartFile audio) throws IOException{
        PronResponse pronResponse = new PronResponse();
        File convFile = CommonConvert.convertFile(audio);

        try{
            SttResponse sttResponse = maumRestClient.muamSttCall(audio);
            pronResponse = pronApiClient.maumPronCall(convFile, sttResponse.getData());
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            convFile.delete();
        }

        return pronResponse;
    }


    public void sttGrpcOnMessage(ByteBuffer data){
        sttClient.onMessage(data);
    }

    public void sttGrpcConn(WebSocketSession session){
        sessionConnStatus = true;
        sttClient = new SttClient(session, mCallback);
        //sttClient.onStart("10.122.64.71", 9801, "eng", 16000, "wps");
        sttClient.onStart(engEduIp, port, "eng", sampling, "wps");
    }

    /*
    * Grpc onComplete에서 callback 후
    * stt 전처리, pron rest 호출
    * */
    SttHandler.Callback mCallback = new SttHandler.Callback() {
        @Override
        public void nextProcess(WebSocketSession session, List<String> resultList) throws IOException {
            if (sessionConnStatus == true) {
                String resultText = preProcess(resultList.get(0));
                String resultFilePath = resultList.get(1);
                String intStr = "";
                int score = 0;

                for (int i = 0; i < resultText.length(); i++) {
                    char ch = resultText.charAt(i);
                    if (48 <= ch && ch <= 57) {
                        intStr += ch;
                    } else {
                        if (intStr != "") {
                            String replaceString = englishNumberToWords(Long.parseLong(intStr));
                            resultText = resultText.replaceAll(intStr, replaceString + " ");
                            intStr = "";
                        }
                    }
                }

                try {
                    score = getScore(examNum, resultText);
                } catch (Exception e) {
                    throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "Exam1 Score exception", "Exam1 calculate score error : " + e.getLocalizedMessage());
                }

                try {
                    File file = new File(resultFilePath);
                    PronResponse pronResponse = pronApiClient.maumPronCall(file, "");

                    pronResponse.setScore(score);
                    pronResponse.setSttResult(resultText);

                    Gson gson = new Gson();
                    String toJson = gson.toJson(pronResponse);

                    log.info("@ ------> ExamService.getPronResult >> {}", pronResponse);
                    session.sendMessage(new TextMessage(toJson));
                } catch (Exception e) {
                    sendErroMessage(session);
                    throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "Exam1 pron exception", "Exam1 pron exception : " + e.getLocalizedMessage());
                } finally {
                    sttClient.deleteFile();
                }
            }
        }
    };

    public int getScore(int examNum, String inputAnswer){
        inputAnswer = preProcess(inputAnswer);
        int score = -1;

        ExamRequestDto examRequestDto = new ExamRequestDto();
        examRequestDto.setExamNum(examNum);
        examRequestDto.setAnswer(inputAnswer.replaceAll(" ", ""));

        try{
            ExamScoreVO examScoreVO = examMapper.getExam1Score(examRequestDto);
            if(examScoreVO == null){
                score = 0;
            }else{
                score = examScoreVO.getScore();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        if(score<0) return 0;
        else return score;
    }

    /*
     * Grpc onComplete
     * */
    public void getPronResult(WebSocketSession session) throws IOException{
        sttClient.gracefulShutdown();
    }

    /*
     * Grpc Stt에서 결과를 가져와
     * Grpc Itf 실행 후 값을 가져옴
     * */
    public void getSttItfResult(WebSocketSession session) throws IOException{
        //String result = sttClient.gracefulShutdown();
        String result = "";

        try {
            log.info("getSttItfResult >> {}", result);
            ItfGrpcResponse itfResponse = itfService.doItf(result);
            session.sendMessage(new TextMessage(itfResponse.toString()));
        }catch (IOException e){
            sendErroMessage(session);
            e.printStackTrace();
        }finally {
            sttClient.deleteFile();
        }
    }

    /*
    * client쪽에서 websocke을 끊기 위해 에러를 보냄
    * */
    public void sendErroMessage(WebSocketSession session){
        try{
            session.sendMessage(new TextMessage("ERROR"));
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void gRpcShutdown(){
        sessionConnStatus = false;
        sttClient.gracefulShutdown();
    }
}
