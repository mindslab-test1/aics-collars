package kr.co.collars.api.notice.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class SetNoticeReq {
    @NotNull
    private String title;
    @NotNull
    private String contents;
}
