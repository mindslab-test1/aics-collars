package kr.co.collars.api.course.service;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import kr.co.collars.api.auth.model.AuthInfoDefault;
import kr.co.collars.api.course.model.AddCourseReq;
import kr.co.collars.common.CommonRes;
import kr.co.collars.dao.mapper.CourseMapper;
import kr.co.collars.dao.vo.*;
import kr.co.collars.external_service.vo.ResponseUploadedFileInfo;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.io.FilenameUtils;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Transactional
@Service
public class CourseService {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    CourseMapper courseMapper;

    @Value("${media.server.url}")
    private String MEDIA_SERVER_URL;
    @Value("${media.server.port}")
    private String MEDIA_SERVER_PORT;
    @Value("${media.server.dir}")
    private String MEDIA_SERVER_DIR;

    public ResponseEntity addCourse(AddCourseReq params) throws Exception {
        CourseVo courseVo = new CourseVo();
        courseVo.setTitle(params.getTitle());
        courseVo.setProductId(params.getProductId());
        courseVo.setDescription(params.getDescription());
        courseVo.setLev(params.getLevel());
        courseVo.setPrice(params.getPrice());
        try {
            courseMapper.addCourse(courseVo);
            if(params.getMediaList() != null) {
                List<CourseItemVo> courseItemVo = new ArrayList<>();
                for (CourseItemVo list : params.getMediaList()) {
                    list.setCourseId(courseVo.getId());
                    courseItemVo.add(list);
                }
                courseMapper.addCourseItem(courseItemVo);
            }
        }catch (Exception e){
            log.error(e.getMessage());
            throw new Exception("addCourse: " + e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.OK).body(new CommonRes("success", courseVo));
    }

    public ResponseEntity addMedia(String name,MultipartFile video){
        String url = MEDIA_SERVER_URL + "/media/file:upload";
        ResponseEntity<ResponseUploadedFileInfo> resMedia = addToMediaServer(URI.create(url), video);
        String mediaPath = extractMediaPath(resMedia.getBody().getFileDownloadUri());
        if(resMedia.getStatusCode().value() != 201){
            return ResponseEntity.status(412).body(new CommonRes("Media Server Error: Can't upload thumbnail image.", null));
        }else{
            MediaVo mediaVo = new MediaVo();
            if(name == null) {
                mediaVo.setName(FilenameUtils.getBaseName(video.getOriginalFilename()));
            }
            else {
                mediaVo.setName(name);
            }
            mediaVo.setUrl(mediaPath);
            int check = courseMapper.addMedia(mediaVo);
            if(check == 1){
                return ResponseEntity.status(HttpStatus.OK).body(new CommonRes("success", mediaVo));
            }
            else {
                return ResponseEntity.status(411).body(new CommonRes("DB Error", null));
            }
        }
    }

    public ResponseEntity updateThumbnail(Integer id, MultipartFile file) throws Exception {
        String url = MEDIA_SERVER_URL + "/media/file:upload";
        ResponseEntity<ResponseUploadedFileInfo> resMedia = addToMediaServer(URI.create(url), file);
        String mediaPath = extractMediaPath(resMedia.getBody().getFileDownloadUri());
        if(resMedia.getStatusCode().value() != 201){
            return ResponseEntity.status(402).body(new CommonRes("Media Server Error: Can't upload thumbnail image.", null));
        }

        CourseVo courseVo = new CourseVo();
        courseVo.setId(id);
        courseVo.setThumbnail(mediaPath);

        try {
            courseMapper.updateCourseThumbnail(courseVo);
        }catch (Exception e){
            log.error(e.getMessage());
            throw new Exception("updateThumbnail: " + e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.OK).body(new CommonRes("success", courseVo));
    }

    public ResponseEntity randomSuggest(AuthInfoDefault authInfo) throws FirebaseAuthException {
        UserRecord userRecord = FirebaseAuth.getInstance().getUser(authInfo.getUid());
        UserVo userVo = new UserVo();
        userVo.setEmail(userRecord.getEmail());
        // GET 수강한 강의
        List<UserCourseVo> userCourseHistory = courseMapper.getUserCourseHistory(userVo);
        if(userCourseHistory.size() == 1 && userCourseHistory.get(0) == null){userCourseHistory.clear();}
        // GET 모든 코스정보
        List<CourseVo> allCousre = courseMapper.getAllCourse();

        CourseVo suggetion = new CourseVo();
        boolean watched = true;
        if(userCourseHistory.size() < allCousre.size()) {
            while (watched) {
                int randomCourseIdx = (int) (Math.random() * allCousre.size());
                suggetion = allCousre.get(randomCourseIdx);
                watched = false;
                for (UserCourseVo history : userCourseHistory) {
                    if (history.getCourseId() == suggetion.getId()) {
                        watched = true;
                    }
                }
            }
            String completeUrl = MEDIA_SERVER_URL + suggetion.getThumbnail();
            suggetion.setThumbnail(completeUrl);
            return ResponseEntity.status(211).body(new CommonRes("success", suggetion));
        }
        else{
            // 모든 강의를 수강 하였다
            return ResponseEntity.status(212).body(new CommonRes("success", null));
        }
    }

    // FUNCTION
    private ResponseEntity<ResponseUploadedFileInfo> addToMediaServer(URI uri, MultipartFile multipartFile){
        HttpHeaders requestMasterHeaders = new HttpHeaders();
        requestMasterHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", multipartFile.getResource());
        body.add("uploadDirectory", MEDIA_SERVER_DIR);
        HttpEntity<MultiValueMap<String, Object>> requestMasterHttpEntity = new HttpEntity<>(body, requestMasterHeaders);
        return restTemplate.exchange(uri,
                HttpMethod.POST,
                requestMasterHttpEntity,
                new ParameterizedTypeReference<ResponseUploadedFileInfo>() {});
    }
    private String extractMediaPath(String url){
        String result[] = url.split("(?=/media/aics)");
        return result[1];
    }
}