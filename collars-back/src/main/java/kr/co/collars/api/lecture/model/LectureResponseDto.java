package kr.co.collars.api.lecture.model;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class LectureResponseDto {
    String startDate;
    String mediaUrl;
    String title;
    List<CourseMeidaVO> courseMeidaVO;
    List<Boolean> isStudyCompleted;
    List<UserCourseVO> userCourseList;
    List<MediaTextVO> mediaTextVO;
}
