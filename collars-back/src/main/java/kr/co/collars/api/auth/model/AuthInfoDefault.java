package kr.co.collars.api.auth.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthInfoDefault {
    private String provider;
    private String uid;
}
