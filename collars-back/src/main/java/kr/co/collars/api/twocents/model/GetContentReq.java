package kr.co.collars.api.twocents.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class GetContentReq {
    @NotNull
    private Integer current;
    @NotNull
    private Integer increment;
}
