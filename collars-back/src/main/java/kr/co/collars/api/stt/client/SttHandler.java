package kr.co.collars.api.stt.client;

import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.List;

public class SttHandler {

    WebSocketSession msession;
    Callback mCallback = null;

    public SttHandler(WebSocketSession session, Callback cb){
        msession = session;
        mCallback = cb;
    }

    public interface Callback{
        public void nextProcess(WebSocketSession session, List<String> result) throws IOException;
    }
}
