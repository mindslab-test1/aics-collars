package kr.co.collars.api.practice.service;

import com.google.gson.Gson;
import kr.co.collars.api.exam.service.CommonExamService;
import kr.co.collars.api.exception.InternalServerException;
import kr.co.collars.api.pron.client.PronApiClient;
import kr.co.collars.api.pron.vo.PronResponse;
import kr.co.collars.api.stt.client.SttClient;
import kr.co.collars.api.stt.client.SttHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

@Slf4j
@Component
public class CopycatService extends CommonExamService {

    @Value("${engedu.stt.gApi}")
    private String engEduIp;
    @Value("${engedu.stt.gPort}")
    private int port;
    @Value("${engedu.stt.gSampling}")
    private int sampling;

    private SttClient sttClient = null;
    private final PronApiClient pronApiClient;

    public CopycatService(PronApiClient pronApiClient){
        this.pronApiClient = pronApiClient;
    }

    public void sttGrpcOnMessage(ByteBuffer data){
        sttClient.onMessage(data);
    }

    public void sttGrpcConn(WebSocketSession session){
        sttClient = new SttClient(session, mCallback);
        //sttClient.onStart("10.122.64.71", 9801, "eng", 16000, "wps");
        sttClient.onStart(engEduIp, port, "eng", sampling, "wps");
    }

    /*
     * Grpc onComplete에서 callback 후
     * pron rest 호출
     * */
    SttHandler.Callback mCallback = new SttHandler.Callback(){
        @Override
        public void nextProcess(WebSocketSession session, List<String> resultList) throws IOException{
            String resultFilePath = resultList.get(1);
            try{
                File file = new File(resultFilePath);
                PronResponse pronResponse = pronApiClient.maumPronCall(file, "");

                Gson gson = new Gson();
                String toJson = gson.toJson(pronResponse);
                session.sendMessage(new TextMessage(toJson));
            }catch (Exception e){
                sendErroMessage(session);
                throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "Exam1 pron exception", "Exam1 pron exception : "+e.getLocalizedMessage());
            }finally {
                sttClient.deleteFile();
            }
        }
    };

    /*
     * Grpc onComplete
     * */
    public void getPronResult(WebSocketSession session) throws IOException {
        sttClient.gracefulShutdown();


    }

    /*
     * client쪽에서 websocke을 끊기 위해 에러를 보냄
     * */
    public void sendErroMessage(WebSocketSession session){
        try{
            session.sendMessage(new TextMessage("ERROR"));
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
