package kr.co.collars.api.stt.client;

import com.google.common.io.Files;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import kr.co.collars.api.exam.service.ExamQ1Service;
import kr.co.collars.api.exception.InternalServerException;
import lombok.extern.slf4j.Slf4j;
import maum.brain.stt.SpeechToTextServiceGrpc;
import maum.brain.stt.Stt;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.io.*;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Slf4j
public class SttClient extends SttHandler{

    /* STTD 연동 정보 */
    //String mModel;
    //int mSampleRate;

    /* GRPC */
    //String mRemoteIp;
    //int mRemotePort;
    ManagedChannel mGrpc_Channel;
    SpeechToTextServiceGrpc.SpeechToTextServiceStub mGrpc_AsyncStub;
    StreamObserver<Stt.Speech> mGrpc_RequestObserver;

    boolean mFlag_GrpcShutdown = false;

    /* STT 결과 데이타 */
    String mStt_ResultText = "";

    /* 음원 관련 변수 */
    String mWavFileName;

    /* 음원 data 생성 */
    FileOutputStream mFileOutStream = null;
    DataOutputStream mOutStream = null;
    int mVoiceTotalSize = 0;

    /* 임시 저장 파일 */
    File tempFile;

    public SttClient(WebSocketSession session, Callback cb){
        super(session, cb);
    }

    public void onStart(String ip, int port, String lang, int sampling, String model){
        try {
            mFlag_GrpcShutdown = false;
            mGrpc_Channel = ManagedChannelBuilder.forAddress(ip, port).usePlaintext().build();
            mGrpc_AsyncStub = SpeechToTextServiceGrpc.newStub(mGrpc_Channel);

            Metadata meta = new Metadata();
            Metadata.Key<String> key1 = Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER);
            Metadata.Key<String> key2 = Metadata.Key.of("in.samplerate", Metadata.ASCII_STRING_MARSHALLER);
            Metadata.Key<String> key3 = Metadata.Key.of("in.model", Metadata.ASCII_STRING_MARSHALLER);

            meta.put(key1, "eng");
            meta.put(key2, "" + sampling);
            meta.put(key3, model);

            Metadata.Key<String> key4 = Metadata.Key.of("in.need.interim", Metadata.ASCII_STRING_MARSHALLER);
            meta.put(key4, "true");

            mGrpc_AsyncStub = MetadataUtils.attachHeaders(mGrpc_AsyncStub, meta);
            mGrpc_RequestObserver = mGrpc_AsyncStub.streamRecognize(mGrpc_ResponseObserver);

            log.debug("@@@ mGrpc_Channel.isShutdown() {}", mGrpc_Channel.isShutdown());

            createWavFile();
        }catch (Exception e){
            throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "Grpc_connection_Erro", "Grpc connection failed : "+e.getLocalizedMessage());
        }
    }

    StreamObserver<Stt.Segment> mGrpc_ResponseObserver = new StreamObserver<Stt.Segment>() {
        @Override
        public void onNext(Stt.Segment value) {
            log.debug("@ mGrpc_ResponseObserver onNext() >> {}", value.toString());


            /* ##로 시작하면, 처리중인 텍스트 알림 메세지임 */
            if(value.getTxt().startsWith("##")) {

            }
            /* ##로 시작하지 않으면, 식별된 텍스트 반환 메세지임 */
            else {
                mStt_ResultText += value.getTxt();
            }
        }

        @Override
        public void onError(Throwable throwable) {
            log.debug("@ mGrpc_ResponseObserver.getMessage() >> {}", throwable.getMessage());
            log.debug("@ mGrpc_ResponseObserver.getStackTrace() >> {}", throwable.getStackTrace());
            log.debug("@ mGrpc_ResponseObserver.getCause() >> {}", throwable.getCause());
        }

        @Override
        public void onCompleted() {
            log.debug("@ mgrpc_ResponseObserver.onCompleted()");
            mGrpc_Channel.shutdownNow();
            mFlag_GrpcShutdown = true;

            finishWavFile();

            List<String> result = new LinkedList();
            result.add(mStt_ResultText);
            result.add(mWavFileName);
            try {
                mCallback.nextProcess(msession, result);
                System.out.println("this is result text : "+mStt_ResultText);
            }catch (IOException e){
                throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "SttClient.onCompleted", "IOException");
            }
        }

    };

    public void onMessage(ByteBuffer data){

        if(mFlag_GrpcShutdown) return;

        byte[] byte_buff = data.array();

        try{
            Stt.Speech speech = Stt.Speech.newBuilder().setBin(ByteString.copyFrom(data)).build();
            mGrpc_RequestObserver.onNext(speech);

            writeWavFile(byte_buff);
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    public void gracefulShutdown(){
        String sttResult = "";
        log.debug("@ SttClient gracefulShutdown() >> {}");

        if(mFlag_GrpcShutdown == false) {

            mFlag_GrpcShutdown = true;

            if (mGrpc_Channel.isShutdown()) return;

            try {
                mGrpc_RequestObserver.onCompleted();
            } catch (Exception e) {
                throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "STT GRPC ERROR", "Grpc shutDown Error : " + e.getLocalizedMessage());
            }
        }
    }

    public void writeWavFile(byte[] data){
        if(mOutStream == null) return;

        try{
            mOutStream.write(data);
            mVoiceTotalSize += data.length;
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public void createWavFile() {
        short  numChannels = 1; // mono
        int BITS_PER_SAMPLE = 16;


        /* 패스 */
        SimpleDateFormat fmtFolder = new SimpleDateFormat("yyyyMM");
        String path = "/home/minds/collars"+ fmtFolder.format(new Date());
        File file = new File(path);
        file.mkdirs();
        tempFile = Files.createTempDir();

        //File testfile = new File("/home/wojung/Desktop");

        /* 파일명 */
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd_HHmmss_SSS");
        String today = fmt.format(new Date());
        mWavFileName = tempFile+"/"+today+"-test.wav";
        //mWavFileName = testfile+"/"+today+"-test.wav";

        /* 파일 생성 */
        try {
            mFileOutStream = new FileOutputStream(mWavFileName);
            mOutStream = new DataOutputStream(mFileOutStream);

            // write the wav file per the wav file format
            mOutStream.writeBytes("RIFF");
            mOutStream.write(intToByteArray(32 + mVoiceTotalSize), 0, 4);
            mOutStream.writeBytes("WAVE");
            mOutStream.writeBytes("fmt ");
            mOutStream.write(intToByteArray(16), 0, 4);
            mOutStream.write(shortToByteArray((short) 1), 0, 2);
            // dulation
            mOutStream.write(shortToByteArray(numChannels), 0, 2);
            mOutStream.write(intToByteArray(16000), 0, 4);
            mOutStream.write(intToByteArray((BITS_PER_SAMPLE / 8) * 16000 * numChannels), 0, 4);
            mOutStream.write(shortToByteArray((short) ((BITS_PER_SAMPLE / 8) * numChannels)), 0, 2);
            mOutStream.write(shortToByteArray((short) BITS_PER_SAMPLE), 0, 2);
            mOutStream.writeBytes("data");
            mOutStream.write(intToByteArray(mVoiceTotalSize), 0, 4);

        } catch (Exception e) {
            e.printStackTrace();

            try {
                if(mOutStream != null) mFileOutStream.close();
                mOutStream = null;

                if(mFileOutStream != null) mFileOutStream.close();
                mFileOutStream = null;

            } catch (Exception e2) {}
        }
    }

    public void finishWavFile() {
        try {
            if(mOutStream != null) mFileOutStream.close();
            mOutStream = null;

            if(mFileOutStream != null) mFileOutStream.close();
            mFileOutStream = null;


            // update가 필요한 header 수정
            RandomAccessFile raf = new RandomAccessFile(mWavFileName, "rw");

            raf.seek(4);
            raf.write(intToByteArray(32 + mVoiceTotalSize), 0, 4);

            raf.seek(40);
            raf.write(intToByteArray(mVoiceTotalSize), 0, 4);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private byte[] intToByteArray(int i) {
        byte[] b = new byte[4];
        b[0] = (byte) (i & 0x00FF);
        b[1] = (byte) ((i >> 8) & 0x000000FF);
        b[2] = (byte) ((i >> 16) & 0x000000FF);
        b[3] = (byte) ((i >> 24) & 0x000000FF);
        return b;
    }

    private byte[] shortToByteArray(short data) {
        return new byte[] { (byte) (data & 0xff), (byte) ((data >>> 8) & 0xff) };
    }

    public void deleteFile() throws IOException{
        FileUtils.cleanDirectory(tempFile);
        tempFile.delete();
    }


}
