package kr.co.collars.api.survey.service;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import kr.co.collars.common.CommonRes;
import kr.co.collars.dao.mapper.SurveyMapper;
import kr.co.collars.dao.mapper.UserMapper;
import kr.co.collars.dao.vo.SurveyVo;
import kr.co.collars.dao.vo.UserSurveyVo;
import kr.co.collars.dao.vo.UserVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
@Slf4j
@Transactional
@Service
public class SurveyService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private SurveyMapper surveyMapper;

    // Service
    public String getData(){
        List<SurveyVo> surveyData = surveyMapper.getData();
        JsonElement element = new Gson().toJsonTree(surveyData,new TypeToken<List<SurveyVo>>(){}.getType());
        return element.toString();
    }
    public ResponseEntity setUserData(Map<String,Object> params) throws FirebaseAuthException {
        try {
            String uid = params.get("uid").toString();
            UserRecord userRecord = FirebaseAuth.getInstance().getUser(uid);
            UserVo userVo = new UserVo();
            userVo.setEmail(userRecord.getEmail());
            UserVo userData = userMapper.getUserId(userVo);
            System.out.println(surveyMapper.checkDataExist(userData));
            if(surveyMapper.checkDataExist(userData).equalsIgnoreCase("N")) {
                Map surveyResult = (Map) params.get("result");
                List<UserSurveyVo> userSurveyVoList = new ArrayList<>();
                Iterator<Map<Integer, Object>> entries = surveyResult.entrySet().iterator();
                while (entries.hasNext()) {
                    Map.Entry entry = (Map.Entry) entries.next();
                    UserSurveyVo userSurveyVo = new UserSurveyVo();
                    userSurveyVo.setUserId(userData.getId());
                    userSurveyVo.setQuestionId(entry.getKey().toString());
                    if (entry.getValue().getClass().equals(String.class)) {
                        userSurveyVo.setAnswerId(entry.getValue().toString());
                        userSurveyVoList.add(userSurveyVo);
                    } else {
                        Map<String, Boolean> multiResult = (Map) entry.getValue();
                        Integer idx = 0;
                        for (String i : multiResult.keySet()) {
                            if (multiResult.get(i)) {
                                UserSurveyVo temp = new UserSurveyVo();
                                temp.setUserId(userSurveyVo.getUserId());
                                temp.setQuestionId(userSurveyVo.getQuestionId());
                                temp.setAnswerId(i);
                                userSurveyVoList.add(temp);
                            }
                        }
                    }

                }
                surveyMapper.setUserData(userSurveyVoList);
                return ResponseEntity.status(HttpStatus.OK).body(new CommonRes("Data is inserted", true));
            }else{
                return ResponseEntity.status(HttpStatus.OK).body(new CommonRes("Data is exist", true));
            }
        } catch (Exception e){
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonRes(e.getMessage(),false));
        }
    }
}
