package kr.co.collars.api.exam.controller;

import kr.co.collars.api.exam.model.*;
import kr.co.collars.api.exam.service.*;
import kr.co.collars.api.pron.vo.PronResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/maumAi/exam")
public class ExamController {

    private final ExamQ1Service examQ1Service;
    private final ExamQ2Service examQ2Service;
    private final ExamQ3Service examQ3Service;
    private final ExamQ4Service examQ4Service;
    private final ExamQ5Service examQ5Service;
    private final ExamService examService;

    public ExamController(ExamQ1Service examQ1Service, ExamQ2Service examQ2Service,
                          ExamQ3Service examQ3Service, ExamQ4Service examQ4Service, ExamQ5Service examQ5Service, ExamService examService){
        this.examQ1Service = examQ1Service;
        this.examQ2Service = examQ2Service;
        this.examQ3Service = examQ3Service;
        this.examQ4Service = examQ4Service;
        this.examQ5Service = examQ5Service;
        this.examService = examService;
    }



    @PostMapping("/pronExam")
    public PronResponse pronExam(@RequestBody MultipartFile audio) throws IOException{
        PronResponse pronResponse = examQ1Service.requestTest(audio);
        return pronResponse;
    }

    /*
     * exam template1 rest api
     * */
    @PostMapping("/q1/getQuestion")
    public ResponseEntity<Exam1VO> getQuestionTemp1(@RequestBody Exam1VO exam1VO){
        HttpHeaders resHeaders = new HttpHeaders();
        resHeaders.add("Content-Type", "application/json;charset=UTF-8");

        return new ResponseEntity(examQ1Service.getQuestion(exam1VO), resHeaders, HttpStatus.OK);
    }

    /*
     * exam template2 rest api
     * */
    @PostMapping("/q2/getQuestion")
    public ResponseEntity<Exam2VO> getQuestionTemp2(@RequestBody Exam2VO exam2VO){
        HttpHeaders resHeaders = new HttpHeaders();
        resHeaders.add("Content-Type", "application/json;charset=UTF-8");

        return new ResponseEntity(examQ2Service.getQuestion(exam2VO), resHeaders, HttpStatus.OK);
    }

    @PostMapping("/q2/getScore")
    public ResponseEntity getScoreTemp2(@RequestBody ExamRequestDto examRequestDto){
        return new ResponseEntity(examQ2Service.getScore(examRequestDto.getExamNum(), examRequestDto.getAnswer()), HttpStatus.OK);
    }

    /*
    * exam template3 rest api
    * */
    @PostMapping("/q3/getQuestion")
    public ResponseEntity<Exam3VO> getQuestionTemp3(@RequestBody Exam3VO exam3VO){
        HttpHeaders resHeaders = new HttpHeaders();
        resHeaders.add("Content-Type", "application/json;charset=UTF-8");

        return new ResponseEntity(examQ3Service.getQuestion(exam3VO), resHeaders, HttpStatus.OK);
    }

    @PostMapping("/q3/getScore")
    public ResponseEntity<Exam3VO> getScoreTemp3(@RequestBody ExamRequestDto examRequestDto){
        return new ResponseEntity(examQ3Service.getScore(examRequestDto.getExamNum(), examRequestDto.getAnswer()), HttpStatus.OK);
    }

    /*
    * exam template4 rest api
    * */
    @PostMapping("/q4/getQuestion")
    public ResponseEntity<Exam4VO> getQuestionTemp4(@RequestBody Exam4VO exam4VO){
        HttpHeaders resHeaders = new HttpHeaders();
        resHeaders.add("Content-Type", "application/json;charset=UTF-8");

        return new ResponseEntity(examQ4Service.getQuestion(exam4VO), resHeaders, HttpStatus.OK);
    }

    @PostMapping("/q4/getScore")
    public ResponseEntity<Integer> getScore(@RequestBody ExamRequestDto examRequestDto){
        int score = examQ4Service.getScore(examRequestDto.getExamNum(), examRequestDto.getAnswer());
        return new ResponseEntity(score, HttpStatus.OK);
    }

    /*
     * exam template4 rest api
     * */
    @PostMapping("/q5/getQuestion")
    public ResponseEntity<Exam5VO> getQuestionTemp5(@RequestBody Exam5VO exam5VO){
        HttpHeaders resHeaders = new HttpHeaders();
        resHeaders.add("Content-Type", "application/json;charset=UTF-8");

        return new ResponseEntity(examQ5Service.getQuestion(exam5VO), resHeaders, HttpStatus.OK);
    }

    /*
    * save exam results
    * */
    @PostMapping("/save/examResult")
    public void saveExamResult(@RequestBody ExamResultVO examResultVO){
        log.info("@ ----> ExamController.saveExamResult {}", examResultVO);
        examService.saveExamResult(examResultVO);
    }
}
