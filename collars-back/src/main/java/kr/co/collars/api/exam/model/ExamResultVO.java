package kr.co.collars.api.exam.model;

import lombok.Data;

import java.util.List;

@Data
public class ExamResultVO extends CommonDto{
    int userId;
    String uid;
    int resultId;
    List<Integer> examNumList;
    String email;
    String accuracy;
    String pron;
    String voca;
    String manner;
    String express;
    String culture;
}
