package kr.co.collars.api.notice.controller;

import kr.co.collars.api.notice.model.GetNoticeReq;
import kr.co.collars.api.notice.model.SetNoticeReq;
import kr.co.collars.api.notice.service.NoticeService;
import kr.co.collars.common.CommonRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.bind.BindResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/notice")
public class NoticeController {

    @Autowired
    NoticeService noticeService;

    @RequestMapping(value = "/setNotice", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity setNotice(
            @RequestBody SetNoticeReq params,
            BindingResult bindingResult
    ) throws BindException {
        if(bindingResult.hasErrors()) {throw new BindException(bindingResult);}
        try {
            return noticeService.setNotice(params);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonRes(e.getMessage(),null));
        }
    }

    @RequestMapping(value = "/getNotice", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity getNotice(
            @RequestBody GetNoticeReq parmas,
            BindingResult bindingResult
    ) throws BindException {
        if(bindingResult.hasErrors()) {throw new BindException(bindingResult);}
        try {
            return noticeService.getNotice(parmas);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonRes(e.getMessage(),null));
        }
    }
}
