package kr.co.collars.api.notice.service;

import kr.co.collars.api.notice.model.GetNoticeReq;
import kr.co.collars.api.notice.model.SetNoticeReq;
import kr.co.collars.common.CommonRes;
import kr.co.collars.dao.mapper.NoticeMapper;
import kr.co.collars.dao.vo.NoticeVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Slf4j
@Transactional
@Service
public class NoticeService {

    @Autowired
    NoticeMapper noticeMapper;

    public ResponseEntity setNotice(SetNoticeReq params){
        NoticeVo noticeVo = new NoticeVo();
        noticeVo.setTitle(params.getTitle());
        noticeVo.setContents(params.getContents());

        noticeMapper.setNotice(noticeVo);

        return ResponseEntity.ok().body(new CommonRes("success", noticeVo));
    }

    public ResponseEntity getNotice(GetNoticeReq params){
        List<NoticeVo> result = noticeMapper.getNotice(params);
        return ResponseEntity.ok().body(new CommonRes("success", result));
    }
}
