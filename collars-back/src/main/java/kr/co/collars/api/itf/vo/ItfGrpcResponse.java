package kr.co.collars.api.itf.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ItfGrpcResponse {
    private String intent;
    private float prob;

    public ItfGrpcResponse(String intent, float prob) {
        this.intent = intent;
        this.prob = prob;
    }
}
