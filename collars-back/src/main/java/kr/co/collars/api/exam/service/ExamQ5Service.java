package kr.co.collars.api.exam.service;

import com.google.gson.Gson;
import kr.co.collars.api.exam.model.CommonDto;
import kr.co.collars.api.exam.model.Exam5VO;
import kr.co.collars.api.exam.model.ExamRequestDto;
import kr.co.collars.api.exam.model.ExamScoreVO;
import kr.co.collars.api.exception.InternalServerException;
import kr.co.collars.api.itf.vo.ItfGrpcResponse;
import kr.co.collars.api.pron.client.PronApiClient;
import kr.co.collars.api.pron.vo.PronResponse;
import kr.co.collars.api.stt.client.SttClient;
import kr.co.collars.api.stt.client.SttHandler;
import kr.co.collars.dao.mapper.ExamMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

@Slf4j
@Service
public class ExamQ5Service extends CommonExamService{

    @Value("${engedu.stt.gApi}")
    private String engEduIp;
    @Value("${engedu.stt.gPort}")
    private int port;
    @Value("${engedu.stt.gSampling}")
    private int sampling;


    @Autowired
    ExamMapper examMapper;

    private final PronApiClient pronApiClient;
    public ExamQ5Service(PronApiClient pronApiClient){this.pronApiClient = pronApiClient;}

    private SttClient sttClient = null;
    int examNum;

    /*
    * set random exam num
    * */
    public Exam5VO getQuestion(Exam5VO exam5VO){
        int exam5Count = examMapper.getExam5Count();
        int randomExamNum = (int)Math.random()*exam5Count+1;
        exam5VO.setExamNum(randomExamNum);

        return examMapper.getQTextExam5(exam5VO);
    }

    public void setExamNum(String examNumStr){
        examNum = Integer.parseInt(examNumStr.split("-")[1]);
    }


    public void sttGrpcConn(WebSocketSession session){
        sttClient = new SttClient(session, mCallback);
        //sttClient.onStart("10.122.64.71", 9801, "eng", 16000, "wps");
        sttClient.onStart(engEduIp, port, "eng", sampling, "wps");
    }

    /*
     * Grpc onComplete에서 callback 후
     * stt 전처리, pron rest 호출
     * */
    SttHandler.Callback mCallback = new SttHandler.Callback() {
        @Override
        public void nextProcess(WebSocketSession session, List<String> resultList) throws IOException {
            String resultText = preProcess(resultList.get(0));
            String resultFilePath = resultList.get(1);
            int score = 0;

            try{
                resultText = preProcess(resultText);
                score = getScore(examNum, resultText);

                PronResponse pronResponse = new PronResponse();
                pronResponse.setScore(score);
                pronResponse.setSttResult(resultText);

                Gson gson = new Gson();
                String toJson = gson.toJson(pronResponse);

                log.info("@ ------> ExamService.getPronResult >> {}", pronResponse);
                session.sendMessage(new TextMessage(toJson));

            }catch (Exception e){
                throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "Exam5 Score exception", "exam5 score error : "+e.getLocalizedMessage());
            }finally {
                sttClient.deleteFile();
            }

            /*try{
                File file = new File(resultFilePath);
                PronResponse pronResponse = pronApiClient.maumPronCall(file, "");

                pronResponse.setScore(score);
                pronResponse.setSttResult(resultText);

                Gson gson = new Gson();
                String toJson = gson.toJson(pronResponse);

                log.info("@ ------> ExamService.getPronResult >> {}", pronResponse);
                session.sendMessage(new TextMessage(toJson));
            }catch (Exception e){
                sendErroMessage(session);
                throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR, "Exam1 pron exception", "Exam1 pron exception : "+e.getLocalizedMessage());
            }finally {
                sttClient.deleteFile();
            }*/
        }
    };

    public void sttGrpcOnMessage(ByteBuffer data){
        sttClient.onMessage(data);
    }

    /*
     * Grpc onComplete
     * */
    public void getPronResult(WebSocketSession session) throws IOException{
        sttClient.gracefulShutdown();
    }


    /*
     * client쪽에서 websocke을 끊기 위해 에러를 보냄
     * */
    public void sendErroMessage(WebSocketSession session){
        try{
            session.sendMessage(new TextMessage("ERROR"));
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    /*
    * stt로 나온 문장 점수 평가
    * */
    public int getScore(int examNum, String inputAnswer){
        inputAnswer = preProcess(inputAnswer);
        int score = -1;

        ExamRequestDto examRequestDto = new ExamRequestDto();
        examRequestDto.setExamNum(examNum);
        examRequestDto.setAnswer(inputAnswer.replaceAll(" ", ""));

        try{
            ExamScoreVO examScoreVO = examMapper.getExam5CorrectScore(examRequestDto);
            if(examScoreVO == null){
                List<ExamScoreVO> examScoreVOIf = examMapper.getExam5IfScore(examRequestDto);
                score = ifSocre(examScoreVOIf, inputAnswer);

                List<ExamScoreVO> examScoreVOIf10 = examMapper.getExam5IF10(examRequestDto);
                score = containWordCheck(examScoreVOIf10, inputAnswer, score);
            }else if(examScoreVO != null){
                score = examScoreVO.getScore();
                List<ExamScoreVO> examScoreVOCorrect10 = examMapper.getExam5Correct10(examRequestDto);
                score = containWordCheck(examScoreVOCorrect10, inputAnswer, score);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        if(score < 0) return 0;
        else return score;
    }
}
