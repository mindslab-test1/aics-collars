package kr.co.collars.api.stt.controller;

import kr.co.collars.api.stt.client.MaumRestClient;
import kr.co.collars.api.stt.vo.SttResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/maumAi/stt")
public class SttController {

    @Autowired
    MaumRestClient maumRestClient;

    @PostMapping(value = "/sttCall")
    @ResponseBody
    public SttResponse requsetStt(@RequestParam MultipartFile audio) throws IOException{
        return maumRestClient.muamSttCall(audio);
    }

    /*@PostMapping(value = "/preTest")
    @ResponseBody
    public String preTest(@RequestParam String text){
        return sttService.intToHangul(text);
    }*/
}
