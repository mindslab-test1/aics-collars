package kr.co.collars.api.itf.client;
import kr.co.collars.api.itf.vo.ItfGrpcResponse;
import io.grpc.ManagedChannelBuilder;
import maum.brain.bert.intent.BertIntentFinderGrpc;
import maum.brain.bert.intent.Itc;
import org.springframework.stereotype.Component;

@Component
public class ItfGrpcCleint {

   private BertIntentFinderGrpc.BertIntentFinderBlockingStub bertIntentFinderBlockingStub;

   public void setDestination(String ip, int port){
      try{
         bertIntentFinderBlockingStub = BertIntentFinderGrpc.newBlockingStub(
                 ManagedChannelBuilder.forAddress(ip, port).usePlaintext()
                 .build()
         );
      }catch (Exception e){
         e.printStackTrace();
      }
   }

   public ItfGrpcResponse getItf(String context){
      Itc.Utterance.Builder utterBuilder = Itc.Utterance.newBuilder();

      Itc.Utterance utter = utterBuilder.setUtter(context).build();
      Itc.Intent output = this.bertIntentFinderBlockingStub.getIntentSimple(utter);

      ItfGrpcResponse itfGrpcResponse = new ItfGrpcResponse(output.getIntent(), output.getProb());

      return itfGrpcResponse;
   }
}
