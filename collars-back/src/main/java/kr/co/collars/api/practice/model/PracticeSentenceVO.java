package kr.co.collars.api.practice.model;

import lombok.Data;

@Data
public class PracticeSentenceVO {
    int id;
    int mediaId;
    String answer;
    String mulChoice1;
    String mulChoice2;
    String mulChoice3;
    String mulChoice4;
    String question;
}
