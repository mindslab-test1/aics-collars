package kr.co.collars.api.lecture.model;

import lombok.Data;

@Data
public class CourseMeidaVO {
    int id;
    int courseId;
    int mediaId;
    int mediaOrder;
    int completed;
    String name;
}
