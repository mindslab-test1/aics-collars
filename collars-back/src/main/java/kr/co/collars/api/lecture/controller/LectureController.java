package kr.co.collars.api.lecture.controller;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserRecord;
import kr.co.collars.api.auth.model.AuthInfoDefault;
import kr.co.collars.api.lecture.model.LectureRequestDto;
import kr.co.collars.api.lecture.model.LectureResponseDto;
import kr.co.collars.api.lecture.service.LectureService;
import kr.co.collars.api.practice.model.PracticeRequestDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequestMapping("/lecture")
@RestController
public class LectureController {

    @Autowired
    LectureService lectureService;

    @PostMapping("/getUserCourseInfo")
    ResponseEntity getUserCourseInfo(@RequestBody PracticeRequestDto practiceRequestDto){
        LectureResponseDto lectureResponseDto = lectureService.getUserCourseInfo(practiceRequestDto);
        HttpHeaders resHeaders = new HttpHeaders();
        resHeaders.add("Content-Type", "application/json;charset=UTF-8");

        return new ResponseEntity(lectureResponseDto, resHeaders, HttpStatus.OK);
    }

    /*@PostMapping("/getLectureName")
    ResponseEntity getLectureName(@RequestBody PracticeRequestDto practiceRequestDto){
        HttpHeaders resHedaers = new HttpHeaders();
        resHedaers.add("Content-Type", "application/json;charset=UTF-8");

        return new ResponseEntity(lectureService.getLectureName(practiceRequestDto), resHedaers, HttpStatus.OK);
    }*/

    @PostMapping("/getLectureData")
    ResponseEntity getLectureData(@RequestBody LectureRequestDto lectureRequestDto){
        HttpHeaders resHedaers = new HttpHeaders();
        resHedaers.add("Content-Type", "application/json;charset=UTF-8");

        return new ResponseEntity(lectureService.getLectureData(lectureRequestDto), resHedaers, HttpStatus.OK);
    }

    @PostMapping("/finishCourse")
    ResponseEntity finishCourse(@RequestBody PracticeRequestDto practiceRequestDto){

        return new ResponseEntity(lectureService.finishCourse(practiceRequestDto), HttpStatus.OK);
    }
}
