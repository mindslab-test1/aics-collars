package kr.co.collars.config;

import kr.co.collars.api.practice.controller.WsHandlerCopycat;
import kr.co.collars.api.exam.controller.WsHandlerExamQ1;
import kr.co.collars.api.exam.controller.WsHandlerExamQ5;
import kr.co.collars.api.exam.controller.WsHandlerSttItf;
import kr.co.collars.api.twocents.controller.WsHandlerTwoCents;
import kr.co.collars.handler.WsHandshakeInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

//@RequiredArgsConstructor
@Slf4j
@Configuration
@EnableWebSocket
public class WebSockConfig implements WebSocketConfigurer {
    //private final WebSocketHandler webSocketHandler;

    private WsHandlerSttItf sttItf;
    private WsHandlerExamQ1 pronExamQ1;
    private WsHandlerExamQ5 pronExamQ5;
    private WsHandlerCopycat copycat;
    private WsHandlerTwoCents twoCents;


    public WebSockConfig(WsHandlerSttItf wsHandlerSttItf, WsHandlerExamQ1 wsHandlerExamQ1, WsHandlerExamQ5 wsHandlerExamQ5, WsHandlerCopycat wsHandlerCopycat, WsHandlerTwoCents wsHandlerTwoCents){
        this.sttItf = wsHandlerSttItf;
        this.pronExamQ1 = wsHandlerExamQ1;
        this.pronExamQ5 = wsHandlerExamQ5;
        this.copycat = wsHandlerCopycat;
        this.twoCents = wsHandlerTwoCents;
    }

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry){
        registry.addHandler(sttItf, "/engedu/websocket/sttItf").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");

        registry.addHandler(pronExamQ1, "/engedu/websocket/pronExamQ1").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");

        registry.addHandler(pronExamQ5, "/engedu/websocket/pronExamQ5").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");

        registry.addHandler(copycat, "/engedu/websocket/copycat").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");

        registry.addHandler(twoCents, "/twocents/pronunciation").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");
    }
}
