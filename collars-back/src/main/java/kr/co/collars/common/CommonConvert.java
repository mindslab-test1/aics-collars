package kr.co.collars.common;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;

public class CommonConvert {
    public static File convertFile(MultipartFile file){
        File convFile = new File(file.getOriginalFilename());
        try{
            convFile.createNewFile();

            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write(file.getBytes());
            fos.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return convFile;
    }
}

