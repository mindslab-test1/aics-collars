
# Firebase
- Android
    - android/build.gradle
    - android/app/build.gradle
    - android/app/google-services.json //config

## Social Sign In
- Naver
    - Android
        - android/app/src/main/res/values/strings.xml
        - android/app/src/main/AndroidManifest.xml
- Google
    - Android
        - android/build.gradle
        - android/app/build.gradle
- Kakao
    - Android
        - .env [KAKAO_CLIENT_ID]
        - lib/screens/layouts/sign_in_screen.dart
