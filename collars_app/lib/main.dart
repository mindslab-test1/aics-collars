
import 'package:collarsApp/providers/home_main_provider.dart';
import 'package:collarsApp/providers/twocents_content_provider.dart';
import 'package:collarsApp/screens/layouts/evereading/evereading_content_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:provider/provider.dart';
// Provider
import 'package:collarsApp/providers/main_view_provider.dart';
import 'package:collarsApp/providers/exam_result.dart';
import 'package:collarsApp/providers/study_status.dart';
import 'package:collarsApp/providers/evereading_main_provider.dart';

// Screen
import 'package:collarsApp/screens/wrap_screen.dart';
import 'package:collarsApp/screens/layouts/exam_main.dart';
import 'package:collarsApp/screens/layouts/survey_intro_screen.dart';
import 'package:collarsApp/screens/layouts/survey_screen.dart';
import 'package:collarsApp/screens/layouts/main/main_screen.dart';
import 'package:collarsApp/screens/layouts/info/user_info_screen.dart';
import 'package:collarsApp/screens/layouts/info/customer_center_screen.dart';
import 'package:collarsApp/screens/layouts/suggest_screen.dart';
import 'package:collarsApp/screens/layouts/info/notice_screen.dart';
import 'package:collarsApp/screens/layouts/main/evereading_screen.dart';

// Service
import 'package:collarsApp/services/auth.dart';
// Styles
import 'package:collarsApp/styles/color/default.dart';

Future main() async {
  await DotEnv().load('.env');
  InAppPurchaseConnection.enablePendingPurchases();

  runApp(CollarsApp());
}

class CollarsApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        StreamProvider(create: (context) => AuthService().user),
        ChangeNotifierProvider(create: (context) => MainViewProvider()),
        ChangeNotifierProvider(create: (context) => ExamScore()),
        ChangeNotifierProvider(create: (context) => StudyStatus()),
        ChangeNotifierProvider(create: (context) => EvereadingMainProvider()),
        ChangeNotifierProvider(create: (context) => HomeMainProvider()),
        ChangeNotifierProvider(create: (context) => TwoCentsContentProvider()),
      ],
      child: MaterialApp(
        theme: ThemeData(
          primaryColor: kPrimaryColor,
          fontFamily: 'Raleway'
        ),
        initialRoute: WrapScreen.id,
        routes: {
          WrapScreen.id: (context) => WrapScreen(),
          MainScreen.id: (context) => MainScreen(),
          // INFO
          UserInfoScreen.id: (context) => UserInfoScreen(),
          SuggestScreen.id: (context) => SuggestScreen(),
          SurveyIntroScreen.id: (context) => SurveyIntroScreen(),
          SurveyScreen.id: (context) => SurveyScreen(),
          ExamMain.id: (context) => ExamMain(),
          CustomerCenterScreen.id: (context) => CustomerCenterScreen(),
          NoticeScreen.id: (context) => NoticeScreen(),
          EvereadingScreen.id: (context) => EvereadingScreen(),
          EvereadingContentScreen.id: (context) => EvereadingContentScreen(),
        },
      ),
    );
  }
}
