import 'package:flutter/material.dart';

// Style
import 'package:collarsApp/styles/color/default.dart';
import 'package:flutter_svg/svg.dart';

class PreparingService extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 100),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 100,
              height: 100,
              padding: EdgeInsets.all(30),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                gradient: kPrimaryGradient2,
              ),
              child: SvgPicture.asset(
                'assets/images/icon/risk.svg',
                color: Colors.white,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(height: 10),
            Text(
              '서비스 준비중입니다.',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10),
            Text(
              '보다 좋은 서비스를 제공해 드리기\n위해 준비중입니다.',
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
