import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

// Style
import 'package:collarsApp/styles/size/default.dart';
import 'package:collarsApp/styles/color/default.dart';

class DefaultAppBar extends StatelessWidget implements PreferredSizeWidget{



  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      title: Container(
        width: kAppBarLogoSize,
        height: kAppBarLogoSize,
        child: Image.asset(
          'assets/images/logo/logo02.png',
          width: kAppBarLogoSize,
          height: kAppBarLogoSize,
        ),
      ),
//      actions: <Widget>[
//        Padding(
//          padding: const EdgeInsets.symmetric(vertical: 12),
//          child: FlatButton(
//            shape: CircleBorder(),
//            child: SvgPicture.asset(
//                'assets/images/icon/account_circle-24px.svg',
//                width: 30,
//                height: 30,
//                color: kInfoItem),
//            onPressed: () {},
//          ),
//        )
//      ],
      backgroundColor: Colors.transparent,
      bottomOpacity: 0.0,
      elevation: 0.0,
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(56);

}
