import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

// Style
import 'package:collarsApp/styles/size/default.dart';
import 'package:collarsApp/styles/color/default.dart';

class EvereadingAppBar extends StatelessWidget implements PreferredSizeWidget{
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text('Collars',style: TextStyle(color: Colors.blue),),
      backgroundColor: Colors.transparent,
      bottomOpacity: 0.0,
      elevation: 0.0,
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(56);
}
