import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:collarsApp/providers/evereading_main_provider.dart';

//Style
import 'package:collarsApp/styles/color/default.dart';

class EvereadingMainCarousel extends StatelessWidget {
  List<dynamic> tags;
  int currentIndex = 0;

  EvereadingMainCarousel({this.tags});

  @override
  Widget build(BuildContext context) {
    EvereadingMainProvider emp = Provider.of<EvereadingMainProvider>(context);

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: SizedBox(
        height: 35,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: tags.length,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                emp.currentTagIndex = index;
                emp.clearColumn();
              },
              child: Container(
                width: 68,
                alignment: Alignment.center,
                margin: EdgeInsets.symmetric(horizontal: 6.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(18),
                  border: Border.all(
                      color:
                          emp.currentTagIndex == index ? kMainBlue : kMainBlack),
                  color: emp.currentTagIndex == index
                      ? kMainBlue
                      : Colors.transparent,
                ),
                child: Text(
                  tags[index]["tag"],
                  style: TextStyle(
                    fontSize: 12,
                    color:
                        emp.currentTagIndex == index ? Colors.white : kMainBlack,
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
