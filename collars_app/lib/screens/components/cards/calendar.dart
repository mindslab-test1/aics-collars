import 'package:collarsApp/providers/user.dart';
import 'package:collarsApp/services/json_parser/lecture/course_media.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/classes/event_list.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart' show CalendarCarousel;
import 'package:intl/intl.dart' show DateFormat;
import 'package:provider/provider.dart';

class CustomCalendar extends StatefulWidget {
  final CourseMedia courseMedia;
  final Function onDayPressedFunc;
  final Function onCalendarChangeFunc;

  CustomCalendar(
      this.courseMedia,
      this.onDayPressedFunc,
      this.onCalendarChangeFunc);

  @override
  _CustomCalendarState createState() => _CustomCalendarState();
}



class _CustomCalendarState extends State<CustomCalendar> {
  static DateTime currentDate = DateTime.now();
  static DateTime currentDate2 = DateTime.now();
  static DateTime targetDateTime = DateTime.now();
  static String currentMonth = DateFormat.yMMM().format(DateTime(2020, 6, 29));
  static List<DateTime> lectureDate = List<DateTime>();
  static EventList<Event> markedDateMap = new EventList<Event>();
  CalendarCarousel calendarCarousel;
  User user;


  @override
  void initState() {
    // TODO: implement initState
    user = Provider.of<User>(context, listen: false);

    if(user.course != null && lectureDate.length<1) setMarkedDateMap();

    super.initState();
  }

  setCalender(){
    calendarCarousel = CalendarCarousel<Event>(
      onDayPressed: (DateTime date, List<Event> events) {
        this.setState(() => currentDate2 = date);
        widget.onDayPressedFunc(date);
      },
      showOnlyCurrentMonthDate: false,
      weekendTextStyle: TextStyle(
        color: Colors.black,
      ),
      thisMonthDayBorderColor: Colors.grey,
      weekFormat: false,
      markedDatesMap: markedDateMap,
      height: 310.0,
      selectedDateTime: currentDate2,
      targetDateTime: targetDateTime,
      customGridViewPhysics: NeverScrollableScrollPhysics(),
      markedDateCustomShapeBorder: CircleBorder(
          side: BorderSide(color: Colors.transparent)
      ),
      markedDateCustomTextStyle: TextStyle(
        fontSize: 18,
        color: Colors.white,
      ),
      showHeader: false,
      todayTextStyle: TextStyle(
          color: Colors.amber,
          fontSize: 18
      ),
      todayButtonColor: Colors.transparent,
      selectedDayTextStyle: TextStyle(
          color: Colors.red,
          fontSize: 18
      ),
      selectedDayButtonColor: Colors.transparent,
      minSelectedDate: currentDate.subtract(Duration(days: 360)),
      maxSelectedDate: currentDate.add(Duration(days: 360)),
      prevDaysTextStyle: TextStyle(
        fontSize: 16,
        color: Colors.black.withOpacity(0.4),
      ),
      inactiveDaysTextStyle: TextStyle(
        color: Colors.tealAccent,
        fontSize: 16,
      ),
      markedDateMoreShowTotal: true,
      markedDateWidget: Expanded(
        flex: 1,
        child: Container(
          color: Colors.white,
        ),
      ),
      //marked된 날짜에 border를 추가 할 것인가
      markedDateMoreCustomDecoration: BoxDecoration(
          shape: BoxShape.circle,
          borderRadius: BorderRadius.circular(30),
          color: Colors.purple
      ),
      showIconBehindDayText: true,
      markedDateIconMargin: 0,
      markedDateIconOffset: 0,
      //markedDateShowIcon: false,
      markedDateIconBuilder: (event){
        return event.icon;
      },
      onCalendarChanged: (DateTime date) {
        this.setState(() {
          targetDateTime = date;
          currentMonth = DateFormat.yMMM().format(targetDateTime);
        });
      },
    );
  }

  setMarkedDateMap(){
    List startday = widget.courseMedia.startDate.toString().split("-");
    var startDateTime = DateTime(
        int.parse(startday[0]), int.parse(startday[1]),
        int.parse(startday[2]));

    //check start date and end date
    for (int i = 0; i < widget.courseMedia.courseMeidaVO.length; i++) {
      var specificDate = startDateTime.add(Duration(days: i));
      if (i == 0 || i == widget.courseMedia.courseMeidaVO.length - 1) {
        setState(() {
          lectureDate.add(specificDate);
        });

        markedDateMap.add(
            specificDate,
            new Event(
                date: specificDate,
                icon: _allowLecture,
                dot: Container(
                  margin: EdgeInsets.symmetric(horizontal: 1.0),
                  color: Colors.red,
                  height: 5.0,
                  width: 5.0,
                )
              //icon: _eventIcon,
            )
        );
      }
    }
  }

  static Widget _allowLecture = Expanded(
      flex: 1,
      child: Container(
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
          color: Color.fromRGBO(82, 98, 242, 1),
        ),
      )
  );

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    setCalender();

    return Container(
      width: size.width*0.877,
      decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color: Color(0xfff2f2f2)
          ),
          color: Colors.white,
          borderRadius: BorderRadius.circular(15)
      ),
      child: Padding(
          padding: EdgeInsets.fromLTRB(12.9, 12.9, 12.9, 0),
          child: calendarCarousel
      ),
    );
  }
}

