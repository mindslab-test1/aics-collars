import 'package:collarsApp/providers/evereading_main_provider.dart';
import 'package:collarsApp/screens/layouts/evereading/evereading_content_screen.dart';
import 'package:collarsApp/styles/size/default.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class EvereadingColumnCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    dynamic data = Provider.of<EvereadingMainProvider>(context).todayColumn;

    List<Widget> buildHighlight() {
      NumberFormat formatter = new NumberFormat("00");
      List<Widget> highlight = [];
      print(data.runtimeType);
      if(data == null) {
        return highlight;
      }
      for (int i = 0; i < data["content"]["eng"]["body"].length; i++) {
        highlight.add(SizedBox(height: 10));
        highlight.add(
          Text(
            '${formatter.format(i + 1)}\t${data["content"]["eng"]["body"][i]['header']}',
            style: TextStyle(
              fontSize: 14,
              color: Color(0xff8a8a8a),
            ),
          ),
        );
      }
      return highlight;
    }

    return Provider.of<EvereadingMainProvider>(context).isTodayLoading
        ? Container()
        : Container(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 24),
            width: double.infinity,
            decoration: BoxDecoration(
              color: Color(0xfff8f8f8),
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: Color(0xffdfdfdf), width: 1),
            ),
            child: Column(
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: 60,
                      height: 83,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                              'assets/images/etc/today column_60x83.jpg'),
                        ),
                      ),
                    ),
                    SizedBox(width: 12),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            data["content"]["eng"]["title"],
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            data["content"]["kor"]["title"],
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: Colors.grey,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (_) =>
                                      EvereadingContentScreen(data: data),
                                ),
                              );
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  '자세히보기',
                                  style: TextStyle(
                                      fontSize: 10, color: Color(0xff7F8787)),
                                ),
                                SvgPicture.asset(
                                  'assets/images/icon/arrow_right_small_16_px.svg',
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: kDefaultPaddingVertical,
                  child: Divider(thickness: 1, color: Color(0xffdfdfdf)),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      child: Text(
                        'Highlight',
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff1b1c1c),
                        ),
                      ),
                    ),
                    SizedBox(height: 10),
                    ...buildHighlight(),
                    SizedBox(height: 10),
                  ],
                ),
              ],
            ),
          );
  }
}
