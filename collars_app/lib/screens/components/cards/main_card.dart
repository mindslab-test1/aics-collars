import 'package:collarsApp/styles/color/default.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:collarsApp/screens/components/cards/common_card.dart';

class MainCard extends StatelessWidget {
  final double margin;
  final EdgeInsetsGeometry padding;
  final double btnSize;
  final Gradient gradient;
  final Color fontColor;
  final Color btnColor;
  final Color btnFontColor;
  final List<BoxShadow> boxShadow;
  final String title;
  final String content;
  final String btnText;
  final Function onPressed;

  MainCard(
      {
      this.margin = 0,
      this.padding,
      this.gradient = kPrimaryLightGradient2,
      this.fontColor = Colors.white,
      this.btnColor = Colors.white,
      this.btnFontColor = kPrimaryColor,
      this.btnSize = 75,
      this.boxShadow,
      this.title,
      this.content,
      this.btnText,
      this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(margin),
      padding: padding,
      decoration: BoxDecoration(
        gradient: gradient,
        borderRadius: BorderRadius.all(Radius.circular(15)),
        boxShadow: boxShadow,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                // Title
                Text(
                  title,
                  style: TextStyle(
                    fontFamily: 'NotoSansCJKkr',
                    color: fontColor,
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 10),
                // Content
                Text(
                  content,
                  style: TextStyle(
                    fontFamily: 'NotoSansCJKkr',
                    fontSize: 12,
                    color: fontColor,
                  ),
                ),
              ],
            ),
          ),
          GestureDetector(
            onTap: () {
              onPressed();
            },
            child: Container(
              width: btnSize,
              height: btnSize,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: btnColor,
              ),
              child: Center(
                child: Text(
                  btnText,
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: btnFontColor,
                  ),
                  overflow: TextOverflow.clip,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
