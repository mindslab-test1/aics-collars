import 'package:collarsApp/providers/exam_result.dart';
import 'package:collarsApp/screens/layouts/exam_screen/CommonPaint.dart';
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/json_parser/ExamTemp/exam_json4.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';

class Tmp4ColumCard extends StatefulWidget {
  final Size size;
  final ExamJson4 examJson4;

  final Function goNextPage;
  final Function getResult;
  final Function setExamScoreZero;

  final bool isExam;

  Tmp4ColumCard({
    this.size,
    this.examJson4,
    this.goNextPage,
    this.getResult,
    this.setExamScoreZero,
    this.isExam
  });

  @override
  _Tmp4ColumCardState createState() => _Tmp4ColumCardState();
}

class _Tmp4ColumCardState extends State<Tmp4ColumCard> {

  String _appServer = DotEnv().env['APP_SERVER'];

  TextEditingController _controller = TextEditingController();
  Color skipBtn = Colors.white;
  ExamScore examScore;

  @override
  void initState() {
    super.initState();

    examScore = Provider.of<ExamScore>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,

        children: <Widget>[
          SizedBox(
            height: widget.size.height*0.05,
          ),
          Text("테스트", style: TextStyle(fontSize: 11, color: Colors.white, fontWeight: FontWeight.bold),),
          CustomPaint(
            size: widget.isExam?Size(widget.size.width, widget.size.height*0.1):Size(0, 0),
            painter: widget.isExam?MyPainter(exam: 4):null,
          ),
          SizedBox(height: 30,),

          Container(
              width: widget.size.width*0.65,
              child: Text("문장의 뉘앙스를 바꿔서 타이핑해주세요.",
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                    fontFamily: 'NotoSansCJKkr'
                ),
                textAlign: TextAlign.center,)
          ),

          SizedBox(
            height: widget.size.height*0.05,
          ),

          Center(
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.3),
                        blurRadius: 6.0,
                        offset: Offset(0, 4)
                    )
                  ]
              ),
              width: widget.size.width*0.85,
              child: (widget.examJson4.qtext2=="")? Text("") : Center(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(24, 24, 24, 24),
                  child: Text(widget.examJson4.qtext2,
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff444444),
                        height: 1.4
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ),

          SizedBox(
            height: widget.size.height*0.03,
          ),
          Center(
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.3),
                        blurRadius: 6.0,
                        offset: Offset(0, 4)
                    )
                  ]
              ),
              child: Padding(
                padding: EdgeInsets.all(13),
                child: TextField(
                  textAlign: TextAlign.center,
                  maxLines: null,
                  keyboardType: TextInputType.multiline,
                  style: TextStyle(fontSize: 17),
                  controller: _controller,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: '터치후 입력해 주세요.',
                    hintStyle: TextStyle(
                      color: Color(0xffcccccc),
                      fontSize: 18,
                      fontFamily: 'NotoSansCJKkr-Bold',
                      fontWeight: FontWeight.bold
                    )
                  ),
                ),
              ),
            ),
          ),

          SizedBox(
            height: widget.size.height*0.03,
          ),

          (widget.examJson4.qtext1=="")?
          Text("") :
          Text(
            widget.examJson4.qtext1,
            style: TextStyle(fontSize: 14, color: Color(0xffd6cafa), height: 1.5), textAlign: TextAlign.center,
          ),

          SizedBox(
            height: widget.size.height*0.05,
          ),
          Container(
            width: widget.size.width*0.347,
            height: widget.size.height*0.072,
            child: RaisedButton(
              onPressed: (){
                widget.getResult(_controller.text, reAssamblePage);
              },
              color: Colors.white,
              child: Padding(
                padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: Center(
                  child: Text("OK",
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'NotoSansCJKkr-Bold',
                        color: Color(0xff5244fe)
                    ),
                  ),
                ),
              ),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(30),
            child: GestureDetector(
              onTap: skipTest,
              child: Text("잘 모르겠어요",
                style: TextStyle(color: skipBtn,
                    fontSize: 15,
                    decoration: TextDecoration.underline),
              ),
            ),
          )
        ],
      ),
    );
  }

  void skipTest(){
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    widget.setExamScoreZero();
    setState(() {
      skipBtn = Colors.purpleAccent;
    });

    Common().showSkipPopup(
        context,
        (){
          skipBtn = Colors.white;
          reAssamblePage();
        },
        widget.goNextPage,
        widget.isExam
    );
  }

  void reAssamblePage(){
    setState(() {
      _controller.clear();
      skipBtn = Colors.white;
    });
  }

}
