import 'package:flutter/material.dart';

import '../../../../services/json_parser/ExamTemp/exam_json5.dart';
import '../../../../services/timer.dart';
import '../../../layouts/exam_screen/CommonPaint.dart';

class Tmp5ColumCard extends StatefulWidget {

  final Size size;
  final ExamJson5 examJson5;
  final Animation<double> animation;
  final Function changeListening;
  final Function skipTest;
  final bool isExam;

  Tmp5ColumCard({
    this.size,
    this.examJson5,
    this.animation,
    this.changeListening,
    this.skipTest,
    this.isExam
  });

  @override
  _Tmp5ColumCardState createState() => _Tmp5ColumCardState();
}

class _Tmp5ColumCardState extends State<Tmp5ColumCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.fromLTRB(25, 0, 25, 0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,

            children: <Widget>[
              SizedBox(
                height: widget.size.height*0.05,
              ),
              Text("테스트", style: TextStyle(fontSize: 11, color: Colors.white, fontWeight: FontWeight.bold),),
              CustomPaint(
                size: Size(widget.size.width, widget.size.height*0.1),
                painter: widget.isExam?MyPainter(exam: 5):null,
              ),

              Container(
                  width: widget.size.width*0.65,
                  child: Text("다음 상황에 적절한 답변을 해보세요.",
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                        fontFamily: 'NotoSansCJKkr'
                    ),
                    textAlign: TextAlign.center,
                  )
              ),

              SizedBox(
                height: widget.size.height*0.05,
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: [
                      BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.3),
                          blurRadius: 6.0,
                          offset: Offset(0, 4)
                      )
                    ]
                ),
                child: Padding(
                  padding: EdgeInsets.fromLTRB(24, 24, 24, 24),
                  child: widget.examJson5.qtext==""?Text("ERROR"):Text(widget.examJson5.qtext,
                    style: TextStyle(
                        fontSize: 18,
                        height: 1.6,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'NotoSansCJKkr',
                        color: Color(0xff444444)
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              SizedBox(
                height: widget.size.height*0.037,
              ),
              widget.examJson5.ptext==""?Text("ERROR"):Padding(
                padding: EdgeInsets.only(left: 15, right: 15),
                child: Text(widget.examJson5.ptext,
                  style: TextStyle(
                      fontSize: 14,
                      color: Color(0xffd6cafa)),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: widget.size.height*0.073,
              ),
              Center(
                child: Container(
                  color: Colors.transparent,
                  height: 120,
                  width: 120,
                  child: new CustomPaint(
                    foregroundPainter: CircleProgress(widget.animation.value, 10, Color.fromRGBO(118, 118, 230, 1)),
                    child: new Padding(
                      padding: EdgeInsets.all(15),
                      child: Container(
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.3),
                                  blurRadius: 7.0,
                                  offset: Offset(
                                      5,5
                                  )
                              )
                            ],
                            shape: BoxShape.circle,
                            gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [Color.fromRGBO( 129, 129, 251, 1), Color.fromRGBO(72, 92, 241, 1), Color.fromRGBO(54, 70, 192, 10)]
                            )
                        ),
                        child: IconButton(
                          icon: Icon(Icons.mic, size: 50, color: Colors.white,),
                          onPressed: widget.changeListening,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(30),
                child: GestureDetector(
                  onTapDown: (TapDownDetails tapDown){
                    widget.skipTest();
                  },
                  child: Text("잘 모르겠어요",
                    style: TextStyle(color: Colors.white, fontSize: 15, decoration: TextDecoration.underline),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
