import 'package:collarsApp/screens/components/buttons/template/template2_button.dart';
import 'package:collarsApp/screens/layouts/exam_screen/CommonPaint.dart';
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/json_parser/ExamTemp/exam_json2.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Tmp2ColumCard extends StatefulWidget {
  final Size size;
  final ExamJson2 examJson2;
  final List<String> choice;

  final Function getResult;
  final Function setExamScoreZero;
  final Function goNextTest;
  final bool isExam;

  Tmp2ColumCard({
    this.size,
    this.examJson2,
    this.choice,
    this.getResult,
    this.setExamScoreZero,
    this.goNextTest,
    this.isExam
  });

  @override
  _Tmp2ColumCardState createState() => _Tmp2ColumCardState();
}

class _Tmp2ColumCardState extends State<Tmp2ColumCard> {

  int selectedIndex = -1;
  Color skipBtn = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,

        children: <Widget>[
          SizedBox(
            height: widget.size.height*0.05,
          ),
          Text("테스트", style: TextStyle(fontSize: 11, color: Colors.white, fontWeight: FontWeight.bold),),
          CustomPaint(
            size: widget.isExam?Size(widget.size.width, widget.size.height*0.1):Size(0, 0),
            painter: widget.isExam?MyPainter(exam: 2):null,
          ),
          SizedBox(height: widget.isExam?0:30,),

          Padding(
            padding: EdgeInsets.fromLTRB(25, 0, 25, 0),
            //child: Text(examJson2.ptext??"error", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),)
            child: Text("빈칸에 들어갈 표현 중\n적절하지 않은 것을 고르세요.",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.white,
                fontFamily: 'NotoSansCJKkr-Medium'
              ),
              textAlign: TextAlign.center,
            ),
          ),

          SizedBox(
            height: widget.size.height*0.05,
          ),

          Padding(
            padding: EdgeInsets.fromLTRB(25, 0, 25, 0),
            child: Container(
              width: widget.size.width*86.7,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  boxShadow: [
                    BoxShadow(
                        color: Color(0x29000000),
                        blurRadius: 6.0,
                        offset: Offset(0,4)
                    )
                  ]
              ),
              child: Padding(
                  padding: EdgeInsets.all(24),
                  child: Text(widget.examJson2.qtext??"error",
                  style: TextStyle(
                      fontSize: 19,
                      fontWeight: FontWeight.bold,
                      height: 1.5
                  ),
                    textAlign: TextAlign.center,
                  )
              ),
            ),
          ),

          SizedBox(
            height: widget.size.height*0.05,
          ),

          Padding(
            padding: EdgeInsets.fromLTRB(25, 0, 25, 0),
            child: Column(
              children: <Widget>[
                Tmp2RadioBtn(answer: widget.choice[0], index: 0, alp: "A", size: widget.size, selectedIndex: selectedIndex, changeIndex: changeIndex,),
                Tmp2RadioBtn(answer: widget.choice[1], index: 1, alp: "B", size: widget.size, selectedIndex: selectedIndex, changeIndex: changeIndex,),
                Tmp2RadioBtn(answer: widget.choice[2], index: 2, alp: "C", size: widget.size, selectedIndex: selectedIndex, changeIndex: changeIndex,),
                Tmp2RadioBtn(answer: widget.choice[3], index: 3, alp: "D", size: widget.size, selectedIndex: selectedIndex, changeIndex: changeIndex,)
              ],
            ),
          ),
          SizedBox(
            height: widget.size.height*0.03,
          ),
          Container(
            width: widget.size.width*0.347,
            height: widget.size.height*0.072,
            child: RaisedButton(
              onPressed: (){widget.getResult(selectedIndex);},
              color: Colors.white,
              child: Padding(
                padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: Center(
                  child: Text("OK",
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'NotoSansCJKkr-Bold',
                      color: Color(0xff5244fe)
                    ),
                  ),
                ),
              ),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(30),
            child: GestureDetector(
              onTapDown: (TapDownDetails tapDwon) {
                skipTest();
              },
              child: Text("잘 모르겠어요",
                style: TextStyle(color: skipBtn,
                    fontSize: 15,
                    decoration: TextDecoration.underline),
              ),
            ),
          )
        ],
      ),
    );
  }

  void changeIndex(int index){
    setState(() {
      selectedIndex = index;
    });
  }

  void skipTest(){

    setState(() {
      skipBtn = Colors.purpleAccent;
      widget.setExamScoreZero();
    });

    Common().showSkipPopup(
        context,
            () {
          setState(() {
            skipBtn = Colors.white;
          });
        },
        widget.goNextTest,
        widget.isExam
    );
  }
}

