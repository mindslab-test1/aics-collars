import 'package:collarsApp/services/json_parser/practice/pron_practice.dart';
import 'package:collarsApp/services/json_parser/practice/quiz_practice_data.dart';
import 'package:collarsApp/services/json_parser/practice/sentence_pratice.dart';
import 'package:collarsApp/services/json_parser/practice/word_practice.dart';
import 'package:flutter/material.dart';

class Course extends StatelessWidget {
  final String practiceName;
  final String time;
  final String subTitle;
  final String title;
  final bool finish;
  final List<dynamic> data;
  final Function func;

  Course({
    this.practiceName,
    this.time,
    this.subTitle,
    this.title,
    this.finish,
    this.data,
    this.func
  });

  @override
  Widget build(BuildContext context) {

    var dataWidgets = List<Widget>();
    var clickEvent = Colors.white;
    Size size = MediaQuery.of(context).size;
    //List keys = List();
    if(data != null) dataWidgets = getWidgetData(data, subTitle);

    return IntrinsicHeight(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color.fromRGBO(34, 42, 100, 1)
                  ),
                  width: size.width * 0.12,
                  height: size.height * 0.027,
                  child: Center(child: Text(time,
                    style: TextStyle(color: Colors.white, fontSize: 13),)),
                ),
                subTitle != "Quiz" ? Expanded(
                  child: VerticalDivider(color: Colors.black,),
                ) : Container()
              ],
            ),
          ),

          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(subTitle,
                style: TextStyle(color: Color.fromRGBO(51, 51, 51, 0.6), fontFamily: 'NotoSansCJKkr'),),
              Text(title,
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, fontFamily: 'NotoSansCJKkr'),),
              Padding(
                padding: EdgeInsets.fromLTRB(
                    0, 10, size.width * 0.07, size.height * 0.04),
                child: Container(
                  width: size.width * 0.7,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10)
                  ),
                  child: Column(
                    children: <Widget>[
                      GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        //onTap: finish&&("강의듣기"!=subTitle)?(){}:func,
                        onTap: func,
                        child: Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.fromLTRB(10, 13, 0, 13),
                                  child: Text(practiceName,
                                    style: TextStyle(fontWeight: FontWeight.bold),)
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    right: size.width * 0.039, top: 13),
                                child: Container(
                                  height: size.height * 0.03,
                                  width: size.height * 0.03,
                                  decoration: BoxDecoration(
                                      color: finish ? Colors.blueAccent : Color
                                          .fromRGBO(246, 247, 251, 1),
                                      shape: BoxShape.circle
                                  ),
                                  child: Icon(Icons.check, color: Colors.white,),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: dataWidgets,
                      )
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  /*
  * 해당 코스에 data를 보여주기 위해
  * */
  List<Widget> getWidgetData(List<dynamic> data, String subTitle){
    var dataWidgets = List<Widget>();
    if("발음학습" == subTitle){
      List<Prons> pronData = data;
      for(int i=0;i<pronData.length;i++){
        dataWidgets.add(Padding(
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(pronData[i].sentence, style: TextStyle(fontFamily: 'Raleway'),),
              i<pronData.length-1?Divider():Container(height: 10,)
            ],
          ),
        ));
      }
    }else if("단어학습" == subTitle){
      List<Words> wordData = data;
      for(int i=0;i<wordData.length;i++){
        Row row = Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
                width: 100,
                child: Text(
                    wordData[i].word,
                    style: TextStyle(fontFamily: 'Raleway', fontSize: 14, fontWeight: FontWeight.w300),

                )
            ),
            Container(
                width: 100,
                child: Text(wordData[i].answer, style: TextStyle(fontFamily: 'Raleway', fontSize: 14, fontWeight: FontWeight.w100))
            )
          ],
        );
        dataWidgets.add(Padding(
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Column(
              children: <Widget>[
                row,
                i<wordData.length-1?Divider():Container(height: 10,)
              ],
            )
        )
        );
      }
    }else if("문장학습" == subTitle){
      List<Sentences> sentenceData = data;
      for(int i=0;i<sentenceData.length;i++){
        Column column = Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(sentenceData[i].question, style: TextStyle(fontFamily: 'Raleway', fontWeight: FontWeight.w300)),
            Text(sentenceData[i].answer, style: TextStyle(fontFamily: 'Raleway', fontWeight: FontWeight.w100))
          ],
        );
        dataWidgets.add(Padding(
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                column,
                i<sentenceData.length-1?Divider():Container(height: 10,)
              ],
            )
        ));
      }
    }else if("Quiz" == subTitle){
      List<Quizs> quizData = data;
      for(int i=0;i<quizData.length;i++){
        Column column = Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(quizData[i].question, style: TextStyle(fontFamily: 'Raleway', fontWeight: FontWeight.w300)),
            Text(quizData[i].correctAnswer, style: TextStyle(fontFamily: 'Raleway', fontWeight: FontWeight.w100))
          ],
        );
        dataWidgets.add(Padding(
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                column,
                i<quizData.length-1?Divider():Container(height: 10,)
              ],
            )
        ));
      }
    }

    return dataWidgets;
  }
}
