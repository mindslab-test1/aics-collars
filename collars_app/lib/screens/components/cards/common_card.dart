import 'package:collarsApp/styles/color/default.dart';
import 'package:flutter/material.dart';

//TODO
class CommonCard extends StatelessWidget {
  final double width;
  final double height;
  final double margin;
  final double padding;
  final Color color;
  final List<BoxShadow> boxShadow;
  Widget child;

  CommonCard(
      {this.width,
      this.height,
      this.margin = 0,
      this.padding = 30,
      this.color = kPrimaryColor,
      this.boxShadow,
      this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
        width: width,
        height: height,
        margin: EdgeInsets.all(margin),
        padding: EdgeInsets.all(padding),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: boxShadow,
        ),
        child: child);
  }
}
