import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

// Provider
import 'package:provider/provider.dart';
import 'package:collarsApp/providers/user.dart';

class HelloCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SvgPicture.asset(
            'assets/images/icon/account_circle-24px.svg',
            color: Color(0xffd0d0d0),
            width: 75,
            height: 75,
          ),
          SizedBox(width: 20),
          Flexible(
            child: Text(
              "Hello, ${Provider.of<User>(context).name}",
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontFamily: 'NotoSansCJKkr',
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
