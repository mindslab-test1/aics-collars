import 'package:collarsApp/styles/size/default.dart';
import 'package:flutter/material.dart';

class EvereadingCard extends StatelessWidget {

  dynamic data;


  EvereadingCard({this.data});

  @override
  Widget build(BuildContext context) {
    String engSector = data["sectorEngTag"];
    String thumbnail = data["thumbnail"];
    String korTitle = data["content"]["kor"]["title"];
    String engTitle = data["content"]["eng"]["title"];

    return Container(
      padding: EdgeInsets.only(bottom: 12),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            height: 170,
            decoration: BoxDecoration(
              color: Colors.white,
              image: DecorationImage(
                  image: NetworkImage(thumbnail), fit: BoxFit.cover),
            ),
          ),
          Container(
            padding: kDefaultPaddingSymmetric,
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  engSector,
                  style: TextStyle(
                      fontSize: 14,
                      color: Color(0xffc0c0c0),
                      fontWeight: FontWeight.w600),
                ),
                SizedBox(height: 4),
                Text(
                  engTitle,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 4),
                Text(
                  korTitle,
                  style: TextStyle(fontSize: 16),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
