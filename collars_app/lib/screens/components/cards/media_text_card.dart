import 'package:flutter/material.dart';

class MediaText extends StatefulWidget {
  final String subject;
  final String content;

  MediaText({this.subject, this.content});

  @override
  _MediaTextState createState() => _MediaTextState();
}

class _MediaTextState extends State<MediaText> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(widget.subject,
          style: TextStyle(
              fontSize: 16,
              color: Color(0xff5262f2),
              fontWeight: FontWeight.bold
          ),
        ),
        SizedBox(height: 10,),
        Text(widget.content,
          style: TextStyle(
              fontSize: 16,
              color: Color(0xff444444),
              height: 1.4
          ),
        )
      ],
    );
  }
}
