import 'package:collarsApp/screens/components/etc/rating_bar.dart';
import 'package:collarsApp/screens/layouts/evereading/evereading_content_screen.dart';
import 'package:collarsApp/screens/layouts/twocents/twocents_content_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

// Provider
import 'package:provider/provider.dart';
import 'package:collarsApp/providers/home_main_provider.dart';

// Service
import 'package:collarsApp/services/types.dart';

// Style
import 'package:collarsApp/styles/color/default.dart';

class IntroOne extends StatelessWidget {
  static const String id = 'intro_one';
  String service;
  int position;

  IntroOne({@required this.service, @required this.position});

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    String appbarTitle;
    dynamic data;
    String background;
    String cardTag;
    String cardEngTitle;
    String cardKorTitle;
    double level;
    Function cardOnTap;
    switch (service) {
      case EVEREADING:
        appbarTitle = "Evereading";
        data = Provider.of<HomeMainProvider>(context).columns[position];
        background = data["image"];
        cardTag = "독해,문법 학습";
        cardEngTitle = data['content']['eng']['title'];
        cardKorTitle = data['content']['kor']['title'];
        level = data['lev'].toDouble();
        cardOnTap = () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => EvereadingContentScreen(
                data: data,
              ),
            ),
          );
        };
        break;
      case TWOCENTS:
        appbarTitle = "Collars Two Cents";
        data = Provider.of<HomeMainProvider>(context).scripts[position];
        background = data["background"];
        cardTag = "회화,발음 학습";
        cardEngTitle = data['content']['title']['eng'];
        cardKorTitle = data['content']['title']['kor'];
        level = data['lev'].toDouble();
        cardOnTap = () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => TwoCentsContentScreen(
                data: data,
              ),
            ),
          );
        };
        print(data['content']['content']);
        break;
    }

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          appbarTitle,
          style: TextStyle(
            fontFamily: 'NotoSansCJKkr',
            fontSize: 12,
            color: Colors.black,
          ),
        ),
        leading: Navigator.canPop(context)
            ? GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: SvgPicture.asset(
                  'assets/images/icon/icon_close.svg',
                  color: kMainBlack,
                ),
              )
            : null,
        backgroundColor: Colors.transparent,
        bottomOpacity: 0.0,
        elevation: 0.0,
      ),
      body: Container(
        child: Stack(
          children: [
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              height: screenHeight * 0.7,
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(background), fit: BoxFit.cover),
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              height: screenHeight * 0.45,
              child: Container(
                padding:
                    EdgeInsets.only(left: 24, right: 24, top: 24, bottom: 30),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(32),
                    topRight: Radius.circular(32),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        cardTagBox(tag: cardTag),
                        // Text('즐겨찾기'),
                      ],
                    ),
                    SizedBox(height: 8),
                    Text(
                      cardEngTitle,
                      style: TextStyle(
                        fontFamily: 'NotoSansCJKkr',
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                        color: kMainBlack,
                      ),
                    ),
                    SizedBox(height: 4),
                    Text(
                      cardKorTitle,
                      style: TextStyle(
                        fontFamily: 'NotoSansCJKkr',
                        fontSize: 16,
                        color: kMainBlack,
                      ),
                    ),
                    SizedBox(height: 10),
                    RatingBar(
                      rating: level,
                      itemCount: 5,
                      activeColor: kStar,
                      inactiveColor: kStar50,
                    ),
                    // SizedBox(height: 14),
                    Expanded(child: Container()),
                    Row(
                      children: [
                        cardMessage(),
                        SizedBox(width: 8),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              cardOnTap();
                            },
                            child: cardStart(),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 8),
                    Text(
                      '혹시 궁금해왔던  주제가 있다면, COLLARS 에 신청해보세요 ~',
                      style: TextStyle(
                        fontFamily: 'NotoSansCJKkr',
                        fontSize: 10,
                        color: kComment,
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget cardTagBox({String tag}) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 6, horizontal: 12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        border: Border.all(color: kMainBlue, width: 1),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SvgPicture.asset(
            'assets/images/icon/icon-mic-12px.svg',
            width: 12,
            height: 12,
            color: kMainBlue,
          ),
          SizedBox(width: 4),
          Text(
            tag,
            style: TextStyle(
              fontFamily: 'NotoSansCJKkr',
              fontSize: 10,
              color: kMainBlue,
            ),
          ),
        ],
      ),
    );
  }

  Widget cardMessage() {
    return Container(
      width: 48,
      height: 48,
      decoration: BoxDecoration(
        color: kMainBlue,
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
      ),
      child: SvgPicture.asset(
        'assets/images/icon/icon_mail-48px.svg',
        color: Colors.white,
      ),
    );
  }

  Widget cardStart() {
    return Container(
      height: 48,
      decoration: BoxDecoration(
        color: kMainBlue,
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
      ),
      child: Center(
        child: Text(
          'Start',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 14,
            color: kMainWhite,
          ),
        ),
      ),
    );
  }
}
