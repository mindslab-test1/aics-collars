import 'package:flutter/material.dart';

class IconTextButton extends StatelessWidget {

  final Icon icon;
  final Text text;
  final Function onPressed;

  const IconTextButton({this.icon, this.text, this.onPressed});



  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onPressed();
      },
      child: Row(
        children: <Widget>[
          icon,
          SizedBox(width: 10),
          text
        ],
      ),
    );
  }
}
