import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SignInButton extends StatelessWidget {
  // Todo: Should change icon to logo
  SignInButton(
      {@required this.title,
      @required this.onClick,
      this.icon,
      this.color,
      this.bgColor,
      this.iconColor});

  final Function onClick;
  final String title;
  final String icon;
  Color color;
  Color bgColor;
  Color iconColor;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onClick,
      child: Container(
        width: 225,
        margin: EdgeInsets.symmetric(vertical: 4),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50), color: bgColor),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 9.0, horizontal: 12),
          child: Row(
            children: <Widget>[
              SvgPicture.asset(
                icon,
                width: 24,
                height: 24,
                color: iconColor,
              ),
              Expanded(
                child: Center(
                  child: Text(
                    title,
                    style: TextStyle(color: color),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
