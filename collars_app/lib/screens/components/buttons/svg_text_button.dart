import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SvgTextButton extends StatelessWidget {
  final SvgPicture svg;
  final Text text;
  final Function onPressed;
  final EdgeInsetsGeometry padding;


  const SvgTextButton({this.svg, this.text, this.onPressed, this.padding});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){onPressed();},
      child: Container(
        padding: padding,
        child: Row(
          children: <Widget>[
            svg,
            SizedBox(width: 10),
            text
          ],
        ),
      ),
    );
  }
}
