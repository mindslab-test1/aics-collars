import 'package:flutter/material.dart';

class Tmp2RadioBtn extends StatelessWidget {
  final int selectedIndex;
  final int index;
  final Size size;
  final Function changeIndex;
  final String alp;
  final String answer;

  Tmp2RadioBtn({
    this.selectedIndex,
    this.index,
    this.size,
    this.changeIndex,
    this.alp,
    this.answer
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
      child: GestureDetector(
        onTap: () => changeIndex(index),
        child: Container(
          width: size.width*0.722,
          height: 37,
          decoration: BoxDecoration(
              boxShadow: selectedIndex == index?[
                BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.3),
                    blurRadius: 6.0,
                    offset: Offset(0,3)
                )
              ]:[],
              /* color: selectedIndex == index ? Colors.grey : Color.fromRGBO(163, 122, 221, 1),*/
              gradient: selectedIndex != index? LinearGradient(
                  colors: [Color(0xffb581e4), Color(0xffb581e4)]
              ): LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Color.fromRGBO(117, 121, 248, 10), Color.fromRGBO(70, 90, 237, 10), Color.fromRGBO(57, 73, 200, 10)]
              ),
              borderRadius: BorderRadius.circular(20)
          ),
          child: Row(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(11, 0, 20, 0),
                child: Text("${alp}.", style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),),
              ),
              Text(answer, style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold))
            ],
          ),
        ),
      ),
    );;
  }
}
