import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  final dynamic id;
  final double width;
  final double height;
  final EdgeInsets margin;
  final String text;
  final Function onPressed;
  final Color bgColor;
  final Color textColor;
  final Color borderColor;

  RoundedButton({
    this.width,
    this.height,
    this.margin,
    this.text,
    this.onPressed,
    this.bgColor,
    this.textColor,
    this.borderColor, this.id,
  });

  onPressedHandler() {
    onPressed();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      margin: margin,
      child: FlatButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50.0),
          side: BorderSide(width: 2, color: borderColor),
        ),
        onPressed: onPressedHandler,
        color: bgColor,
        child: Text(
          text,
          style: TextStyle(
            fontFamily: 'NotoSansCJKkr',
            color: textColor,
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}
