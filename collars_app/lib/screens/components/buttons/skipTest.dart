import 'package:flutter/material.dart';

class SkipTestBtn extends StatelessWidget {

  final Function skipTest;
  final Color skipBtn;

  SkipTestBtn({
    this.skipTest,
    this.skipBtn
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: skipTest,
      child: Text("잘 모르겠어요",
          style: TextStyle(
            color: skipBtn,
            fontSize: 15,
            decoration: TextDecoration.underline
          ),
      ),
    );
  }
}
