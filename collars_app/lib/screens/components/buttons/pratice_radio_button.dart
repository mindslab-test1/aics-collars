import 'package:flutter/material.dart';

class PracticeRadioBtn extends StatelessWidget {
  final int answerNum;
  final String answer;
  final int index;
  final List selectedIndex;
  final Function changeIndex;

  PracticeRadioBtn({
    this.answerNum,
    this.answer,
    this.index,
    this.selectedIndex,
    this.changeIndex
  });

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
      child: GestureDetector(
        onTap: () => changeIndex(answerNum, index),
        child: Container(
          width: size.width*0.864,
          constraints: new BoxConstraints(
              minHeight: 60
          ),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.white),
              color: selectedIndex[answerNum] == index? Colors.white:
              Colors.transparent,
              borderRadius: BorderRadius.circular(40)
          ),
          child: Padding(
            padding: EdgeInsets.fromLTRB(20, 11, 20, 11),
            child: Center(
              child: Text(
                answer,
                style: TextStyle(
                    color: selectedIndex[answerNum] == index? Color.fromRGBO(82, 98, 242, 1): Colors.white,
                    fontSize: 16,
                    fontWeight: selectedIndex[answerNum] == index? FontWeight.w700
                      :FontWeight.w300,
                    fontFamily: 'NotoSansCJKkr'
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
      ),
    );;
  }
}
