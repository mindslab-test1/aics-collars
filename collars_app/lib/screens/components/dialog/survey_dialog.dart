import 'package:flutter/material.dart';

Future<void> surveyDialog(BuildContext context) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        content: Text('설문을 완료해 주세요'),
        actions: <Widget>[
          FlatButton(
            child: Text('확인'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
