import 'package:collarsApp/screens/layouts/exam_main.dart';
import 'package:collarsApp/screens/layouts/survey_intro_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// Provider
import 'package:collarsApp/providers/user.dart';

// Screen
import 'package:collarsApp/screens/layouts/main/main_screen.dart';
import 'package:collarsApp/screens/layouts/sign_in_screen.dart';
import 'layouts/survey_screen.dart';

// Service
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/auth.dart';
import 'package:collarsApp/services/survey.dart';



class WrapScreen extends StatelessWidget {
  static const String id = 'wrap_screen';

  Common _common = Common();

  @override
  Widget build(BuildContext context) {
    AuthService _authService = AuthService();
    SurveyService _surveyService = SurveyService();
    final user = Provider.of<User>(context);

    if (user != null) {
      return FutureBuilder(
        future:  _authService.getUserData(user.uid),
        builder: (context, snapshot) {
          switch (snapshot.connectionState){
            case ConnectionState.none:
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator(),);
            default:
              if(snapshot.hasError){
                return Center(child: CircularProgressIndicator(),);
              }else{
                user.name = snapshot.data['name'];
                user.signUpDate = DateTime.parse(snapshot.data['signUpDate']);
                user.course = snapshot.data['courseName'];
                user.first = snapshot.data['first'];
                user.survey = snapshot.data['survey'];

                if(_common.equalsIgnoreCase(user.first, 'N')) { return ExamMain();}
                else if(_common.equalsIgnoreCase(user.survey, 'N')) { return SurveyIntroScreen(); }
                else { return MainScreen(); }
              }
          }
        },
      );
    } else {
      return SignInScreen();
    }
  }
}
