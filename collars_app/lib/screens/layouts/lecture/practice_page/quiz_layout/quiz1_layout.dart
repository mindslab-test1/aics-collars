import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import '../../../../../services/common.dart';
import '../../../../../services/json_parser/ExamTemp/exam_json2.dart';
import '../../../../components/cards/template/tmp2_card.dart';

class Quiz1Layout extends StatefulWidget {

  final Function goNextPage;
  final ExamJson2 examJson2;
  final List<String> userAnswer;
  final List<String> question;
  final List<String> score;

  Quiz1Layout({
    this.goNextPage,
    this.examJson2,
    this.userAnswer,
    this.question,
    this.score
  });

  @override
  _Quiz1LayoutState createState() => _Quiz1LayoutState();
}

class _Quiz1LayoutState extends State<Quiz1Layout> {

  String _appServer = DotEnv().env['APP_SERVER'];
  List<String> choice = List<String>();

  @override
  void initState() {
    super.initState();
    choice.add(widget.examJson2.mulChoice1);
    choice.add(widget.examJson2.mulChoice2);
    choice.add(widget.examJson2.mulChoice3);
    choice.add(widget.examJson2.mulChoice4);
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [Color.fromRGBO(255, 87, 159, 0.78), Color.fromRGBO(168, 87, 213, 0.98),Color.fromRGBO(161, 87, 217, 1), Color.fromRGBO(80, 67, 255, 1)]
        )
      ),
      width: size.width,
      height: size.height,
      child: Padding(
        padding: EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
        child: SingleChildScrollView(
          child: Tmp2ColumCard(
            size: size,
            examJson2: widget.examJson2,
            choice: choice,
            getResult: getResult,
            setExamScoreZero: setExamScoreZero,
            goNextTest: widget.goNextPage,
            isExam: false,
          ),
        ),
      ),
    );
  }

  void setExamScoreZero(){
    widget.question[0] = widget.examJson2.qtext;
    widget.userAnswer[0] = '';
    widget.score[0] = '0';
  }

  Future<void> getResult(int selectedIndex) async{
    if(selectedIndex<0) return;

    Map<String, dynamic> request = {
      "examNum" : widget.examJson2.examNum,
      "answer" : choice[selectedIndex]
    };

    int finalScore = int.parse(await Common().httpRequest(_appServer+"/maumAi/exam/q2/getScore", request));
    String score;

    if(finalScore == 100) score = 'A';
    else score = 'D';

    setState(() {
      widget.question[0] = widget.examJson2.qtext;
      widget.userAnswer[0] = choice[selectedIndex];
      widget.score[0] = finalScore.toString();
    });

    Common().showPopup(score, "Answer", widget.examJson2.correct, "You choose", choice[selectedIndex], context, (){},
            widget.goNextPage,false);
  }

}
