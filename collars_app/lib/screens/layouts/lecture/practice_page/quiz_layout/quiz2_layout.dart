import 'package:collarsApp/services/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import '../../../../../services/json_parser/ExamTemp/exam_json4.dart';
import '../../../../components/cards/template/tmp4_card.dart';

class Quiz2Layout extends StatefulWidget {

  final ExamJson4 examJson4;
  final Function goNextPage;

  final List<String> userAnswer;
  final List<String> question;
  final List<String> score;

  Quiz2Layout({
    this.examJson4,
    this.goNextPage,
    this.userAnswer,
    this.question,
    this.score
  });

  @override
  _Quiz2LayoutState createState() => _Quiz2LayoutState();
}

class _Quiz2LayoutState extends State<Quiz2Layout> {

  bool isLoading = false;

  String _appServer = DotEnv().env['APP_SERVER'];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Color.fromRGBO(255, 87, 159, 0.78), Color.fromRGBO(168, 87, 213, 0.98),Color.fromRGBO(161, 87, 217, 1), Color.fromRGBO(80, 67, 255, 1)]
          )
      ),
      width: size.width,
      height: size.height,
      child: Padding(
        padding: EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.fromLTRB(25, 0, 25, 0),
            child: Tmp4ColumCard(
              size: size,
              examJson4: widget.examJson4,
              goNextPage: widget.goNextPage,
              getResult: getResult,
              setExamScoreZero: setExamScoreZero,
              isExam: false,
            ),
          ),
        ),
      ),
    );
  }

  void setExamScoreZero(){
    widget.question[1] = widget.examJson4.qtext2;
    widget.userAnswer[1] = '';
    widget.score[1] = '0';
  }

  Future<void> getResult(String answer, Function reAssamblePage) async{
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    //String answer = controller.text;
    Map<String, dynamic> request = {
      "examNum" : widget.examJson4.examNum,
      "answer" : answer
    };

    if(answer!=null && answer!=""){
      int finalScore = int.parse(await Common().httpRequest(_appServer+"/maumAi/exam/q4/getScore", request));
      String score;

      if(finalScore>70) score = "A";
      else if(finalScore>40 && finalScore<=70) score = "B";
      else if(finalScore>10 && finalScore<=40) score = "C";

      setState(() {
        widget.question[1] = widget.examJson4.qtext2;
        widget.userAnswer[1] = answer;
        widget.score[1] = finalScore.toString();
      });

      Common().showPopup(
          score,
          "Answer",
          widget.examJson4.correctAnswer,
          "You typed",
          answer,
          context,
          reAssamblePage,
          widget.goNextPage,
          false
      );
    }
  }
}
