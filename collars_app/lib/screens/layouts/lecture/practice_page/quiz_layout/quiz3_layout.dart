import 'dart:async';
import 'dart:convert';

import 'package:collarsApp/screens/components/cards/template/tmp5_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import '../../../../../services/common.dart';
import '../../../../../services/json_parser/ExamTemp/exam_json5.dart';
import '../../../../../services/json_parser/pron.dart';
import '../../../../../services/mic_streaming.dart';

class Quiz3Layout extends StatefulWidget {

  static String _webSockServer = DotEnv().env['WEB_SOCK_SERVER'];
  static Uri uri  = Uri.parse(_webSockServer+"/engedu/websocket/pronExamQ5");

  final ExamJson5 examJson5;
  final Function goNextPage;

  final List<String> userAnswer;
  final List<String> question;
  final List<String> score;

  Quiz3Layout({
    this.examJson5,
    this.goNextPage,
    this.userAnswer,
    this.question,
    this.score
  });

  @override
  _Quiz5LayoutState createState() => _Quiz5LayoutState();
}

class _Quiz5LayoutState extends State<Quiz3Layout> with SingleTickerProviderStateMixin{

  static String _webSockServer = DotEnv().env['WEB_SOCK_SERVER'];
  Color skipBtn = Colors.white;

  MicStream micStream = MicStream();

  AnimationController progressController;
  Animation<double> animation;

  WebSocketChannel channel;
  StreamSubscription<List<int>> listener;

  bool isRecording = false;
  bool isWebScokConn = false;

  @override
  void dispose() {
    stopTest();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    //hide keyboard
    SystemChannels.textInput.invokeMethod('TextInput.hide');

    progressController = AnimationController(vsync: this,duration: Duration(seconds: 10));
    animation = Tween<double>(begin: 0,end: 100).animate(progressController)..addListener((){
      if(animation.value.toInt() == 100 && isRecording) _stopWebSocket();
      setState(() {});
    });
  }

  void scroeResult(String result) {
    PronResponse pronResponse = PronResponse.fromJson(jsonDecode(result));
    setState(() {
      widget.userAnswer[2] = pronResponse.sttResult;
      widget.question[2] = widget.examJson5.correctAnswer;
      widget.score[2] = pronResponse.score.toString();
    });

    String score;

    if(pronResponse.score>70) score= "A";
    else if(pronResponse.score>40 && pronResponse.score<=70) score = "B";
    else if(pronResponse.score>10 && pronResponse.score<=40) score = "C";
    else score = "D";

    Common().showPopup(score, "Answer", widget.examJson5.correctAnswer, "You said", pronResponse.sttResult,
        context, reAssamblePage, goNextPage, false);
  }

  /*
  * byte로 변환된 데이터를 websocket을 보냄
  * websocket으로 부터 data가 들어올 시 websocket conn 종료 후 popup
  * */
  bool _sendWebsocket(){

    if(isRecording) return false;

    setState(() {
      channel = WebSocketChannel.connect(Quiz3Layout.uri);
      isWebScokConn = true;
      isRecording = true;
    });

    //처음 exam num를 보내줌
    channel.sink.add(utf8.encode("Exam5-"+widget.examJson5.examNum.toString()));

    //animation
    progressController.forward();

    listener = micStream.startListening().listen((samples) => {
      channel.sink.add(samples)
    });

    channel.stream.listen((event) {
      channel.sink.close();
      setState(() {isWebScokConn = false;});
      scroeResult(event);
    });
  }

  /*
  * 1byte를 보낼 경우 grpc를 끊어 줌
  * */
  Future<void> _stopWebSocket(){
    List<int> end = List<int>(1);
    end[0] = 1;

    changeAnimationState('stop');
    changeMicStreamStatus('stop');

    channel.sink.add(end);

    setState(() { isRecording = false;});

    return null;
  }

  void _changeListening() => !isRecording ? _sendWebsocket() : _stopWebSocket();

  @override
  Widget build(BuildContext context) {

    final Size size = MediaQuery.of(context).size;

    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Color.fromRGBO(255, 87, 159, 0.78), Color.fromRGBO(168, 87, 213, 0.98),Color.fromRGBO(161, 87, 217, 1), Color.fromRGBO(80, 67, 255, 1)]
            )
        ),
        width: size.width,
        height: size.height,
        child: Tmp5ColumCard(
          size: size,
          examJson5: widget.examJson5,
          animation: animation,
          changeListening: _changeListening,
          skipTest: skipTest,
          isExam: false,
        )
    );
  }

  /*
  * 페이지가 dispose될 경우
  * */
  void stopTest(){
    changeAnimationState('dispose');
    changeMicStreamStatus('dispose');
    shutDownWebSocket();
  }

  void goNextPage(){
    shutDownWebSocket();
    //changeAnimationState('dispose');
    changeMicStreamStatus('dispose');

    widget.goNextPage();
    //save exam number in provider
    //examScore.addExamlist(examJson5.examNum);

    //Navigator.push(context, CupertinoPageRoute(builder: (context) => ExamResult()));
  }

  void reAssamblePage(){
    changeAnimationState('reset');

    setState(() {
      skipBtn = Colors.white;
      isRecording = false;
    });
  }

  /*
  * 사용자가 발화를 했을 경우 결과를 popup,
  * 발화를 하지 않았을 경우 스킵 popup
  * */
  void skipTest(){
    setState(() {
      skipBtn = Colors.purpleAccent;
    });

    if (isRecording == true) {
      List<int> end = List<int>(1);
      end[0] = 1;

      changeAnimationState('stop');
      changeMicStreamStatus('stop');

      channel.sink.add(end);

      setState(() {
        isRecording = false;
      });
    }else{
      setState(() {
        widget.question[2] = widget.examJson5.qtext;
        widget.userAnswer[2] = '';
        widget.score[2] = '0';
      });

      Common().showSkipPopup(context, reAssamblePage, goNextPage, false);
    }
  }

  void changeAnimationState(String state){

    if(progressController.status == AnimationStatus.forward ||
        progressController.status == AnimationStatus.reverse){

      if(state == 'stop') progressController.stop();
      if(state == 'reset') progressController.reset();
      if(state == 'dispose' && progressController.status != AnimationStatus.dismissed) progressController.dispose();
    }
  }

  void shutDownWebSocket(){
    if(isWebScokConn){
      channel.sink.close();
      setState(() {
        isWebScokConn = false;
      });
    }
  }

  void changeMicStreamStatus(String status){
    if(isRecording){
      listener.cancel();
      if(status == 'stop') {
        if(micStream.getRecordingStatus())
          micStream.stopMicpListening();
      }
      else if(status == 'dispose'){
        micStream.endMicStream();
      }
    }
  }
}
