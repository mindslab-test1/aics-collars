import 'package:collarsApp/providers/user.dart';
import 'package:collarsApp/screens/layouts/lecture/practice_page/quiz_layout/quiz2_layout.dart';
import 'package:collarsApp/screens/layouts/lecture/practice_page/quiz_layout/quiz3_layout.dart';
import 'package:collarsApp/screens/layouts/lecture/practice_page/quiz_layout/quiz4_layout.dart';
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/json_parser/practice/quiz_practice.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';

import '../../../../services/json_parser/practice/quiz_practice.dart';
import 'quiz_layout/quiz1_layout.dart';
import 'quiz_layout/quiz1_layout.dart';

class QuizPracticeMain extends StatefulWidget {
  static const String id = 'quiz_practice';
  final int mediaId;
  QuizPracticeMain({this.mediaId});

  @override
  _QuizPracticeState createState() => _QuizPracticeState();
}

class _QuizPracticeState extends State<QuizPracticeMain> {
  String _appServer = DotEnv().env['APP_SERVER'];

  PageController pageController = PageController(initialPage: 0);
  bool isLoading;

  List<Widget> pages = List();

  //DB에 학습 data를 쌓기 위해 정보 저장
  List<String> userAnswer = List();
  List<String> question = List();
  List<String> score = List();

  User user;

  @override
  void initState() {
    super.initState();
    user = Provider.of<User>(context, listen: false);

    for(int i =0;i<4;i++){
      userAnswer.add('');
      question.add('');
      score.add('');
    }

    isLoading = true;

    getQuizPractice();

  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;


    return Scaffold(
      body: Container(
        child:isLoading?Common().spinkit(size, "로딩중입니다", Colors.white):SafeArea(
          child: PageView(
            controller: pageController,
            children: pages,
            physics: NeverScrollableScrollPhysics(),
          ),
        ),
      ),
    );
  }

  Future<void> getQuizPractice() async{
    Map<String, int> request = {"mediaId" : widget.mediaId};

    QuizPractice quizPractice = QuizPractice.fromJson(await Common().httpRequest(_appServer+"/practice/getQuizData", request));

    setPage(quizPractice);
  }

  void setPage(QuizPractice quizPractice){

    pages.add(Quiz1Layout(
      examJson2: quizPractice.quiz1,
      goNextPage: goNextPage,
      userAnswer: userAnswer,
      question: question,
      score: score
    ));
    pages.add(Quiz2Layout(
      examJson4: quizPractice.quiz2,
      goNextPage: goNextPage,
      userAnswer: userAnswer,
      question: question,
      score: score,
    ));
    pages.add(Quiz3Layout(
      examJson5: quizPractice.quiz3,
      goNextPage: goNextPage,
      userAnswer: userAnswer,
      question: question,
      score: score,
    ));
    pages.add(Quiz4Layout(
      examJson4: quizPractice.quiz4,
      goNextPage: goNextPage,
      userAnswer: userAnswer,
      question: question,
      score: score,
    ));

    setState(() {
      isLoading = false;
    });
  }

  goNextPage(){
    if(pageController.page.toInt() == 3) {
      savePracticeQuiz();
      Navigator.pop(context);
    }
    else
      pageController.animateToPage((pageController.page+1).toInt(), duration: Duration(microseconds: 200000), curve: Curves.bounceInOut);
  }

  Future<void> savePracticeQuiz() async{
    setState(() {
      isLoading = true;
    });

    Map<String, dynamic> request = {
      "uid" : user.uid,
      "mediaId" : widget.mediaId,
      "question" : question,
      "userAnswer" : userAnswer,
      "score" : score,
      "practice" : "quiz"
    };

    await Common().httpRequest(_appServer+"/practice/savePracticeResult", request);
  }
}
