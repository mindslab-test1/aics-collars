import 'dart:convert';

import 'package:collarsApp/providers/user.dart';
import 'package:collarsApp/screens/components/buttons/pratice_radio_button.dart';
import 'package:collarsApp/screens/layouts/lecture/daily_course.dart';
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/json_parser/practice/word_practice.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';

class WordPractice extends StatefulWidget {
  static const String id = 'word_practice';
  final int mediaId;
  WordPractice({this.mediaId});
  @override
  _WordPracticeState createState() => _WordPracticeState();
}

class _WordPracticeState extends State<WordPractice> {
  String _appServer = DotEnv().env['APP_SERVER'];
  List<List<String>> mulChoiceData = List();
  Color skipBtn = Colors.white;

  List selectedIndex = List();
  WordPracticeData wordPracticeData;
  bool isLoading;

  PageController pageController;

  //DB에 학습 data를 쌓기 위해 정보 저장
  List<String> userAnswer = List();
  List<String> question = List();
  List<int> correct = List();

  User user;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    user = Provider.of<User>(context, listen: false);
    getWordPractice();
    _initPageView(0);
    isLoading = true;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Color.fromRGBO(58, 192, 250, 1), Color.fromRGBO(70, 123, 253, 1),Color.fromRGBO(80, 67, 255, 1),]
            )
        ),
        child: isLoading?Common().spinkit(size, "로딩중입니다", Colors.white):SafeArea(
          child: PageView.builder(
            physics: NeverScrollableScrollPhysics(),
            controller: pageController,
            itemCount: wordPracticeData.words.length,
            itemBuilder: (context, index){
              List<Widget> multiChoice = [];

              for(int i=0;i<mulChoiceData[index].length;i++){
                multiChoice.add(Padding(
                  padding: EdgeInsets.all(5),
                  child: PracticeRadioBtn(
                    answerNum: index,
                    answer: mulChoiceData[index][i], index: i, selectedIndex: selectedIndex, changeIndex: changeIndex,),
                ));
              }

              return Container(
                color: Colors.transparent,
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: size.height*0.052,),
                      Center(child: Text("단어확습",
                        style: TextStyle(
                            fontSize: 12,
                            color: Colors.white,
                            fontFamily: 'NotoSansCJKkr',
                            fontWeight: FontWeight.w500
                        ),)
                      ),
                      SizedBox(height: size.height*0.07,),
                      Text("다음 단어의 뜻을 선택해주세요.",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontFamily: 'NotoSansCJKkr',
                            fontWeight: FontWeight.w200
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10, bottom: 30),
                        child: Container(
                            width: size.width*0.867,
                            child: Center(
                                child: Text(wordPracticeData.words[index].word,
                                  style: TextStyle(
                                      fontSize:35,
                                      color: Colors.white,
                                      fontFamily: 'Raleway',
                                      fontWeight: FontWeight.w500
                                  ),
                                )
                            )
                        ),
                      ),
                      Column(
                        children: multiChoice,
                      ),
                      SizedBox(height: 30,),
                      Container(
                        color: Colors.transparent,
                        height: size.height*0.072,
                        width: size.width*0.347,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25)
                          ),
                          color: Colors.white,
                          onPressed: (){
                            userAnswer.add(mulChoiceData[index][selectedIndex[index]]);
                            question.add(wordPracticeData.words[index].word);

                            if(mulChoiceData[index][selectedIndex[index]] == wordPracticeData.words[index].answer) {
                              correct.add(1);
                              Common().showPopup(
                                  "A",
                                  "Answer",
                                  wordPracticeData.words[index].answer,
                                  "You choose",
                                  mulChoiceData[index][selectedIndex[index]],
                                  context, () {}, () {
                                //Navigator.pop(context);
                                if (pageController.page.toInt() == wordPracticeData.words.length - 1) {
                                  savePracticeResult();
                                }
                                else {
                                  pageController.animateToPage(
                                      (pageController.page + 1).toInt(),
                                      duration: Duration(microseconds: 200000),
                                      curve: Curves.bounceInOut);
                                }},
                                false
                              );
                            }
                            else {
                              correct.add(0);
                              Common().showPopup(
                                  "D",
                                  "Answer",
                                  wordPracticeData.words[index].answer,
                                  "You choose",
                                  mulChoiceData[index][selectedIndex[index]],
                                  context, () {}, () {
                                    //Navigator.pop(context);
                                    if (pageController.page.toInt() == wordPracticeData.words.length - 1) {
                                      savePracticeResult();
                                    }
                                    else {
                                      pageController.animateToPage(
                                      (pageController.page + 1).toInt(),
                                      duration: Duration(microseconds: 200000),
                                      curve: Curves.bounceInOut);
                                    }},
                                  false
                              );
                            }
                          },
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                            child: Center(
                              child: Text("OK",
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xff5244fe),
                                    fontFamily: 'NotoSansCJKkr',
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(30),
                        child: GestureDetector(
                          onTapDown: (TapDownDetails tap){
                            skipTest(index);
                          },
                          child: Text("잘 모르겠어요",
                            style: TextStyle(
                                color: skipBtn,
                                fontSize: 15,
                                decoration: TextDecoration.underline
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        )
      ),
    );
  }



  void changeIndex(int answerNum, int index){
    setState(() {
      selectedIndex[answerNum] = index;
    });
  }

  _initPageView(int page){
    pageController = PageController(
      initialPage: page
    );
  }

  Future<void> savePracticeResult() async{
    setState(() {
      isLoading = true;
    });

    Map<String, dynamic> request = {
      "uid" : user.uid,
      "mediaId" : widget.mediaId,
      "question" : question,
      "userAnswer" : userAnswer,
      "correct" : correct,
      "practice" : "word"
    };
    
    await Common().httpRequest(_appServer+"/practice/savePracticeResult", request);

    Navigator.pop(context);
  }

  Future<void> getWordPractice() async{
    Map<String, dynamic> request = {"mediaId" : widget.mediaId};
    List<dynamic> requestList = await Common().httpRequest(_appServer+"/practice/getWordData", request);
    WordPracticeData wordPracticeRes = WordPracticeData.fromJson(requestList);

    setState(() {
      wordPracticeData = wordPracticeRes;
      wordPracticeRes.words.forEach((word) {
        selectedIndex.add(-1);

        List<String> choice = List();
        choice.add(word.mulChoice1);
        choice.add(word.mulChoice2);
        choice.add(word.mulChoice3);
        choice.add(word.mulChoice4);
        mulChoiceData.add(choice);
      });

      isLoading = false;
    });
  }

  skipTest(int index){

    setState(() {
      skipBtn = Colors.purpleAccent;
    });

    userAnswer.add('');
    question.add(wordPracticeData.words[index].word);
    correct.add(0);

    Common().showSkipPopup(context,
        (){
          setState(() {
            skipBtn = Colors.white;
          });
        },
        (){
          if (pageController.page.toInt() == wordPracticeData.words.length - 1) {
            savePracticeResult();
          }
          else{
            setState(() {
              skipBtn = Colors.white;
            });
            pageController.animateToPage(
                (pageController.page + 1).toInt(),
                duration: Duration(microseconds: 200000),
                curve: Curves.bounceInOut);
          }
        },
        false
    );
  }
}
