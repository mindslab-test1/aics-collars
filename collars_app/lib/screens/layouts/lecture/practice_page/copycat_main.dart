import 'dart:async';
import 'dart:collection';
import 'dart:convert';

import 'package:collarsApp/providers/user.dart';
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/json_parser/practice/pron_practice.dart';
import 'package:collarsApp/services/json_parser/pron.dart';
import 'package:collarsApp/services/json_parser/speehcopycat.dart';
import 'package:collarsApp/services/mic_streaming.dart';
import 'package:collarsApp/services/timer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:collarsApp/screens/layouts/lecture/practice_page/copycat_layout.dart';

class CopycatScreen extends StatefulWidget {
  static const String id = 'copycat_screen';
  final int mediaId;
  CopycatScreen({this.mediaId});

  static String _webSockServer = DotEnv().env['WEB_SOCK_SERVER'];
  static Uri uri  = Uri.parse(_webSockServer+"/engedu/websocket/copycat");
  //WebSocketChannel channel = WebSocketChannel.connect(uri);

  @override
  _CopycatScreenState createState() => _CopycatScreenState();
}

class _CopycatScreenState extends State<CopycatScreen> with SingleTickerProviderStateMixin{

  Color skipBtn = Colors.white;
  bool isLoading = false;
  //따라말하기 문장들
  PronPracticeData pronPracticeData;
  //List<SpeechCopycat> speechConcatList = List<SpeechCopycat>();
  //따라말하기 문장들 matching 검사용( 0 = not matching, 1 = matching)
  List<LinkedHashMap> speechMatching = List<LinkedHashMap<int, String>>();

  AnimationController progressController;
  Animation<double> animation;

  //websocket
  MicStream micStream;
  StreamSubscription<List<int>> listener;
  bool isRecording = false;
  bool isWebScokConn = false;
  WebSocketChannel channel;

  String _appServer = DotEnv().env['APP_SERVER'];

  //pageControl
  PageController pageController;
  List pages = List();

  //DB에 학습 data를 쌓기 위해 정보 저장
  String question;
  String scoreResult;
  String userAnswer;
  List<String> userAnswers = List();
  List<String> questions = List();
  List<String> scores = List();

  Icon getIcon() => (isRecording) ? Icon(Icons.stop, size: 60,) : Icon(Icons.keyboard_voice, size: 60,);


  User user;

  @override
  void dispose() {
    stopTest();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    user = Provider.of<User>(context, listen: false);
    _initPageView(0);

    //따라말하기 data 가져온 후 init
    getCopycatData();
    _initPageView(0);

    //websocket
    micStream = MicStream();
    isLoading = true;


    //animation
    progressController = AnimationController(vsync: this,duration: Duration(seconds: 20));
    animation = Tween<double>(begin: 0,end: 100).animate(progressController)..addListener((){
      if(animation.value.toInt() == 100 && isRecording) _stopWebSocket();
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Color.fromRGBO(255, 87, 159, 0.78), Color.fromRGBO(168, 87, 213, 0.98),Color.fromRGBO(161, 87, 217, 1), Color.fromRGBO(80, 67, 255, 1)]
            )
        ),
        child: isLoading?Common().spinkit(size, "로딩중입니다.", Colors.white):SafeArea(
            child: PageView.builder(
              //physics: NeverScrollableScrollPhysics(),
              controller: pageController,
              itemCount: pronPracticeData.prons.length,
              itemBuilder: (context, index){
                return CopycatMain(skipBtn, size, index, speechMatching[index], getIcon, changeListening, skipTest, animation);
              },
              onPageChanged: (int value){
                if(isRecording) {
                  _stopWebSocket();
                }
              },
            )
        ),
      ),
    );
  }

  _initPageView(int page) {
    pageController = PageController(
      initialPage: page
    );
  }

  /*
  * byte로 변환된 데이터를 websocket을 보냄
  * websocket으로 부터 data가 들어올 시 websocket conn 종료 후 popup
  * */
  bool _sendWebsocket(int index){

    if(isRecording) return false;

    setState(() {
      channel = WebSocketChannel.connect(CopycatScreen.uri);
    });

    //animation
    progressController.forward();

    listener = micStream.startListening().listen((samples) => {
      channel.sink.add(samples)
    });

    channel.stream.listen((event) {
      channel.sink.close();
      setState(() { isWebScokConn = false; isLoading = false;});

      PronResponse pronResponse = PronResponse.fromJson(jsonDecode(event));
      _SetSpeechMatching(pronResponse.result.resultText, index);

      String score = "";
      if(pronResponse.result.totalScore*2<=100 && pronResponse.result.totalScore*2>=70) score = "A";
      else if(pronResponse.result.totalScore*2<70 && pronResponse.result.totalScore*2>=30) score = "B";
      else score = "D";

      userAnswer = pronResponse.result.resultText;
      scoreResult = score;
      question = pronPracticeData.prons[index].sentence;

      Common().showPopup(score, "Answer",pronPracticeData.prons[index].sentence, "You said", pronResponse.result.resultText,
          context, reAssamblePage, goNextPractice, false);
    });

    setState(() { isRecording = true; });
  }

  /*
  * 1byte를 보낼 경우 grpc를 끊어 줌
  * */
  Future<void> _stopWebSocket(){
    List<int> end = List<int>(1);
    end[0] = 1;

    progressController.stop();
    micStream.stopMicpListening();
    listener.cancel();

    channel.sink.add(end);

    setState(() { isRecording = false;});

    return null;
  }

  void changeListening(int index) {
    !isRecording ? _sendWebsocket(index) : _stopWebSocket();
  }

  void stopTest(){
    if(progressController.status == AnimationStatus.forward || progressController.status == AnimationStatus.reverse){
      progressController.notifyStatusListeners(AnimationStatus.dismissed);
    }

    progressController.dispose();

    if(isRecording){
      listener.cancel();
      micStream.endMicStream();
    }

    if(isWebScokConn){
      channel.sink.close();
    }
  }

  /*
  * 발틈평가로 부터 결과가 온 경우
  * 각각 매칭을 확인함
  * */
  _SetSpeechMatching(String responseText, int index){
    List<String> responseTexts = responseText.trim().replaceAll("\.", "").split(" ");
    int i =0;
    int responseLen = responseTexts.length;
    bool stop = false;

    speechMatching[index].forEach((key, value) {
      if(i<responseLen) {
        if (responseTexts[i] != null && responseTexts[i] != "" && !stop) {
          if (value.toString().split("#")[0] == responseTexts[i]) {
            i++;
            speechMatching[index].update(
                key, (value) => value.toString().split("#")[0] + "#1");
          }
          else {
            stop = true;
            speechMatching[index].update(
                key, (value) => value.toString().split("#")[0] + "#0");
          }
        } else {
          speechMatching[index].update(
              key, (value) => value.toString().split("#")[0] + "#0");
        }
      }
    });
  }

  Widget scroeResult(PronResponse pronResponse){
    return Column(
      children: <Widget>[
        Text("resultText : ${pronResponse.result.resultText}"),
        Text("TotalScore : ${pronResponse.result.totalScore}")
      ],
    );
  }

  /*
   * 따라말하기 문장 가져온 후
   * speechConcatList에 문장들을 담고
   * speechMatching에 matching 상태 담음
   * */
  Future<void> getCopycatData() async{
    Map<String, dynamic> request = {"mediaId" : widget.mediaId};
    print(await Common().httpRequest(_appServer+"/practice/getCopycatData", request));
    PronPracticeData pronPracticeDataRes = PronPracticeData.fromJson(await Common().httpRequest(_appServer+"/practice/getCopycatData", request));

    setState(() {
      //for(Map i in data){speechConcatList.add(SpeechCopycat.fromJson(i));}
      pronPracticeData = pronPracticeDataRes;
      pronPracticeDataRes.prons.forEach((pron) {
        List<String> speechs = pron.sentence.trim().split(" ");
        LinkedHashMap<int, String> initMatching = LinkedHashMap<int, String>();

        for(int i=0; i<speechs.length;i++){initMatching.addAll({i:speechs[i]+"#0"});}

        speechMatching.add(initMatching);
      });
      isLoading = false;
    });
  }

  /*
    * goNextTest, reAssamblePage
    * popup에 넘겨줄 함수
    * */
  void goNextPractice(){
    questions.add(question);
    userAnswers.add(userAnswer);
    scores.add(scoreResult);

    progressController.reset();
    micStream.stopMicpListening();
    //Navigator.pop(context);

    if(pageController.page.toInt() == pronPracticeData.prons.length-1){
      savePracticeResult();
    }
    else {
      pageController.nextPage(duration: Duration(microseconds: 200000), curve: Curves.easeIn);
    }
    setState(() {
      skipBtn = Colors.white;
      isRecording = false;
    });
  }

  Future<void> savePracticeResult() async{
    setState(() {
      isLoading = true;
    });
    Map<String, dynamic> request = {
      "uid" : user.uid,
      "mediaId" : widget.mediaId,
      "question" : questions,
      "userAnswer" : userAnswers,
      "score" : scores,
      "practice" : "pron"
    };

    await Common().httpRequest(_appServer+"/practice/savePracticeResult", request);

    Navigator.pop(context, true);
  }


  void reAssamblePage(){
    //channel = WebSocketChannel.connect(CopycatScreen.uri);
    progressController.reset();
    micStream.stopMicpListening();

    setState(() {
      skipBtn = Colors.white;
      isRecording = false;
    });
  }

  void skipTest(){
    setState(() {
      skipBtn = Colors.purpleAccent;
    });

    if(isRecording == true){
      _stopWebSocket();
    }
    else{
      Common().showSkipPopup(context, reAssamblePage, (){
        questions.add(pageController.page.toString());
        userAnswers.add('');
        scores.add('D');

        setState(() {
          isRecording = false;
          skipBtn = Colors.white;
        });

        if(pageController.page.toInt() == pronPracticeData.prons.length-1){
          savePracticeResult();
        }
        else {
          pageController.nextPage(duration: Duration(microseconds: 200000), curve: Curves.easeIn);
        }},
        false
      );
    }
  }

}
