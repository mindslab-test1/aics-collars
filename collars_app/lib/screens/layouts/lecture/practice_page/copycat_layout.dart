import 'dart:collection';

import 'package:collarsApp/services/json_parser/speehcopycat.dart';
import 'package:collarsApp/services/timer.dart';
import 'package:flutter/material.dart';

Widget CopycatMain(Color skipBtn, Size size, int index, LinkedHashMap speechMatching,
    Function getIcon, Function changeListening, Function skipTest,Animation<double> animation) => Container(
    width: size.width,
  decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Color.fromRGBO(255, 87, 159, 0.78), Color.fromRGBO(168, 87, 213, 0.98),Color.fromRGBO(161, 87, 217, 1), Color.fromRGBO(80, 67, 255, 1)]
        )
    ),
  child: Padding(
    padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: size.height*0.05,
        ),
        Center(child: Text("발음학습",
          style: TextStyle(
              fontSize: 12,
              color: Colors.white,
              fontFamily: 'NotoSansCJKkr',
              fontWeight: FontWeight.w500
          ),)
        ),
        SizedBox(height: size.height*0.10,),
        Text("아래 지문을 읽어주세요.",
          style: TextStyle(
              fontWeight: FontWeight.w500,
              color: Colors.white,
              fontSize: 20,
              fontFamily: 'NotoSansCJKkr',
          ),
        ),
        SizedBox(height: size.height*0.039,),
        SpeekBox(size, speechMatching),
        SizedBox(height: size.height*0.079,),
        //SubTitle(speechCopycat.speech, speechMatching),
        InkWell(
          child: Container(
            color: Colors.transparent,
            height: 120,
            width: 120,
            child: CustomPaint(
              foregroundPainter: CircleProgress(animation.value, 10, Color.fromRGBO(118, 118, 230, 1)),
              child: Padding(
                padding: EdgeInsets.all(15),
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                        colors: [Color.fromRGBO( 129, 129, 251, 1), Color.fromRGBO(72, 92, 241, 1), Color.fromRGBO(54, 70, 192, 10)]
                    )
                  ),
                  child: Icon(Icons.mic, size: 50, color: Colors.white,),
                ),
              ),
            ),
          ),
          onTap: (){changeListening(index);},
        ),
       SizedBox(height: size.height*0.105,),
       GestureDetector(
         onTapDown: (TapDownDetails tapDwon) {
           skipTest();
         },
         child: Text("잘 모르겠어요",
           style: TextStyle(
               color: skipBtn,
               fontSize: 15,
               decoration: TextDecoration.underline
           ),
         ),
       )
       // MicButton(animation, changeListening, index)
      ],
    ),
  ),
);

Widget SpeekBox(Size size, LinkedHashMap speechMatching) =>
    Container(
      width: size.width*0.867,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Color(0x29000000),
                blurRadius: 6.0,
                offset: Offset(
                  0, 4.0
                )
            )
          ]
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Center(
            /*child: Text(
              speech,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),*/
            child: Padding(
              padding: EdgeInsets.fromLTRB(24, 24, 24, 24),
              child: SubTitle(speechMatching),
            ),
          ),
        ],
      ),);


Widget SubTitle(LinkedHashMap speechMatching){

  List<Widget> splitSpeech = List<Widget>();

  speechMatching.forEach((key, value) {
    String data = value.toString().split("#")[0];
    String matching = value.toString().split("#")[1];
    splitSpeech.add(
        matching=="0"?Text(data,
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Color(0xff444444), fontFamily: 'NotoSansCJKkr'),
        ):
        Text(data, style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Colors.pink, fontFamily: 'NotoSansCJKkr'),
        )
    );
  });

  /*return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: splitSpeech,
  );*/
  return Wrap(
    direction: Axis.horizontal,
    alignment: WrapAlignment.center,
    spacing: 4.0,
    children: splitSpeech,
  );
}

Widget MicButton(Animation<double> animation, Function changeListening, int index) => Container(
  color: Colors.transparent,
  height: 120,
  width: 120,
  child: new CustomPaint(
    //foregroundPainter: CircleProgress(animation.value),
    child: new Padding(
      padding: EdgeInsets.all(15),
      child: Container(
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.3),
                  blurRadius: 7.0,
                  offset: Offset(
                      5,5
                  )
              )
            ],
            shape: BoxShape.circle,
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Color.fromRGBO( 129, 129, 251, 1), Color.fromRGBO(72, 92, 241, 1), Color.fromRGBO(54, 70, 192, 10)]
            )
        ),
        child: IconButton(
          icon: Icon(Icons.mic, size: 50, color: Colors.white,),
          //onPressed: changeListening(index),
        ),
      ),
    ),
  ),
);


