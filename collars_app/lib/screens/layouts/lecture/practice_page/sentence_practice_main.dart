import 'package:collarsApp/providers/user.dart';
import 'package:collarsApp/screens/components/buttons/pratice_radio_button.dart';
import 'package:collarsApp/screens/components/buttons/skipTest.dart';
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/json_parser/practice/sentence_pratice.dart';
import 'package:collarsApp/services/json_parser/practice/word_practice.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';

class SentencePractice extends StatefulWidget {
  static const String id = 'sentence_practice';
  final int mediaId;
  SentencePractice({this.mediaId});

  @override
  _SentencePracticeState createState() => _SentencePracticeState();
}

class _SentencePracticeState extends State<SentencePractice> {

  String _appServer = DotEnv().env['APP_SERVER'];
  List<List<String>> mulChoiceData = List();

  bool isLoading;
  Color skipBtn = Colors.white;

  List selectedIndex = List();
  SentencePracticeData sentencePracticeData;
  PageController pageController;

  //DB에 학습 data를 쌓기 위해 정보 저장
  List<String> userAnswer = List();
  List<String> question = List();
  List<int> correct = List();

  User user;

  @override
  void initState() {
    super.initState();
    user = Provider.of<User>(context, listen: false);
    getWordPractice();
    _initPageView(0);
    isLoading = true;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Color.fromRGBO(58, 192, 250, 1), Color.fromRGBO(70, 123, 253, 1),Color.fromRGBO(80, 67, 255, 1),]
              )
          ),
          child: isLoading?Common().spinkit(size, "로딩중입니다", Colors.white):SafeArea(
            child: PageView.builder(
              physics: NeverScrollableScrollPhysics(),
              controller: pageController,
              itemCount: sentencePracticeData.sentences.length,
              itemBuilder: (context, index){
                //List wrongAnswers = wrongAnswer[index];
                List<Widget> multiChoice = [];

                for(int i=0;i<mulChoiceData[index].length;i++){
                  multiChoice.add(Padding(
                    padding: EdgeInsets.all(5),
                    //child: customRadio(index, mulChoiceData[index][i], i, size),
                    child: PracticeRadioBtn(answerNum: index, answer: mulChoiceData[index][i], index: i, selectedIndex: selectedIndex, changeIndex: changeIndex,),
                  ));
                }

                return Container(
                  color: Colors.transparent,
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: size.height*0.052,),
                        Center(
                            child: Text("문장학습",
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.white,
                                fontFamily: 'NotoSansCJKkr',
                                fontWeight: FontWeight.w500
                              ),
                            )
                        ),
                        SizedBox(height: size.height*0.07,),
                        Padding(
                            padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                            child: Text(
                              "아래 문장에 적합한 변역을 선택해주세요.\n비즈니스 영어에 적절한 매너와 톤을 고려해주세요.",
                              style: TextStyle(
                                  color: Colors.white.withOpacity(0.7),
                                  fontSize: 15,
                                  fontFamily: 'NotoSansCJKkr',
                                  fontWeight: FontWeight.w300
                              ),
                              textAlign: TextAlign.center,
                            )
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10, bottom: 40, left: 30, right: 30),
                          child: Text(sentencePracticeData.sentences[index].question,
                            style: TextStyle(fontSize:20, color: Colors.white, fontFamily: 'NotoSansCJKkr', fontWeight: FontWeight.w500),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Column(
                          children: multiChoice,
                        ),
                        SizedBox(height: 30,),
                        Container(
                          color: Colors.transparent,
                          height: size.height*0.069,
                          width: size.width*0.347,
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(25)
                            ),
                            color: Colors.white,
                            onPressed: (){
                              userAnswer.add(mulChoiceData[index][selectedIndex[index]]);
                              question.add(sentencePracticeData.sentences[index].question);

                              if(mulChoiceData[index][selectedIndex[index]] == sentencePracticeData.sentences[index].answer) {
                                correct.add(1);
                                Common().showPopup(
                                    "A",
                                    "Answer",
                                    sentencePracticeData.sentences[index]
                                        .answer,
                                    "You choose",
                                    mulChoiceData[index][selectedIndex[index]],
                                    context, () {}, () {
                                      //Navigator.pop(context);
                                      if(pageController.page.toInt() == sentencePracticeData.sentences.length-1){
                                        savePracticeResult();
                                      }else{
                                        pageController.animateToPage(
                                            (pageController.page + 1).toInt(),
                                            duration: Duration(microseconds: 200000),
                                            curve: Curves.bounceInOut);
                                      }},
                                    false
                                );
                              }
                              else {
                                correct.add(0);
                                Common().showPopup(
                                    "D",
                                    "Answer",
                                    sentencePracticeData.sentences[index]
                                        .answer,
                                    "You choose",
                                    mulChoiceData[index][selectedIndex[index]],
                                    context, () {}, () {
                                      //Navigator.pop(context);
                                      if(pageController.page.toInt() == sentencePracticeData.sentences.length-1){
                                        savePracticeResult();
                                      }else{
                                        pageController.animateToPage(
                                            (pageController.page + 1).toInt(),
                                            duration: Duration(microseconds: 200000),
                                            curve: Curves.bounceInOut);
                                      }},
                                    false
                                );
                              }
                            },
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                              child: Center(
                                child: Text("OK",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w700,
                                      color: Color.fromRGBO(82, 68, 254, 1),
                                      fontFamily: 'NotoSansCJKkr'
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(30),
                          child: GestureDetector(
                            onTapDown: (TapDownDetails tap){
                              skipTest(index);
                            },
                            child: Text("잘 모르겠어요",
                              style: TextStyle(
                                  color: skipBtn,
                                  fontSize: 15,
                                  decoration: TextDecoration.underline
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              },
            ),
          )
      ),
    );
  }

  skipTest(int index){

    setState(() {
      skipBtn = Colors.purpleAccent;
    });

    userAnswer.add('');
    question.add(sentencePracticeData.sentences[index].question);
    correct.add(0);

    Common().showSkipPopup(context,
        (){
          setState(() {
            skipBtn = Colors.white;
          });
        },
        (){
          if(pageController.page.toInt() == sentencePracticeData.sentences.length-1){
            savePracticeResult();
          }else{
            setState(() {
              skipBtn = Colors.white;
            });
            pageController.animateToPage(
                (pageController.page + 1).toInt(),
                duration: Duration(microseconds: 200000),
                curve: Curves.bounceInOut);
          }
        }
        , false
    );
  }

  void changeIndex(int answerNum, int index){
    setState(() {
      selectedIndex[answerNum] = index;
    });
  }

  _initPageView(int page){
    pageController = PageController(
        initialPage: page
    );
  }

  Future<void> savePracticeResult() async{
    setState(() {
      isLoading = true;
    });
    Map<String, dynamic> request = {
      "uid" : user.uid,
      "mediaId" : widget.mediaId,
      "question" : question,
      "userAnswer" : userAnswer,
      "correct" : correct,
      "practice" : "sentence"
    };

    await Common().httpRequest(_appServer+"/practice/savePracticeResult", request);
    Navigator.pop(context);
  }


  Future<void> getWordPractice() async{
    Map<String, dynamic> request = {"mediaId" : widget.mediaId};
    List<dynamic> requestList = await Common().httpRequest(_appServer+"/practice/getSentenceData", request);
    SentencePracticeData sentencePracticeDataRes = SentencePracticeData.fromJson(requestList);

    setState(() {
      sentencePracticeData = sentencePracticeDataRes;
      sentencePracticeDataRes.sentences.forEach((sentence) {
        selectedIndex.add(-1);

        List<String> choice = List();
        choice.add(sentence.mulChoice1);
        choice.add(sentence.mulChoice2);
        choice.add(sentence.mulChoice3);
        choice.add(sentence.mulChoice4);
        mulChoiceData.add(choice);
      });

      isLoading = false;
    });
  }
}
