
import 'package:chewie/chewie.dart';
import 'package:collarsApp/main.dart';
import 'package:collarsApp/providers/user.dart';
import 'package:collarsApp/screens/components/cards/media_text_card.dart';
import 'package:collarsApp/screens/layouts/lecture/video_lecture/custom_controller.dart';
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/json_parser/lecture/course_media.dart';
import 'package:collarsApp/services/json_parser/lecture_media.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';


class Lecture extends StatefulWidget {
  static const String id = 'Lecture';
  int mediaId;

  bool isfinishLecture;
  Lecture({this.mediaId, this.isfinishLecture});
  @override
  _LectureState createState() => _LectureState();
}

class _LectureState extends State<Lecture> {
  String _appServer = DotEnv().env['APP_SERVER'];
  VideoPlayerController _videoPlayerController;
  ChewieController _chewieController;
  //LectureMedia lectureMedia;
  bool isLoading;
  List<Widget> mediaTextList = List();

  User user;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    user = Provider.of<User>(context, listen: false);
    isLoading = true;
    getLectureSetting();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp
    ]);

    _videoPlayerController.dispose();
    _chewieController.dispose();
  }


  Future<void> getLectureSetting() async{
    Map<String, dynamic> request = {"mediaId": widget.mediaId};
    CourseMedia courseMediaRes = CourseMedia.fromJson(await Common().httpRequest(_appServer+"/lecture/getLectureData", request));

    setState(() {
      _videoPlayerController = VideoPlayerController.network(
          courseMediaRes.mediaUrl
      );
      _chewieController = ChewieController(
        videoPlayerController: _videoPlayerController,
        aspectRatio: 5 / 3,
        autoInitialize: true,
        looping: false,
        customControls: CustomController(mediaId: widget.mediaId, isLectureFinish: widget.isfinishLecture, uid: user.uid,),
      );

      mediaTextList.add(Padding(
        padding: EdgeInsets.only(top: 30),
        child: Text(
          courseMediaRes.title,
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold
            )
        ),
      ));
      
      courseMediaRes.mediaTextVO.forEach((element) {
        mediaTextList.add(SizedBox(height: 20,));
        mediaTextList.add(MediaText(subject: element.subject, content: element.content,));
      });

      isLoading=false;
    });
  }

  void disableRotate(){
    setState(() {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp
      ]);

    });
  }

  void enableRotate(){


    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    var isPortrait = MediaQuery.of(context).orientation;

    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.black,
        ),
        title: Text("강의듣기", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        bottomOpacity: 0.0,
        elevation: 0.0,
      ),
      body: isLoading?Container():Container(
        child: Column(
          children: <Widget>[
            Container(
              height: size.height*0.3,
              child: Center(
                child: Chewie(
                  controller: _chewieController,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 24, right: 24),
              child: Container(
                height: (isPortrait==Orientation.portrait)?size.height*0.57:size.height*0.47,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: mediaTextList,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

}
