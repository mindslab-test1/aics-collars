import 'dart:convert';

import 'package:collarsApp/providers/study_status.dart';
import 'package:collarsApp/providers/user.dart';
import 'package:collarsApp/screens/components/cards/course.dart';
import 'package:collarsApp/screens/layouts/lecture/practice_page/quiz_main.dart';
import 'package:collarsApp/screens/layouts/lecture/video_lecture/video_lecture.dart';
import 'package:collarsApp/screens/layouts/lecture/practice_page/copycat_main.dart';
import 'package:collarsApp/screens/layouts/lecture/practice_page/sentence_practice_main.dart';
import 'package:collarsApp/screens/layouts/lecture/practice_page/word_practice_main.dart';
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/json_parser/practice/practice_result_data.dart';
import 'package:collarsApp/services/json_parser/practice/pron_practice.dart';
import 'package:collarsApp/services/json_parser/practice/quiz_practice.dart';
import 'package:collarsApp/services/json_parser/practice/quiz_practice_data.dart';
import 'package:collarsApp/services/json_parser/practice/sentence_pratice.dart';
import 'package:collarsApp/services/json_parser/practice/word_practice.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';

import '../../../services/json_parser/practice/pron_practice.dart';
import '../../../services/json_parser/practice/sentence_pratice.dart';
import '../../../services/json_parser/practice/word_practice.dart';

class DailyCourse extends StatefulWidget {
  static const String id = 'daily_course';

  String date;
  DailyCourse({this.date});
  @override
  _DailyCourseState createState() => _DailyCourseState();
}

class _DailyCourseState extends State<DailyCourse> {
  bool isLoading;
  String _appServer = DotEnv().env['APP_SERVER'];
  String lectureName;
  Map practiceCheck = Map();
  //practice data
  WordPracticeData wordPracticeData = WordPracticeData();
  SentencePracticeData sentencePracticeData = SentencePracticeData();
  PronPracticeData pronPracticeData = PronPracticeData();
  QuizPracticeData quizPracticeData = QuizPracticeData();

  User user;
  StudyStatus studyStatus;

  @override
  void initState() {
    super.initState();
    user = Provider.of<User>(context, listen: false);
    studyStatus = Provider.of<StudyStatus>(context, listen: false);
    isLoading = true;

    GetPracticeData();
  }

  Future<void> refleshPage() async{
    setState(() {
      isLoading = true;
      wordPracticeData = WordPracticeData();
      sentencePracticeData = SentencePracticeData();
      pronPracticeData = PronPracticeData();
      quizPracticeData = QuizPracticeData();
      practiceCheck = Map();
    });

    await GetPracticeData();

    setState(() {
      isLoading = false;
    });
  }
  
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar:  PreferredSize(
        preferredSize: Size.fromHeight(90),
        child: AppBar(
          bottomOpacity: 0,
          elevation: 0,
          backgroundColor: Colors.white,
          flexibleSpace: Padding(
            padding: EdgeInsets.only(top: 33, left: 33, right: 33),
            child: SizedBox.expand(
              child: Stack(
                children: <Widget>[
                  Align(
                      alignment: Alignment.centerLeft,
                      child: Icon(Icons.arrow_back_ios, color: Colors.black,)
                  ),
                  Center(
                    child: Text(
                      studyStatus.currentLectureName,
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
      body: isLoading?Common().spinkit(size, "로딩중입니다.", Colors.grey):Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(246, 247, 251, 1),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40),
            topRight: Radius.circular(40)
          )
        ),
        height: size.height,
        child: Padding(
          padding: EdgeInsets.fromLTRB(size.width*0.061, size.height*0.009, 0, 0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: size.height*0.040,),
                Padding(
                  padding: EdgeInsets.only(right: size.width*0.059),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Daily Collars",
                        style: TextStyle(
                            fontSize: 20,
                            fontFamily: 'Raleway-Medium',
                            color: Color(0xFF333333)
                        ),
                      ),
                      Text(widget.date, style: TextStyle(color: Color.fromRGBO(51, 51, 51, 0.7)))
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 3, 0, size.height*0.026),
                    child: Text("${user.name}님 오늘 학습할 내용입니다.",
                        style: TextStyle(
                            fontSize: 10 ,
                            color: Color.fromRGBO(51, 51, 51, 0.6),
                            fontFamily: 'Raleway-Regular'
                        )
                    )
                ),
                Course(
                  practiceName: lectureName,
                  time: "01",
                  subTitle: "강의듣기",
                  title: "강의듣기",
                  finish: practiceCheck['lectureCheck'],
                  data: null,
                  func: (){
                    Navigator.push(context, CupertinoPageRoute(builder: (context) => Lecture(mediaId: studyStatus.currentMediaId, isfinishLecture: practiceCheck['lectureCheck'],))).then((value) => refleshPage());
                  },
                ),
                Course(
                  practiceName: lectureName,
                  time: "02",
                  subTitle: "발음학습",
                  title: "발음학습",
                  finish: practiceCheck['pronCheck'],
                  data: pronPracticeData.prons,
                  func: (){
                    Navigator.push(context, CupertinoPageRoute(builder: (context) => CopycatScreen(mediaId: studyStatus.currentMediaId,))).then((value) => refleshPage());
                  },
                ),
                Course(
                  practiceName: lectureName,
                  time: "03",
                  subTitle: "단어학습",
                  title: "단어학습",
                  finish: practiceCheck['wordCheck'],
                  data: wordPracticeData.words,
                  func: (){
                    Navigator.push(context, CupertinoPageRoute(builder: (context) => WordPractice(mediaId: studyStatus.currentMediaId,))).then((value) => refleshPage());
                  },
                ),
                Course(
                  practiceName: lectureName,
                  time: "04",
                  subTitle: "문장학습",
                  title: "문장학습",
                  finish: practiceCheck['sentenceCheck'],
                  data: sentencePracticeData.sentences,
                  func: (){
                    Navigator.push(context, CupertinoPageRoute(builder: (context) => SentencePractice(mediaId: studyStatus.currentMediaId,))).then((value) => refleshPage());
                  },
                ),
                Course(
                  practiceName: lectureName,
                  time: "05",
                  subTitle: "Quiz",
                  title: "Quiz",
                  finish: practiceCheck['quizCheck'],
                  data: quizPracticeData.quizs,
                  func: (){
                    Navigator.push(context, CupertinoPageRoute(builder: (context) => QuizPracticeMain(mediaId: studyStatus.currentMediaId,))).then((value) => refleshPage());
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  /*
  * 완료된 practice 화면에 나올 data
  * */
  Future<void> GetPracticeData() async{
    Map<String, dynamic> request = {
      "uid" : user.uid,
      "mediaId" : studyStatus.currentMediaId,
      "lectureNum" : studyStatus.currentLectureNum
    };

    Map responseMap = await Common().httpRequest(_appServer + "/practice/getAllPracticeData", request);

    lectureName = responseMap['lectureName'];

    setState(() {
      practiceCheck = responseMap['check'];
    });

    if(practiceCheck['pronCheck']==true) {
      PronPracticeData pronPracticeDataRes = PronPracticeData.fromJson(jsonDecode(responseMap['data']['pronData']));
      setState(() {
        pronPracticeData = pronPracticeDataRes;
      });
    }
    if(practiceCheck['wordCheck']==true) {
      WordPracticeData wordPracticeDataRes = WordPracticeData.fromJson(jsonDecode(responseMap['data']['wordData']));
      setState(() {
        wordPracticeData = wordPracticeDataRes;
      });
    }
    if(practiceCheck['sentenceCheck']==true) {
      SentencePracticeData sentencePracticeDataRes = SentencePracticeData.fromJson(jsonDecode(responseMap['data']['sentenceData']));
      setState(() {
        sentencePracticeData = sentencePracticeDataRes;
      });
    }

    if(practiceCheck['quizCheck'] == true){
      QuizPracticeData quizPracticeDataRes = QuizPracticeData.fromJson(json.decode(responseMap['data']['quizData']));
      setState(() {
        quizPracticeData = quizPracticeDataRes;
      });
    }
    if(practiceCheck['lectureCheck'] && practiceCheck['pronCheck'] && practiceCheck['wordCheck'] && practiceCheck['sentenceCheck'] && practiceCheck['quizCheck']
        && !studyStatus.isStudyCompleted[studyStatus.currentLectureNum]){

      List<bool> isCompleted = studyStatus.isStudyCompleted;
      isCompleted[studyStatus.currentLectureNum] = true;
      setState(() {
        studyStatus.setIsStudyCompleted(isCompleted);
      });

    }

    setState(() {
      isLoading = false;
    });
  }

}
