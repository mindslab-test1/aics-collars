import 'package:collarsApp/providers/study_status.dart';
import 'package:collarsApp/providers/user.dart';
import 'package:collarsApp/screens/layouts/lecture/daily_course.dart';
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/json_parser/lecture/course_media.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';

class FullCourse extends StatefulWidget {
  final String date;
  FullCourse({this.date});

  @override
  _FullCourseState createState() => _FullCourseState();
}

class _FullCourseState extends State<FullCourse> {
  bool isLoading = false;
  String _appServer = DotEnv().env['APP_SERVER'];

  User user;
  StudyStatus studyStatus;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    studyStatus = Provider.of<StudyStatus>(context, listen: false);
    user = Provider.of<User>(context, listen: false);

  }

  List<Widget> checkDailyCompleteRow(StudyStatus studyStatus){
    var isCompletedWidget = List<Widget>();
    
    for(int i=0;i<studyStatus.courseMediaVO.length;i++){
      Widget completeWidget = Container(
        decoration: BoxDecoration(
          color: studyStatus.isStudyCompleted[i]?Color.fromRGBO(82, 98, 242, 1):Colors.white,
          border: Border.all(
            width: 2,
            color: studyStatus.isStudyCompleted[i]?Colors.transparent:Color.fromRGBO(112, 112, 112, 0.3)
          ),
          borderRadius: BorderRadius.circular(10)
        ),
        width: 55,
        height: 55,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Day", style: TextStyle(color: studyStatus.isStudyCompleted[i]?Colors.white:Color.fromRGBO(36, 36, 36, 0.7), fontSize: 10, fontFamily: 'Raleway'), ),
              Text("0${i+1}", style: TextStyle(color: studyStatus.isStudyCompleted[i]?Colors.white:Colors.black, fontWeight: FontWeight.bold, fontSize: 19, ))
            ],
          ),
        ),
      );
      isCompletedWidget.add(completeWidget);
    }
    return isCompletedWidget;
  }

  List<Widget> dailyStudyColum(Size size, StudyStatus studyStatus){
    var dailyColumn = List<Widget>();
    Widget divider = Divider(color: Colors.black.withOpacity(0.3),);

    for(int i=0;i<studyStatus.courseMediaVO.length;i++){
      bool openCourse = false;
      if(i==0) openCourse = true;
      else if(i>0){
        if(studyStatus.isStudyCompleted[i-1]) openCourse = true;
        if(studyStatus.isStudyCompleted[i]) openCourse = true;
      }

      dailyColumn.add(divider);
      Widget dailyCourse = Container(
        child: Row(
         mainAxisAlignment: MainAxisAlignment.spaceBetween,
         crossAxisAlignment: CrossAxisAlignment.center,
         children: <Widget>[
           Text("Day 0${i+1}",
             style: TextStyle(
               fontFamily: 'Raleway',
               fontWeight: FontWeight.w500
             ),),
           Padding(
             padding: EdgeInsets.only(top: 10, bottom: 10),
             child: Container(
               width: size.width*0.62,
               height: size.height*0.083,
               color: Colors.transparent,
               child: RaisedButton(
                 elevation: 0,
                 shape: RoundedRectangleBorder(
                   borderRadius: BorderRadius.circular(15)
                 ),
                 color: Colors.white,
                 onPressed: openCourse?(){
                   studyStatus.setCurrentMediaId(studyStatus.courseMediaVO[i].mediaId);
                   studyStatus.setCurrentLectureNum(studyStatus.courseMediaVO[i].mediaOrder);
                   studyStatus.setCurrentLectureName(studyStatus.courseMediaVO[i].name);
                   Navigator.push(
                       context, MaterialPageRoute(builder: (context) =>
                       DailyCourse(date: widget.date,))).then((value) => print("after navigation pop ${studyStatus.isStudyCompleted}"));
                 }:null,
                 child: Consumer<StudyStatus>(
                   builder: (context, value, child) =>
                       Row(
                         crossAxisAlignment: CrossAxisAlignment.center,
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         children: <Widget>[
                           Column(
                             crossAxisAlignment: CrossAxisAlignment.start,
                             mainAxisAlignment: MainAxisAlignment.center,
                             children: <Widget>[
                               Text("학습${value.courseMediaVO[i].mediaOrder}", style: TextStyle(fontSize: 12, fontFamily: 'NotoSansCJKkr', color: Color.fromRGBO(36, 36, 36, 0.6)),),
                               Text(value.courseMediaVO[i].name, style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, fontFamily: 'NotoSansCJKkr', color: Color.fromRGBO(36, 36, 36, 0.9
                               )))
                             ],
                           ),
                           Container(
                             width: 24,
                             height: 24,
                             decoration: BoxDecoration(
                                 shape: BoxShape.circle,
                                 color: value.isStudyCompleted[i]?Color.fromRGBO(82, 98, 242, 1):Colors.black.withOpacity(0.1)
                             ),
                             child: Icon(
                               Icons.check,
                               color: value.isStudyCompleted[i]?Colors.white:Colors.white.withOpacity(0.1),
                             ),
                           )
                         ],
                       ),
                 ),
               ),
             ),
           )
         ],
        ),
      );

      dailyColumn.add(dailyCourse);
    }
    dailyColumn.add(divider);

    return dailyColumn;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(90),
        child: AppBar(
          bottomOpacity: 0,
          elevation: 0,
          backgroundColor: Colors.white,
          flexibleSpace: Padding(
            padding: EdgeInsets.only(top: 33, left: 33, right: 33),
            child: SizedBox.expand(
              child: Stack(
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                      child: Icon(Icons.arrow_back_ios, color: Colors.black,)
                  ),
                  Center(
                    child: Text(
                      studyStatus.lectureTitle,
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
      body: isLoading?Common().spinkit(size, "로딩중입니다.", Colors.grey):SizedBox.expand(
        child: Container(
          decoration: BoxDecoration(
            color: Color.fromRGBO(246, 247, 251, 1),
            borderRadius: BorderRadius.only(topLeft: Radius.circular(40), topRight: Radius.circular(40)),
          ),
          child: Padding(
            padding: EdgeInsets.only(left: size.width*0.077, right: size.width*0.077, top: size.width*0.077),
            child: Consumer<StudyStatus>(
              builder: (context, value, child) =>
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Daily Collars", style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, fontFamily: 'Raleway'),),
                                Text("최적화된 Daily 학습을 제공합니다.", style: TextStyle(color: Color.fromRGBO(51, 51, 51, 0.7), fontFamily: 'Raleway'),)
                              ],
                            ),
                          ),
                          Align(
                              heightFactor: 2,
                              alignment: Alignment.bottomRight,
                              child: Text(widget.date, style: TextStyle(color: Color.fromRGBO(51, 51, 51, 0.7), fontFamily: 'Raleway'))
                          )
                        ],
                      ),
                      SizedBox(height: 50,),

                      Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20)
                        ),
                        child: Padding(
                          padding: EdgeInsets.only(top: 8.1, bottom: 8.1),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: checkDailyCompleteRow(value),
                          ),
                        ),
                      ),

                      SizedBox(
                        height: size.height*0.049,
                      ),

                      Expanded(
                        child: Container(
                          child: SingleChildScrollView(
                            child: Column(
                              children: dailyStudyColum(size, value),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
            ),
          ),
        ),
      ),
    );
  }

}
