import 'package:collarsApp/screens/layouts/main/main_screen.dart';
import 'package:collarsApp/screens/layouts/survey_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

// Provider
import 'package:provider/provider.dart';
import 'package:collarsApp/providers/user.dart';

// Style
import 'package:collarsApp/styles/color/default.dart';
import 'package:collarsApp/styles/size/default.dart';

class SurveyIntroScreen extends StatelessWidget {
  static const String id = 'survey_intro_screen';

  @override
  Widget build(BuildContext context) {
    User _user = Provider.of<User>(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        title: SvgPicture.asset(
          'assets/images/logo/icon_01.svg',
          width: kAppBarLogoSize,
          height: kAppBarLogoSize,
          color: kMainWhite,
        ),
        leading: Navigator.canPop(context)
            ? GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: SvgPicture.asset(
                  'assets/images/icon/icon_back.svg',
                  color: kMainWhite,
                ),
              )
            : null,
        backgroundColor: Colors.transparent,
        bottomOpacity: 0.0,
        elevation: 0.0,
      ),
      body: Container(
        padding: kDefaultPaddingHorizontal,
        decoration: BoxDecoration(gradient: kPrimaryLightGradient),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: kDefaultPaddingAll,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25),
                  color: kMainSky,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            '강의를 추천드립니다.',
                            style: TextStyle(
                                fontFamily: 'NotoSansCJKkr',
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 16),
                          ),
                          SizedBox(height: 10),
                          Text(
                            '맞춤형 강의를 추천하기 위해 설문조사를 시작합니다.\n\n인공지능 기반의 정확한 추천을 위해 다음 설문조사를 시작합니다.\n\n1. 업종 및 직무정보\n2. 학습정보\n3. 직급정보\n4. 학습시간',
                            style: TextStyle(
                              fontFamily: 'NotoSansCJKkr',
                              color: Colors.white,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 24),
                    Container(
                      width: 143,
                      height: 52,
                      child: FlatButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50.0),
                        ),
                        color: Colors.white,
                        child: Text(
                          "Start",
                          style: TextStyle(
                              fontFamily: 'NotoSansCJKkr',
                              color: kMainIndigo,
                              fontWeight: FontWeight.bold,
                              fontSize: 14),
                        ),
                        onPressed: () {
                          Navigator.pushReplacementNamed(
                              context, SurveyScreen.id);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 24),
              GestureDetector(
                onTap: () {
                  Navigator.canPop(context)
                      ? Navigator.of(context).pop()
                      : Navigator.pushReplacementNamed(context, MainScreen.id);
                },
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(color: Colors.white, width: 1.0),
                    ),
                  ),
                  child: Text(
                    "다음에 할래요",
                    style: TextStyle(
                        fontFamily: 'NotoSansCJKkr',
                        color: Colors.white,
                        fontSize: 14),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
