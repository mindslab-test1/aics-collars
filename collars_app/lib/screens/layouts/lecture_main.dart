import 'dart:collection';

import 'package:collarsApp/providers/main_view_provider.dart';
import 'package:collarsApp/providers/study_status.dart';
import 'package:collarsApp/providers/user.dart';
import 'package:collarsApp/screens/components/cards/calendar.dart';
import 'package:collarsApp/screens/components/cards/main_card.dart';
import 'package:collarsApp/screens/layouts/exam_main.dart';
import 'package:collarsApp/screens/layouts/exam_screen/CommonPaint.dart';
import 'package:collarsApp/screens/layouts/lecture/daily_course.dart';
import 'package:collarsApp/screens/layouts/lecture/full_course.dart';
import 'package:collarsApp/screens/layouts/lecture/video_lecture/video_lecture.dart';
import 'package:collarsApp/screens/layouts/suggest_screen.dart';
import 'package:collarsApp/screens/layouts/survey_intro_screen.dart';
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/json_parser/lecture/course_media.dart';
import 'package:collarsApp/services/timer.dart';
import 'package:collarsApp/styles/color/default.dart';
import 'package:collarsApp/styles/size/default.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart'
    show CalendarCarousel;
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/classes/event_list.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'package:provider/provider.dart';

class LectureMain extends StatefulWidget {
  static const String id = 'lecture_screen';
  @override
  _LectureMainState createState() => _LectureMainState();
}

class _LectureMainState extends State<LectureMain> {
  Common _common = Common();
  String _appServer = DotEnv().env['APP_SERVER'];
  Map<String, dynamic> dateData;
  bool isLoading;

  List<UserCourseList> completedCourse = List();
  UserCourseList nCompletedCourse = UserCourseList();
  CourseMedia courseMedia;

  //calendar 관련 변수

  DateTime _currentDate2 = DateTime.now();

  bool finishedLecture = false;
  bool isCompleteLecture = false;
  User user;
  StudyStatus studyStatus;

  // Card variable & controller
  String cardTitle;
  String cardContent;
  Gradient gradient;
  Function mainCardHandler;

  @override
  void initState() {
    super.initState();
    studyStatus = Provider.of<StudyStatus>(context, listen: false);
    user = Provider.of<User>(context, listen: false);

    isLoading = true;

    getLectureDate();
  }


  Future<void> getLectureDate() async{
    Map<String, dynamic> request = {"uid": user.uid, "course": user.course};
    final data = await Common().httpRequest(_appServer+"/lecture/getUserCourseInfo", request);

    courseMedia = CourseMedia.fromJson(data);

    if(user.course != null) {
      studyStatus.setStartDate(courseMedia.startDate);
      studyStatus.setCourseMediaVO(courseMedia.courseMeidaVO);
      studyStatus.setIsStudyCompleted(courseMedia.isStudyCompleted);
      studyStatus.setUserCourseList(courseMedia.userCourseList);
      //getCompletePercentage(courseMedia.isStudyCompleted);
    }

    //check completed course, not completed course
    courseMedia.userCourseList.forEach((userCourse) {
      if(userCourse.completed == 0) {
        setState(() {
          nCompletedCourse = userCourse;
          studyStatus.setLectureTitle(nCompletedCourse.title);
        });
      }
      else if(userCourse.completed == 1) setState(() {
        completedCourse.add(userCourse);
      });
    });

    /*completedCourse.forEach((element) {
      print(element.toJson());
    });*/

    setState(() {
      isLoading = false;
    });
  }

  getCompletePercentage(StudyStatus value){
    List complete = value.isStudyCompleted;
    double completePercentage;
    double count = 0;
    complete.forEach((element) {
      if(element == true) count+=1;
    });

    if(count == 0) completePercentage = 0;
    else completePercentage = count/complete.length*100;

    return completePercentage;
  }

  onDayPressedFunc(DateTime date){
    setState(() {
      _currentDate2 = date;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // Card
    if (_common.equalsIgnoreCase(user.first, "N")) {
      cardTitle = "레벨을 확인해보세요.";
      cardContent = "테스트를 받고\n비즈니스 영어 레벨을 확인하세요.";
      gradient = kSecondaryGradient2;
      mainCardHandler = () async {
        Navigator.pushNamed(context, ExamMain.id);
      };
    } else if (_common.equalsIgnoreCase(user.survey, "N")) {
      cardTitle = "강의를 추천받아 보세요.";
      cardContent = "맞춤형 정보를 입력하면\n인공지능이 강의를 추천합니다.";
      gradient = kPrimaryLightGradient2;
      mainCardHandler = () {
        Navigator.pushNamed(context, SurveyIntroScreen.id);
      };
    } else if (user.course == null) {
      cardTitle = "강의를 추천받아 보세요.";
      cardContent = "맞춤형 정보를 입력하면\n인공지능이 강의를 추천합니다.";
      gradient = kPrimaryLightGradient2;
      mainCardHandler = () {
        Navigator.pushNamed(context, SuggestScreen.id);
      };
    } else {
      cardTitle = "강의학습\n시간입니다.";
      cardContent = "맞춤형 강의를 확인하세요.";
      gradient = kPrimaryLightGradient2;
      mainCardHandler = () {
        MainViewProvider _mainView =
        Provider.of<MainViewProvider>(context, listen: false);
        _mainView.currentPage = 2;
        _mainView.pageController.jumpToPage(1);
      };
    }

    return isLoading?Common().spinkit(size, "로딩중입니다.", Colors.grey):Container(
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
             /* Align(
                alignment: Alignment.center,
                child: (!finishedLecture && user.course!=null)?CustomPaint(
                  size: size,
                  painter: LectureBackGround(),
                ):SizedBox(),
              ),*/
              Align(
                alignment: Alignment.center,
                child: Column(
                  //crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, size.height*0.038, 0, size.height*0.026),
                      child: Container(
                        width: size.width*0.877,
                        height: 55,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30),
                            border: Border.all(color: Color.fromRGBO(82, 98, 242, 1), width: 2)
                        ),
                        child:Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: 55,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: finishedLecture?Colors.transparent:Color.fromRGBO(82, 98, 242, 1),
                                ),
                                child: GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    onTap: (){
                                      setState(() {
                                        finishedLecture = false;
                                      });
                                    },
                                    child: Center(
                                        child: Text("수강중인 강의",
                                          style: TextStyle(
                                              fontSize: 15,
                                              color: finishedLecture?Color.fromRGBO(82, 98, 242, 1):Colors.white,
                                              fontFamily: 'NotoSansCJKkr'
                                          ),
                                        )
                                    )
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                height: 55,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: finishedLecture?Color.fromRGBO(82, 98, 242, 1):Colors.transparent,
                                ),
                                child: GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    onTap: (){
                                      setState(() {
                                        finishedLecture = true;
                                      });
                                    },
                                    child: Center(
                                        child: Text("수강완료 강의",
                                          style: TextStyle(
                                              fontSize: 15,
                                              color: finishedLecture?Colors.white:Color.fromRGBO(82, 98, 242, 1),
                                              fontFamily: 'NotoSansCJKkr'
                                          ),
                                        )
                                    )
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),

                    finishedLecture?Column(
                      children: setCompletedLecture(size),
                    ):(user.course != null)?
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: size.width*0.061),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(nCompletedCourse.title,
                                style: TextStyle(
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold,
                                    color: Color(0xff444444),
                                    fontFamily: 'NotoSansCJKkr'
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10, top: 5),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: setStarLevel(nCompletedCourse.lev),
                                ),
                              ),
                              SizedBox(width: size.width*0.172,),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(size.width*0.061, 0 , size.width*0.061, size.height*0.051),
                          child: Container(
                            color: Colors.transparent,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  width: size.width*0.867,
                                  height: size.height*0.211,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(20),
                                      boxShadow:  [
                                        BoxShadow(
                                            color: Color.fromRGBO(0, 0, 0, 0.16),
                                            blurRadius: 6.0,
                                            //offset: Offset(-5, 10)
                                        )
                                      ]
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Expanded(
                                              flex: 1,
                                              child: Container(
                                                margin: EdgeInsets.only(left: 30,top: 10),
                                                child: Align(
                                                  alignment: Alignment.centerLeft,
                                                  child: Text(
                                                    "Goals",
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color: Color.fromRGBO(34, 42, 100, 1),
                                                        fontWeight: FontWeight.w900,
                                                        fontFamily: 'Raleway'
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),

                                            Expanded(
                                              flex: 2,
                                              child: Container(
                                                margin: EdgeInsets.only(left: size.width*0.1),
                                                child: Consumer<StudyStatus>(
                                                  builder: (context, value, child) =>
                                                      Column(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        children: <Widget>[
                                                          Row(
                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                            children: <Widget>[
                                                              Container(
                                                                height: 14.1,
                                                                width: 14.1,
                                                                decoration: BoxDecoration(
                                                                    shape: BoxShape.circle,
                                                                    color: getCompletePercentage(value) <100?Color.fromRGBO(82, 98, 242, 1):Color.fromRGBO(239, 240, 241, 1)
                                                                ),
                                                              ),
                                                              SizedBox(width: 12,),
                                                              Text("Completed",
                                                                style: TextStyle(
                                                                    color: Color(0xff8f8f8f),
                                                                    fontWeight: FontWeight.w500,
                                                                    fontFamily: 'Raleway',
                                                                    fontSize: 10
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                          SizedBox(height: 15.9,),
                                                          Row(
                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                            children: <Widget>[
                                                              Container(
                                                                height: 14.1,
                                                                width: 14.1,
                                                                decoration: BoxDecoration(
                                                                    shape: BoxShape.circle,
                                                                    color: getCompletePercentage(value) >= 100?Color.fromRGBO(82, 98, 242, 1):Color.fromRGBO(239, 240, 241, 1)
                                                                ),
                                                              ),
                                                              SizedBox(width: 12,),
                                                              Text("Uncompleted",
                                                                style: TextStyle(
                                                                    color: Color(0xff8f8f8f),
                                                                    fontWeight: FontWeight.w500,
                                                                    fontFamily: 'Raleway',
                                                                    fontSize: 10
                                                                ),
                                                              )
                                                            ],
                                                          )
                                                        ],
                                                      ),
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Consumer<StudyStatus>(
                                          builder: (context, value, child) =>
                                              Padding(
                                                padding: const EdgeInsets.only(top: 10),
                                                child: Container(
                                                  child: Center(
                                                    child: Container(
                                                      color: Colors.transparent,
                                                      height: 103,
                                                      width: 104,
                                                      child: new CustomPaint(
                                                        foregroundPainter: CircleProgress(getCompletePercentage(value), 20, Color.fromRGBO(82, 98, 242, 1)),
                                                        child: new Padding(
                                                          padding: EdgeInsets.all(15),
                                                          child: Container(
                                                            child: Center(
                                                              child: Text(
                                                                "${getCompletePercentage(value).toStringAsFixed(1)}%",
                                                                style: TextStyle(
                                                                    fontSize: 17.1,
                                                                    color: Color.fromRGBO(143, 143, 143, 1),
                                                                    fontWeight: FontWeight.bold,
                                                                    fontFamily: 'Raleway'
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                        ),
                                      )

                                    ],
                                  ),
                                ),

                                SizedBox(height: size.height*0.026,),
                                CustomCalendar(
                                    courseMedia,
                                    onDayPressedFunc,
                                    (){}
                                )
                              ],
                            ),
                          ),
                        ),

                        Center(
                          child: RaisedButton(
                            color: Color.fromRGBO(82, 98, 242, 1),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0)
                            ),
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                              child: Column(
                                children: <Widget>[
                                  Text("${_currentDate2.month}/${_currentDate2.day}",
                                    style: TextStyle(fontSize: 17, color: Colors.white, fontFamily: 'Raleway', fontWeight: FontWeight.w100),
                                  ),
                                  Text("학습하기",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 17, color: Colors.white, fontWeight: FontWeight.bold, fontFamily: 'NotoSansCJKkr'),
                                  ),
                                ],
                              )
                            ),
                            onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => FullCourse(date: "${_currentDate2.month}/${_currentDate2.day}")));
                            },
                          ),
                        ),

                        (getCompletePercentage(Provider.of<StudyStatus>(context))>=100)?Center(
                          child: Padding(
                            padding: EdgeInsets.only(top: 20),
                            child: RaisedButton(
                              color: Color.fromRGBO(82, 98, 242, 1),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0)
                              ),
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                                child: Text("학습완료",
                                  textAlign: TextAlign.center, style: TextStyle(fontSize: 17, color: Colors.white, fontWeight: FontWeight.bold)
                                ),
                              ),
                              onPressed: (){
                                finishLecture(context);
                              },
                            ),
                          ),
                        ):SizedBox(),
                        SizedBox(
                          height: 100,
                        )
                      ],
                    ):Container(
                      padding: kSmallPaddingSymmetric,
                      child: MainCard(
                        title: cardTitle,
                        padding: kDefaultPaddingSymmetric,
                        gradient: gradient,
                        content: cardContent,
                        btnText: "Start!",
                        btnSize: 75,
                        onPressed: () => mainCardHandler(),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      );
  }


  List<Widget> setCompletedLecture(Size size){
    List<Widget> completedCourseWidget = List();

    if(completedCourse.length == 0){
      completedCourseWidget.add(
        Padding(
          padding: EdgeInsets.only(top: size.height*0.31),
          child: Container(
            child: Center(
              child: Text(
                '수강완료된 강의가 없습니다.',
                style: TextStyle(
                  fontSize: 18,
                  fontFamily: 'NotoSansCJKkr',
                  fontWeight: FontWeight.w500
                ),
              ),
            ),
          ),
        )
      );
    }else {
      completedCourse.forEach((course) {
        completedCourseWidget.add(
          Padding(
            padding: EdgeInsets.only(bottom: size.height * 0.013),
            child: Container(
              child: Padding(
                padding: EdgeInsets.only(left: size.width * 0.077,
                    top: 24,
                    bottom: 24,
                    right: size.width * 0.077),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(course.title,
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'NotoSansCJKkr',
                          color: Color(0xff444444)
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: setStarLevel(course.lev),
                        ),
                        Text(
                          '${DateFormat('yy/MM/dd').format(
                              DateTime.parse(course.startDate))}'
                              + ' ~ '
                              + (course.endDate == null ? '' : '${DateFormat(
                              'yy/MM/dd').format(
                              DateTime.parse(course.endDate))}'),
                          style: TextStyle(
                              fontSize: 12,
                              color: Color(0xff444444),
                              fontFamily: 'Raleway',
                              fontWeight: FontWeight.w500
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),

              width: size.width * 0.867,
              decoration: BoxDecoration(
                border: Border.all(
                    color: Colors.transparent
                ),
                color: Color(0xffeeeeee),
                borderRadius: BorderRadius.circular(20),
              ),
            ),
          ),
        );
      });
    }

    completedCourseWidget.add(SizedBox(height: 100,));
    return completedCourseWidget;
  }

  List<Widget> setStarLevel(int level){
    List<Widget> starList = List();
    for(int i=0;i<5;i++){
      if(i<level) starList.add(Icon(Icons.star, color: Colors.black, size: 15,));
      else starList.add(Icon(Icons.star, color: Colors.grey, size: 17,));
    }
    starList.add(Padding(
        padding: EdgeInsets.only(left: 5),
        child: Text("Level",
          style: TextStyle(
              color: Color(0xff7f7f7f),
              fontSize: 12,
              fontFamily: 'Raleway',
              fontWeight: FontWeight.w500
          ),
        )
    ));

    return starList;
  }

  void finishLecture(BuildContext context){
    Common().customPopup(context,
        "학습완료",
        "학습을 완료하시면 다시 강의를 들을 수 없습니다. 학습완료를 하시겠습니까?",
        requestFinishCourse
    );
  }

  Future<void> requestFinishCourse() async{
    Navigator.pop(context);
    setState(() {isLoading = true;});
    Map<String, dynamic> request = {"uid": user.uid};
    final data = await Common().httpRequest(_appServer+"/lecture/finishCourse", request);
    if(data.toString() == 'SUCCESS'){
      setState(() {
        user.course = null;
      });
    }

    setState(() {isLoading = false;});

  }

}
