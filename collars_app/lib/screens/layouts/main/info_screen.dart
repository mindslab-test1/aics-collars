import 'package:collarsApp/screens/components/buttons/svg_text_button.dart';
import 'package:collarsApp/screens/layouts/info/customer_center_screen.dart';
import 'package:collarsApp/screens/layouts/info/notice_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

// Screen
import 'package:collarsApp/screens/layouts/info/user_info_screen.dart';

// Style
import 'package:collarsApp/styles/color/default.dart';
import 'package:collarsApp/styles/size/default.dart';

class InfoScreen extends StatelessWidget {
  static const String id = 'info_screen';


  @override
  Widget build(BuildContext context) {

    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    return Stack(
      children: [
        Positioned(
          bottom: 0,
          right: 0,
          child: SvgPicture.asset('assets/images/bg/info_bg.svg',fit: BoxFit.cover,alignment: Alignment.bottomRight,),
        ),
        Positioned(
          width: screenWidth,
          height: screenHeight,
          child: Container(
            padding: kDefaultPaddingSymmetric,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SvgTextButton(
                  svg: SvgPicture.asset(
                    'assets/images/icon/account_circle-24px.svg',
                    width: kInfoIconSize,
                    height: kInfoIconSize,
                    color: kInfoItem,
                  ),
                  text: Text('내정보', style: kInfoItemText),
                  padding: EdgeInsets.symmetric(vertical: 11),
                  onPressed: () {
                    Navigator.pushNamed(context, UserInfoScreen.id);
                  },
                ),
                SizedBox(
                  height: kInfoItemGap,
                ),
                SvgTextButton(
                  svg: SvgPicture.asset(
                    'assets/images/icon/notification_important-24px.svg',
                    width: kInfoIconSize,
                    height: kInfoIconSize,
                    color: kInfoItem,
                  ),
                  text: Text('공지사항', style: kInfoItemText),
                  padding: EdgeInsets.symmetric(vertical: 11),
                  onPressed: () {
                    Navigator.pushNamed(context, NoticeScreen.id);
                  },
                ),
                SizedBox(
                  height: kInfoItemGap,
                ),
                SvgTextButton(
                  svg: SvgPicture.asset(
                    'assets/images/icon/credit_card-24px.svg',
                    width: kInfoIconSize,
                    height: kInfoIconSize,
                    color: kInfoItem,
                  ),
                  text: Text('결제내역', style: kInfoItemText),
                  padding: EdgeInsets.symmetric(vertical: 11),
                  onPressed: () {},
                ),
                SizedBox(
                  height: kInfoItemGap,
                ),
                SvgTextButton(
                  svg: SvgPicture.asset(
                    'assets/images/icon/question_answer-24px.svg',
                    width: kInfoIconSize,
                    height: kInfoIconSize,
                    color: kInfoItem,
                  ),
                  text: Text('고객센터', style: kInfoItemText),
                  padding: EdgeInsets.symmetric(vertical: 11),
                  onPressed: () {
                    Navigator.pushNamed(context, CustomerCenterScreen.id);
                  },
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
