import 'package:collarsApp/providers/home_main_provider.dart';
import 'package:collarsApp/providers/main_view_provider.dart';
import 'package:collarsApp/screens/components/intro/intro_one.dart';
import 'package:collarsApp/screens/layouts/exam_main.dart';
import 'package:collarsApp/screens/layouts/suggest_screen.dart';
import 'package:collarsApp/screens/layouts/survey_intro_screen.dart';
import 'package:collarsApp/styles/size/default.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:provider/provider.dart';

// Provider
import 'package:collarsApp/providers/user.dart';

// Screen
import 'package:collarsApp/screens/components/cards/main_card.dart';

// Service
import 'package:collarsApp/services/types.dart';
import 'package:collarsApp/services/common.dart';

// Style
import 'package:collarsApp/styles/color/default.dart';

class HomeScreen extends StatelessWidget {
  static const String id = 'home_screen';
  Common _common = Common();

  @override
  Widget build(BuildContext context) {
    User _user = Provider.of<User>(context);

    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    HomeMainProvider _hmp = Provider.of<HomeMainProvider>(context);

    String cardTitle;
    String cardContent;
    Gradient gradient;
    Function mainCardHandler;
    if (_common.equalsIgnoreCase(_user.first, "N")) {
      cardTitle = "레벨을 확인해보세요.";
      cardContent = "테스트를 받고\n비즈니스 영어 레벨을 확인하세요.";
      gradient = kSecondaryGradient2;
      mainCardHandler = () async {
        Navigator.pushNamed(context, ExamMain.id);
      };
    } else if (_common.equalsIgnoreCase(_user.survey, "N")) {
      cardTitle = "강의를 추천받아 보세요.";
      cardContent = "맞춤형 정보를 입력하면\n인공지능이 강의를 추천합니다.";
      gradient = kPrimaryLightGradient2;
      mainCardHandler = () {
        Navigator.pushNamed(context, SurveyIntroScreen.id);
      };
    } else if (_user.course == null) {
      cardTitle = "강의를 추천받아 보세요.";
      cardContent = "맞춤형 정보를 입력하면\n인공지능이 강의를 추천합니다.";
      gradient = kPrimaryLightGradient2;
      mainCardHandler = () {
        Navigator.pushNamed(context, SuggestScreen.id);
      };
    } else {
      cardTitle = "강의학습\n시간입니다.";
      cardContent = "맞춤형 강의를 확인하세요.";
      gradient = kPrimaryLightGradient2;
      mainCardHandler = () {
        MainViewProvider _mainView =
            Provider.of<MainViewProvider>(context, listen: false);
        _mainView.currentPage = 2;
        _mainView.pageController.jumpToPage(1);
      };
    }

    Widget buildTwoCents() {
      return LazyLoadScrollView(
        isLoading: _hmp.isSectorLoading,
        onEndOfPage: () {
          if (!_hmp.isScriptLoading) _hmp.loadScriptData();
        },
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          physics: BouncingScrollPhysics(),
          itemCount: _hmp.scripts.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => IntroOne(
                      service: TWOCENTS,
                      position: index,
                    ),
                  ),
                );
              },
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 4),
                width: screenWidth * 0.8,
                height: 200,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(16)),
                  image: DecorationImage(
                      image: NetworkImage(_hmp.scripts[index]['thumbnail']),
                      fit: BoxFit.cover),
                ),
              ),
            );
          },
        ),
      );
    }

    Widget buildSector() {
      return Container(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: SizedBox(
          height: 35,
          child: ListView.builder(
            physics: BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            itemCount: _hmp.sectors.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  _hmp.currentSectorIndex = index;
                  _hmp.clearColumn();
                },
                child: Container(
                  width: 68,
                  alignment: Alignment.center,
                  margin: EdgeInsets.symmetric(horizontal: 6.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(18),
                    border: Border.all(
                      color: _hmp.currentSectorIndex == index
                          ? kMainBlue
                          : kMainBlack,
                    ),
                    color: _hmp.currentSectorIndex == index
                        ? kMainBlue
                        : Colors.transparent,
                  ),
                  child: Text(
                    _hmp.sectors[index]["tag"],
                    style: TextStyle(
                      fontSize: 12,
                      color: _hmp.currentSectorIndex == index
                          ? Colors.white
                          : kMainBlack,
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      );
    }

    Widget buildEvereading() {
      return LazyLoadScrollView(
        isLoading: _hmp.isColumnLoading,
        onEndOfPage: () {
          if (!_hmp.isColumnLoading) _hmp.loadColumnData();
        },
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          physics: BouncingScrollPhysics(),
          itemCount: _hmp.columns.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => IntroOne(
                      service: EVEREADING,
                      position: index,
                    ),
                  ),
                );
              },
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 4),
                width: screenWidth * 0.8,
                height: 200,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(16)),
                  image: DecorationImage(
                      image: NetworkImage(_hmp.columns[index]['thumbnail']),
                      fit: BoxFit.cover),
                ),
              ),
            );
          },
        ),
      );
    }

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            padding: kSmallPaddingSymmetric,
            child: MainCard(
              title: cardTitle,
              padding: kDefaultPaddingSymmetric,
              gradient: gradient,
              content: cardContent,
              btnText: "Start!",
              btnSize: 75,
              onPressed: () => mainCardHandler(),
            ),
          ),
          SizedBox(height: 20),
          Container(
            padding: kDefaultPaddingHorizontal,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  'Two Cents',
                  style: TextStyle(
                    color: kEvereadingMainBlack,
                    fontFamily: 'NotoSansCJKkr',
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
                SizedBox(width: 10),
                Text(
                  '비지니스 스몰토크 5분 연습하기',
                  style: TextStyle(
                    color: kEvereadingMainBlack,
                    fontFamily: 'NotoSansCJKkr',
                    fontSize: 10,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 12),
          Container(
            height: 200,
            child: buildTwoCents(),
          ),
          SizedBox(height: 30),
          Container(
            width: double.infinity,
            padding: kDefaultPaddingHorizontal,
            child: Text(
              '하루 5분, 영어읽는 습관',
              textAlign: TextAlign.start,
              style: TextStyle(
                color: kEvereadingMainBlack,
                fontFamily: 'NotoSansCJKkr',
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
          ),
          SizedBox(height: 16),
          buildSector(),
          SizedBox(height: 12),
          Container(
            height: 200,
            child: buildEvereading(),
          ),
        ],
      ),
    );
  }
}
