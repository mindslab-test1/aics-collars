import 'package:collarsApp/screens/layouts/evereading/evereading_body.dart';
import 'package:collarsApp/styles/size/default.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:collarsApp/providers/evereading_main_provider.dart';

// Components
import 'package:collarsApp/screens/components/etc/image_slider.dart';
import 'package:collarsApp/screens/components/cards/evereading_column_card.dart';
import 'package:collarsApp/screens/components/carousels/evereading_main_carousel.dart';

class EvereadingScreen extends StatelessWidget {
  static const String id = 'evereading_screen';

  @override
  Widget build(BuildContext context) {
    return Provider.of<EvereadingMainProvider>(context).isLoading
        ? Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          )
        : SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                // Banner
                Container(
                  height: 250,
                  child: ImageSlider(
                      items:
                          Provider.of<EvereadingMainProvider>(context).banners),
                ),
                SizedBox(height: 20),
                Container(
                  padding: kDefaultPaddingHorizontal,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        '에디터 추천 칼럼',
                        style: TextStyle(
                            fontSize: 20,
                            color: Color(0xff212121),
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 10),
                      EvereadingColumnCard()
                    ],
                  ),
                ),
                SizedBox(height: 30),
                EvereadingMainCarousel(
                  tags: Provider.of<EvereadingMainProvider>(context).tags,
                ),
                EvereadingBody(),
              ],
            ),
          );
  }
}
