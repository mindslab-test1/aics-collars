
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';

// Provider
import 'package:provider/provider.dart';
import 'package:collarsApp/providers/user.dart';
import 'package:collarsApp/providers/main_view_provider.dart';

// Screen
import 'package:collarsApp/screens/layouts/main/home_screen.dart';
import 'package:collarsApp/screens/layouts/main/info_screen.dart';
import 'package:collarsApp/screens/layouts/lecture_main.dart';
import 'package:collarsApp/screens/layouts/main/evereading_screen.dart';

// Component
import 'package:collarsApp/screens/components/etc/preparing_service.dart';
import 'package:collarsApp/screens/components/appbars/default_app_bar.dart';
import 'package:collarsApp/screens/components/appbars/evereading_app_bar.dart';

// Service
import 'package:collarsApp/services/common.dart';

// Style
import 'package:collarsApp/styles/size/default.dart';
import 'package:collarsApp/styles/color/default.dart';

class MainScreen extends StatelessWidget {
  static const String id = 'main_screen';
  Common _common = Common();

  @override
  Widget build(BuildContext context) {
    User _user = Provider.of<User>(context);
    MainViewProvider _mainView = Provider.of<MainViewProvider>(context);
    final screenWidtgh = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    List<Widget> pages = [
      HomeScreen(),
      LectureMain(),
      EvereadingScreen(),
      InfoScreen()
    ];
    Widget appBar;
    switch(_mainView.currentPage){
//      case 2:
//        appBar = EvereadingAppBar();
//        break;
      default:
        appBar = DefaultAppBar();
        break;
    }

    return Scaffold(
      appBar: appBar,
      body: SafeArea(
        child: PageView(
          controller: _mainView.pageController,
          onPageChanged: (index) {
            _mainView.currentPage = index;
          },
          physics: NeverScrollableScrollPhysics(),
          children: pages,
        ),
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          border: Border(top: BorderSide(width: 2, color: Color(0xffdfdfdf)))
        ),
        child: BottomNavigationBar(
          currentIndex: _mainView.currentPage,
          onTap: (int index) {
            _mainView.currentPage = index;
            _mainView.pageController.jumpToPage(index);
          },
          backgroundColor: kBottomBarBg,
          unselectedItemColor: kBottomBarItemUnselected,
          selectedItemColor: kBottomBarItemSelected,

          type: BottomNavigationBarType.fixed,
          items: [
            CustomBottomNavigationBarItem(image: 'assets/images/icon/home-30px.svg', title: 'My Home'),
            CustomBottomNavigationBarItem(image: 'assets/images/icon/mycollars-30px.svg', title: 'My Collars'),
            CustomBottomNavigationBarItem(image: 'assets/images/icon/evereading-30px.svg', title: 'Evereading'),
            CustomBottomNavigationBarItem(image: 'assets/images/icon/mypage-30px.svg', title: 'My Page'),
          ],
        ),
      ),
    );
  }

  BottomNavigationBarItem CustomBottomNavigationBarItem({String image, String title}){
    return BottomNavigationBarItem(
      icon: SvgPicture.asset(
        image,
        width: kBottomBarItemSize,
        height: kBottomBarItemSize,
        color: kBottomBarItemUnselected,
      ),
      activeIcon: SvgPicture.asset(
        image,
        width: kBottomBarItemSize,
        height: kBottomBarItemSize,
        color: kBottomBarItemSelected,
      ),
      title: Text(
        title,
        style: TextStyle(fontSize: kBottomBarItemLabelSize),
      ),
    );
  }
}
