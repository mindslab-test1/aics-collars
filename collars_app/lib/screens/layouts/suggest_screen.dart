import 'dart:async';
import 'dart:io';

import 'package:collarsApp/providers/user.dart';
import 'package:collarsApp/screens/components/etc/preparing_service.dart';
import 'package:collarsApp/screens/components/etc/rating_bar.dart';
import 'package:collarsApp/screens/layouts/main/main_screen.dart';
import 'package:collarsApp/services/course.dart';
import 'package:collarsApp/services/purchase.dart';
import 'package:collarsApp/styles/color/default.dart';
import 'package:collarsApp/styles/size/default.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:provider/provider.dart';

class SuggestScreen extends StatefulWidget {
  static const String id = 'suggest_screen';

  @override
  _SuggestScreenState createState() => _SuggestScreenState();
}

class _SuggestScreenState extends State<SuggestScreen> {
  CourseService courseService = CourseService();
  Purchase purchaseService = Purchase();

  bool isPurchased = false;

  InAppPurchaseConnection _iap = InAppPurchaseConnection.instance;
  bool _available = true;
  List<ProductDetails> _products = [];
  List<PurchaseDetails> _purchases = [];
  StreamSubscription _subscription;
  Map<String, dynamic> _result;
  int status;
  dynamic payload;

  String _title;
  String _description;
  String _price;

  @override
  void initState() {
    final _uid = Provider.of<User>(context, listen: false).uid;
    _initialize(_uid);
    super.initState();
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }

  void _initialize(String uid) async {
    _available = await _iap.isAvailable();
    _result = await courseService.getRandomSuggest(uid);
    print(_result);
    setState(() {
      status = _result['status'];
      payload = _result['payload'];
    });
    if (_available) {
      await _getProducts();
      await _getPastPurchases();
      _verifyPurchase(uid);

      _subscription = _iap.purchaseUpdatedStream.listen((data) {
        _purchases.addAll(data);
        _verifyPurchase(uid);
      });
    }
  }

  Future<void> _getProducts() async {
    Set<String> ids = Set.from([payload["productId"]]);
    ProductDetailsResponse response = await _iap.queryProductDetails(ids);
    setState(() {
      _products = response.productDetails;
      _title = _products[0].title;
      _description = _products[0].description;
      _price = _products[0].price;
    });
  }

  Future<void> _getPastPurchases() async {
    QueryPurchaseDetailsResponse response = await _iap.queryPastPurchases();

    for (PurchaseDetails purchase in response.pastPurchases) {
      if (Platform.isIOS) {
        _iap.completePurchase(purchase);
      }
    }
    setState(() {
      _purchases = response.pastPurchases;
    });
  }

  Future _buyProducts(ProductDetails prod) async {
    if (!isPurchased) {
      final PurchaseParam purchaseParam = PurchaseParam(productDetails: prod);
      await _iap.buyConsumable(purchaseParam: purchaseParam, autoConsume: true);
    }
  }

  PurchaseDetails _hasPurchased(String productID) {
    return _purchases.firstWhere((purchase) {
      return purchase.productID == productID;
    }, orElse: () => null);
  }

  void _verifyPurchase(String uid) async {
    PurchaseDetails purchase = await _hasPurchased(payload["productId"]);
    if (purchase != null &&
        purchase.status == PurchaseStatus.purchased &&
        !isPurchased) {
      isPurchased = true;

      String productId = purchase.productID;
      String purchaseToken = purchase.billingClientPurchase.purchaseToken;
      dynamic _verifyPayload = await purchaseService.verifyPurchase(
          uid, payload["id"], productId, purchaseToken);
      Provider.of<User>(context, listen: false).course =
          _verifyPayload["title"];
      Navigator.pop(context);
      //      purchaseAlert(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          '강의추천',
          style: TextStyle(fontFamily: 'NotoSansCJKkr', fontSize: 12),
        ),
        leading: Navigator.canPop(context)
            ? GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: SvgPicture.asset(
                  'assets/images/icon/icon_back.svg',
                  color: Colors.white,
                ),
              )
            : null,
        backgroundColor: Colors.transparent,
        bottomOpacity: 0.0,
        elevation: 0.0,
      ),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: kPrimaryLightGradient,
        ),
        child: Stack(
          children: [
            SvgPicture.asset(
              'assets/images/bg/suggest_bg.svg',
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
            ),
            _title != null
                ? SafeArea(
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  SizedBox(height: 12),
                                  Text(
                                    '다음 강의를 추천드립니다.',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(height: 40),
                                  Container(
                                    height: 250,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        image: DecorationImage(
                                            image: NetworkImage(
                                                payload['thumbnail']),
                                            fit: BoxFit.fill),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(30.0))),
                                  ),
                                  SizedBox(height: 20),
                                  Expanded(
                                    child: Container(
                                      width: double.infinity,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            payload['title'],
                                            style: TextStyle(
                                              fontFamily: 'NotoSansCJKkr',
                                              color: Colors.white,
                                              fontSize: 24,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            _price,
                                            style: TextStyle(
                                              fontFamily: 'NotoSansCJKkr',
                                              color: Colors.white,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          SizedBox(height: 8),
                                          RatingBar(
                                            rating: 3.5,
                                            itemCount: 5,
                                            activeColor: Colors.white,
                                            inactiveColor: kMainWhite50,
                                            label: Text(
                                              'Level',
                                              style: TextStyle(
                                                fontSize: 15,
                                                color: kMainWhite50,
                                              ),
                                            ),
                                          ),
                                          SizedBox(height: 4),
                                          Expanded(
                                            child: SingleChildScrollView(
                                              child: Text(
                                                payload['description'],
                                                style: TextStyle(
                                                  fontFamily: 'NotoSansCJKkr',
                                                  color: Colors.white,
                                                  fontSize: 12,
                                                ),
//                                        overflow: TextOverflow.ellipsis,
                                                softWrap: true,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          SizedBox(height: 4),
                          GestureDetector(
                            onTap: () async {
                              await _buyProducts(_products[0]);
                            },
                            child: Container(
                              width: 125,
                              height: 55,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: kMainBtnBg,
                              ),
                              child: Center(
                                child: Text(
                                  '신청하기',
                                  style: TextStyle(
                                    fontFamily: 'NotoSansCJKkr',
                                    fontWeight: FontWeight.bold,
                                    color: kMainBtnText,
                                    fontSize: 14,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 24),
                          GestureDetector(
                            onTap: () {
                              Navigator.popAndPushNamed(
                                context,
                                SuggestScreen.id,
                              );
                            },
                            child: Container(
                              margin: EdgeInsets.only(bottom: 20),
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                      color: Colors.white, width: 1.0),
                                ),
                              ),
                              child: Text(
                                "다른 강의 추천받기",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 14),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          ],
        ),
      ),
    );
  }
}
