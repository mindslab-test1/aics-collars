import 'dart:async';

import 'package:async/async.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:collarsApp/providers/twocents_content_provider.dart';
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/styles/color/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:quiver/async.dart';
import 'package:sound_stream/sound_stream.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class TwoCentsContentCard extends StatefulWidget {
  static const String id = 'twocents_content_card';
  final dynamic data;

  TwoCentsContentCard({this.data});

  @override
  _TwoCentsContentCardState createState() => _TwoCentsContentCardState();
}

class _TwoCentsContentCardState extends State<TwoCentsContentCard> {
  static String _mediaServer = DotEnv().env['MEDIA_SERVER'];
  static String _webSockServer = DotEnv().env['WEB_SOCK_SERVER'];
  final WebSocketChannel channel = IOWebSocketChannel.connect(_webSockServer+"/twocents/pronunciation");

  Common _common = Common();
  AudioPlayer _audioPlayer = AudioPlayer();
  RecorderStream _recorderStream = RecorderStream();
  StreamSubscription _recorderStatus;
  StreamSubscription _audioStream;

  bool iskor;
  bool isAudio;
  bool isLeft;
  dynamic content;

  double tempA = 10;
  double temp;

  @override
  void initState() {
    setState(() {
      content = widget.data;
      _common.equalsIgnoreCase(content['floating'], 'left')
          ? isLeft = true
          : isLeft = false;
      iskor = false;
      isAudio = false;
    });
    _recorderStream.initialize();
    super.initState();
  }

  @override
  void dispose() {
    _audioPlayer.stop();

    _recorderStatus?.cancel();
    _audioStream?.cancel();

    channel.sink.close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return card();
  }

  Widget card() {
    return Column(
      crossAxisAlignment:
          isLeft ? CrossAxisAlignment.start : CrossAxisAlignment.end,
      children: [
//        StreamBuilder(
//          stream: channel.stream,
//          builder: (context, snapshot){
//            return Text('${snapshot.data}');
//          },
//        ),
        isLeft ? leftHeader() : rightHeader(),
        SizedBox(height: 4),
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(15),
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15),
              ),
              border: Border.all(color: kLeftMsgBorder, width: 1),
              color: kLeftMsgBg),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.all(12.0),
                child: Text(content['eng']),
              ),
              iskor
                  ? Container(
                      padding: const EdgeInsets.all(12.0),
                      child: Text(content['kor']),
                    )
                  : Container(width: 0),
            ],
          ),
        )
      ],
    );
  }

  Widget leftHeader() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          content['name'],
          style: TextStyle(
            color: kEvereadingMainBlack,
            fontFamily: 'NotoSansCJKkr',
            fontWeight: FontWeight.bold,
            fontSize: 13,
          ),
        ),
        SizedBox(width: 12),
        // Audio
        GestureDetector(
          onTap: () {
            playAudio();
          },
          child: button(
            image: 'assets/images/icon/sound-24px.svg',
            isActive: isAudio,
          ),
        ),
        SizedBox(width: 4),
        // Translate
        GestureDetector(
          onTap: () {
            setState(() {
              iskor = !iskor;
            });
          },
          child: button(
            image: 'assets/images/icon/han-24px.svg',
            isActive: iskor,
          ),
        ),
        SizedBox(width: 4),
        // Mic
        GestureDetector(
          onTap: () {
            recordAudio();
          },
          child: button(image: 'assets/images/icon/mic-24px.svg'),
        ),
      ],
    );
  }

  Widget rightHeader() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        // Mic
        button(image: 'assets/images/icon/mic-24px.svg'),
        SizedBox(width: 4),
        // Translate
        GestureDetector(
          onTap: () {
            setState(() {
              iskor = !iskor;
            });
          },
          child: button(
            image: 'assets/images/icon/han-24px.svg',
            isActive: iskor,
          ),
        ),
        SizedBox(width: 4),
        // Audio
        GestureDetector(
          onTap: () {
            playAudio();
          },
          child: button(
            image: 'assets/images/icon/sound-24px.svg',
            isActive: isAudio,
          ),
        ),
        SizedBox(width: 12),
        Text(
          content['name'],
          style: TextStyle(
            color: kEvereadingMainBlack,
            fontFamily: 'NotoSansCJKkr',
            fontWeight: FontWeight.bold,
            fontSize: 13,
          ),
        ),
      ],
    );
  }

  Widget button({String image, bool isActive = false}) {
    return Container(
      width: 28,
      height: 28,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: kMainBlue, width: 1),
        color: isActive ? kMainBlue : Colors.transparent,
      ),
      child: SizedBox(
        width: 10,
        height: 10,
        child: SvgPicture.asset(
          image,
          color: isActive ? kMainWhite : kMainBlue,
        ),
      ),
    );
  }

  void playAudio() async {
    TwoCentsContentProvider tcp =
        Provider.of<TwoCentsContentProvider>(context, listen: false);
    if (!tcp.isAudioActive) {
      int result = await _audioPlayer.play('$_mediaServer${content['audio']}');
      if (result == 1) {
        tcp.isAudioActive = true;
        setState(() {
          isAudio = true;
        });
      }
      _audioPlayer.onPlayerStateChanged.listen((event) {
        switch (event) {
          case AudioPlayerState.COMPLETED:
          case AudioPlayerState.STOPPED:
          case AudioPlayerState.PAUSED:
            tcp.isAudioActive = false;
            setState(() {
              isAudio = false;
            });
            break;
        }
      });
    }
  }

  void recordAudio() async {
    TwoCentsContentProvider tcp =
        Provider.of<TwoCentsContentProvider>(context, listen: false);
    if (!tcp.isMicActive) {
      tcp.isMicActive = true;
      _recorderStream.start();
      _recorderStatus = _recorderStream.status.listen((event) {
        print("STATUS");
        print(event);
        switch (event) {
          case SoundStreamStatus.Stopped:
            tcp.isMicActive = false;
            _recorderStatus?.cancel();
            break;
        }
      });
      _audioStream = _recorderStream.audioStream.listen((event) {
        channel.sink.add(event);
        print(event);
      });
      Timer(Duration(seconds: 3), () async {
        print("TIMER");
        List<int> end = List<int>(1);
        end[0] = 1;
        channel.sink.add(end);
        await _audioStream?.cancel();
        await _recorderStream?.stop();
      });
    }
  }
}
