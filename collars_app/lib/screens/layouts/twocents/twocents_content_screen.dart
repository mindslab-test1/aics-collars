import 'package:collarsApp/screens/layouts/twocents/twocents_content_card.dart';
import 'package:collarsApp/styles/color/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TwoCentsContentScreen extends StatelessWidget {
  dynamic data;

  TwoCentsContentScreen({this.data});

  @override
  Widget build(BuildContext context) {
    print(data);

    dynamic content = data['content'];

    List<Widget> buildBody() {
      List<Widget> items = [];
      content['content'].forEach((data) {
        items.add(SizedBox(height: 16));
        items.add(TwoCentsContentCard(data: data));
      });
      return items;
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          data['content']['title']['eng'],
          style: TextStyle(
            fontFamily: 'NotoSansCJKkr',
            fontSize: 12,
            color: Colors.black,
          ),
        ),
        leading: Navigator.canPop(context)
            ? GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: SvgPicture.asset(
                  'assets/images/icon/icon_back.svg',
                  color: Colors.black,
                ),
              )
            : null,
        backgroundColor: Colors.transparent,
        bottomOpacity: 0.0,
        elevation: 0.0,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: Column(
              children: [...buildBody()],
            ),
          ),
        ),
      ),
    );
  }
}
