import 'dart:async';
import 'dart:convert';
import 'package:collarsApp/providers/exam_result.dart';
import 'package:collarsApp/screens/components/cards/template/tmp5_card.dart';
import 'package:collarsApp/screens/layouts/exam_screen//CommonPaint.dart';
import 'package:collarsApp/screens/layouts/exam_screen//exam_result.dart';
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/json_parser/ExamTemp/exam_json5.dart';
import 'package:collarsApp/services/timer.dart';
import 'package:collarsApp/services/json_parser/pron.dart';
import 'package:collarsApp/services/mic_streaming.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import '../exam_main.dart';


class ExamQ5 extends StatefulWidget {
  final MicStream micStream;
  ExamQ5({this.micStream});

  static String _webSockServer = DotEnv().env['WEB_SOCK_SERVER'];
  static Uri uri  = Uri.parse(_webSockServer+"/engedu/websocket/pronExamQ5");

  @override
  _ExamQ5State createState() => _ExamQ5State();
}

class _ExamQ5State extends State<ExamQ5> with SingleTickerProviderStateMixin{
  bool isLoading = false;
  Color skipBtn = Colors.white;

  AnimationController progressController;
  Animation<double> animation;

  //webSocket
  WebSocketChannel channel;
  StreamSubscription<List<int>> listener;
  bool isRecording = false;
  bool isWebScokConn = false;

  //exam template5
  String _appServer = DotEnv().env['APP_SERVER'];
  ExamJson5 examJson5;

  ExamScore examScore;

  @override
  void dispose() {
    stopTest();
    //examScore.resetExamScore();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    SystemChannels.textInput.invokeMethod('TextInput.hide');

    examScore = Provider.of<ExamScore>(context, listen: false);
    isLoading = true;

    getExam5();

    progressController = AnimationController(vsync: this,duration: Duration(seconds: 10));
    animation = Tween<double>(begin: 0,end: 100).animate(progressController)..addListener((){
      if(animation.value.toInt() == 100 && isRecording) _stopWebSocket();
      setState(() {});
    });
  }

  /*
  * byte로 변환된 데이터를 websocket을 보냄
  * websocket으로 부터 data가 들어올 시 websocket conn 종료 후 popup
  * */
  bool _sendWebsocket(){

    if(isRecording) return false;

    setState(() {
      channel = WebSocketChannel.connect(ExamQ5.uri);
      isWebScokConn = true;
      isRecording = true;
    });

    //처음 exam num를 보내줌
    channel.sink.add(utf8.encode("Exam5-"+examJson5.examNum.toString()));

    //animation
    progressController.forward();

    listener = widget.micStream.startListening().listen((samples) => {
      channel.sink.add(samples)
    });

    channel.stream.listen((event) {
      channel.sink.close();
      setState(() {isWebScokConn = false;});
      scroeResult(event);
    });
  }

  /*
  * 1byte를 보낼 경우 grpc를 끊어 줌
  * */
  Future<void> _stopWebSocket(){
    List<int> end = List<int>(1);
    end[0] = 1;

    changeAnimationState('stop');
    changeMicStreamStatus('stop');

    channel.sink.add(end);

    setState(() { isRecording = false;});

    return null;
  }

  void _changeListening() => !isRecording ? _sendWebsocket() : _stopWebSocket();

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Color.fromRGBO(255, 87, 159, 0.78), Color.fromRGBO(168, 87, 213, 0.98),Color.fromRGBO(161, 87, 217, 1), Color.fromRGBO(80, 67, 255, 1)]
          )
      ),
      width: size.width,
      height: size.height,
      child: isLoading? Common().spinkit(size, "로딩중입니다.", Colors.white)
          : Tmp5ColumCard(
              size: size,
              examJson5: examJson5,
              animation: animation,
              changeListening: _changeListening,
              skipTest: skipTest,
              isExam: true,
            )
    );
  }

  Future<void> getExam5() async{
    Map<String, dynamic> request = {};
    ExamJson5 examJson5Res = ExamJson5.fromJson(await Common().httpRequest(_appServer+"/maumAi/exam/q5/getQuestion", request));

    setState(() {
      examJson5 = examJson5Res;
      isLoading = false;
    });
  }

  void scroeResult(String result) {
    PronResponse pronResponse = PronResponse.fromJson(jsonDecode(result));
    setState(() {
      examScore.setExpress(pronResponse.score.toDouble());
    });

    Common().showPopup(examScore.express, "Answer", examJson5.correctAnswer, "You said", pronResponse.sttResult,
        context, reAssamblePage, goNextPage, true);
  }

  /*
  * 페이지가 dispose될 경우
  * */
  void stopTest(){
    changeAnimationState('dispose');
    changeMicStreamStatus('dispose');
    shutDownWebSocket();
  }

  void goNextPage(){
    shutDownWebSocket();
    changeAnimationState('dispose');
    changeMicStreamStatus('dispose');

    //save exam number in provider
    examScore.addExamlist(examJson5.examNum);

    Navigator.push(context, CupertinoPageRoute(builder: (context) => ExamResult()));
  }

  void reAssamblePage(){
    changeAnimationState('reset');

    setState(() {
      skipBtn = Colors.white;
      isRecording = false;
    });
  }

  /*
  * 사용자가 발화를 했을 경우 결과를 popup,
  * 발화를 하지 않았을 경우 스킵 popup
  * */
  void skipTest(){
    setState(() {
      skipBtn = Colors.purpleAccent;
    });

    if (isRecording == true) {
      List<int> end = List<int>(1);
      end[0] = 1;

      changeAnimationState('stop');
      changeMicStreamStatus('stop');

      channel.sink.add(end);

      setState(() {
        isRecording = false;
      });
    }else{
      setState(() {
        examScore.setExpress(0);
      });

      Common().showSkipPopup(context, reAssamblePage, goNextPage, true);
    }
  }

  void changeAnimationState(String state){

    if(progressController.status == AnimationStatus.forward ||
        progressController.status == AnimationStatus.reverse){

      if(state == 'stop') progressController.stop();
      if(state == 'reset') progressController.reset();
      if(state == 'dispose') progressController.dispose();
    }
  }

  void shutDownWebSocket(){
    if(isWebScokConn){
      channel.sink.close();
      setState(() {
        isWebScokConn = false;
      });
    }
  }

  void changeMicStreamStatus(String status){
    if(isRecording){
      listener.cancel();
      if(status == 'stop') {
        if(widget.micStream.getRecordingStatus())
          widget.micStream.stopMicpListening();
      }
      else if(status == 'dispose'){
        widget.micStream.endMicStream();
      }
    }
  }

}

