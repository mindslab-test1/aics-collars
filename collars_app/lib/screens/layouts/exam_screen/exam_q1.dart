import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:collarsApp/providers/exam_result.dart';
import 'package:collarsApp/providers/user.dart';
import 'package:collarsApp/screens/layouts/exam_screen/CommonPaint.dart';
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/json_parser/ExamTemp/exam_json1.dart';
import 'package:collarsApp/services/timer.dart';
import 'package:collarsApp/services/json_parser/pron.dart';
import 'package:collarsApp/services/mic_streaming.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class ExamQ1 extends StatefulWidget {
  final Function goNextPage;
  final MicStream micStream;
  ExamQ1({this.micStream, this.goNextPage});

  static String _webSockServer = DotEnv().env['WEB_SOCK_SERVER'];
  static Uri uri  = Uri.parse(_webSockServer+"/engedu/websocket/pronExamQ1");

  @override
  _ExamQ1State createState() => _ExamQ1State();
}

class _ExamQ1State extends State<ExamQ1> with SingleTickerProviderStateMixin {
  //click ripple
  Color skipBtn = Colors.white;

  //template1 관련 변수
  String _appServer = DotEnv().env['APP_SERVER'];
  bool isLoading = false;

  //Animation
  AnimationController progressController;
  Animation<double> animation;

  //Websocket
  WebSocketChannel channel;
  StreamSubscription<List<int>> listener;
  bool isRecording = false;
  bool isWebScokConn = false;

  //save result to provider
  ExamScore examScore;
  ExamJson1 examJson1;

  User user;

  @override
  void dispose() {
    print("dispose page");
    stopTest();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    user = Provider.of<User>(context, listen: false);
    examScore = Provider.of<ExamScore>(context, listen: false);
    isLoading = true;

    getExam1();

    progressController = AnimationController(vsync: this,duration: Duration(seconds: 20));

    animation = Tween<double>(begin: 0,end: 100).animate(progressController)..addListener((){
      if(animation.value.toInt() == 100 && isRecording) _stopWebSocket();
      setState(() {});
    });
  }

  /*
  * byte로 변환된 데이터를 websocket을 보냄
  * websocket으로 부터 data가 들어올 시 websocket conn 종료 후 popup
  * */
  bool _sendWebsocket(){
    if(isRecording) return false;

    setState(() {
      channel = WebSocketChannel.connect(ExamQ1.uri);
      isWebScokConn = true;
      isRecording = true;
    });

    //처음 exam num를 보내줌
    channel.sink.add(utf8.encode("Exam1-"+examJson1.examNum.toString()));

    //animation
    progressController.forward();

    listener = widget.micStream.startListening().listen((samples) => {
      sendMicStream(samples)
    });

    //응답이 올경우 websocket 연결 끊음
    channel.stream.listen((event) {
      channel.sink.close();
      setState(() { isWebScokConn = false; });
      scroeResult(event);
    });
  }

  sendMicStream(var samples){
    channel.sink.add(samples);
  }


  /*
  * 1byte를 보낼 경우 grpc를 끊어 줌
  * */
  Future<void> _stopWebSocket(){
    List<int> end = List<int>(1);
    end[0] = 1;

    changeAnimationState('stop');
    changeMicStreamStatus('stop');

    channel.sink.add(end);

    setState(() { isRecording = false;});

    return null;
  }

  void _changeListening() => !isRecording ? _sendWebsocket() : _stopWebSocket();

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    setState(() {examScore = Provider.of<ExamScore>(context);});

    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Color.fromRGBO(255, 87, 159, 0.78), Color.fromRGBO(168, 87, 213, 0.98),Color.fromRGBO(161, 87, 217, 1), Color.fromRGBO(80, 67, 255, 1)]
          )
      ),
      width: size.width,
      height: size.height,
      child: Padding(
        padding: new EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
        child: Container(
          height: size.height,
          child: isLoading?Common().spinkit(size, "로딩중입니다.", Colors.white):Stack(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: size.height*0.05,
                  ),
                  Text("테스트", style: TextStyle(fontSize: 11, color: Colors.white, fontWeight: FontWeight.bold),),
                  CustomPaint(
                    size: Size(size.width, size.height*0.1),
                    painter: MyPainter(exam: 1),
                  ),

                  examJson1.ptext==""
                      ?Text("ERROR")
                      :Text(examJson1.ptext,
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontFamily: 'NotoSansCJKkr-Medium',
                    ),
                  ),

                  SizedBox(height: size.height*0.039,),

                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 32),
                    child: Center(
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                            boxShadow: [
                              BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.25),
                                  blurRadius: 9.0,
                                  offset: Offset(
                                      10,
                                      10.0
                                  )
                              )
                            ]
                        ),
                        width: size.width*0.85,
                        child: Center(
                          child: examJson1.qtext==""?Text("ERROR"):Padding(
                              padding: EdgeInsets.fromLTRB(24, 24, 24, 24),
                              child: Text(examJson1.qtext,
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'NotoSansCJKkr-Bold',
                                    height: 1.5
                                ),
                                textAlign: TextAlign.center,
                              )
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: size.height*0.079,
                  ),

                  Container(
                    color: Colors.transparent,
                    height: 120,
                    width: 120,
                    child: CustomPaint(
                      foregroundPainter: CircleProgress(animation.value, 10, Color.fromRGBO(129, 129, 251, 1)),
                      child: Padding(
                        padding: EdgeInsets.all(15),
                        child: Container(
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                    color: Color.fromRGBO(0, 0, 0, 0.3),
                                    blurRadius: 7.0,
                                    offset: Offset(
                                        5,5
                                    )
                                )
                              ],
                              shape: BoxShape.circle,
                              gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [Color.fromRGBO( 129, 129, 251, 1), Color.fromRGBO(72, 92, 241, 1), Color.fromRGBO(54, 70, 192, 10)]
                              )
                          ),
                          child: IconButton(
                            icon: Icon(Icons.mic, size: 50, color: Colors.white,),
                            onPressed: _changeListening,
                          ),
                        ),
                      ),
                    ),
                  ),

                  SizedBox(height: size.height*0.105,),
                  GestureDetector(
                    //onTap: skipTest,
                    onTapDown: (TapDownDetails tapDown) {
                      skipTest();
                    },
                    child: Text("잘 모르겠어요",
                      style: TextStyle(color: skipBtn,
                          fontSize: 15,
                          decoration: TextDecoration.underline),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> getExam1() async{
    Map<String, dynamic> request = {};
    ExamJson1 examJson1Res = ExamJson1.fromJson(await Common().httpRequest(_appServer+"/maumAi/exam/q1/getQuestion", request));

    setState(() {
      examJson1 = examJson1Res;
      isLoading= false;
    });
  }

  Future<void> scroeResult(String result) async{
    PronResponse pronResponse = PronResponse.fromJson(jsonDecode(result));

    setState(() {
      examScore.setAccuracy(pronResponse.score.toDouble());
      examScore.setPron(pronResponse.result.totalScore*2);
    });

    Common().showPopup(examScore.accuracy, "Answer", examJson1.correctAnswer, "You said", pronResponse.sttResult, context, reAssamblePage, goNextTest, true);
  }


  void goNextTest(){
    //save exam number in provider
    examScore.addExamlist(examJson1.examNum);
    stopTest();
    widget.goNextPage();
  }

  /*
  * 페이지가 dispose될 경우
  * */
  void stopTest(){
    changeAnimationState('dispose');
    changeMicStreamStatus('dispose');
    shutDownWebSocket('dispose');
  }

  /*
  * 사용자가 제시도 할 경우 리셋
  * */
  void reAssamblePage(){
    changeAnimationState('reset');
    setState(() {
      skipBtn = Colors.white;
      isRecording = false;
    });
  }

  /*
  * 사용자가 발화를 했을 경우 결과를 popup,
  * 발화를 하지 않았을 경우 스킵 popup
  * */
  void skipTest(){
    setState(() {
      skipBtn = Colors.purpleAccent;
    });

    if (isRecording == true) {
      List<int> end = List<int>(1);
      end[0] = 1;

      changeAnimationState('stop');
      changeMicStreamStatus('stop');

      channel.sink.add(end);

      setState(() {
        isRecording = false;
      });
    }
    else {
      setState(() {
        examScore.setAccuracy(0);
        examScore.setPron(0);
      });

      Common().showSkipPopup(context, reAssamblePage, goNextTest, true);
    }
  }

  void changeAnimationState(String state) {
    if (progressController.status == AnimationStatus.forward ||
        progressController.status == AnimationStatus.reverse) {
      //progressController.notifyStatusListeners(AnimationStatus.dismissed);
      if (state == 'stop') progressController.stop();
      if (state == 'reset') progressController.reset();
      if (state == 'dispose') progressController.dispose();
    }
  }

  void shutDownWebSocket(String status) {
    if (isWebScokConn) {
      channel.sink.close();

      if(status != 'dispose') {
        setState(() {
          isWebScokConn = false;
        });
      }
    }
  }

  void changeMicStreamStatus(String status) {
    if (isRecording) {
      listener.cancel();
      if (status == 'stop') {
        if (widget.micStream.getRecordingStatus())
          widget.micStream.stopMicpListening();
      }
      else if (status == 'dispose') {
        widget.micStream.endMicStream();
      }
    }
  }
}
