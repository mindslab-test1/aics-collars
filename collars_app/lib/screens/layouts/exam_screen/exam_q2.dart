import 'package:collarsApp/providers/exam_result.dart';
import 'package:collarsApp/screens/components/buttons/template/template2_button.dart';
import 'package:collarsApp/screens/components/cards/template/tmp2_card.dart';
import 'package:collarsApp/screens/layouts/exam_screen/CommonPaint.dart';
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/json_parser/ExamTemp/exam_json2.dart';
import 'package:collarsApp/services/mic_streaming.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:provider/provider.dart';

class ExamQ2 extends StatefulWidget {
  final Function goNextPage;
  ExamQ2({this.goNextPage});
  @override
  _ExamQ2State createState() => _ExamQ2State();
}

class _ExamQ2State extends State<ExamQ2> {

  String _appServer = DotEnv().env['APP_SERVER'];
  bool isLoading = false;
  List<String> choice = List<String>();

  //save result to provider
  ExamScore examScore;
  ExamJson2 examJson2;

  @override
  void initState() {
    super.initState();

    examScore = Provider.of<ExamScore>(context, listen: false);
    isLoading = true;

    getExam2();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                  colors: [Color.fromRGBO(255, 87, 159, 0.78), Color.fromRGBO(168, 87, 213, 0.98),Color.fromRGBO(161, 87, 217, 1), Color.fromRGBO(80, 67, 255, 1)]
              )
            ),
            width: size.width,
            height: size.height,
            child:isLoading?Common().spinkit(size, "로딩중입니다.", Colors.white): Padding(
              padding: new EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
              child: SingleChildScrollView(
                child: isLoading?Container() :Tmp2ColumCard(
                  size: size,
                  examJson2: examJson2,
                  choice: choice,
                  getResult: getResult,
                  setExamScoreZero: setExamScoreZero,
                  goNextTest: goNextTest,
                  isExam: true,
                ),
              ),
            ),
          );
  }

  Future<void> getExam2() async{
    Map<String, dynamic> request = {};
    ExamJson2 examJson2Res = ExamJson2.fromJson(await Common().httpRequest(_appServer+"/maumAi/exam/q2/getQuestion", request));

    setState(() {
      examJson2 = examJson2Res;
      choice.add(examJson2.mulChoice1);
      choice.add(examJson2.mulChoice2);
      choice.add(examJson2.mulChoice3);
      choice.add(examJson2.mulChoice4);
      isLoading = false;
    });
  }

  Future<void> getResult(int selectedIndex) async{

    if(selectedIndex < 0) return;

    Map<String, dynamic> request = {
      "examNum" : examJson2.examNum,
      "answer" : choice[selectedIndex]
    };
    int finalScore = int.parse(await Common().httpRequest(_appServer+"/maumAi/exam/q2/getScore", request));
    setState(() {examScore.setVoca(finalScore.toDouble()); });
    String score;
    if(finalScore == 100) score = "A";
    else score = "D";

    Common().showPopup(score, "Answer", choice[selectedIndex], "You choose", examJson2.correct, context, (){}, goNextTest, true);
  }

  void goNextTest(){
    //save exam number in provider
    examScore.addExamlist(examJson2.examNum);

    widget.goNextPage();
  }

  void setExamScoreZero(){
    setState(() {
      examScore.setVoca(0);
    });
  }

}
