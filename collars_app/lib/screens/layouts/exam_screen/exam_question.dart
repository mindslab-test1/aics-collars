import 'package:collarsApp/screens/layouts/exam_screen/exam_q1.dart';
import 'package:collarsApp/screens/layouts/exam_screen/exam_q2.dart';
import 'package:collarsApp/screens/layouts/exam_screen/exam_q3.dart';
import 'package:collarsApp/screens/layouts/exam_screen/exam_q4.dart';
import 'package:collarsApp/screens/layouts/exam_screen/exam_q5.dart';
import 'package:collarsApp/services/mic_streaming.dart';
import 'package:flutter/material.dart';

class ExamQuestion extends StatelessWidget {
  PageController pageController = PageController(initialPage: 0);
  MicStream micStream = MicStream();

  @override
  Widget build(BuildContext context) {

    List<Widget> pages =[
      ExamQ1(micStream: micStream, goNextPage: (){pageController.animateToPage((pageController.page+1).toInt(), duration: Duration(microseconds: 200000), curve: Curves.bounceInOut);},),
      ExamQ2(goNextPage: (){pageController.animateToPage((pageController.page+1).toInt(), duration: Duration(microseconds: 200000), curve: Curves.bounceInOut);},),
      ExamQ3(goNextPage: (){pageController.animateToPage((pageController.page+1).toInt(), duration: Duration(microseconds: 200000), curve: Curves.bounceInOut);},),
      ExamQ4(goNextPage: (){pageController.animateToPage((pageController.page+1).toInt(), duration: Duration(microseconds: 200000), curve: Curves.bounceInOut);},),
      ExamQ5(micStream: micStream,)
    ];

    return Scaffold(
      body: SafeArea(
        child: Container(
          child: PageView(
            controller: pageController,
            physics: NeverScrollableScrollPhysics(),
            children: pages,
          ),
        ),
      ),
    );
  }
}
