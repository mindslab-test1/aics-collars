import 'package:collarsApp/providers/exam_result.dart';
import 'package:collarsApp/screens/layouts/exam_screen/CommonPaint.dart';
import 'package:collarsApp/screens/layouts/exam_screen/exam_q4.dart';
import 'package:collarsApp/screens/wrap_screen.dart';
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/json_parser/ExamTemp/exam_json3.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:collarsApp/screens/layouts/exam_main.dart';
import 'package:provider/provider.dart';

class ExamQ3 extends StatefulWidget {
  final Function goNextPage;
  ExamQ3({this.goNextPage});

  @override
  _ExamQ3State createState() => _ExamQ3State();
}

class _ExamQ3State extends State<ExamQ3> {

  Color skipBtn = Colors.white;

  TextEditingController _controllerA = TextEditingController();
  TextEditingController _controllerB = TextEditingController();
  ScrollPhysics _physics = ScrollPhysics();
  bool isLoading = false;

  //exam template3
  String _appServer = DotEnv().env['APP_SERVER'];

  //save result to provider
  ExamScore examScore;
  ExamJson3 examJson3;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    examScore = Provider.of<ExamScore>(context, listen: false);
    isLoading = true;

    getExam3();
  }

  @override
  Widget build(BuildContext context) {
    _physics = NeverScrollableScrollPhysics();

    final Size size = MediaQuery.of(context).size;

    return /*Scaffold(
        body: WillPopScope(
          onWillPop: () => Common().onWillPop(context, WrapScreen.id, "시험을 종료하시겠습니까?",(){}),
          child: */Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Color.fromRGBO(255, 87, 159, 0.78), Color.fromRGBO(168, 87, 213, 0.98),Color.fromRGBO(161, 87, 217, 1), Color.fromRGBO(80, 67, 255, 1)]
          )
      ),
      width: size.width,
      height: size.height,
      child:isLoading?Common().spinkit(size, "로딩중입니다.", Colors.white): Padding(
        padding: new EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
        child: SingleChildScrollView(
          child: isLoading ? Container() : Padding(
            padding: EdgeInsets.fromLTRB(25, 0, 25, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,

              children: <Widget>[
                SizedBox(
                  height: size.height*0.05,
                ),
                Text("테스트", style: TextStyle(fontSize: 11, color: Colors.white, fontWeight: FontWeight.bold),),
                CustomPaint(
                  size: Size(size.width, size.height*0.1),
                  painter: MyPainter(exam: 3),
                ),

                Text("적절하지 않은 표현을 터치해서 하이라트 해주세요.", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white), textAlign: TextAlign.center,),

                SizedBox(
                  height: size.height*0.05,
                ),

                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                      boxShadow: [
                        BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.3),
                            blurRadius: 4.0,
                            offset: Offset(10,10)
                        )
                      ]
                  ),
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(24, 24, 24, 24),
                    child: Column(
                      children: <Widget>[
                        Center(child: Text("A",
                          style: TextStyle(
                              color: Color(0xff5262f2),
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                              fontFamily: ''
                          ),
                        )),
                        TextFormField(
                          textAlign: TextAlign.center,
                          maxLines: null,
                          style: TextStyle(fontSize: 18, height: 1.4, fontWeight: FontWeight.bold),
                          controller: _controllerA,
                          readOnly: true,
                          scrollPhysics: _physics,
                          decoration: InputDecoration(
                              border: InputBorder.none
                          ),
                          toolbarOptions: ToolbarOptions(
                              copy: false,
                              cut: false,
                              paste: false,
                              selectAll: false
                          ),
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        Center(child: Text("B",
                          style: TextStyle(
                              color: Color(0xff5262f2),
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'NotoSansCJKkr-Bold'
                          ),)),
                        TextFormField(
                          textAlign: TextAlign.center,
                          maxLines: null,
                          style: TextStyle(fontSize: 18, height: 1.3, fontWeight: FontWeight.bold),
                          controller: _controllerB,
                          readOnly: true,
                          scrollPhysics: _physics,
                          decoration: InputDecoration(
                              border: InputBorder.none
                          ),
                          toolbarOptions: ToolbarOptions(
                              copy: false,
                              cut: false,
                              paste: false,
                              selectAll: false
                          ),
                        )

                      ],
                    ),
                  ),
                ),

                SizedBox(height: size.height*0.03,),

                Text("다음 대화를 읽고 비즈니스 대화라는 점을 감안하여 적절하지 않은 표현을 터치해서 하이라이트 해주세요.",
                  style: TextStyle(fontSize: 14, color: Colors.white), textAlign: TextAlign.center,
                ),

                SizedBox(
                  height: size.height * 0.05,
                ),

                Container(
                  width: size.width*0.347,
                  height: size.height*0.072,
                  child: RaisedButton(
                    onPressed: (){getResult();},
                    color: Colors.white,
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Center(
                        child: Text("OK",
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: 'NotoSansCJKkr-Bold',
                              color: Color(0xff5244fe),
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                    ),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(30),
                  child: GestureDetector(
                    onTapDown: (TapDownDetails tapDown) {
                      skipTest();
                    },
                    child: Text("잘 모르겠어요",
                      style: TextStyle(color: skipBtn,
                          fontSize: 15,
                          decoration: TextDecoration.underline),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    )/*,
        )
    )*/;
  }

  Future<void> getExam3() async{
    Map<String, dynamic> request = {};
    ExamJson3 examJson3Res = ExamJson3.fromJson(await Common().httpRequest(_appServer+"/maumAi/exam/q3/getQuestion", request));

    setState(() {
      examJson3 = examJson3Res;
      _controllerA.text = examJson3.qtext1;
      _controllerB.text = examJson3.qtext2;

      isLoading = false;
    });
  }

  Future<void> getResult() async{
    if(!_controllerA.selection.isValid && !_controllerB.selection.isValid) return;

    String answer = "";

    if(_controllerA.selection.isValid) {
      int baseOffset = _controllerA.selection.baseOffset;
      int extentOffset = _controllerA.selection.extentOffset;

      if(baseOffset<=extentOffset) answer = _controllerA.text.substring(baseOffset, extentOffset);
      else answer = _controllerA.text.substring(extentOffset, baseOffset);
    }
    else if(_controllerB.selection.isValid){
      int baseOffset = _controllerB.selection.baseOffset;
      int extentOffset = _controllerB.selection.extentOffset;

      if(baseOffset<=extentOffset) answer = _controllerB.text.substring(baseOffset, extentOffset);
      else answer = _controllerB.text.substring(extentOffset, baseOffset);
    }

    if (answer != "") {
      Map<String, dynamic> request = {
        "examNum" : examJson3.examNum,
        "answer" : answer
      };

      int finalScore = int.parse(await Common().httpRequest(_appServer+"/maumAi/exam/q3/getScore", request));

      setState(() {
        examScore.setCulture(finalScore.toDouble());
      });

      Common().showPopup(
          examScore.culture,
          "Answer",
          examJson3.correct.split("@@")[0],
          "You highlighted",
          answer,
          context,
          reAssamblePage,
          goNextTest,
          true
      );
    }
  }

  void reAssamblePage(){
    _controllerA.clear();
    _controllerB.clear();
    _controllerA.text = examJson3.qtext1;
    _controllerB.text = examJson3.qtext2;

    setState(() {
      skipBtn = Colors.white;
    });
  }

  void goNextTest(){
    //save exam number in provider
    examScore.addExamlist(examJson3.examNum);

    widget.goNextPage();
  }

  void skipTest(){
    setState(() {
      skipBtn = Colors.purpleAccent;
      examScore.setCulture(0);
    });

    Common().showSkipPopup(context, reAssamblePage, goNextTest, true);
  }
}
