import 'dart:convert';

import 'package:collarsApp/providers/exam_result.dart';
import 'package:collarsApp/screens/components/cards/template/tmp4_card.dart';
import 'package:collarsApp/screens/layouts/exam_screen/CommonPaint.dart';
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/json_parser/ExamTemp/exam_json4.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import '../exam_main.dart';

class ExamQ4 extends StatefulWidget {
  final Function goNextPage;
  ExamQ4({this.goNextPage});

  @override
  _ExamQ4State createState() => _ExamQ4State();
}

class _ExamQ4State extends State<ExamQ4> {

  bool isLoading = false;

  String _appServer = DotEnv().env['APP_SERVER'];
  ExamJson4 examJson4;

  //save result to provider
  ExamScore examScore;

  @override
  void initState() {
    super.initState();

    examScore = Provider.of<ExamScore>(context, listen: false);
    isLoading = true;

    getExam4();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Color.fromRGBO(255, 87, 159, 0.78), Color.fromRGBO(168, 87, 213, 0.98),Color.fromRGBO(161, 87, 217, 1), Color.fromRGBO(80, 67, 255, 1)]
          )
      ),
      width: size.width,
      height: size.height,
      child: isLoading?Common().spinkit(size, "로딩중입니다.", Colors.white): Padding(
        padding: new EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
        child: SingleChildScrollView(
          child: isLoading? Container() : Padding(
            padding: EdgeInsets.fromLTRB(25, 0, 25, 0),
            child: Tmp4ColumCard(
              size: size,
              examJson4: examJson4,
              goNextPage: goNextPage,
              getResult: getResult,
              setExamScoreZero: setExamScoreZero,
              isExam: true,
            ),
          ),
        ),
      ),
    );
  }

  Future<void> getExam4() async{
    Map<String, dynamic> request = {};
    ExamJson4 examJson4Res = ExamJson4.fromJson(await Common().httpRequest(_appServer+"/maumAi/exam/q4/getQuestion", request));

    setState(() {
      examJson4 = examJson4Res;
      isLoading = false;
    });
  }

  void goNextPage(){
    //save exam number in provider
    examScore.addExamlist(examJson4.examNum);

    widget.goNextPage();
  }

  void setExamScoreZero(){
    setState(() {
      examScore.setManner(0);
    });
  }

  Future<void> getResult(String answer, Function reAssamblePage) async{
    SystemChannels.textInput.invokeMethod('TextInput.hide');

    //String answer = controller.text;
    Map<String, dynamic> request = {
      "examNum" : examJson4.examNum,
      "answer" : answer
    };

    if(answer!=null && answer!=""){
      int finalScore = int.parse(await Common().httpRequest(_appServer+"/maumAi/exam/q4/getScore", request));

      setState(() {
        examScore.setManner(finalScore.toDouble());
      });

      Common().showPopup(
          examScore.manner,
          "Answer",
          examJson4.correctAnswer,
          "You typed",
          answer,
          context,
          reAssamblePage,
          widget.goNextPage,
          true
      );
    }
  }
}
