import 'dart:async';

import 'package:collarsApp/providers/exam_result.dart';
import 'package:collarsApp/providers/user.dart';
import 'package:collarsApp/screens/layouts/main/main_screen.dart';
import 'package:collarsApp/screens/wrap_screen.dart';
import 'package:collarsApp/services/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';
import 'CommonPaint.dart';


class ExamResult extends StatefulWidget {
  static const String id = 'Exam_result';

  @override
  _ExamResultState createState() => _ExamResultState();
}

class _ExamResultState extends State<ExamResult> {
  bool isLoading = true;

  //save result to provider
  String _appServer = DotEnv().env['APP_SERVER'];
  ExamScore examScore;
  User user;
  String totalScore="A";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    user = Provider.of<User>(context, listen: false);
    examScore = Provider.of<ExamScore>(context, listen: false);

    getTotalScore();
    saveExamResult();
    startTimer();
  }

  void startTimer(){
    Timer(Duration(seconds: 1), (){
      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: WillPopScope(
        onWillPop: () => Common().onWillPop(context, WrapScreen.id, "메인 페이지로 이동하시겠습니까?", moveMainPage),
        child: Container(
          width: size.width,
          height: size.height,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Color.fromRGBO(156, 226, 228, 1), Color.fromRGBO(121, 161, 227, 1)]
              )
          ),
          child:isLoading? Common().spinkit(size, "정보를 분석하고 있습니다.", Colors.white) : SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 24,),
                Container(
                  width: size.width,
                  height: size.height*0.067,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 16, right: 16),
                    child: Stack(
                      children: [
                        Align(
                            alignment: Alignment.centerLeft,
                            child: InkWell(
                                onTap: moveMainPage,
                                child: Icon(Icons.arrow_back_ios, color: Colors.white,)
                            )
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Text("결과",
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.white,
                              fontFamily: 'NotoSansCJKkr',
                              fontWeight: FontWeight.w500
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: size.height*0.031),
                  child: Text(
                    "칼라스 인공지능 \n테스트 결과입니다.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                        fontFamily: 'NotoSansCJKkr',
                    ),
                  ),
                ),

                Text(
                  totalScore,
                  style: TextStyle(
                    fontSize: 102,
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'NotoSansCJKkr'
                  ),
                ),

                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(25),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0x47000000),
                          blurRadius: 10,
                          offset: Offset(10, 10)
                      )
                    ]
                  ),
                  width: size.width*0.867,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 13, bottom: 13),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        scoreResult(examScore.pron, "발음", size),
                        scoreResult(examScore.culture, "문화적 소양", size),
                        scoreResult(examScore.manner, "매너", size),
                        scoreResult(examScore.express, "표현력", size),
                        scoreResult(examScore.voca, "어휘", size),
                        scoreResult(examScore.accuracy, "정확도", size),

                      ],
                    ),
                  ),
                ),

                SizedBox(
                  height: size.height*0.057,
                ),

                Container(
                  width: size.width*0.9,
                  height: size.height*0.4,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.transparent,
                  ),
                  child: Center(
                    child: CustomPaint(
                      size: Size(size.width*0.42, size.height),
                      painter: ResultHexagon(examScore: examScore),
                    ),
                  ),
                ),

                SizedBox(
                  height: size.height*0.057,
                ),

                //todo change popUtil
                RaisedButton(
                  onPressed: moveMainPage,
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)
                  ),
                  child: Padding(
                    padding: EdgeInsets.only(left: 56.4, right: 56.4, top: 18, bottom: 18),
                    child: Text(
                      "칼라스 AI 강의추천 받기",
                      style: TextStyle(
                        fontSize: 14.1,
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(82, 68, 254, 1)
                      ),
                    ),
                  ),
                ),

                SizedBox(
                  height: size.height*0.05,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void moveMainPage(){
    user.first ="Y";
    int count = 0;
    Navigator.popUntil(context, (route) {
      return count++ == 3;
    });

    Navigator.pushReplacementNamed(context, MainScreen.id);
  }

  Future<void> saveExamResult() async{
    Map<String, dynamic> request = {
      "uid" : user.uid,
      "examNumList" : examScore.examNumList,
      "accuracy" : examScore.accuracy=="D"?"C":examScore.accuracy,
      "pron" : examScore.pron=="D"?"C":examScore.pron,
      "voca" : examScore.voca=="D"?"C":examScore.voca,
      "manner" : examScore.manner=="D"?"C":examScore.manner,
      "express" : examScore.express=="D"?"C":examScore.express,
      "culture" : examScore.culture=="D"?"C":examScore.culture
    };

    await Common().httpRequest(_appServer+"/maumAi/exam/save/examResult", request);
  }

  getTotalScore(){
    print("GET TOTAL SCORE TEST : ${examScore.accuracy}, ${examScore.pron}, ${examScore.voca}, ${examScore.manner}, ${examScore.express}, ${examScore.culture}");

    int score =0;
    score += getScore(examScore.accuracy);
    score += getScore(examScore.pron);
    score += getScore(examScore.voca);
    score += getScore(examScore.manner);
    score += getScore(examScore.express);
    score += getScore(examScore.culture);

    setState(() {
      if((score~/6).toInt() == 3) totalScore = "A";
      if((score~/6).toInt() == 2) totalScore = "B";
      if((score~/6).toInt() == 1) totalScore = "C";
    });
  }

  int getScore(String score){
    if(score == "A") return 3;
    else if(score == "B") return 2;
    else return 1;
  }

  Widget scoreResult(String score, String test, Size size) => Container(
    width: size.width*0.087,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(score,
          style: TextStyle(
              fontFamily: 'Raleway',
              fontWeight: FontWeight.w500,
              fontSize: size.height*0.055,
              color: Color.fromRGBO(82, 98, 242, 1)),
        ),
        Text(test,
          style: TextStyle(
              fontSize: 11,
              fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        )
      ],
    ),
  );
}
