import 'package:collarsApp/providers/exam_result.dart';
import 'package:collarsApp/screens/layouts/exam_screen/exam_result.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyPainter extends CustomPainter{
  int exam;
  MyPainter({this.exam});

  @override
  void paint(Canvas canvas, Size size) {

    Paint complete = Paint()
      ..color = Colors.white
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 10.0;

    Offset p1 = Offset(size.width*0.375, size.height*0.5);
    Offset p2 = Offset(size.width*0.431, size.height*0.5);
    Offset p3 = Offset(size.width*0.486, size.height*0.5);
    Offset p4 = Offset(size.width*0.542, size.height*0.5);
    Offset p5 = Offset(size.width*0.597, size.height*0.5);

    canvas.drawCircle(p1, 5, complete);
    canvas.drawCircle(p2, 5, complete);
    canvas.drawCircle(p3, 5, complete);
    canvas.drawCircle(p4, 5, complete);
    canvas.drawCircle(p5, 5, complete);

    if(exam>=2){
      canvas.drawLine(p1, p2, complete);
    }
    if(exam>=3){
      canvas.drawLine(p2, p3, complete);
    }
    if(exam>=4){
      canvas.drawLine(p3, p4, complete);
    }
    if(exam>=5){
      canvas.drawLine(p4, p5, complete);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}


/*육각형 그래프*/
class ResultHexagon extends CustomPainter{
  ExamScore examScore;
  ResultHexagon({this.examScore});

  int getOffset(String score){
    if(score == "A") return 8;
    if(score == "B") return 6;
    if(score == "C") return 3;
    else return 1;
  }

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color=Colors.white.withOpacity(0.3)
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 2.0;

    Paint polygonPaint = Paint()
      ..color = Color.fromRGBO(82, 68, 254, 0.4)..strokeWidth = 5.0..style = PaintingStyle.fill;


    List<double> standardPron = [0.4, 0.4];
    List<double> standardManner = [0.3, 0.5];
    List<double> standardExpress = [0.4, 0.6];
    List<double> standardQuality = [0.6, 0.6];
    List<double> standardVoca = [0.7, 0.5];
    List<double> standardAccuracy = [0.6, 0.4];

    List<Offset> vertextPron = new List<Offset>();
    List<Offset> vertextManner = new List<Offset>();
    List<Offset> vertextExpress = new List<Offset>();
    List<Offset> vertextQuality = new List<Offset>();
    List<Offset> vertextVoca = new List<Offset>();
    List<Offset> vertextAccuracy = new List<Offset>();


    for(int i=0;i<13;i++){
      Offset pron = Offset(size.width*(standardPron[0] - (i * 0.04)),
          size.height*(standardPron[1] - (i*0.036))
      );

      Offset manner = Offset(size.width*(standardManner[0] - (i*0.07)),
          size.height*standardManner[1]
      );

      Offset express = Offset(size.width*(standardExpress[0] - (i*0.04)),
          size.height*(standardExpress[1] + (i*0.036))
      );

      Offset quality = Offset(size.width*(standardQuality[0] + (i*0.04)),
          size.height*(standardQuality[1] + (i*0.036))
      );

      Offset voca = Offset(size.width*(standardVoca[0] + (i*0.07)),
          size.height*standardVoca[1]
      );

      Offset accuacy = Offset(size.width*(standardAccuracy[0] + (i*0.04)),
          size.height*(standardAccuracy[1] - (i*0.036))
      );

      vertextPron.add(pron);
      vertextManner.add(manner);
      vertextExpress.add(express);
      vertextQuality.add(quality);
      vertextVoca.add(voca);
      vertextAccuracy.add(accuacy);

    }

    for(int i=0; i<9; i++){
      double circle = 1;
      if(i==8) circle=5;

      canvas.drawCircle(vertextPron[i], circle, paint);
      canvas.drawCircle(vertextManner[i], circle, paint);
      canvas.drawCircle(vertextExpress[i], circle, paint);
      canvas.drawCircle(vertextQuality[i], circle, paint);
      canvas.drawCircle(vertextVoca[i], circle, paint);
      canvas.drawCircle(vertextAccuracy[i], circle, paint);

      canvas.drawLine(vertextPron[i], vertextManner[i], paint);
      canvas.drawLine(vertextManner[i], vertextExpress[i], paint);
      canvas.drawLine(vertextExpress[i], vertextQuality[i], paint);
      canvas.drawLine(vertextQuality[i], vertextVoca[i], paint);
      canvas.drawLine(vertextVoca[i], vertextAccuracy[i], paint);
      canvas.drawLine(vertextAccuracy[i], vertextPron[i], paint);
    }

    List<Offset> polygon = List<Offset>();
    polygon.add(vertextPron[getOffset(examScore.pron)]);
    polygon.add(vertextManner[getOffset(examScore.manner)]);
    polygon.add(vertextExpress[getOffset(examScore.express)]);
    polygon.add(vertextQuality[getOffset(examScore.culture)]);
    polygon.add(vertextVoca[getOffset(examScore.voca)]);
    polygon.add(vertextAccuracy[getOffset(examScore.accuracy)]);
    polygon.add(vertextPron[getOffset(examScore.pron)]);


    Path path = Path();
    path.moveTo(vertextPron[getOffset(examScore.pron)].dx, vertextPron[getOffset(examScore.pron)].dy);
    path.lineTo(vertextManner[getOffset(examScore.manner)].dx, vertextManner[getOffset(examScore.manner)].dy);
    path.lineTo(vertextExpress[getOffset(examScore.express)].dx, vertextExpress[getOffset(examScore.express)].dy);
    path.lineTo(vertextQuality[getOffset(examScore.culture)].dx, vertextQuality[getOffset(examScore.culture)].dy);
    path.lineTo(vertextVoca[getOffset(examScore.voca)].dx, vertextVoca[getOffset(examScore.voca)].dy);
    path.lineTo(vertextAccuracy[getOffset(examScore.accuracy)].dx, vertextAccuracy[getOffset(examScore.accuracy)].dy);
    path.lineTo(vertextPron[getOffset(examScore.pron)].dx, vertextPron[getOffset(examScore.pron)].dy);

    canvas.drawPath(path, polygonPaint);



    final textStyle = TextStyle(
        color: Colors.white,
        fontSize: 14.1,
    );

    final textSpanPron = TextSpan(text: '발음', style: textStyle);
    final textSpanManner = TextSpan(text: '매너', style: textStyle);
    final textSpanExpress = TextSpan(text: '표현력', style: textStyle);
    final textSpanQuality = TextSpan(text: '문화적 소향', style: textStyle);
    final textSpanVoca = TextSpan(text: '어휘', style: textStyle);
    final textSpanAccuacy = TextSpan(text: '정확도', style: textStyle);

    final textPainterPron = TextPainter(text: textSpanPron, textDirection:  TextDirection.ltr);
    final textPainterManner = TextPainter(text: textSpanManner, textDirection:  TextDirection.ltr);
    final textPainterExpress = TextPainter(text: textSpanExpress, textDirection:  TextDirection.ltr);
    final textPainterQuality = TextPainter(text: textSpanQuality, textDirection:  TextDirection.ltr);
    final textPainterVoca = TextPainter(text: textSpanVoca, textDirection:  TextDirection.ltr);
    final textPainterAccuacy = TextPainter(text: textSpanAccuacy, textDirection:  TextDirection.ltr);

    textPainterPron.layout(minWidth: 0, maxWidth: size.width);
    textPainterPron.paint(canvas, vertextPron[11]);

    textPainterManner.layout(minWidth: 0, maxWidth: size.width);
    textPainterManner.paint(canvas, Offset(vertextManner[11].dx-size.width*0.02, vertextManner[11].dy-size.height*0.04));

    textPainterExpress.layout(minWidth: 0, maxWidth: size.width);
    textPainterExpress.paint(canvas, Offset(vertextExpress[11].dx, vertextExpress[10].dy-size.height*0.04));

    textPainterQuality.layout(minWidth: 0, maxWidth: size.width);
    textPainterQuality.paint(canvas, Offset(vertextQuality[11].dx-size.width*0.4, vertextQuality[10].dy-size.height*0.04));

    textPainterVoca.layout(minWidth: 0, maxWidth: size.width);
    textPainterVoca.paint(canvas, Offset(vertextVoca[9].dx-size.width*0.03, vertextVoca[9].dy-size.width*0.08));

    textPainterAccuacy.layout(minWidth: 0, maxWidth: size.width);
    textPainterAccuacy.paint(canvas, Offset(vertextAccuracy[4].dx, vertextAccuracy[11].dy));

  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class LectureBackGround extends CustomPainter{
  @override
  void paint(Canvas canvas, Size size) {
    // TODO: implement paint
    Paint paint = Paint()..color = Color.fromRGBO(239, 240, 241, 1);
    //Paint paint = Paint()..color = Colors.red;
    canvas.drawCircle(Offset(size.width*0.5, size.height*0.94), size.height*0.5, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return false;
  }
}