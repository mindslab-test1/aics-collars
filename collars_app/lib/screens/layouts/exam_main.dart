import 'package:collarsApp/providers/user.dart';
import 'package:collarsApp/screens/layouts/exam_screen//exam_q1.dart';
import 'package:collarsApp/screens/layouts/exam_screen/exam_question.dart';
import 'package:collarsApp/screens/layouts/main/main_screen.dart';
import 'package:collarsApp/styles/color/default.dart';
import 'package:collarsApp/styles/size/default.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class ExamMain extends StatefulWidget {
  static const String id = 'Exam_main';

  @override
  _ExamMainState createState() => _ExamMainState();
}

class _ExamMainState extends State<ExamMain> {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    double introHeight = size.height * 0.415;
    double introWidth = size.width * 0.834;

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        title: SvgPicture.asset(
          'assets/images/logo/icon_01.svg',
          width: kAppBarLogoSize,
          height: kAppBarLogoSize,
          color: kMainWhite,
        ),
        leading: Navigator.canPop(context)
            ? GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: SvgPicture.asset(
                  'assets/images/icon/icon_back.svg',
                  color: kMainWhite,
                ),
              )
            : null,
        backgroundColor: Colors.transparent,
        bottomOpacity: 0.0,
        elevation: 0.0,
      ),
      body: Container(
        padding: kDefaultPaddingHorizontal,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
              Color.fromRGBO(255, 87, 159, 0.78),
              Color.fromRGBO(168, 87, 213, 0.98),
              Color.fromRGBO(161, 87, 217, 1),
              Color.fromRGBO(80, 67, 255, 1)
            ],
                stops: [
              0,
              0.39,
              0.51,
              1
            ])),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: kDefaultPaddingAll,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25),
                  color: Color(0x40ffffff),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "환영합니다.",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w800,
                              color: Colors.white,
                              fontFamily: 'NotoSansCJKkr',
                            ),
                          ),
                          SizedBox(
                            height: 12,
                          ),
                          Container(
                              //height: introHeight*0.109,
                              child: Text(
                            "칼라스 인공지능이 여러분의 비즈니스 영어 실력을 테스트합니다.",
                            style: TextStyle(
                                fontSize: 12,
                                color: Colors.white,
                                fontFamily: 'NotoSansCJKkr'),
                          )),
                          SizedBox(height: 12),
                          Container(
                              //height: introHeight*0.109,
                              child: Text(
                            "정확한 측정을 위해 다음 테스트를 시작하겠습니다.",
                            style: TextStyle(
                                fontSize: 12,
                                color: Colors.white,
                                fontFamily: 'NotoSansCJKkr'),
                          )),
                          SizedBox(height: 12),
                          Container(
                              //height: introHeight*0.274,
                              child: Text(
                            "1. 지문읽기\n2. 부적절한 어휘 찾기\n3. 부적절한 표현 찾기\n4. 문장 바꿔 쓰기\n5. 적절한 표현 말하기",
                            style: TextStyle(
                                fontSize: 12,
                                color: Colors.white,
                                fontFamily: 'NotoSansCJKkr'),
                          ))
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    Container(
                      width: 143,
                      height: 52,
                      child: FlatButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50.0),
                        ),
                        color: Colors.white,
                        child: Text(
                          "Start",
                          style: TextStyle(
                              fontFamily: 'NotoSansCJKkr',
                              color: kMainIndigo,
                              fontWeight: FontWeight.bold,
                              fontSize: 14),
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ExamQuestion()),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 24,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.canPop(context)
                      ? Navigator.of(context).pop()
                      : Navigator.pushReplacementNamed(context, MainScreen.id);
                },
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(color: Colors.white, width: 1.0),
                    ),
                  ),
                  child: Text(
                    "다음에 할래요",
                    style: TextStyle(
                        fontFamily: 'NotoSansCJKkr',
                        color: Colors.white,
                        fontSize: 14),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
