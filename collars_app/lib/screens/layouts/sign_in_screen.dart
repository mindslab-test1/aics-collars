import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

// Screen
import 'package:collarsApp/screens/components/buttons/sign_in_button.dart';

// Service
import 'package:collarsApp/services/auth.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kakao_flutter_sdk/all.dart';

// Color
import 'package:collarsApp/styles/color/default.dart';

class SignInScreen extends StatelessWidget {
  static const String id = 'sign_in_screen';

  AuthService _authService = AuthService();

  @override
  Widget build(BuildContext context) {
    KakaoContext.clientId = DotEnv().env['KAKAO_CLIENT_ID'];

    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: kPrimaryGradient,
        ),
        height: double.infinity,
        child: Stack(
          children: <Widget>[
            // CREATE ACCOUNT BG IMAGES
            Positioned(
              top: 0,
              left: 0,
              child: SvgPicture.asset('assets/images/bg/back_signin.svg',fit: BoxFit.fitWidth,alignment: Alignment.topLeft,)
            ),
            // SOCIAL LOGIN BUTTONS
            Positioned(
              width: screenWidth,
              height: screenHeight,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 48),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(flex: 1,child: Container(),),
                    Text('Create\nAccount', style: TextStyle(fontSize: 36,color: Colors.white),),
                    Expanded(
                      flex: 3,
                      child: Container(
                        width: double.infinity,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SignInButton(
                              icon: 'assets/images/social/naver.svg',
                              title: '네이버 계정으로 시작하기',
                              onClick: _authService.signInWithNaver,
                              color: Colors.white,
                              bgColor: kNaver,
                              iconColor: kNaverIcon,
                            ),
                            SignInButton(
                              icon: 'assets/images/social/google.svg',
                              title: '구글로 시작하기',
                              onClick: _authService.signInWithGoogle,
                              color: Colors.white,
                              bgColor: kGoogle,
                              iconColor: kGoogleIcon,
                            ),
                            SignInButton(
                              icon: 'assets/images/social/kakao.svg',
                              title: '카카오톡으로 시작하기',
                              onClick: _authService.signInWithKakao,
                              color: Colors.black87,
                              bgColor: kKakao,
                              iconColor: kKakaoIcon,
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
