import 'package:collarsApp/services/notice.dart';
import 'package:collarsApp/styles/color/default.dart';
import 'package:collarsApp/styles/size/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

class NoticeScreen extends StatefulWidget {
  static const String id = "notice_screen";

  @override
  _NoticeScreenState createState() => _NoticeScreenState();
}

class _NoticeScreenState extends State<NoticeScreen> {
  NoticeService noticeService = NoticeService();

  List<dynamic> data = [];
  int currentLength = 0;
  final int incremnet = 20;
  bool isLoading = false;

  @override
  void initState() {
    _loadData();
    super.initState();
  }

  Future _loadData() async {
    setState(() {
      isLoading = true;
    });
    List<dynamic> newData =
        await noticeService.getNotice(currentLength, incremnet);
    await new Future.delayed(const Duration(seconds: 2));
    if (data == null) {
      data = newData;
    } else if (newData == null) {
      // DO NOTHING
    } else {
      data = data + newData;
    }

    setState(() {
      isLoading = false;
      currentLength = data.length;
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context)
        .copyWith(dividerColor: Colors.transparent, accentColor: Colors.black);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          '공지사항',
          style: TextStyle(
            fontFamily: 'NotoSansCJKkr',
            fontSize: 12,
            color: kMainBlack,
          ),
        ),
        leading: Navigator.canPop(context)
            ? GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: SvgPicture.asset(
                  'assets/images/icon/icon_back.svg',
                  color: kMainBlack,
                ),
              )
            : null,
        backgroundColor: Colors.transparent,
        bottomOpacity: 0.0,
        elevation: 0.0,
      ),
      body: SafeArea(
        child: Theme(
          data: theme,
          child: Padding(
            padding: kDefaultPaddingSymmetric,
            child: LazyLoadScrollView(
              isLoading: isLoading,
              onEndOfPage: () => _loadData(),
              child: ListView.builder(
                itemCount: data.length + 1,
                itemBuilder: (context, position) {
                  if (isLoading && position == data.length) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else if (position == data.length) {
                    return Container();
                  } else {
                    return ListTileTheme(
                        contentPadding: EdgeInsets.all(0),
                        child: ExpansionTileItem(data[position]));
                  }
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget ExpansionTileItem(dynamic itemData) {
    return Container(
      child: ExpansionTile(
        title: Text(
          itemData['title'],
          style: TextStyle(
            fontFamily: 'NotoSansCJKkr',
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: kMainBlack,
          ),
        ),
        subtitle: Text(
          itemData['createDate'],
          style: TextStyle(
            fontFamily: 'NotoSansCJKkr',
            color: Colors.grey,
            fontSize: 12,
          ),
        ),
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              border: Border(
                  top: BorderSide(width: 2, color: kGrayBorder),
                  bottom: BorderSide(width: 2, color: kGrayBorder)),
              color: kMainGray,
            ),
            width: double.infinity,
            child: Text(
              itemData['contents'],
              style: TextStyle(
                fontFamily: 'NotoSansCJKkr',
                fontSize: 14,
              ),
            ),
          )
        ],
      ),
    );
  }
}
