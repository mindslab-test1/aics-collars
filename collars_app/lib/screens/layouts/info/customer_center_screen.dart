import 'package:collarsApp/screens/components/buttons/icon_text_button.dart';
import 'package:collarsApp/screens/components/buttons/svg_text_button.dart';
import 'package:collarsApp/styles/color/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';

class CustomerCenterScreen extends StatelessWidget {
  static const String id = 'customer_center_screen';

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            '고객센터',
            style: TextStyle(
              fontFamily: 'NotoSansCJKkr',
              fontSize: 12,
              color: kMainWhite,
            ),
          ),
          leading: Navigator.canPop(context)
              ? GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: SvgPicture.asset(
                    'assets/images/icon/icon_back.svg',
                    color: kMainWhite,
                  ),
                )
              : null,
          backgroundColor: Colors.transparent,
          bottomOpacity: 0.0,
          elevation: 0.0,
        ),
        body: Container(
          decoration: BoxDecoration(
            gradient: kPrimaryGradient,
          ),
          height: double.infinity,
          child: Stack(
            children: [
              Positioned(
                  top: 0,
                  left: 0,
                  child: SvgPicture.asset(
                    'assets/images/bg/back_signin.svg',
                    fit: BoxFit.fitWidth,
                    alignment: Alignment.topLeft,
                  )),
              Positioned(
                width: screenWidth,
                height: screenHeight,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Expanded(
                      flex: 3,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 30.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 164),
                            Text(
                              'Customer\nCenter',
                              style: TextStyle(
                                color: kMainWhite,
                                fontSize: 35,
                              ),
                            ),
                            SizedBox(height: 20),
                            IconTextButton(
                              icon: Icon(Icons.phone,
                                  color: Colors.white, size: 30.0),
                              text: Text(
                                "1600-5261",
                                style: TextStyle(
                                  color: kMainWhite,
                                  fontSize: 16,
                                ),
                              ),
                              onPressed: () {
                                launch("tel:1600 5261");
                              },
                            ),
                            SizedBox(height: 8),
                            IconTextButton(
                              icon: Icon(Icons.email,
                                  color: Colors.white, size: 30.0),
                              text: Text(
                                "collars@collars.co.kr",
                                style: TextStyle(
                                  color: kMainWhite,
                                  fontSize: 16,
                                ),
                              ),
                              onPressed: () {
                                final Uri _emailLaunchUri = Uri(
                                  scheme: 'mailto',
                                  path: 'collars@collars.co.kr',
                                  queryParameters: {'subject': '문의'},
                                );
                                launch(_emailLaunchUri.toString());
                              },
                            ),
                            SizedBox(height: 43),
                            Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom:
                                      BorderSide(color: kMainWhite, width: 1.0),
                                ),
                              ),
                              child: Text(
                                '이용약관',
                                style: TextStyle(
                                  fontFamily: 'NotoSansCJKkr',
                                  color: kMainWhite,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                            SizedBox(height: 10),
                            Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom:
                                      BorderSide(color: kMainWhite, width: 1.0),
                                ),
                              ),
                              child: Text(
                                '개인정보취급방침',
                                style: TextStyle(
                                  fontFamily: 'NotoSansCJKkr',
                                  color: kMainWhite,
                                  fontSize: 14,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
