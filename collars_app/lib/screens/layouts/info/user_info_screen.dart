import 'package:collarsApp/styles/color/default.dart';
import 'package:collarsApp/styles/size/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

// Provider
import 'package:provider/provider.dart';
import 'package:collarsApp/providers/user.dart';

// Screen
import 'package:collarsApp/screens/components/cards/hello_card.dart';

class UserInfoScreen extends StatelessWidget {
  static const String id = 'user_info_screen';

  @override
  Widget build(BuildContext context) {
    User _user = Provider.of<User>(context);
    DateTime _signUpDate = _user.signUpDate;

    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          '내정보',
          style: TextStyle(
            fontFamily: 'NotoSansCJKkr',
            fontSize: 12,
            color: kMainBlack,
          ),
        ),
        leading: Navigator.canPop(context)
            ? GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: SvgPicture.asset(
                  'assets/images/icon/icon_back.svg',
                  color: kMainBlack,
                ),
              )
            : null,
        backgroundColor: Colors.transparent,
        bottomOpacity: 0.0,
        elevation: 0.0,
      ),
      body: SafeArea(
          child: Stack(
        children: [
          Positioned(
            bottom: 0,
            right: 0,
            child: SvgPicture.asset(
              'assets/images/bg/info_bg.svg',
              fit: BoxFit.cover,
              alignment: Alignment.bottomRight,
            ),
          ),
          Positioned(
            width: screenWidth,
            height: screenHeight,
            child: Container(
              padding: kDefaultPaddingSymmetric,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  HelloCard(),
                  SizedBox(height: 10),
                  Text(
                    '${_user.email}',
                    style: TextStyle(
                      fontFamily: 'NotoSansCJKkr',
                      fontSize: 20,
                      color: kMainBlack,
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    '${_signUpDate.year}년 ${_signUpDate.month}월 ${_signUpDate.day}일 가입',
                    style: TextStyle(
                      fontFamily: 'NotoSansCJKkr',
                      fontSize: 20,
                      color: kMainBlack,
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      )),
    );
  }
}
