import 'package:collarsApp/screens/components/cards/evereading_card.dart';
import 'package:collarsApp/screens/layouts/evereading/evereading_content_screen.dart';
import 'package:collarsApp/services/media_server.dart';
import 'package:collarsApp/styles/color/default.dart';
import 'package:flutter/material.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:provider/provider.dart';

// Provider
import 'package:collarsApp/providers/evereading_main_provider.dart';

class EvereadingBody extends StatelessWidget {
  MediaServer _mediaServer = MediaServer();

  @override
  Widget build(BuildContext context) {
    EvereadingMainProvider emp = Provider.of<EvereadingMainProvider>(context);

    return Container(
      color: kColumnBg,
      child: LazyLoadScrollView(
        isLoading: emp.isLoading,
        onEndOfPage: () {},
        child: ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: emp.columns.length + 1,
          itemBuilder: (context, position) {
            if (position == emp.columns.length) {
              return Container(
                padding: EdgeInsets.only(top: 18, bottom: 30),
                child: Column(
                  children: [
                    GestureDetector(
                      onTap: () => {emp.loadColumnData()},
                      child: Container(
                        width: 250,
                        height: 55,
                        decoration: BoxDecoration(
                          border:
                              Border.all(width: 1, color: Color(0xff707070)),
                          borderRadius: BorderRadius.circular(30),
                        ),
                        child: Center(
                          child: Text("더 보기"),
                        ),
                      ),
                    ),
                    SizedBox(height: 60),
                    Text('© Copyright (주)칼라스컴퍼니. All Rights Reserved.')
                  ],
                ),
              );
            } else {
              return GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) =>
                          EvereadingContentScreen(data: emp.columns[position]),
                    ),
                  );
                },
                child: EvereadingCard(data: emp.columns[position]),
              );
            }
          },
        ),
      ),
    );
  }
}
