import 'package:collarsApp/providers/evereading_main_provider.dart';
import 'package:collarsApp/styles/color/default.dart';
import 'package:collarsApp/styles/size/default.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class EvereadingContentScreen extends StatefulWidget {
  static const String id = 'evereading_content_screen';
  final dynamic data;

  EvereadingContentScreen({Key key, this.data}) : super(key: key);

  @override
  _EvereadingContentScreenState createState() =>
      _EvereadingContentScreenState();
}

class _EvereadingContentScreenState extends State<EvereadingContentScreen> {
  bool isKor = false;
  dynamic data;

  @override
  void initState() {
    setState(() {
      data = widget.data;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget contentBuilder() {
      NumberFormat formatter = new NumberFormat("00");
      dynamic contentData;
      if (isKor) {
        contentData = data["content"]["kor"];
      } else {
        contentData = data["content"]["eng"];
      }
      dynamic vocaData = data["content"]["vocabulary"];
      List<Widget> header = [];
      List<Widget> body = [];
      List<Widget> voca = [];

      body.add(
        Text(
          '${contentData["description"]}',
          style: TextStyle(
            color: kEvereadingMainBlack,
            fontFamily: 'NotoSansCJKkr',
            fontSize: 16,
          ),
        ),
      );
      for (int i = 0; i < contentData["body"].length; i++) {
        header.add(SizedBox(height: 10));
        header.add(
          Text(
            '${formatter.format(i + 1)}\t${contentData["body"][i]["header"]}',
            style: TextStyle(
              color: kEvereadingMainBlack,
              fontFamily: 'THEJung140',
              fontSize: 14,
            ),
          ),
        );
        body.add(SizedBox(
          height: 20,
        ));
        body.add(Text(
          '${i + 1}. ${contentData["body"][i]["header"]}',
          style: TextStyle(
            color: kEvereadingMainBlack,
            fontFamily: 'NotoSansCJKkr',
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
        ));
        body.add(SizedBox(
          height: 10,
        ));
        body.add(
          Text(
            '${contentData["body"][i]["contents"]}',
            style: TextStyle(
              color: kEvereadingMainBlack,
              fontFamily: 'NotoSansCJKkr',
              fontSize: 16,
            ),
          ),
        );
      }
      if (contentData["footer"] != null) {
        body.add(Text(
          '${contentData["footer"]}',
          style: TextStyle(
            color: kEvereadingMainBlack,
            fontFamily: 'NotoSansCJKkr',
            fontSize: 16,
          ),
        ));
      }

      for (int i = 0; i < vocaData.length; i++) {
        voca.add(
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 1,
                child: Text(
                  '${i + 1}. ${vocaData[i]["eng"]}',
                  style: TextStyle(
                    color: kEvereadingMainBlack,
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Text(
                  '${vocaData[i]["kor"]}',
                  style: TextStyle(
                    color: kEvereadingMainBlack,
                    fontFamily: 'NotoSansCJKkr',
                    fontSize: 14,
                  ),
                ),
              ),
            ],
          ),
        );
        voca.add(SizedBox(height: 10));
      }

      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            color: Color(0xfff0f0f0),
            padding: kDefaultPaddingSymmetric,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Contents',
                  style: TextStyle(
                    color: Color(0xff212121),
                    fontFamily: 'NotoSansCJKkr',
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 2),
                ...header
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 24, vertical: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: body,
            ),
          ),
          Container(
            color: Color(0xfff0f0f0),
            padding: kDefaultPaddingSymmetric,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Vocabulary',
                  style: TextStyle(
                    color: kEvereadingMainBlack,
                    fontWeight: FontWeight.bold,
                    fontSize: 19,
                  ),
                ),
                SizedBox(
                  height: 12,
                ),
                Divider(),
                SizedBox(
                  height: 12,
                ),
                ...voca
              ],
            ),
          )
        ],
      );
    }

    Widget suggestColumn() {
      // todo
      return Container(
        padding: EdgeInsets.symmetric(vertical: 20),
        child: Column(
          children: <Widget>[
            Text(
              '추천 칼럼',
              style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 12),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: <Widget>[],
              ),
            ),
          ],
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          '에브리딩',
          style: TextStyle(
            fontFamily: 'NotoSansCJKkr',
            fontSize: 12,
            color: Colors.black,
          ),
        ),
        leading: Navigator.canPop(context)
            ? GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: SvgPicture.asset(
                  'assets/images/icon/icon_back.svg',
                  color: Colors.black,
                ),
              )
            : null,
        backgroundColor: Colors.transparent,
        bottomOpacity: 0.0,
        elevation: 0.0,
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      height: 400,
                      padding: kDefaultPaddingSymmetric,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: NetworkImage(data["image"]),
                            fit: BoxFit.cover),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            data["content"]["eng"]["title"],
                            style: TextStyle(
                                fontFamily: 'NotoSansCJKkr',
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          ),
                          SizedBox(height: 4),
                          Text(
                            data["content"]["kor"]["title"],
                            style: TextStyle(
                                fontFamily: 'NotoSansCJKkr',
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 18),
                          ),
                          SizedBox(height: 4),
                          Text(
                            data["sectorEngTag"],
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ),
                        ],
                      ),
                    ),
                    contentBuilder(),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  isKor = !isKor;
                });
              },
              child: Container(
                width: double.infinity,
                height: kBottomNavigationBarHeight,
                decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(width: 2, color: Color(0xffdfdfdf)),
                  ),
                ),
                child: Center(
                  child: isKor ? Text('Eng') : Text('Kor'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
