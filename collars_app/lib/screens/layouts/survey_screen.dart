import 'package:collarsApp/screens/components/dialog/survey_dialog.dart';
import 'package:collarsApp/screens/layouts/main/main_screen.dart';
import 'package:collarsApp/styles/size/default.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// Provider
import 'package:collarsApp/providers/user.dart';

// Screen
// - Component
import 'package:collarsApp/screens/components/buttons/rounded_button.dart';

// Service
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/survey.dart';

// Style
import 'package:collarsApp/styles/color/default.dart';

class SurveyScreen extends StatefulWidget {
  static const String id = 'survey_screen';

  @override
  _SurveyScreenState createState() => _SurveyScreenState();
}

class _SurveyScreenState extends State<SurveyScreen> {
  PageController _pageController = PageController(
    initialPage: 0,
  );
  Common _common = Common();
  SurveyService _surveyService = SurveyService();
  dynamic surveyData;
  Map<String, dynamic> surveyResult = {};

  @override
  void initState() {
    super.initState();
    _fetchData();
  }

  Future _fetchData() async {
    dynamic _data = await _surveyService.getSurveyData();
    if (_data != null) {
      setState(() {
        surveyData = _data;
        surveyResult = addSurveyResultForm(_data);
      });
    }
  }

  Map<String, dynamic> addSurveyResultForm(dynamic surveyData) {
    Map<String, dynamic> result = {};
    surveyData.forEach((data) {
      if (_common.equalsIgnoreCase(data['single'], 'N')) {
        dynamic checkboxList = {};
        data['answer'].forEach((answer) => checkboxList[answer[0]] = false);
        result[data['id'].toString()] = checkboxList;
      } else {
        result[data['id'].toString()] = null;
      }
    });
    return result;
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    // HEADER
    Widget surveyHeader() {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 20),
        child: Center(
          child: Text(
            '학습설계',
            style: TextStyle(
              fontFamily: 'NotoSansCJKkr',
              fontSize: 12,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      );
    }

    // BODY
    Widget surveyBody(dynamic data, int position) {
      Widget builder;
      if (_common.equalsIgnoreCase(data['type'].toString(), 'grid')) {
        builder = GridView.builder(
          itemCount: data['answer'].length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            childAspectRatio: 2,
          ),
          itemBuilder: (context, index) {
            if (_common.equalsIgnoreCase(data['single'].toString(), 'N')) {
              return Center(
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: RoundedButton(
                    id: data['answer'][index][0],
                    width: 120,
                    height: 55,
                    text: data['answer'][index][1],
                    onPressed: () {
                      setState(() {
                        dynamic resultTemp = surveyResult[data['id'].toString()];
                        int selectedAnswer = 0;
                        resultTemp.forEach((k, v) => {
                          if (v) {selectedAnswer++}
                        });
                        if(surveyResult[data['id'].toString()]
                        [data['answer'][index][0]]){
                          surveyResult[data['id'].toString()]
                          [data['answer'][index][0]] =
                          !surveyResult[data['id'].toString()]
                          [data['answer'][index][0]];
                        }
                        else{
                          if (selectedAnswer < 3) {
                            surveyResult[data['id'].toString()]
                            [data['answer'][index][0]] =
                            !surveyResult[data['id'].toString()]
                            [data['answer'][index][0]];
                          }
                        }
                      });
                    },
                    bgColor: surveyResult[data['id'].toString()]
                            [data['answer'][index][0]]
                        ? Colors.white
                        : Colors.transparent,
                    textColor: surveyResult[data['id'].toString()]
                            [data['answer'][index][0]]
                        ? kPrimaryColor
                        : Colors.white,
                    borderColor: Colors.white,
                  ),
                ),
              );
            } else {
              return Center(
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: RoundedButton(
                    id: data['answer'][index][0],
                    width: 120,
                    height: 55,
                    text: data['answer'][index][1],
                    onPressed: () {
                      setState(() {
                        surveyResult[data['id'].toString()] =
                            data['answer'][index][0];
                      });
                    },
                    bgColor: surveyResult[data['id'].toString()] ==
                            data['answer'][index][0]
                        ? Colors.white
                        : Colors.transparent,
                    textColor: surveyResult[data['id'].toString()] ==
                            data['answer'][index][0]
                        ? kPrimaryColor
                        : Colors.white,
                    borderColor: Colors.white,
                  ),
                ),
              );
            }
          },
        );
      } else {
        builder = ListView.builder(
          shrinkWrap: true,
          itemCount: data['answer'].length,
          itemBuilder: (context, index) {
            if (_common.equalsIgnoreCase(data['single'].toString(), 'N')) {
              return RoundedButton(
                id: data['answer'][index][0],
                width: 120,
                height: 55,
                margin: EdgeInsets.symmetric(vertical: 10.0),
                text: data['answer'][index][1],
                onPressed: () {
                  setState(() {
                    dynamic resultTemp = surveyResult[data['id'].toString()];
                    int selectedAnswer = 0;
                    resultTemp.forEach((k, v) => {
                          if (v) {selectedAnswer++}
                        });
                    if(surveyResult[data['id'].toString()]
                    [data['answer'][index][0]]){
                      surveyResult[data['id'].toString()]
                      [data['answer'][index][0]] =
                      !surveyResult[data['id'].toString()]
                      [data['answer'][index][0]];
                    }
                    else{
                      if (selectedAnswer < 3) {
                        surveyResult[data['id'].toString()]
                        [data['answer'][index][0]] =
                        !surveyResult[data['id'].toString()]
                        [data['answer'][index][0]];
                      }
                    }
                  });
                },
                bgColor: surveyResult[data['id'].toString()]
                        [data['answer'][index][0]]
                    ? Colors.white
                    : Colors.transparent,
                textColor: surveyResult[data['id'].toString()]
                        [data['answer'][index][0]]
                    ? kPrimaryColor
                    : Colors.white,
                borderColor: Colors.white,
              );
            } else {
              return RoundedButton(
                id: data['answer'][index][0],
                width: 120,
                height: 55,
                margin: EdgeInsets.symmetric(vertical: 10.0),
                text: data['answer'][index][1],
                onPressed: () {
                  setState(() {
                    surveyResult[data['id'].toString()] =
                        data['answer'][index][0];
                  });
                },
                bgColor: surveyResult[data['id'].toString()] ==
                        data['answer'][index][0]
                    ? Colors.white
                    : Colors.transparent,
                textColor: surveyResult[data['id'].toString()] ==
                        data['answer'][index][0]
                    ? kPrimaryColor
                    : Colors.white,
                borderColor: Colors.white,
              );
            }
          },
        );
      }

      List<Widget> multiAnswerGuide = [];
      if (_common.equalsIgnoreCase(data['single'].toString(), 'N')) {
        multiAnswerGuide.add(SizedBox(height: 10));
        multiAnswerGuide.add(Text(
          '(최대 3개 복수정답 가능)',
          style: TextStyle(
            fontFamily: 'NotoSansCJKkr',
            fontSize: 16,
            color: Colors.white,
          ),
        ));
      }

      return Expanded(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 19),
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  data['title'],
                  style: TextStyle(
                    fontFamily: 'NotoSansCJKkr',
                    fontSize: 18,
                    color: Colors.white,
                  ),
                ),
              ),
              Container(
                child: Text(
                  data['question'],
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
              ...multiAnswerGuide,
              SizedBox(height: 40),
              Expanded(
                child: builder,
              ),
            ],
          ),
        ),
      );
    }

    // FOOTER
    Widget surveyFooter(int index, int dataLength) {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        child: Column(
          children: <Widget>[
            SizedBox(
              width: 120.0,
              height: 50.0,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50.0),
                ),
                onPressed: () async {
                  dynamic _data = surveyData[index];
                  void nextButtonHandler() async {
                    int nextPage = index + 1;
                    if (nextPage != dataLength) {
                      _pageController.jumpToPage(nextPage);
                    } else {
                      bool isSucceed = await _surveyService.setUserSurveyData(
                          uid: user.uid, surveyResult: surveyResult);
                      if (isSucceed) {
                        Provider.of<User>(context, listen: false).survey = 'Y';
                        Navigator.of(context)
                            .pushReplacementNamed(MainScreen.id);
                      }
                    }
                  }

                  if (_common.equalsIgnoreCase(_data['single'], 'N')) {
                    bool _isChecked = false;
                    surveyResult[_data['id'].toString()].forEach((k, v) {
                      if (v) _isChecked = v;
                    });
                    if (_isChecked) {
                      nextButtonHandler();
                    } else {
                      surveyDialog(context);
                    }
                  } else if (surveyResult[_data['id'].toString()] != null &&
                      _common.equalsIgnoreCase(_data['single'], 'Y')) {
                    nextButtonHandler();
                  } else {
                    surveyDialog(context);
                  }
                },
                color: Colors.white,
                textColor: kPrimaryColor,
                child: Text(
                  "OK".toUpperCase(),
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              '${index + 1}/$dataLength',
              style: TextStyle(fontSize: 18, color: Colors.white),
            ),
          ],
        ),
      );
    }

    return Scaffold(
      body: SafeArea(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(gradient: kPrimaryLightGradient),
          child: surveyData != null
              ? PageView.builder(
                  controller: _pageController,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: surveyData.length,
                  itemBuilder: (context, position) {
                    return Column(
                      children: <Widget>[
                        surveyHeader(),
                        surveyBody(surveyData[position], position),
                        surveyFooter(position, surveyData.length),
                      ],
                    );
                  },
                )
              : Center(child: CircularProgressIndicator()),
        ),
      ),
    );
  }
}
