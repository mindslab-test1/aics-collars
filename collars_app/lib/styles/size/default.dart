import 'package:flutter/material.dart';

const kHeightGap = 15.0;
const kAppBarLogoSize = 36.0;
const kAppBarLeadingIconSize = 36.0;
const kBottomBarItemSize = 30.0;
const kBottomBarItemLabelSize = 12.0;
const kInfoIconSize = 24.0;
const kInfoItemText = TextStyle(fontSize: 20.1);
const kInfoItemGap = 8.0;

// Padding
const kDefaultPaddingAll = EdgeInsets.all(24);
const kDefaultPaddingSymmetric = EdgeInsets.symmetric(horizontal: 24, vertical: 20);
const kSmallPaddingSymmetric = EdgeInsets.symmetric(horizontal: 24, vertical: 20);
const kDefaultPaddingHorizontal = EdgeInsets.symmetric(horizontal: 24);
const kDefaultPaddingVertical = EdgeInsets.symmetric(vertical: 20);