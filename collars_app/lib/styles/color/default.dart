import 'package:flutter/material.dart';

// Color
const kPrimaryColor = Color(0xff4036cc);
const kPrimaryColor2 = Color(0xff3862ca);
const kPrimaryColor3 = Color(0xff2e9ac8);
const kPrimaryLight = Color(0xff5043ff);
const kPrimaryLight2 = Color(0xff467bfd);
const kPrimaryLight3 = Color(0xff3ac0fa);
const kSecondaryColor = Color(0xffa157d9);
const kSecondaryColor2 = Color.fromRGBO(168, 87, 213, 0.98);
const kSecondaryColor3 = Color.fromRGBO(255, 87, 159, 0.78);

const kMainBlue = Color(0xff5262f2);
const kMainIndigo = Color(0xff3646c0);
const kMainSky = Color(0xff71b1fb);
const kMainBlack = Color(0xff333333);
const kMainGray = Color(0xfff5f5f5);
const kMainWhite = Colors.white;
const kMainWhite50 = Color.fromRGBO(255, 255, 255, 0.5);

const kStar = Color(0xffe05c5c);
const kStar50 = Color(0xfff5cdcd);
const kComment = Color(0xff707070);

const kEvereadingMainBlack = Color(0xff1b1c1c);

const kMainBtnBg = Color(0xffffffff);
const kMainBtnText = Color(0xff5244fe);

const kGrayBorder = Color(0xffdfdfdf);

const kInfoItem = Color.fromRGBO(0, 0, 0, 0.3);

// BottomBar
const kBottomBarBg = kMainGray;
const kBottomBarItemUnselected =  Color(0xff71707b);
const kBottomBarItemSelected = kMainBlue;
// Evereading
const kColumnBg = Color(0xffeaeaea);
// TwoCents
const kLeftMsgBorder = Color(0xfff8f8f8);
const kLeftMsgBg = Color(0xffeaeaea);
const kRightMsgBorder = Color(0xffeaeaea);
const kRightMsgBg = Color(0xffc7c7c7);

const kNaver = Color(0xff1ec800);
const kNaverIcon = Color(0xffffffff);
const kGoogle = Color(0xffdb4437);
const kGoogleIcon = Color(0xffffffff);
const kKakao = Color(0xffffe500);
const kKakaoIcon = Color(0xff391b1b);

const kPrimaryGradient = LinearGradient(
  colors: [
    kPrimaryColor,
    kPrimaryColor2,
    kPrimaryColor3
  ],
  begin: Alignment.bottomCenter,
  end: Alignment.topCenter
);
const kPrimaryGradient2 = LinearGradient(
    colors: [
      kPrimaryColor,
      kPrimaryColor2,
      kPrimaryColor3
    ],
    begin: Alignment.centerRight,
    end: Alignment.centerLeft
);
const kPrimaryLightGradient = LinearGradient(
    colors: [
      kPrimaryLight,
      kPrimaryLight2,
      kPrimaryLight3
    ],
    begin: Alignment.bottomCenter,
    end: Alignment.topCenter
);
const kPrimaryLightGradient2 = LinearGradient(
    colors: [
      kPrimaryLight,
      kPrimaryLight2,
      kPrimaryLight3
    ],
    begin: Alignment.centerRight,
    end: Alignment.centerLeft
);
const kSecondaryGradient2 = LinearGradient(
  colors: [
    kPrimaryColor,
    kSecondaryColor,
    kSecondaryColor2,
    kSecondaryColor3
  ],
  begin: Alignment.centerRight,
  end: Alignment.centerLeft
);