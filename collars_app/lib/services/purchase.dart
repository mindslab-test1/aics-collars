
import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class Purchase {
  String _appServer = DotEnv().env['APP_SERVER'];
  Future verifyPurchase (String uid, int userCourseId,String productId, String purchaseToken) async {
    String url = '$_appServer/inapp/consumeGet';
    Map<String, String> headers = { 'content-type': 'application/json' };
    Map<String, dynamic> body = {
      'uid': uid,
      'courseId': userCourseId,
      'productId': productId,
      'purchaseToken': purchaseToken
    };
    http.Response response = await http.post( url, headers: headers, body: jsonEncode(body));
    return jsonDecode(utf8.decode(response.bodyBytes))["payload"];
  }
}