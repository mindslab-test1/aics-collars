import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class SurveyService {
  String _appServer = DotEnv().env['APP_SERVER'];

  // Function
  Future getSurveyData() async {
    String url = '$_appServer/survey/getData';
    http.Response response = await http.get(url);
    String jsonString = jsonDecode(response.body)['payload'];
    if(jsonString != null) {
      List<dynamic> data = jsonDecode(jsonString);
      data.forEach((e) {
        List jsonArray = List();
        List tempArr = e['answer'].split('|');
        tempArr.forEach((arr) {
          List tempArr2 = List();
          tempArr2 = arr.split('=>');
          jsonArray.add(tempArr2);
        });
        jsonArray.forEach((e) {
          if(e[1] == "기타"){
            dynamic temp = e;
            jsonArray.remove(e);
            jsonArray.add(temp);
          }
        });
        e['answer'] = jsonArray;
        e['answer'] = jsonArray;
      });

      return data;
    }
    else {
      return null;
    }
  }
  Future setUserSurveyData({String uid,dynamic surveyResult}) async {
    String url = '$_appServer/survey/setUserData';
    Map<String, String> headers = { 'content-type': 'application/json' };
    Map<String, dynamic> body = {
      'uid': uid,
      'result': surveyResult
    };
    http.Response response = await http.post( url, headers: headers, body: jsonEncode(body));
    if(jsonDecode(response.body)['payload']){
      return true;
    }else{
      return false;
    }
  }
}