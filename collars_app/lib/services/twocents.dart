import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'media_server.dart';

class TwoCentsService {
  String _appServer = DotEnv().env['APP_SERVER'];
  MediaServer _mediaServer = MediaServer();

  Future getContents(int current, int increment) async {
    String url = '$_appServer/twocents/getContents';
    Map<String, String> headers = {'content-type': 'application/json'};
    Map<String, int> body = {
      'current': current,
      'increment': increment
    };
    http.Response response = await http.post(url, headers: headers, body: jsonEncode(body));
    List<dynamic> jsonData = jsonDecode(utf8.decode(response.bodyBytes))['payload'];
    for(int i = 0; i < jsonData.length; i ++){
      jsonData[i]["content"] = await _mediaServer.getContent(jsonData[i]["url"]);
    }

    return jsonData;
  }
}
