import 'dart:convert';

import 'package:collarsApp/screens/layouts/exam_main.dart';
import 'package:collarsApp/screens/wrap_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;

class Common{

  /*
  * 뒤로가기 버튼 눌렀을 경우
  * 특정 페이지로 가기 위함
  * */
  Future<bool> onWillPop(BuildContext context, String route, String content, Function function) async{
    //function();
    return (await showDialog(
        context: context,
        builder: (context) => new AlertDialog(
          content: new Text(content),
          actions: <Widget>[
            new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text("No")
            ),
            new FlatButton(
                onPressed: function!=null?function:(){Navigator.popUntil(context, ModalRoute.withName(route));},
                child: Text("yes")
            )
          ],
        )
    ));
  }

  void customPopup(BuildContext context, String title, String body, Function function){
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context){
        return AlertDialog(
          shape: RoundedRectangleBorder(
            side: BorderSide(color: Color.fromRGBO(82, 98, 242, 1)),
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          title: Center(
              child: Text(title, style: TextStyle(fontSize: 17.9, fontWeight: FontWeight.bold),)
          ),
          content: Builder(
            builder: (context){
              return Container(
                height: 132,
                width: 327,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                        body
                    ),
                    SizedBox(height: 20.9,),
                    RaisedButton(
                      color: Colors.white,
                      elevation: 8,
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Color.fromRGBO(82, 98, 242, 1)),
                          borderRadius: BorderRadius.all(Radius.circular(30))
                      ),
                      child: Container(
                        width: 143.1,
                        height: 60.9,
                        child: Center(
                          child: Text(
                            title,
                            style: TextStyle(
                              fontSize: 20,
                              color: Color.fromRGBO(54, 70, 192, 1),
                              fontWeight: FontWeight.bold
                            ),
                          ),
                        ),
                      ),
                      onPressed: function,
                    )
                  ],
                ),
              );
            },
          ),
        );
      }
    );
  }

  /*
  * exam이나 practice시 다음으로 넘어가거나
  * retry하기 위한 popup
  * */
  void showPopup(String score, String answer, String answerText, String choice, String choiceText, BuildContext context, Function onPressRetry, Function goNextTest, bool isExam){
    String title ="";
    bool rightAnswer = true;
    if(score == "A") title = "Excellent";
    else if(score == "B") title = "Great";
    else if(score == "C") title = "Correct";
    else {
      title = "Wrong";
      rightAnswer = false;
    }
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context){
          Size size = MediaQuery.of(context).size;
          return WillPopScope(
            onWillPop:()=> isExam?Common().onWillPop(context, WrapScreen.id, "테스트를 종료하시겠습니까?", null)
                :Common().onWillPop(context, WrapScreen.id, "학습을 종료하시겠습니까?",
                    (){
                      int count = 0;
                      Navigator.popUntil(context, (route) {
                        return count++ == 3;
                      });
                    }),
            child: Container(
              child: AlertDialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20))
                ),
                content: Container(
                  width: size.width*0.8,
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.all(15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(bottom: 20),
                            child: Center(
                              child: Text(
                                title,
                                style: TextStyle(
                                    color: (rightAnswer==true)?Color.fromRGBO(82, 98, 242, 1):Color(0xfff677b6),
                                    fontSize: 36,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'Raleway'
                                ),
                              ),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                color: (rightAnswer==true)?Color.fromRGBO(82, 98, 242, 1):Color(0xfff677b6)
                            ),
                            child: Padding(
                                padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                                child: Text(answer, style: TextStyle(fontSize: 12,color: Colors.white, fontFamily: 'NotoSansCJKkr', fontWeight: FontWeight.w500),)
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.only(top: 10),
                              child: Text(answerText,
                                style: TextStyle(
                                    fontFamily: 'Raleway',
                                    fontWeight: FontWeight.w500,
                                    fontSize: 16
                                ),
                              )
                          ),
                          Padding(
                              padding: EdgeInsets.only(top: 15, bottom: 15),
                              child: Divider(height: 2, color: Colors.black.withOpacity(0.6),)
                          ),
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                color: (rightAnswer==true)?Color.fromRGBO(82, 98, 242, 1):Color(0xfff677b6)
                            ),
                            child: Padding(
                                padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                                child: Text(choice, style: TextStyle(fontSize: 12,color: Colors.white, fontFamily: 'NotoSansCJKkr', fontWeight: FontWeight.w500),)
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.only(top: 10, bottom: 30),
                              child: Text(choiceText,
                                  style: TextStyle(
                                      fontFamily: 'Raleway',
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16
                                  ))
                          ),
                          goNextTest==null?Container():Center(
                            child: Container(
                              width: 124,
                              height: 55,
                              child: RaisedButton(
                                color: (rightAnswer==true)?Color.fromRGBO(82, 98, 242, 1):Color(0xfff677b6),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25)
                                ),
                                child: Text("OK", style: TextStyle(fontSize: 14, color: Colors.white, fontWeight: FontWeight.bold),),
                                onPressed: (){
                                  Navigator.pop(context);
                                  goNextTest();
                                },
                              ),
                            ),
                          ),
                          SizedBox(height: 8,),
                          onPressRetry==null?SizedBox(height: 0,):Center(
                            child: FlatButton(
                              child: Text("retry",
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Color(0xff52c7f2),
                                  decoration: TextDecoration.underline
                                ),
                              ),
                              onPressed: (){
                                onPressRetry();
                                Navigator.pop(context);
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        }
    );
  }

  /*
  * 문제 스킵시 보여주기 위한 팝업
  * */
  void showSkipPopup(BuildContext context, Function onPressRetry, Function goNextTest, bool isExam){

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context){
          Size size = MediaQuery.of(context).size;
          return WillPopScope(
            onWillPop:()=> isExam?Common().onWillPop(context, WrapScreen.id, "테스트를 종료하시겠습니까?", null)
                :Common().onWillPop(context, WrapScreen.id, "학습을 종료하시겠습니까?",
                    (){
                  int count = 0;
                  Navigator.popUntil(context, (route) {
                    return count++ == 3;
                  });
                }),
            child: Container(
              child: AlertDialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20))
                ),
                content: Container(
                  width: size.width*0.8,
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.only(top: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(bottom: 24),
                            child: Center(
                              child: Text(
                                'SKIP',
                                style: TextStyle(
                                    color: Color(0xff5262f2),
                                    fontSize: 36,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'Raleway'
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 24),
                            child: Center(child: Text('다음 테스트로 넘어가시겠습니까',
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: 'NotoSansCJKkr',
                                  fontWeight: FontWeight.w500
                              ),
                            )
                            )
                          ),

                          goNextTest==null?Container():Center(
                            child: Container(
                              width: 124,
                              height: 55,
                              child: RaisedButton(
                                color: Color.fromRGBO(82, 98, 242, 1),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)
                                ),
                                child: Text("OK",
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700,
                                      fontFamily: 'NotoSansCJKkr'
                                  ),
                                ),
                                onPressed: (){
                                  Navigator.pop(context);
                                  goNextTest();
                                },
                              ),
                            ),
                          ),
                          SizedBox(height: 8,),
                          Center(
                            child: FlatButton(
                              child: Text("retry",
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Color(0xff52c7f2),
                                    decoration: TextDecoration.underline
                                ),
                              ),
                              onPressed: (){
                                onPressRetry();
                                Navigator.pop(context);
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        }
    );
  }

  /*
  * 로딩시 보여주기 위함
  * */
  Widget spinkit(Size size, String info, Color color) => Stack(
    children: <Widget>[
      Positioned.fill(
        bottom: size.height*0.35,
        child: Center(
          child: Text(info, style: TextStyle(color: color, fontSize: size.height*0.034, fontWeight: FontWeight.bold),),
        ),
      ),
      Positioned.fill(
        top: size.height*0.25,
        child: Center(
          child: Text("Loading...", style: TextStyle(color: color, fontSize: size.height*0.03, fontWeight: FontWeight.bold),),
        ),
      ),
      Center(
        child: Image.asset('assets/images/logo/logo.webp', width: size.width*0.118, height: size.height*0.048, color: color,),
      ),
      SpinKitFadingCircle(
        size: size.width*0.3,
        itemBuilder: (BuildContext context, int index) {
          return DecoratedBox(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: index.isEven ? color : color,
            ),
          );
        },
      )
    ],
  );

  Future<dynamic> httpRequest(String url, Map<String, dynamic> request) async{
    Map<String, String> headers = { 'content-type': 'application/json' };
    http.Response response = await http.post(
        url,
        headers: headers,
        body: jsonEncode(request),
        encoding: Encoding.getByName("utf-8")
    );

    if(!response.body.contains(":"))
      return response.body;
    else if(response.body != null && response.body != "")
      return jsonDecode(response.body);
    else
      return null;
  }

  // 대소문자 구분없이 두개의 String을 비교한다
  bool equalsIgnoreCase(String string1, String string2) {
    return string1?.toLowerCase() == string2?.toLowerCase();
  }
}