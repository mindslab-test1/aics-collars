import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter_dotenv/flutter_dotenv.dart';

import '../providers/user.dart';

class CourseService{
  String _appServer = DotEnv().env['APP_SERVER'];

  // Function
  Future getRandomSuggest (String uid) async {
    String url = '$_appServer/course/randomSuggest';
    Map<String, String> headers = { 'content-type': 'application/json' };
    Map<String, String> body = {
      'uid': uid,
    };
    http.Response response = await http.post( url, headers: headers, body: jsonEncode(body) );
    Map<String, dynamic> result;
    switch(response.statusCode){
      case 211:
        result = {
          "status": 1,
          "payload": jsonDecode(utf8.decode(response.bodyBytes))['payload']
        };
        break;
      case 212:
        result = {
          "status": 2,
          "payload": null
        };
        break;
      default:
        result = {
          "status": 999,
          "payload": null
        };
        break;
    }
    return result;
  }
}