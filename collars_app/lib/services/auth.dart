import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
// Provider
import 'package:collarsApp/providers/user.dart';
// Social Sign In
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_naver_login/flutter_naver_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:kakao_flutter_sdk/all.dart' as Kakao;

class AuthService {
  String _appServer = DotEnv().env['APP_SERVER'];
  final FirebaseAuth _auth = FirebaseAuth.instance;

  // Create user obj based on FirebaseUser
  User _userFromFirebaseUser(FirebaseUser user) {
    print('USER FROM FIREBASE');
    return user != null ? User(uid: user.uid, email: user.email) : null;
  }

  // Auth change user stream
  Stream<User> get user {
    return _auth.onAuthStateChanged.map((FirebaseUser user) => _userFromFirebaseUser(user)); //.map(_userFromFirebaseUser);
  }

  // Sign In
  Future<void> signInWithNaver() async {
    NaverLoginResult resLogin = await FlutterNaverLogin.logIn();
    NaverAccessToken resToken = await FlutterNaverLogin.currentAccessToken;

    String customToken = await getCustomToken('NAVER', resLogin.account.email, resLogin.account.name);

    final AuthResult authResult = await _auth.signInWithCustomToken(token: customToken);
    final FirebaseUser user = authResult.user;
  }
  Future<void> signInWithKakao() async {
    String authCode = await Kakao.AuthCodeClient.instance.request();
    Kakao.AccessTokenResponse token = await Kakao.AuthApi.instance.issueAccessToken(authCode);
    await Kakao.AccessTokenStore.instance.toStore(token);
    Kakao.User kakaoUser = await Kakao.UserApi.instance.me();

    String customToken = await getCustomToken('KAKAO', kakaoUser.kakaoAccount.email, kakaoUser.kakaoAccount.profile.nickname);

    final AuthResult authResult = await _auth.signInWithCustomToken(token: customToken);
    final FirebaseUser user = authResult.user;
  }
  Future<void> signInWithGoogle() async {
    final GoogleSignIn googleSignIn = GoogleSignIn();
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();

    bool isValid = await validUser('GOOGLE', googleSignInAccount.email, googleSignInAccount.displayName);
    if(isValid) {
      final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount
          .authentication;
      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );

      final AuthResult authResult = await _auth.signInWithCredential(
          credential);
      final FirebaseUser user = authResult.user;
    }
  }

  // Sign Out
  Future<void> signOut() async {
//    final GoogleSignIn googleSignIn = GoogleSignIn();
//    await googleSignIn.signOut();
//    await FlutterNaverLogin.logOut();
//    await Kakao.UserApi.instance.unlink();
    await _auth.signOut();
  }

  // Function
  // - Sign In
  //  - For social login, doesn't support as a default from Firebase.
  Future<String> getCustomToken(String provider, String email, String name) async {
    String url = '$_appServer/auth/getCustomToken';
    Map<String, String> headers = { 'content-type': 'application/json' };
    Map<String, String> body = {
      'provider': provider,
      'email': email,
      'name': name
    };
    // Todo: Ip address should be changed.
    http.Response response = await http.post( url, headers: headers, body: jsonEncode(body) );
    return jsonDecode(response.body)['payload'];
  }
  //  - For social login, support as a default from Firebase.
  Future<bool> validUser(String provider, String email, String name) async {
    String url = '$_appServer/auth/validUser';
    Map<String, String> headers = { 'content-type': 'application/json' };
    Map<String, String> body = {
      'provider': provider,
      'email': email,
      'name': name
    };
   http.Response response = await http.post( url, headers: headers, body: jsonEncode(body) );
    return jsonDecode(response.body)['payload'];
  }
  //  - To get user id from db
  Future<Map<String, dynamic>> getUserData(String uid) async {
    String url = '$_appServer/auth/getUserData';
    Map<String, String> headers = { 'content-type': 'application/json' };
    Map<String, String> body = {
      'uid': uid,
    };
    http.Response response = await http.post( url, headers: headers, body: jsonEncode(body) );
    print( jsonDecode(utf8.decode(response.bodyBytes))['payload']);
    return jsonDecode(utf8.decode(response.bodyBytes))['payload'];
  }

}