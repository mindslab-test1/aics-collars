
class SpeechCopycat {
  String menu;
  String title;
  String speech;
  String translate;
  int star;

  SpeechCopycat({this.menu, this.title, this.speech, this.translate, this.star});

  SpeechCopycat.fromJson(Map<String, dynamic> json) {
    menu = json['menu'];
    title = json['title'];
    speech = json['speech'];
    translate = json['translate'];
    star = json['star'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['menu'] = this.menu;
    data['title'] = this.title;
    data['speech'] = this.speech;
    data['translate'] = this.translate;
    data['star'] = this.star;
    return data;
  }
}