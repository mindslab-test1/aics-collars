class LectureMedia {
  int id;
  String url;
  String name;

  LectureMedia(
      {this.id,
        this.url,
        this.name});

  LectureMedia.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    url = json['url'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['url'] = this.url;
    data['name'] = this.name;
    return data;
  }
}