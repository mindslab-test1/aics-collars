class PracticeResultData {
  int id;
  int userId;
  int mediaId;
  String question;
  String userAnswer;
  int correct;
  String score;
  int countResult;

  PracticeResultData(
      {this.id,
        this.userId,
        this.mediaId,
        this.question,
        this.userAnswer,
        this.correct,
        this.score,
        this.countResult});

  PracticeResultData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['userId'];
    mediaId = json['mediaId'];
    question = json['question'];
    userAnswer = json['userAnswer'];
    correct = json['correct'];
    score = json['score'];
    countResult = json['countResult'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['userId'] = this.userId;
    data['mediaId'] = this.mediaId;
    data['question'] = this.question;
    data['userAnswer'] = this.userAnswer;
    data['correct'] = this.correct;
    data['score'] = this.score;
    data['countResult'] = this.countResult;
    return data;
  }
}