class WordPracticeData {
  List<Words> words;

  WordPracticeData({this.words});

  WordPracticeData.fromJson(List<dynamic> json) {
    /*if (json['words'] != null) {
      words = new List<Words>();
      json['words'].forEach((v) {
        words.add(new Words.fromJson(v));
      });
    }*/
    if(json != null){
      words = new List<Words>();
      json.forEach((word) {
        words.add(new Words.fromJson(word));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.words != null) {
      data['words'] = this.words.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Words {
  int id;
  int mediaId;
  String word;
  String answer;
  String mulChoice1;
  String mulChoice2;
  String mulChoice3;
  String mulChoice4;

  Words(
      {this.id,
        this.mediaId,
        this.word,
        this.answer,
        this.mulChoice1,
        this.mulChoice2,
        this.mulChoice3,
        this.mulChoice4});

  Words.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    mediaId = json['mediaId'];
    word = json['word'];
    answer = json['answer'];
    mulChoice1 = json['mulChoice1'];
    mulChoice2 = json['mulChoice2'];
    mulChoice3 = json['mulChoice3'];
    mulChoice4 = json['mulChoice4'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['mediaId'] = this.mediaId;
    data['word'] = this.word;
    data['answer'] = this.answer;
    data['mulChoice1'] = this.mulChoice1;
    data['mulChoice2'] = this.mulChoice2;
    data['mulChoice3'] = this.mulChoice3;
    data['mulChoice4'] = this.mulChoice4;
    return data;
  }
}