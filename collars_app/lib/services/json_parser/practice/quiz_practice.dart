import 'package:collarsApp/services/json_parser/ExamTemp/exam_json2.dart';
import 'package:collarsApp/services/json_parser/ExamTemp/exam_json4.dart';
import 'package:collarsApp/services/json_parser/ExamTemp/exam_json5.dart';

class QuizPractice{
  ExamJson2 quiz1;
  ExamJson4 quiz2;
  ExamJson5 quiz3;
  ExamJson4 quiz4;

  QuizPractice({
    this.quiz1,
    this.quiz2,
    this.quiz3,
    this.quiz4
  });

  QuizPractice.fromJson(Map<String, dynamic> json){
   quiz1 = (new ExamJson2.fromJson(json['quiz1']));
   quiz2 = (new ExamJson4.fromJson(json['quiz2']));
   quiz3 = (new ExamJson5.fromJson(json['quiz3']));
   quiz4 = (new ExamJson4.fromJson(json['quiz4']));
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['quiz1'] = this.quiz1;
    data['quiz2'] = this.quiz2;
    data['quiz3'] = this.quiz3;
    data['quiz4'] = this.quiz4;
  }
}