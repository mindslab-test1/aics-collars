class QuizPracticeData{
  List<Quizs> quizs;

  QuizPracticeData({this.quizs});

  QuizPracticeData.fromJson(List<dynamic> list){
    if(list != null){
      quizs = new List<Quizs>();
      list.forEach((quiz) {
        quizs.add(Quizs.fromJson(quiz));
      });
    }
  }
}

class Quizs{
  int id;
  int userId;
  int ucourseId;
  String question;
  String correctAnswer;

  Quizs(
  {
    this.id,
    this.userId,
    this.ucourseId,
    this.question,
    this.correctAnswer
  });

  Quizs.fromJson(Map<String, dynamic> json){
    id = json['id'];
    userId = json['userId'];
    ucourseId = json['ucourseId'];
    question = json['question'];
    correctAnswer = json['correctAnswer'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['userId'] = this.userId;
    data['ucourseId'] = this.ucourseId;
    data['question'] = this.question;
    data['correctAnswer'] = this.correctAnswer;
    return data;
  }
}