class PronPracticeData {
  List<Prons> prons;

  PronPracticeData({this.prons});

  PronPracticeData.fromJson(List<dynamic> list) {
    /*if (json['prons'] != null) {
      prons = new List<Prons>();
      json['prons'].forEach((v) {
        prons.add(new Prons.fromJson(v));
      });
    }*/
    if(list != null){
      prons = new List<Prons>();
      list.forEach((pron) {
        prons.add(new Prons.fromJson(pron));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.prons != null) {
      data['prons'] = this.prons.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Prons {
  int id;
  int mediaId;
  String sentence;

  Prons({this.id, this.mediaId, this.sentence});

  Prons.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    mediaId = json['mediaId'];
    sentence = json['sentence'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['mediaId'] = this.mediaId;
    data['sentence'] = this.sentence;
    return data;
  }
}