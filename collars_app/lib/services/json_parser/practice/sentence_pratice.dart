class SentencePracticeData {
  List<Sentences> sentences;

  SentencePracticeData({this.sentences});

  SentencePracticeData.fromJson(List<dynamic> list) {
    if(list != null){
      sentences = new List<Sentences>();
      list.forEach((sentence) {
        sentences.add(new Sentences.fromJson(sentence));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.sentences != null) {
      data['sentences'] = this.sentences.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Sentences {
  int id;
  int mediaId;
  String answer;
  String mulChoice1;
  String mulChoice2;
  String mulChoice3;
  String mulChoice4;
  String question;

  Sentences(
      {this.id,
        this.mediaId,
        this.answer,
        this.mulChoice1,
        this.mulChoice2,
        this.mulChoice3,
        this.mulChoice4,
        this.question});

  Sentences.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    mediaId = json['mediaId'];
    answer = json['answer'];
    mulChoice1 = json['mulChoice1'];
    mulChoice2 = json['mulChoice2'];
    mulChoice3 = json['mulChoice3'];
    mulChoice4 = json['mulChoice4'];
    question = json['question'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['mediaId'] = this.mediaId;
    data['answer'] = this.answer;
    data['mulChoice1'] = this.mulChoice1;
    data['mulChoice2'] = this.mulChoice2;
    data['mulChoice3'] = this.mulChoice3;
    data['mulChoice4'] = this.mulChoice4;
    data['question'] = this.question;
    return data;
  }
}