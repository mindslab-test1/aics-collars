class PronResponse {
  int score;
  String sttResult;
  Result result;

  PronResponse({this.score, this.sttResult, this.result});

  PronResponse.fromJson(Map<String, dynamic> json) {
    score = json['score'];
    sttResult = json['sttResult'];
    result =
    json['result'] != null ? new Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['score'] = this.score;
    data['sttResult'] = this.sttResult;
    if (this.result != null) {
      data['result'] = this.result.toJson();
    }
    return data;
  }
}

class Result {
  String resultText;
  String answerText;
  String recordUrl;
  double speedScore;
  List<Words> words;
  double resCode;
  double rhythmScore;
  double segmentalScore;
  double sentenceScore;
  double totalScore;
  double pronScore;
  double intonationScore;

  Result(
      {this.resultText,
        this.answerText,
        this.recordUrl,
        this.speedScore,
        this.words,
        this.resCode,
        this.rhythmScore,
        this.segmentalScore,
        this.sentenceScore,
        this.totalScore,
        this.pronScore,
        this.intonationScore});

  Result.fromJson(Map<String, dynamic> json) {
    resultText = json['resultText'];
    answerText = json['answerText'];
    recordUrl = json['recordUrl'];
    speedScore = json['speedScore'];
    if (json['words'] != null) {
      words = new List<Words>();
      json['words'].forEach((v) {
        words.add(new Words.fromJson(v));
      });
    }
    resCode = json['resCode'];
    rhythmScore = json['rhythmScore'];
    segmentalScore = json['segmentalScore'];
    sentenceScore = json['sentenceScore'];
    totalScore = json['totalScore'];
    pronScore = json['pronScore'];
    intonationScore = json['intonationScore'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['resultText'] = this.resultText;
    data['answerText'] = this.answerText;
    data['recordUrl'] = this.recordUrl;
    data['speedScore'] = this.speedScore;
    if (this.words != null) {
      data['words'] = this.words.map((v) => v.toJson()).toList();
    }
    data['resCode'] = this.resCode;
    data['rhythmScore'] = this.rhythmScore;
    data['segmentalScore'] = this.segmentalScore;
    data['sentenceScore'] = this.sentenceScore;
    data['totalScore'] = this.totalScore;
    data['pronScore'] = this.pronScore;
    data['intonationScore'] = this.intonationScore;
    return data;
  }
}

class Words {
  String word;
  double pronScore;

  Words({this.word, this.pronScore});

  Words.fromJson(Map<String, dynamic> json) {
    word = json['word'];
    pronScore = json['pronScore'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['word'] = this.word;
    data['pronScore'] = this.pronScore;
    return data;
  }
}
