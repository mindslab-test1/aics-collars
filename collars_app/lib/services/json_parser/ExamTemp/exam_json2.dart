class ExamJson2 {
  int id;
  int examNum;
  String mulChoice1;
  String mulChoice2;
  String mulChoice3;
  String mulChoice4;
  String correct;
  String qtext;
  String ptext;

  ExamJson2(
      {this.id,
        this.examNum,
        this.mulChoice1,
        this.mulChoice2,
        this.mulChoice3,
        this.mulChoice4,
        this.correct,
        this.qtext,
        this.ptext});

  ExamJson2.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    examNum = json['examNum'];
    mulChoice1 = json['mulChoice1'];
    mulChoice2 = json['mulChoice2'];
    mulChoice3 = json['mulChoice3'];
    mulChoice4 = json['mulChoice4'];
    correct = json['correct'];
    qtext = json['qtext'];
    ptext = json['ptext'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['examNum'] = this.examNum;
    data['mulChoice1'] = this.mulChoice1;
    data['mulChoice2'] = this.mulChoice2;
    data['mulChoice3'] = this.mulChoice3;
    data['mulChoice4'] = this.mulChoice4;
    data['correct'] = this.correct;
    data['qtext'] = this.qtext;
    data['ptext'] = this.ptext;
    return data;
  }
}