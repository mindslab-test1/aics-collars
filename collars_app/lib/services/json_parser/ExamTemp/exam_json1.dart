class ExamJson1 {
  int id;
  int examNum;
  String correctAnswer;
  String accuracyTag;
  String pronTag;
  String qtext;
  String ptext;

  ExamJson1(
      {
        this.id,
        this.examNum,
        this.correctAnswer,
        this.accuracyTag,
        this.pronTag,
        this.qtext,
        this.ptext});

  ExamJson1.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    examNum = json['examNum'];
    correctAnswer = json['correctAnswer'];
    accuracyTag = json['accuracyTag'];
    pronTag = json['pronTag'];
    qtext = json['qtext'];
    ptext = json['ptext'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['examNum'] = this.examNum;
    data['correctAnswer'] = this.correctAnswer;
    data['accuracyTag'] = this.accuracyTag;
    data['pronTag'] = this.pronTag;
    data['qtext'] = this.qtext;
    data['ptext'] = this.ptext;
    return data;
  }
}