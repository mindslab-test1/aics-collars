class ExamJson3 {
  int id;
  int examNum;
  String correct;
  String qtext1;
  String qtext2;

  ExamJson3({this.id, this.correct, this.qtext1, this.qtext2, this.examNum});

  ExamJson3.fromJson(Map<String, dynamic> json) {
    examNum = json['examNum'];
    id = json['id'];
    correct = json['correct'];
    qtext1 = json['qtext1'];
    qtext2 = json['qtext2'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['examNum'] = this.examNum;
    data['id'] = this.id;
    data['correct'] = this.correct;
    data['qtext1'] = this.qtext1;
    data['qtext2'] = this.qtext2;
    return data;
  }
}