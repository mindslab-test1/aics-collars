class ExamJson5 {
  String email;
  int id;
  String correctAnswer;
  String expressTag;
  int examNum;
  String ptext;
  String qtext;
  String wordArray;

  ExamJson5(
      {this.email,
        this.id,
        this.correctAnswer,
        this.expressTag,
        this.examNum,
        this.ptext,
        this.qtext,
        this.wordArray});

  ExamJson5.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    id = json['id'];
    correctAnswer = json['correctAnswer'];
    expressTag = json['expressTag'];
    examNum = json['examNum'];
    ptext = json['ptext'];
    qtext = json['qtext'];
    wordArray = json['wordArray'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['id'] = this.id;
    data['correctAnswer'] = this.correctAnswer;
    data['expressTag'] = this.expressTag;
    data['examNum'] = this.examNum;
    data['ptext'] = this.ptext;
    data['qtext'] = this.qtext;
    data['wordArray'] = this.wordArray;
    return data;
  }
}