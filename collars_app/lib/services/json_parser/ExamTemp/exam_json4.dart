class ExamJson4 {
  String email;
  int id;
  int examNum;
  String correctAnswer;
  String mannerTag;
  String qtext1;
  String qtext2;

  ExamJson4(
      {this.email,
        this.id,
        this.examNum,
        this.correctAnswer,
        this.mannerTag,
        this.qtext1,
        this.qtext2});

  ExamJson4.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    id = json['id'];
    examNum = json['examNum'];
    correctAnswer = json['correctAnswer'];
    mannerTag = json['mannerTag'];
    qtext1 = json['qtext1'];
    qtext2 = json['qtext2'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['id'] = this.id;
    data['examNum'] = this.examNum;
    data['correctAnswer'] = this.correctAnswer;
    data['mannerTag'] = this.mannerTag;
    data['qtext1'] = this.qtext1;
    data['qtext2'] = this.qtext2;
    return data;
  }
}