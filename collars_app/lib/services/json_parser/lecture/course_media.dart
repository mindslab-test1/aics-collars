class CourseMedia {
  String startDate;
  String mediaUrl;
  String title;
  List<CourseMeidaVO> courseMeidaVO;
  List<bool> isStudyCompleted;
  List<UserCourseList> userCourseList;
  List<MediaTextVO> mediaTextVO;

  CourseMedia({this.startDate,
        this.mediaUrl,
        this.title,
        this.courseMeidaVO,
        this.isStudyCompleted,
        this.userCourseList});

  CourseMedia.fromJson(Map<String, dynamic> json) {
    startDate = json['startDate'];
    mediaUrl = json['mediaUrl'];
    title = json['title'];

    if (json['courseMeidaVO'] != null) {
      courseMeidaVO = new List<CourseMeidaVO>();
      json['courseMeidaVO'].forEach((v) {
        courseMeidaVO.add(new CourseMeidaVO.fromJson(v));
      });
    }
    if(json['userCourseList'] != null){
      userCourseList = new List<UserCourseList>();
      json['userCourseList'].forEach((v) {
        userCourseList.add(new UserCourseList.fromJson(v));
      });
    }
    if(json['mediaTextVO'] != null){
      mediaTextVO = new List<MediaTextVO>();
      json['mediaTextVO'].forEach((v) {
        mediaTextVO.add(new MediaTextVO.fromJson(v));
      });
    }
    if(json['isStudyCompleted'] != null) isStudyCompleted = json['isStudyCompleted'].cast<bool>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['startDate'] = this.startDate;
    data['title'] = this.title;
    if (this.courseMeidaVO != null) {
      data['courseMeidaVO'] =
          this.courseMeidaVO.map((v) => v.toJson()).toList();
    }
    if(this.userCourseList != null){
      data['userCourseList'] = this.userCourseList.map((v) => v.toJson()).toList();
    }
    if(this.mediaTextVO != null){
      data['mediaTextVO'] = this.mediaTextVO.map((v) => v.toJson()).toList();
    }
    data['isStudyCompleted'] = this.isStudyCompleted;
    return data;
  }
}

class UserCourseList {
  int id;
  int userId;
  int active;
  int completed;
  int lev;
  String title;
  String description;
  String startDate;
  String endDate;

  UserCourseList({this.id, this.userId, this.active, this.completed, this.lev, this.title, this.description, this.startDate, this.endDate});

  UserCourseList.fromJson(Map<String, dynamic> json){
    id = json['id'];
    userId = json['userId'];
    active = json['active'];
    completed = json['completed'];
    lev = json['lev'];
    title = json['title'];
    description = json['description'];
    startDate = json['startDate'];
    endDate = json['endDate'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['userId'] = this.userId;
    data['active'] = this.active;
    data['completed'] = this.completed;
    data['lev'] = this.lev;
    data['title'] = this.title;
    data['description'] = this.description;
    data['startDate'] = this.startDate;
    data['endDate'] = this.endDate;
    return data;
  }
}

class CourseMeidaVO {
  int id;
  int courseId;
  int mediaId;
  int mediaOrder;
  int completed;
  String name;

  CourseMeidaVO(
      {this.id, this.courseId, this.mediaId, this.mediaOrder, this.completed, this.name});

  CourseMeidaVO.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    courseId = json['courseId'];
    mediaId = json['mediaId'];
    mediaOrder = json['mediaOrder'];
    completed = json['completed'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['courseId'] = this.courseId;
    data['mediaId'] = this.mediaId;
    data['mediaOrder'] = this.mediaOrder;
    data['completed'] = this.completed;
    data['name'] = this.name;
    return data;
  }
}

class MediaTextVO {
  int id;
  String mediaId;
  String subject;
  String content;

  MediaTextVO({this.id, this.mediaId, this.subject, this.content});

  MediaTextVO.fromJson(Map<String, dynamic> json){
    id = json['id'];
    mediaId = json['mediaId'];
    subject = json['subject'];
    content = json['content'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['mediaId'] = this.mediaId;
    data['subject'] = this.subject;
    data['content'] = this.content;
    return data;
  }
}

