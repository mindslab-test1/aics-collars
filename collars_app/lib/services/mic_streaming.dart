import 'dart:async';
import 'dart:typed_data';
import 'package:mic_stream/mic_stream.dart';
import 'package:sound_stream/sound_stream.dart';

enum Command {
  start,
  stop,
  change,
}

const AUDIO_FORMAT = AudioFormat.ENCODING_PCM_16BIT;

class MicStream {

  RecorderStream _recorder = RecorderStream();
  //StreamSubscription _recorderStatus;
  bool isRecording = false;

  MicStream(){
    _initPlugin();
  }

  Future<void> _initPlugin() async{
    print("@@@@@@@@@ init recorder");

    await Future.wait([
      _recorder.initialize()
    ]);
  }

  /*
  * stream 상태에 따라 해당 fucntion 실행
  * */
  void controlMicStream({Command command: Command.change}) async {
    switch (command) {
      case Command.change:
        _changeListening();
        break;
      case Command.start:
        startListening();
        break;
      case Command.stop:
        stopMicpListening();
        break;
    }
  }

  /*
  * 마이크 스트림을 들어온 데이터를
  * stream으로 내보냄
  * */
  Stream<List<int>> startListening() async*{
    print("start Listening to the microphone");

    //_initPlugin();

    _recorder.start();

    await for (Uint8List samples in _recorder.audioStream){
      yield samples;
    }

    isRecording = true;

  }

  bool stopMicpListening() {
    _recorder?.stop();
    isRecording = false;

    return true;
  }


  bool _changeListening() {
    print(isRecording);
    !isRecording ? startListening() : stopMicpListening();
  }

  void endMicStream() {
    isRecording = false;
    _recorder.stop();
  }

  bool getRecordingStatus() {
    return isRecording;
  }
}