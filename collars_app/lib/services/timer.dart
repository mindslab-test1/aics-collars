import 'package:flutter/material.dart';
import 'dart:math' as math;

class CircleProgress extends CustomPainter{

  double currentProgress;
  double circleWeight;
  Color completeColor;

  CircleProgress(this.currentProgress, this.circleWeight, this.completeColor);

  @override
  void paint(Canvas canvas, Size size) {

    //this is base circle
    Paint outerCircle = Paint()
      ..strokeWidth = circleWeight
      ..color = Color.fromRGBO(0, 0, 0, 0.17)
      ..style = PaintingStyle.stroke;

    Paint completeArc = Paint()
      ..strokeWidth = circleWeight
      ..color = completeColor
      ..style = PaintingStyle.stroke
      ..strokeCap  = StrokeCap.round;

    Offset center = Offset(size.width/2, size.height/2);
    double radius = math.min(size.width/2,size.height/2) - 10;

    canvas.drawCircle(center, radius, outerCircle); // this draws main outer circle

    double angle = 2 * math.pi * (currentProgress/100);

    canvas.drawArc(Rect.fromCircle(center: center,radius: radius), -math.pi/2, angle, false, completeArc);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}

