import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter_dotenv/flutter_dotenv.dart';

class NoticeService {
  String _appServer = DotEnv().env['APP_SERVER'];

  Future getNotice(int current, int increment) async {
    String url = '$_appServer/notice/getNotice';
    Map<String, String> headers = { 'content-type': 'application/json' };
    Map<String, int> body = {
      'current': current,
      'increment': increment
    };
    http.Response response = await http.post(url, headers: headers, body: jsonEncode(body));
    return jsonDecode(utf8.decode(response.bodyBytes))['payload'];
  }
}