import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_dotenv/flutter_dotenv.dart';

class MediaServer{
  String _mediaServer = DotEnv().env['MEDIA_SERVER'];

  Future getContent(String filePath) async {
    String url = '$_mediaServer$filePath';
    http.Response response = await http.get(url);
    return jsonDecode(utf8.decode(response.bodyBytes));
  }
}