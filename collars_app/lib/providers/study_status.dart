import 'package:collarsApp/services/json_parser/lecture/course_media.dart';
import 'package:flutter/material.dart';

class StudyStatus extends ChangeNotifier{
  String _startDate;
  List<CourseMeidaVO> _courseMediaVO;
  List<bool> _isStudyCompleted;
  List<UserCourseList> _userCourseList;
  int _currentMediaId;
  int _currentLectureNum;
  String _currentLectureName;
  String _lectureTitle;

  String get startDate => _startDate;
  List<CourseMeidaVO> get courseMediaVO => _courseMediaVO;
  List<bool> get isStudyCompleted => _isStudyCompleted;
  List<UserCourseList> get userCourseList => _userCourseList;
  int get currentMediaId => _currentMediaId;
  int get currentLectureNum => _currentLectureNum;
  String get currentLectureName => _currentLectureName;
  String get lectureTitle => _lectureTitle;

  void setStartDate(String startDate){
    _startDate = startDate;
    notifyListeners();
  }

  void setCourseMediaVO(List<CourseMeidaVO> courseMediaVO){
    _courseMediaVO = courseMediaVO;
    notifyListeners();
  }

  void setIsStudyCompleted(List<bool> isStudyCompleted){
    _isStudyCompleted = isStudyCompleted;
    notifyListeners();
  }

  void setUserCourseList(List<UserCourseList> userCourseList){
    _userCourseList = userCourseList;
    notifyListeners();
  }

  void setCurrentMediaId(int currentMediaId){
    _currentMediaId = currentMediaId;
    notifyListeners();
  }

  void setCurrentLectureNum(int currentLectureNum){
    _currentLectureNum = currentLectureNum;
    notifyListeners();
  }

  void setCurrentLectureName(String currentLectureName){
    _currentLectureName = currentLectureName;
    notifyListeners();
  }

  void setLectureTitle(String lectureTitle){
    _lectureTitle = lectureTitle;
    notifyListeners();
  }
}