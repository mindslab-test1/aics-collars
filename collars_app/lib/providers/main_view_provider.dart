import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainViewProvider with ChangeNotifier {
  int _currentPage;
  PageController _pageController;

  MainViewProvider(){
    _currentPage = 0;
    _pageController = PageController(
      initialPage: _currentPage
    );
  }

  PageController get pageController => _pageController;
  set pageController(PageController value) { _pageController = value; }

  int get currentPage => _currentPage;
  set currentPage(int value) {
    _currentPage = value;
    notifyListeners();
  }
}