import 'package:collarsApp/services/twocents.dart';
import 'package:flutter/material.dart';
import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/evereading.dart';

class HomeMainProvider extends ChangeNotifier {
  Common _common = Common();
  EvereadingService _evereadingService = EvereadingService();
  TwoCentsService _twoCentsService = TwoCentsService();

  // EVREADING
  // - Sector
  bool _isSectorLoading = false;
  int _currentSectorIndex = 0;
  List<dynamic> _sectors = [];

  // - Column
  bool _isColumnLoading = false;
  int _currentColumnLength = 0;
  int _columnIncrement = 2;
  List<dynamic> _columns = [];

  // TWOCENTS
  bool _isScriptLoading = false;
  int _currentScriptLength = 0;
  int _scriptIncrement = 2;
  List<dynamic> _scripts = [];

  HomeMainProvider() {
    initLoad();
  }

  // Function
  Future initLoad() async {
    await loadSectorData("kor", init: true);
    await loadScriptData(init: true);
    await loadColumnData(init: true);
  }

  // - EVEREADING
  Future loadSectorData(String lang, {bool init = false}) async {
    if (!init) _isSectorLoading = true;
    notifyListeners();
    List<dynamic> _sectorData = [];
    if (_common.equalsIgnoreCase(lang, "eng")) {
      _sectorData.add({"id": 0, "tag": "all"});
    } else {
      _sectorData.add({"id": 0, "tag": "전체"});
    }
    _sectorData += await _evereadingService.getAllSectors(lang);
    _sectors = _sectorData;
    if (!init) _isSectorLoading = false;
    notifyListeners();
  }
  Future loadColumnData({bool init = false}) async {
    if (!init) _isColumnLoading = true;
    notifyListeners();
    int tagId = _sectors[_currentSectorIndex]["id"];
    _columns += await _evereadingService.getColumns(
      tagId,
      _currentColumnLength,
      _columnIncrement,
    );
    _currentColumnLength = _columns.length;
    if (!init) _isColumnLoading = false;
    notifyListeners();
  }

  void clearColumn() {
    _currentColumnLength = 0;
    _columns = [];
    loadColumnData();
  }

  // - TWOCENTS
  Future loadScriptData({bool init = false}) async {
    if (!init) _isScriptLoading = true;
    notifyListeners();
    _scripts += await _twoCentsService.getContents(_currentScriptLength, _scriptIncrement);
    _currentScriptLength = _scripts.length;
    if (!init) _isScriptLoading = false;
    notifyListeners();
  }
  void clearScript() {
    _currentScriptLength = 0;
    _scripts = [];
    loadColumnData();
  }
  // Getter & Setter
  // + Sector
  bool get isSectorLoading => _isSectorLoading;

  set isSectorLoading(bool value) {
    _isSectorLoading = value;
    notifyListeners();
  }

  int get currentSectorIndex => _currentSectorIndex;

  set currentSectorIndex(int value) {
    _currentSectorIndex = value;
    notifyListeners();
  }

  List<dynamic> get sectors => _sectors;
  set sectors(List<dynamic> value) {
    _sectors = value;
    notifyListeners();
  }

  // + Column
  bool get isColumnLoading => _isColumnLoading;

  set isColumnLoading(bool value) {
    _isColumnLoading = value;
    notifyListeners();
  }

  List<dynamic> get columns => _columns;

  set columns(List<dynamic> value) {
    _columns = value;
    notifyListeners();
  }


// Script

  List<dynamic> get scripts => _scripts;
  set scripts(List<dynamic> value) {
    _scripts = value;
    notifyListeners();
  }

  bool get isScriptLoading => _isScriptLoading;

  set isScriptLoading(bool value) {
    _isScriptLoading = value;
    notifyListeners();
  }

}
