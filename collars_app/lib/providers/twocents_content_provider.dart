import 'dart:async';
import 'package:quiver/async.dart';
import 'package:flutter/material.dart';

class TwoCentsContentProvider extends ChangeNotifier {
  bool _isAudioActive = false;
  bool _isMicActive = false;
  StreamSubscription<CountdownTimer> countdown;

  // GETTER & SETTER
  bool get isAudioActive => _isAudioActive;
  set isAudioActive(bool value) {
    _isAudioActive = value;
    notifyListeners();
  }

  bool get isMicActive => _isMicActive;
  set isMicActive(bool value) {
    _isMicActive = value;
    notifyListeners();
  }
}
