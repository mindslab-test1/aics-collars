import 'package:flutter/material.dart';

class ExamScore extends ChangeNotifier{
  String _accuracy = "";
  String _pron = "";
  String _voca = "";
  String _culture = "";
  String _manner = "";
  String _express = "";
  List<int> _examNumList = List();

  String get accuracy => _accuracy;
  String get pron => _pron;
  String get voca => _voca;
  String get culture => _culture;
  String get manner => _manner;
  String get express => _express;
  List<int> get examNumList => _examNumList;

  void setAccuracy(double totalScore){
    if(totalScore>70) _accuracy = "A";
    else if(totalScore>40 && totalScore<=70) _accuracy = "B";
    else if(totalScore>10 && totalScore<=40) _accuracy = "C";
    else _accuracy = "D";
  }

  void setPron(double totalScore){
    if(totalScore>70) _pron= "A";
    else if(totalScore>40 && totalScore<=70) _pron = "B";
    else if(totalScore>10 && totalScore<=40) _pron = "C";
    else _pron = "D";
  }

  void setVoca(double totalScore){
    if(totalScore>70) _voca= "A";
    else if(totalScore>40 && totalScore<=70) _voca = "B";
    else if(totalScore>10 && totalScore<=40) _voca = "C";
    else _voca = "D";
  }

  void setCulture(double totalScore){
    if(totalScore>70) _culture= "A";
    else if(totalScore>40 && totalScore<=70) _culture = "B";
    else if(totalScore>10 && totalScore<=40)_culture = "C";
    else _culture = "D";
  }

  void setManner(double totalScore){
    if(totalScore>70) _manner = "A";
    else if(totalScore>40 && totalScore<=70) _manner = "B";
    else if(totalScore>10 && totalScore<=40) _manner = "C";
    else _manner = "D";
  }

  void setExpress(double totalScore){
    if(totalScore>70) _express = "A";
    else if(totalScore>40 && totalScore<=70) _express = "B";
    else if(totalScore>10 && totalScore<=40) _express = "C";
    else _express = "D";
  }

  void addExamlist(int examNum){
    _examNumList.add(examNum);
  }

  void resetExamScore(){
    _accuracy = "";
    _pron = "";
    _voca = "";
    _culture = "";
    _manner = "";
    _express = "";
    _examNumList = List();
  }
}