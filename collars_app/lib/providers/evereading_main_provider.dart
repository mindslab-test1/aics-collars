import 'package:collarsApp/services/common.dart';
import 'package:collarsApp/services/evereading.dart';
import 'package:flutter/material.dart';

class EvereadingMainProvider extends ChangeNotifier {
  Common _common = Common();
  EvereadingService _evereadingService = EvereadingService();

  bool _isLoading = false;
  bool _isColumnLoading = false;
  bool _isTodayLoading = false;

  int _currentTagIndex = 0;
  List<dynamic> tags = [];
  List<String> banners = [];
  int _currentColumnLength = 0;
  int _columnIncrement = 5;
  List<dynamic> columns = [];

  dynamic _todayColumn;

  EvereadingMainProvider() {
    initLoad();
  }

  // Function
  Future initLoad() async {
    _isLoading = true;
    await loadTagData("kor", init: true);
    await loadBannerData(init: true);
    await loadColumnData(init: true);
    await setTodayColumn(init: true);
    _isLoading = false;
    notifyListeners();
  }
  Future loadTagData(String lang, {bool init = false}) async {
    if(!init)
      _isLoading = true;
    notifyListeners();
    List<dynamic> tagData = [];
    if (_common.equalsIgnoreCase(lang, "eng")) {
      tagData.add({"id": 0, "tag": "all"});
    } else {
      tagData.add({"id": 0, "tag": "전체"});
    }
    tagData += await _evereadingService.getAllSectors(lang);
    tags = tagData;
    if(!init) _isLoading = false;
    notifyListeners();
  }
  Future loadBannerData({bool init = false}) async {
    if(!init)
      _isLoading = true;
    notifyListeners();
    List<dynamic> bannerData = await _evereadingService.getBanners();
    bannerData.forEach((e) {
      banners.add(e["url"]);
    });
    if(!init) _isLoading = false;
    notifyListeners();
  }
  Future loadColumnData({bool init = true}) async {
    _isColumnLoading = true;
    notifyListeners();
    int tagId = tags[_currentTagIndex]["id"];
    columns += await _evereadingService.getColumns(tagId, _currentColumnLength, _columnIncrement);
    _currentColumnLength = columns.length;
    _isColumnLoading = false;
    notifyListeners();
  }
  void clearColumn(){
    _currentColumnLength = 0;
    columns = [];
    loadColumnData();
  }
  void setTodayColumn({bool init = false}) {
      _isTodayLoading = true;
      notifyListeners();
      _todayColumn = columns[0];
      _isTodayLoading = false;
      notifyListeners();
  }

  // Getter & Setter
  int get currentTagIndex => _currentTagIndex;
  set currentTagIndex(int value) {
    _currentTagIndex = value;
    notifyListeners();
  }

  bool get isLoading => _isLoading;
  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  bool get isColumnLoading => _isColumnLoading;

  set isColumnLoading(bool value) {
    _isColumnLoading = value;
    notifyListeners();
  }


  bool get isTodayLoading => _isTodayLoading;

  set isTodayLoading(bool value) {
    _isTodayLoading = value;
    notifyListeners();
  }

  dynamic get todayColumn => _todayColumn;
  set todayColumn(dynamic value) {
    _todayColumn = value;
    notifyListeners();
  }
}
