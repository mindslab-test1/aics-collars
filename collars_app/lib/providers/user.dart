class User {
  final String uid;
  final String email;
  String _name; // user name
  DateTime _signUpDate;
  String _course; // 수강중인 강의
  String _first; // 첫 시험
  String _survey; // 설문조사

  User({this.uid, this.email});


  String get name => _name;
  set name(String value) { _name = value; }

  DateTime get signUpDate => _signUpDate;
  set signUpDate(DateTime value) { _signUpDate = value; }

  String get survey => _survey;
  set survey(String value) { _survey = value; }

  String get first => _first;
  set first(String value) { _first = value; }

  String get course => _course;
  set course(String value) { _course = value; }
}